# -*- coding: utf-8 -*-

class Solution(object):
    def numSubarrayProductLessThanK(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: int
        """

        # sliding window: [begin, i] -- 它的所有数的product < k
        # 每次向后移动一个, 若product >= k, 则begin后移, 知道product < k
        n = len(nums)

        # 初始化: 找到第一个比k小的数, 设为sliding window的起点
        begin = 0
        while begin < n and nums[begin] >= k:
            begin += 1

        if begin == n:  # 说明nums中的所有数都>=k
            return 0

        tot_cnt = 0
        cur_cnt = 0
        product = 1
        for i in xrange(begin, len(nums)):
            # 每次往sliding window中添加nums[i]
            product *= nums[i]
            cur_cnt += 1  # 添加成功一个新的元素, 本次将增加cur_cnt个subarray

            # 若添加了nums[i]之后, product>=k了, 则将begin右移, 直到product<k为止
            # 每右移一次begin, 本次要增加的subarray个数cur_cnt要减1
            while product >= k:
                product /= nums[begin]
                begin += 1
                cur_cnt -= 1

            tot_cnt += cur_cnt  # 更新此次增加的subarray个数

        return tot_cnt



