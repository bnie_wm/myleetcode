import collections

class Solution(object):
    def ink_bleeding(self, m, n, ink_list):
        paper = [[0 for _ in xrange(n)] for _ in xrange(m)]

        # sort drops by ink value decreasingly
        ink_list.sort(key=lambda x: x[2], reverse=True)
        for i, j, darkness in ink_list:
            # skip invalid positions
            if not (0 <= i < m and 0 <= j < n):
                continue

            # skip ink_val that is lower than current position
            if darkness <= paper[i][j]:
                continue

            # self.bleeding(i, j, darkness, m, n, paper)   # DFS
            self.bleeding_bfs(i, j, darkness, m, n, paper)  # BFS

        self.print_paper(paper)

        tot_darkness = 0
        for i in xrange(m):
            tot_darkness += sum(paper[i])
        return tot_darkness


    ### DFS
    def bleeding(self, i, j, darkness, m, n, paper):
        paper[i][j] = darkness

        directions = [(-1, 0), (1, 0), (0, -1), (0, 1)]
        for dir_i, dir_j in directions:
            x, y = i + dir_i, j + dir_j
            new_darkness = darkness - 1
            if (0 <= x < m and 0 <= y < n) and (new_darkness > 0) and (paper[x][y] < new_darkness):
                self.bleeding(x, y, new_darkness, m, n, paper)


    ### BFS
    def bleeding_bfs(self, i, j, darkness, m, n, paper):
        directions = [(-1, 0), (1, 0), (0, -1), (0, 1)]

        # BFS
        queue = collections.deque([(i, j, darkness)])
        while queue:
            x, y, val = queue.popleft()
            paper[x][y] = val

            for dir_x, dir_y in directions:
                new_x, new_y = x + dir_x, y + dir_y
                new_val = val - 1
                if (0 <= new_x < m and 0 <= new_y < n) and (new_val > 0) and (paper[new_x][new_y] < new_val):
                    queue.append((new_x, new_y, new_val))


    def print_paper(self, paper):
        for i in xrange(len(paper)):
            print paper[i]


if __name__ == "__main__":
    s = Solution()
    m, n = 5, 6
    ink_list = [[1, 0, 10], [2, 2, 9], [2, 3, 5], [4, 2, 9]]
    print s.ink_bleeding(m, n, ink_list)

    m, n = 3, 4
    ink_list = [[0, 0, 255], [1, 2, 255]]
    print s.ink_bleeding(m, n, ink_list)