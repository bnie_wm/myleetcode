# -*- coding: utf-8 -*-
import collections


class Solution(object):
    def isBipartite(self, graph):
        """
        :type graph: List[List[int]]
        :rtype: bool
        """
        if not graph:
            return True

        # graph level-order traversal
        # 注意: 图可能有包含多个连通图 -- 只有当所有的连通图都是bipartite时, 才返回True
        nodes = range(len(graph))
        visited = dict()
        while nodes:
            # 以nodes[0]为起点, 进行level order访问, 每访问一个node, 从nodes中删除它
            # 若nodes不为空, 说明途中还有未访问的部分, 继续
            queue = collections.deque([(nodes[0], 1)])  # (node, color) -- color could be 1 or -1
            while queue:
                size = len(queue)  # size控制只访问当前level
                for i in xrange(size):
                    cur, color = queue.popleft()
                    if cur in visited:
                        continue
                    visited[cur] = color
                    nodes.remove(cur)

                    for nei in graph[cur]:
                        # 如果nei和cur的颜色相同 -- 返回False
                        if nei in visited and visited[nei] == color:
                            return False
                        if nei not in visited:
                            queue.append((nei, -color))
        return True