# Follow up for "Unique Paths":
#
# Now consider if some obstacles are added to the grids. How many unique paths would there be?
#
# An obstacle and empty space is marked as 1 and 0 respectively in the grid.
#
# For example,
# There is one obstacle in the middle of a 3x3 grid as illustrated below.
#
# [
#   [0,0,0],
#   [0,1,0],
#   [0,0,0]
# ]
# The total number of unique paths is 2.
#
# Note: m and n will be at most 100.
#

"""
DP
Time: O(m*n)
Space: O(n)
"""
class Solution(object):
    def uniquePathsWithObstacles(self, obstacleGrid):
        """
        :type obstacleGrid: List[List[int]]
        :rtype: int
        """
        if not obstacleGrid:
            return 0
        m, n = len(obstacleGrid), len(obstacleGrid[0])
        dp = [0] * n
        dp[0] = 1 if obstacleGrid[0][0] == 0 else 0  # before 1st row
        for i in xrange(m):
            for j in xrange(n):
                if j == 0:
                    dp[j] = dp[j] if obstacleGrid[i][j] == 0 else 0
                else:
                    dp[j] = dp[j-1] + dp[j] if obstacleGrid[i][j] == 0 else 0
        return dp[n-1]


if __name__ == "__main__":
    obstacleGrid = [
                      [0,0,0],
                      [0,1,0],
                      [0,0,0]
                   ]
    # obstacleGrid = [[1, 0]]
    print Solution().uniquePathsWithObstacles(obstacleGrid)