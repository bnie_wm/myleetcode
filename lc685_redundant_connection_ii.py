#### http://zxi.mytechroad.com/blog/graph/leetcode-685-redundant-connection-ii/
#### iterate edges -- build "parents"
#### use UnionFind -- detect cycle as if the graph is treated as undirected
#### two cases:
#### (1) if NO nodes with two parents -- same as redundant connection I  e.g., [[1,2],[2,3],[3,4],[4,1],[1,5]]
#### else:
####    (2-1) if there is directed cycle  e.g., [[2,1],[3,1],[4,2],[1,4]]
####    (2-2) else  e.g., [[1,2],[1,3],[2,3]]

class UnionFind(object):
    def __init__(self, n):
        self.parents = range(n)
        self.ranks = [0] * (n)

    def find(self, x):
        if x != self.parents[x]:
            self.parents[x] = self.find(self.parents[x])
        return self.parents[x]

    def union(self, x, y):
        px, py = self.find(x), self.find(y)
        if px == py:
            return False

        if self.ranks[px] > self.ranks[py]:
            self.parents[py] = px
        elif self.ranks[px] < self.ranks[py]:
            self.parents[px] = py
        else:
            self.parents[py] = px
            self.ranks[px] += 1
        return True


class Solution(object):
    def findRedundantDirectedConnection(self, edges):
        """
        :type edges: List[List[int]]
        :rtype: List[int]
        """
        # find N
        N = 0
        for (u, v) in edges:
            N = max(N, u, v)

        # for case2: 找是否存在一个node有两个parents
        # 如果有记录两条边 -- 最后删除的必然是这两条边的其中之一 (case2.1&2.2)
        parents = dict()
        candidate1, candidate2 = None, None  # 按输入顺序, 记录两条边
        for i, (u, v) in enumerate(edges):
            if v not in parents:
                parents[v] = [u]
            else:  # find a node with two parents
                candidate1 = [parents[v][0], v]  # 第一条
                candidate2 = [u, v]  # 第二条
                edges[i] = [-1, -1]  # 删除第二条边!!! (依题目要求, case2.1)
                break
                parents[v].append(u)

        # 使用union&find建立graph (treat as undirected)
        uf = UnionFind(N)
        for (u, v) in edges:
            if u < 0 and v < 0:  # 这是第一遍历时删除的那条边
                continue
            # 若把图当成无向图, 找到了一个环 (有两种可能)
            # 若can1=can2=None, 说明没有node是有两个parents的 -- case1 -- 返回当前edge[u, v]
            # 否则, case2.2 -- 因为can2已经在第一步删除了, 所以can1一定是这种情况下在环内的哪条 -- 返回can1
            if not uf.union(u - 1, v - 1):
                if candidate1 == candidate2 == None:  # case1
                    return [u, v]  # same as redundant connection I
                else:
                    return candidate1  # case2.2

        # 到这儿, 说明删除了can2以后图中无环, 返回can2, 两种原因:
        # case2.1 -- 依据题意删除后出现的那条, 即can2
        # case2.2 -- can2才是在环中要删除的那条 (所以删除can2了, 图中就没有环了), 所以也返回can2
        return candidate2


