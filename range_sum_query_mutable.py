# -*- coding: utf-8 -*-
# Given an integer array nums, find the sum of the elements between indices i and j (i ≤ j), inclusive.
#
# The update(i, val) function modifies nums by updating the element at index i to val.
# Example:
# Given nums = [1, 3, 5]
#
# sumRange(0, 2) -> 9
# update(1, 2)
# sumRange(0, 2) -> 8
# Note:
# The array is only modifiable by the update function.
# You may assume the number of calls to update and sumRange function is distributed evenly

"""
Segment Tree
"""
class NumArray(object):
    def __init__(self, nums):
        """
        initialize your data structure here.
        :type nums: List[int]
        """
        # use array (of len=4n) to save seg tree
        self.__n = len(nums)
        self.__tree = [0] * 4 * self.__n
        self.buildTree(nums, 0, 0, self.__n - 1)

    # O(n)
    def buildTree(self, nums, tid, low, high):
        if low == high:  # leaf
            self.__tree[tid] = nums[low]
            return

        mid = low + (high - low) / 2
        self.buildTree(nums, 2 * tid + 1, low, mid)  # left child
        self.buildTree(nums, 2 * tid + 2, mid + 1, high)  # right child

        # merge
        self.__tree[tid] = self.__tree[2 * tid + 1] + self.__tree[2 * tid + 2]

    def update(self, i, val):
        """
        :type i: int
        :type val: int
        :rtype: int
        """
        pass

    def sumRange(self, i, j):
        """
        sum of elements nums[i..j], inclusive.
        :type i: int
        :type j: int
        :rtype: int
        """
        return self.queryTree(0, 0, self.__n - 1, i, j)

    def queryTree(self, tid, low, high, i, j):
        if j < low or i > high:  # query range outside current interval
            return 0
        if i <= low and high <= j: # query range completely cover current interval
            return self.__tree[tid]

        mid = low + (high - low) / 2
        if i >= mid:
            return self.queryTree(2 * tid + 2, mid + 1, high, i, j)
        elif j <= mid:
            return self.queryTree(2 * tid + 1, low, mid, i, j)

        left = self.queryTree(2 * tid + 1, low, mid, i, j)
        right = self.queryTree(2 * tid + 2, mid + 1, high, i, j)

        return left + right


"""
BIT
# Time:  ctor:   O(n),
#        update: O(logn),
#        query:  O(logn)
# Space: O(n)
"""
class NumArrayBIT(object):
    def __init__(self, nums):
        """
        initialize your data structure here.
        :type nums: List[int]
        """
        self.nums = nums

        # build BIT
        self.bit = [0]  # similarily, first 0 is dummy
        for i in xrange(1, len(self.nums) + 1):
            self.bit.append(self.bit[i - 1] + self.nums[i - 1])

        for i in reversed(xrange(1, len(self.nums) + 1)):
            parent_idx = i - (i & (-i))
            self.bit[i] -= self.bit[parent_idx]

    def _sum(self, i):
        i += 1  # BIT index starts from 1
        this_sum = 0
        while i > 0:
            this_sum += self.bit[i]
            i -= (i & -i)  # to its parent node
        return this_sum

    def _add(self, i, dif):
        i += 1  # BIT index starts from 1
        while i < len(self.bit):
            self.bit[i] += dif
            i += (i & -i)  # to its descendant node

    def update(self, i, val):
        """
        :type i: int
        :type val: int
        :rtype: int
        """
        dif = val - self.nums[i]
        if dif:
            self.nums[i] = val
            self._add(i, dif)

    def sumRange(self, i, j):
        """
        sum of elements nums[i..j], inclusive.
        :type i: int
        :type j: int
        :rtype: int
        """
        return self._sum(j) - self._sum(i - 1)


if __name__ == "__main__":
    # Your NumArray object will be instantiated and called as such:
    # nums = [2,1,1,3,2,3,4,5,6,7,8,9]
    nums = [18, 17, 13, 19, 15, 11, 20, 12, 33, 25]
    numArray = NumArray(nums)
