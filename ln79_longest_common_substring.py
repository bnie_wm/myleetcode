class Solution:
    """
    @param A: A string
    @param B: A string
    @return: the length of the longest common substring.
    """

    def longestCommonSubstring(self, A, B):
        # write your code here
        if not A or not B:
            return 0

        m, n = len(A), len(B)
        maxLen = 0
        dp = [[0 for _ in xrange(n + 1)] for _ in xrange(m + 1)]
        for i in xrange(1, m + 1):
            for j in xrange(1, n + 1):
                if A[i - 1] == B[j - 1]:
                    dp[i][j] = dp[i - 1][j - 1] + 1
                    maxLen = max(maxLen, dp[i][j])
                else:
                    dp[i][j] = 0  # substr must be continuous!
        return maxLen
