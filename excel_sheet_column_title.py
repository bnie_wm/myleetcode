# Time:  O(logn)
# Space: O(1)

# Given a positive integer, return its corresponding column title as appear in an Excel sheet.
#
# For example:
#
#     1 -> A
#     2 -> B
#     3 -> C
#     ...
#     26 -> Z
#     27 -> AA
#     28 -> AB

"""
Math:
Time: O(logn)
Space: O(1)

"""
class Solution(object):
    def convertToTitle(self, n):
        """
        :type n: int
        :rtype: str
        """

        """
        Note that, normally one digit is 0-->9 (index starts from 0)
        However, here A-->B (index starts from 1)
        So, every iteration: remember to dvd-1
        """

        title, dvd = [], n
        while dvd:
            title.append(chr((dvd - 1) % 26 + ord('A')))
            dvd = (dvd - 1) / 26
        return "".join(reversed(title))

if __name__ == "__main__":
    for i in xrange(1, 29):
        print Solution().convertToTitle(i)