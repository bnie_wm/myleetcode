# A simple implementation of PriorityQueue using Queue.
class PriorityQueue(object):
    def __init__(self):
        self.queue = []

    def __str__(self):
        return str(self.queue)

    def isEmpty(self):
        return len(self.queue) == 0

    def push(self, data):
        self.queue.append(data)

    def pop(self):
        if self.queue:
            max_val = float("-inf")
            for val in self.queue:
                max_val = max(val, max_val)
            self.queue.remove(max_val)
            return max_val




if __name__ == '__main__':
    myQueue = PriorityQueue()
    myQueue.push(12)
    myQueue.push(1)
    myQueue.push(14)
    myQueue.push(7)
    print myQueue
    while not myQueue.isEmpty():
        print myQueue.pop()
