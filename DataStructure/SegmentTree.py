class TreeNode(object):
    def __init__(self, start, end):
        self.start, self.end = start, end  # range
        self.total = 0  # range sum in [start, end] (both inclusive)
        self.left, self.right = None, None  # left/right child


class NumArray(object):

    def __init__(self, nums):
        """
        :type nums: List[int]
        """
        self.root = self.createSegmentTree(0, self.n - 1, nums)

    def createSegmentTree(self, left, right, nums):
        if left > right:  # invalid
            return None

        # leaf node stores a single value in nums
        if left == right:
            root = TreeNode(left, right)
            root.total = nums[left]
            return root

        # internal node stores sum from left to right (inclusive)
        mid = left + (right - left) / 2
        root = TreeNode(left, right)
        root.left = self.createSegmentTree(left, mid, nums)
        root.right = self.createSegmentTree(mid + 1, right, nums)
        root.total = root.left.total + root.right.total

        return root

    def update(self, i, val):
        """
        :type i: int
        :type val: int
        :rtype: void
        """
        self.update_helper(self.root, i, val)

    def update_helper(self, root, i, val):
        if root.start == root.end:  # leaf
            root.total = val
            return

        mid = root.start + (root.end - root.start) / 2
        # If the index is less than the mid, that leaf must be in the left subtree
        if i <= mid:
            self.update_helper(root.left, i, val)
        else:
            self.update_helper(root.right, i, val)

        # update
        root.total = root.left.total + root.right.total

    def sumRange(self, i, j):
        """
        :type i: int
        :type j: int
        :rtype: int
        """
        return self.sumRange_helper(self.root, i, j)

    def sumRange_helper(self, root, i, j):
        if root.start == i and root.end == j:
            return root.total

        mid = root.start + (root.end - root.start) / 2
        if j <= mid:
            return self.sumRange_helper(root.left, i, j)
        elif i > mid:
            return self.sumRange_helper(root.right, i, j)
        else:
            return self.sumRange_helper(root.left, i, mid) + self.sumRange_helper(root.right, mid + 1, j)

