import collections, bisect

"""
1. Close to O(1) performance for as many of the following four operations.
		1-1 insert: O(n)
2. Maintaining sorted order while inserting an object into the container.
3. Ability to peek at last value (the largest value) contained in the object.
4. Allowing for pops on both sides (getting the smallest or largest values).
5. Capability of getting the total size or number of objects being stored.
6. Being a ready made solution like the code in Python's standard library.
"""


class FastTable:
    def __init__(self):
        self.__deque = collections.deque()

    def __len__(self):
        return self.__deque.count()

    def head(self):
        return self.__deque.popleft()  ## smallest

    def tail(self):
        return self.__deque.pop()  ## largest

    def peek(self):
        return self.__deque[-1]  ## peek largest

    def insert(self, obj):
        idx = bisect.bisect_left(self.__deque, obj)  ## get insertion loc (left most if duplication exists)
        self.__deque.rotate(-idx)  ## rotate left <idx> positions
        self.__deque.appendleft(obj)  ## append obj to the left
        self.__deque.rotate(idx)  ## rotate right <idx> positions

    def __str__(self):
        return str([v for v in self.__deque])


ft = FastTable()
for i in xrange(10):
    ft.insert(i)
print ft

obj = 3.5
ft.insert(obj)
print ft