import heapq
import bisect


class Solution2(object):
    def getSkyline(self, buildings):
        """
        :type buildings: List[List[int]]
        :rtype: List[List[int]]
        """
        inputs = []  # (x, event, height)
        # (1) x: small --> large
        # (2) if same x: prioritize entering(-1) over leaving(1)
        #       -- because if next entering a equal or higher building,
        #       -- no need to add current leaving point
        # (3) if same (x, event):
        #       -- (3-1) for leaving event, height: small --> large (same x, add the lowest building)
        #                (otherwise, multiple leaving points with same x)
        #       -- (3-2) for entering event, height: large --> small (same x, add the highest building)
        #                (otherwise, multiple entering points with same x)
        # bisect is able to maintain all above orders, except (3-2)
        # so, we have to handle (3-2) in the code specifically by peak the next value
        # it is also ok to use heapq, but exceed time limits
        for (left, right, height) in buildings:
            bisect.insort(inputs, (left, -1, height))  # entering
            bisect.insort(inputs, (right, 1, height))  # leaving

        skylines = []

        heights = [0]  # dummy boundary, no need to handle empty heights
        for i, (x, event, height) in enumerate(inputs):
            if event == -1:  # entering
                # peak next event for (3-2)
                peak_x, peak_event, peak_height = inputs[i + 1]
                if x == peak_x and event == peak_event:
                    if peak_height > height:
                        bisect.insort(heights, height)
                        continue
                if height > heights[-1]:
                    skylines.append((x, height))
                bisect.insort(heights, height)
            else:
                heights.remove(height)
                cur_top = heights[-1]
                if cur_top < height:
                    skylines.append((x, cur_top))

        return skylines


class Solution(object):
    def getSkyline(self, buildings):
        """
        :type buildings: List[List[int]]
        :rtype: List[List[int]]
        """

        inputs = []
        # -1=entering, 1=leaving: so, if x is the same, prioritize entering event
        # by default: height from small to large
        #           # ok for leaving event
        #           # special check for entering event -- if next is also entering event with the same x, do not add it to skylines, but add to heights
        for (left, right, height) in buildings:
            inputs.append((left, -1, height))  # -1: entering
            inputs.append((right, 1, height))  # 1: leaving
        heapq.heapify(inputs)  # sort by x-axis values

        skylines = []
        # max heap
        # init: heights=[0] --> indicates the end of building blocks --> add right border
        heights = [0]
        while inputs:
            x, sign, height = heapq.heappop(inputs)
            if sign == -1:  # start
                # peak:
                # if next event is also entering and its height is greater than height, skip current
                peak_x, peak_sign, peak_height = inputs[0]
                if peak_x == x and peak_sign == sign:
                    if peak_height > height:
                        heapq.heappush(heights, -height)
                        continue
                if height > -heights[0]:
                    skylines.append((x, height))
                heapq.heappush(heights, -height)
            else:
                self.remove(heights, -height)
                cur_top = -heights[0]
                if cur_top < height:
                    skylines.append((x, cur_top))

        return skylines

    def remove(self, heap, x):
        i = heap.index(x)
        heap[i] = heap[-1]
        heap.pop()
        heapq.heapify(heap)
