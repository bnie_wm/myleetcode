import collections

class Solution(object):
    def lengthOfLongestSubstringTwoDistinct(self, s):
        """
        :type s: str
        :rtype: int
        """

        if len(s) <= 2:
            return len(s)

        max_len = 0
        for i in xrange(len(s)):
            bucket = collections.defaultdict(int)
            for j in xrange(i, len(s)):
                if s[j] not in bucket.keys():
                    if len(bucket) >= 2:
                        break
                    else:
                        bucket[s[j]] = 1
                else:
                    bucket[s[j]] += 1
            max_len = max(max_len, sum(bucket.values()))
        return max_len

