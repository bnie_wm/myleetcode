class Solution(object):
    def numDistinctIslands(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        if not grid:
            return 0
        m, n = len(grid), len(grid[0])
        visited = [[False for j in xrange(n)] for i in xrange(m)]
        distinct_paths = set()

        num_islands = 0
        for i in xrange(m):
            for j in xrange(n):
                if (grid[i][j] == 1) and (not visited[i][j]):
                    cur_path = []
                    self.dfs(i, j, visited, grid, m, n, cur_path)
                    cur_path = "".join(cur_path)
                    if cur_path not in distinct_paths:
                        num_islands += 1
                        distinct_paths.add(cur_path)
        return num_islands

    def dfs(self, i, j, visited, grid, m, n, cur_path):
        visited[i][j] = True
        for dir in [(0, 1), (0, -1), (-1, 0), (1, 0)]:
            ii, jj = i + dir[0], j + dir[1]
            if 0 <= ii < m and 0 <= jj < n and (grid[ii][jj] == 1) and (not visited[ii][jj]):
                cur_path.append(str(dir[0]) + str(dir[1]))
                self.dfs(ii, jj, visited, grid, m, n, cur_path)
                cur_path.append(".")  # one dot repesents one "go-back" in the DFS searching!!!
                # if not added
                # 1 1 1  and 1 1 1 and 1 1 1  will share the same path!
                # 1            1           1


if __name__ == "__main__":
    # input = [[1,1,0,0,0],[1,1,0,0,0],[0,0,0,1,1],[0,0,0,1,1]]
    # input = [[1,1,0,1,1],[1,0,0,0,0],[0,0,0,0,1],[1,1,0,1,1]]
    input = [[0,0,1,0,1,0,1,1,1,0,0,0,0,1,0,0,1,0,0,1,1,1,0,1,1,1,0,0,0,1,1,0,1,1,0,1,0,1,0,1,0,0,0,0,0,1,1,1,1,0],[0,0,1,0,0,1,1,1,0,0,1,0,1,0,0,1,1,0,0,1,0,0,0,1,0,1,1,1,0,0,0,0,0,0,0,1,1,1,0,0,0,1,0,1,1,0,1,0,0,0],[0,1,0,1,0,1,1,1,0,0,1,1,0,0,0,0,1,0,1,0,1,1,1,0,1,1,1,0,0,0,1,0,1,0,1,0,0,0,1,1,1,1,1,0,0,1,0,0,1,0],[1,0,1,0,0,1,0,1,0,0,1,0,0,1,1,1,0,1,0,0,0,0,1,0,1,0,0,1,0,1,1,1,0,1,0,0,0,1,1,1,0,0,0,0,1,1,1,1,1,1]]
    print Solution().numDistinctIslands(input)

    # for i in xrange(len(input)):
    #     for j in range(len(input[0])):
    #         print input[i][j],
    #     print