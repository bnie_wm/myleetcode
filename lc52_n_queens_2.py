class Solution(object):
    def totalNQueens(self, n):
        """
        :type n: int
        :rtype: int
        """
        if n == 1: return 1
        if n < 4: return 0

        count = 0
        for j in xrange(n):  # iterate through n potential positions for the first Queen
            visited = [[0 for _ in xrange(n)] for _ in xrange(n)]
            visited[0][j] = True
            count += self.dfs(0, n, visited)
        return count

    def dfs(self, row, n, visited):
        if row == n - 1:  # find one placement
            return 1

        # place the next Queen
        count = 0
        for j in xrange(n):
            if self.isValidPosition(row + 1, j, n, visited):
                visited[row + 1][j] = True
                count += self.dfs(row + 1, n, visited)
                visited[row + 1][j] = False
        return count

    def isValidPosition(self, row, col, n, visited):
        # (1) cannot in the same row -- already satisfied

        # (2) cannot in the same coloumn
        for i in xrange(row + 1):
            if visited[i][col]:
                return False

        # (3) cannot in the same diagnal
        for i in xrange(row + 1):
            for j in xrange(n):
                if visited[i][j]:
                    if row - i == j - col:  # in the same diagnal
                        return False
                    if i - j == row - col:  # in the same back-diagnal
                        return False
        return True
