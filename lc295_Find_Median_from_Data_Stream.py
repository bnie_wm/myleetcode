import heapq


class MedianFinder(object):
    def __init__(self):
        """
        initialize your data structure here.
        """
        self.middle = None
        self.left = []  # max heap
        self.right = []  # min heap
        ### (1) left_size == right_size: median=self.middle
        ### (2) left_szie = right_size+1: median=(-self.left[0] + self.middle)/2.0
        ### otherwise, adjust left/right sizes to either (1) or (2)

    def addNum(self, num):
        """
        :type num: int
        :rtype: void
        """
        if self.middle is None:
            self.middle = num
        else:  # left_size=right_size or left_size=right_size+1
            if num <= self.middle:
                heapq.heappush(self.left, -num)
            else:
                heapq.heappush(self.right, num)

        # adjust left/right
        left_size, right_size = len(self.left), len(self.right)
        if left_size > right_size + 1:
            heapq.heappush(self.right, self.middle)
            top = heapq.heappop(self.left)
            self.middle = -top
        elif left_size < right_size:
            heapq.heappush(self.left, -self.middle)
            top = heapq.heappop(self.right)
            self.middle = top

    def findMedian(self):
        """
        :rtype: float
        """
        left_size, right_size = len(self.left), len(self.right)
        if left_size == right_size:
            return self.middle
        else:
            return (-self.left[0] + self.middle) / 2.

# Your MedianFinder object will be instantiated and called as such:
# obj = MedianFinder()
# obj.addNum(num)
# param_2 = obj.findMedian()