class UnionFind(object):
    def __init__(self, n):  # n: number of students
        self.parents = range(n)
        self.ranks = [0] * n
        self.num_circles = n  # init: every student is one circle

    def find(self, x):
        if x != self.parents[x]:  # if not while!
            self.parents[x] = self.find(self.parents[x])  # data compression
        return self.parents[x]

    def union(self, x, y):
        x_parent, y_parent = self.find(x), self.find(y)  # get the parent of x, y using self.find, NOT self.parent
        if x_parent == y_parent:  # x, y already in the same circle
            return False

        # union: compare the rank of x_parent and y_parent, not x/y!
        if self.ranks[x_parent] > self.ranks[y_parent]:
            self.parents[y_parent] = x_parent
        elif self.ranks[x_parent] < self.ranks[y_parent]:
            self.parents[x_parent] = y_parent
        else:
            self.parents[y_parent] = x_parent
            self.ranks[x_parent] += 1
        self.num_circles -= 1
        return True

    def getNumCircles(self):
        return self.num_circles


class Solution(object):
    def findCircleNum(self, M):
        """
        :type M: List[List[int]]
        :rtype: int
        """
        if not M:
            return 0

        n = len(M)
        uf = UnionFind(n)
        for i in xrange(n):
            for j in xrange(i + 1, n):
                if M[i][j] == 1:
                    uf.union(i, j)

        return uf.getNumCircles()