class Solution(object):
    def searchMatrix(self, matrix, target):
        """
        :type matrix: List[List[int]]
        :type target: int
        :rtype: bool
        """

        if not matrix:
            return False

        m, n = len(matrix), len(matrix[0])

        if n == 0 or target < matrix[0][0] or target > matrix[-1][-1]:
            return False

        # 从右上角的数开始比较: 它的左边都比它小, 下边都比它大
        # 每次可以row++或col--
        # O(m+n)
        row, col = 0, n - 1
        while row < m and col >= 0:
            if target == matrix[row][col]:
                return True
            elif target < matrix[row][col]:
                col -= 1
            else:
                row += 1

        return False


