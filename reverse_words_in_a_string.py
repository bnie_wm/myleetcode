class Solution:
    # @param s, a list of 1 length strings, e.g., s = ['h','e','l','l','o']
    # @return nothing

    # test case: "hi!"
    def reverseWords(self, s):
        self.reverse(s, 0, len(s) - 1)

        i, j = 0, 0
        while j < len(s):
            if s[j] == " ":
                if i == j:
                    i += 1
                    j += 1
                else:
                    self.reverse(s, i, j - 1)
                    i = j
            else:
                j += 1

        self.reverse(s, i, j - 1)  # do not forget to reverse the last word!

    def reverse(self, s, i, j):
        while i < j:
            s[i], s[j] = s[j], s[i]
            i += 1
            j -= 1


if __name__ == "__main__":
    for s in ["the sky is blue", "", "hi!", "a b"]:
        Solution().reverseWords(s)
        print s