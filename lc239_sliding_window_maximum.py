class Solution(object):
    def maxSlidingWindow(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: List[int]
        """
        #### using Monotonic Queue to maintain the window
        #### before pushing a new element x, first pop the elements in queue that are smaller than x
        #### 所以, monotonic queue永远是从左到右递减的 (window[i] >= window[i+1]) !!! window[0]是最大值
        #### Time: O(n) -- as each element only been pushed and popped once
        #### Space: O(k)

        n = len(nums)
        if n == 0:
            return []

        if k >= n:
            return [max(nums)]

        # sliding window
        # we don't need a separate list to store elements in window,
        # as given current index i, we know begin=i-k+1
        window = collections.deque([])  # monotoic queue

        # init
        for i in xrange(k):
            self.poppush(window, nums[i])

        # update sliding window
        res = []
        for i in xrange(k - 1, n):
            res.append(window[0])

            # update window
            if i < n - 1:
                # remove window begin(i-k+1)  # e.g., [1, -1]
                if nums[i - k + 1] == window[0]:  # 若begin的元素是最大值, popleft; 否则在poppush的过程中已经pop掉了.
                    window.popleft()
                # add new element in the next window
                self.poppush(window, nums[i + 1])

        return res

    def poppush(self, window, x):
        while window and window[-1] < x:
            window.pop()
        window.append(x)


if __name__ == "__main__":
    nums = [1,3,-1,-3,5,3,6,7]
    k = 3
    print Solution().maxSlidingWindow(nums, k)