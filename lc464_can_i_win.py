class Solution(object):
    def canIWin(self, maxChoosableInteger, desiredTotal):
        #### two special cases
        if maxChoosableInteger >= desiredTotal:
            return True
        if (1 + maxChoosableInteger) * maxChoosableInteger / 2. < desiredTotal:
            return False  # no one can win

        # 优化1:
        # 因为maxChoosableInteger<=20, 所以一个32位整数的bit就可以记录是否visited了.
        # 不需要开辟一个长度为20的visited数组了
        # bit = visited & (1 << i) --> 获取第idx位的值: 1->visited, 0-> not visited
        # visited = visited | (1 << i) --> 设置第idx位的值为1 (mark as visited)
        visited = 0  # init: all zeros (all unvisited)

        # 优化2: memo: 避免重复计算. combination问题: 一共有2^M种不同的状态
        # 1 << M = power(2, M)
        memo = [None] * (1 << maxChoosableInteger)
        return self.choose_number(desiredTotal, visited, maxChoosableInteger, memo)

    def choose_number(self, remaining, visited, maxChoosableInteger, memo):
        if memo[visited] is not None:
            return memo[visited]

        for idx in xrange(maxChoosableInteger):
            # get the bit representing current position
            # =1: visited, =0: not visited
            bit = visited & (1 << idx)
            if bit == 0:
                # 我方胜利条件: 1 OR 2 !!! (不是1 AND 2)
                # 1. remaining - (idx+1) <= 0 --> 我方先达到desiredTotal
                # 2. 对方必输 --> 我方选择完毕后, 轮到对方选择, 对方(试过各种可能之后)没有赢的可能性(即递归结果为False), 我们就赢了
                #   递归: 我们选完后, 对手选对他最有利的; 对手选完后, 我们再选对我们最有利的. 谁先达到desiredTot谁赢.
                #   所以, 若op_res==False, 说明在某一轮, 我们先手赢了. 否则, 在某一轮, 对方先手赢了.
                op_res = self.choose_number(remaining - (idx + 1), visited | (1 << idx), maxChoosableInteger, memo)
                if (remaining - (idx + 1) <= 0) or (not op_res):  # current user reach desiredTotal
                    memo[visited] = True
                    return True

        memo[visited] = False
        return False

if __name__ == "__main__":
    Solution().canIWin(maxChoosableInteger=3, desiredTotal=4)