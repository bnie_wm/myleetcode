class Solution(object):
    def rotate(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: void Do not return anything, modify matrix in-place instead.
        """

        if not matrix:
            return matrix

        m, n = len(matrix), len(matrix[0])

        # 第一步: 沿着竖中轴线, 左右翻转: matrix[i][j] <--> matrix[i][n-1-j], where i in [0, m), j in [0, n/2)
        for i in xrange(m):
            for j in xrange(n / 2):
                matrix[i][j], matrix[i][n - 1 - j] = matrix[i][n - 1 - j], matrix[i][j]

        # 第二步: 沿着斜对角线(左下to右上), 翻转: matrix[i][j] <--> matrix[n-1-j][n-1-i], where i in [0, m), j in [0, n-1-i)
        # 如果找谁与matrix[i][j]交换? (i, j)之于(0, n-1) == (x, y)之于(0, n-1), 即
        # (n-1) - i == y - 0 --> y = n - 1- i
        # j - 0 == (n - 1) - x --> x = n - 1- j
        for i in xrange(m):
            for j in xrange(n - 1 - i):
                matrix[i][j], matrix[n - 1 - j][n - 1 - i] = matrix[n - 1 - j][n - 1 - i], matrix[i][j]

        # #### 法二:
        # #### 第一步: 沿着横的中轴线, 上下翻转
        # for i in xrange(m / 2):
        #     for j in xrange(n):
        #         matrix[i][j], matrix[n-1-i][j] = matrix[n-1-i][j], matrix[i][j]
        # #### 第二步: 沿着斜对角线(左上to右下), 翻转
        # for i in xrange(m):
        #     for j in xrange(i):
        #         matrix[i][j], matrix[j][i] = matrix[j][i], matrix[i][j]


