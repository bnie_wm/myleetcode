class Solution(object):
    def subsets(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """

        if not nums:
            return nums

        visited = [0] * len(nums)
        results = []
        self.dfs_helper(0, [], nums, visited, results)
        return results


    def dfs_helper(self, cur_idx, res, nums, visited, results):
        results.append(res)

        for i in xrange(cur_idx, len(nums)):
            visited[i] = True
            self.dfs_helper(i+1, res + [nums[i]], nums, visited, results)
            visited[i] = False
