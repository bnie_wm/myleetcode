# -*- coding: utf-8 -*-

class Solution(object):
    def search(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        if not nums:
            return -1

        left, right = 0, len(nums) - 1
        while left + 1 < right:
            mid = left + (right - left) / 2

            # 左边是有序的
            if nums[left] < nums[mid]:
                if nums[left] <= target <= nums[mid]:
                    right = mid
                else:
                    left = mid
            # 右边是有序的
            else:
                if nums[mid] <= target <= nums[right]:
                    left = mid
                else:
                    right = mid

        if nums[left] == target:
            return left

        if nums[right] == target:
            return right

        return -1


if __name__ == "__main__":
    nums = [5, 6, 7, 1, 2, 3, 4]
    print Solution().search(nums=nums, target=3)