# -*- coding: utf-8 -*-
import copy


class Solution(object):
    def wiggleSort(self, nums):
        """
        :type nums: List[int]
        :rtype: void Do not return anything, modify nums in-place instead.
        """
        # 我们可以先给数组排序，然后在做调整。
        # 调整的方法是找到数组的中间的数，相当于把有序数组从中间分成两部分，
        # 然后从前半段的末尾取一个，在从后半的末尾去一个
        # e.g., [4, 5(1), 5(2), 6]
        #     mid-1, mid, mid+1, mid+2
        # 从后往前交替 -- [5(1), 6, 4, 5(2)] -- 可以使得当mid与mid+1相等时,至少被两个数(mid-1和mid+2)隔开
        # 若从前往后交替 -- [4, 5(2), 5(1), 6] -- mid+1与mid又相邻了

        nums.sort()

        n = len(nums)
        if n <= 2:
            return

        x = copy.deepcopy(nums)
        mid = (n - 1) / 2
        i, j = mid, n - 1
        idx = 0
        while j > mid:
            nums[idx], nums[idx + 1] = x[i], x[j]
            idx += 2
            i -= 1
            j -= 1
        if i == 0:
            nums[idx] = x[i]


if __name__ == "__main__":
    nums = [4, 5, 5, 6]
    Solution().wiggleSort(nums)
    print nums

    nums = [1, 2, 3, 3, 2, 2]
    Solution().wiggleSort(nums)
    print nums

