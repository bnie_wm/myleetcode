import collections


class SolutionDFS(object):
    def calcEquation(self, equations, values, queries):
        """
        :type equations: List[List[str]]
        :type values: List[float]
        :type queries: List[List[str]]
        :rtype: List[float]
        """

        # build graph
        nodes = set()
        graph = collections.defaultdict(list)
        for i, (u, v) in enumerate(equations):
            graph[u].append((v, values[i]))
            graph[v].append((u, 1.0 / values[i]))
            nodes.add(u)
            nodes.add(v)
        nodes = list(nodes)

        # for each queries -- use DFS to validate division
        results = []
        for src, tgt in queries:
            if src not in nodes or tgt not in nodes:
                results.append(-1.0)
            elif src == tgt:
                results.append(1.0)
            else:  # DFS
                # #### DFS-1: save path result in visited dictionary
                # # visited saves the distance from src to each node
                # # init: dist=INF --> unreachable
                # visited = {node: float("inf") for node in nodes}
                # visited[src] = 1.0
                # self.dfs(src, graph, visited)
                # ans = -1.0 if visited[tgt] == float("inf") else visited[tgt]
                # results.append(ans)

                ### DFS-2: return product along path
                visited = {node: False for node in nodes}
                ans = self.dfs2(src, tgt, graph, visited)
                ans = -1.0 if ans == float("inf") else ans
                results.append(ans)

        return results

    def dfs(self, cur, graph, visited):
        # update the distance from src to each node
        for nei, weight in graph[cur]:
            if visited[nei] == float("inf"):
                visited[nei] = visited[cur] * weight
                self.dfs(nei, graph, visited)  # get result of nei --> tgt

    def dfs2(self, cur, tgt, graph, visited):
        if cur == tgt:
            return 1.0

        visited[cur] = True

        for nei, weight in graph[cur]:
            if not visited[nei]:
                # get the result from nei to tgt
                res = self.dfs2(nei, tgt, graph, visited)
                if res != float("inf"):
                    return res * weight

        return float("inf")  # unreachable


class UnionFind(object):
    def __init__(self, n):
        self.parents = range(n)
        self.ranks = [0] * n
        # weights[idx]: the distance of node idx to its parents
        self.weights = [1.0] * n  # A/A=1.0

    def find(self, x):
        if x != self.parents[x]:
            px = self.parents[x]  # record the previous parent
            self.parents[x] = self.find(self.parents[x])  # link to the root
            # update weights: multiple by weights[px]
            # (already updated by deeper recursive calls, if any compression performed)
            self.weights[x] *= self.weights[px]
        return self.parents[x]

    def union(self, x, y, val):
        # x/y=val; y/x=1/val
        px, py = self.find(x), self.find(y)
        if px == py:
            return False  # already in the same tree
        if self.ranks[px] > self.ranks[py]:
            # merge y(tgt) to x(src)
            self.parents[py] = px
            self.weights[y] = self.weights[x] / val
        elif self.ranks[px] < self.ranks[py]:
            # merge x(src) to y(tgt)
            self.parents[px] = py
            self.weights[x] = self.weights[y] * val
        else:
            self.parents[px] = py
            self.weights[x] = self.weights[y] * val
            self.ranks[py] += 1
        return True

    def divide(self, x, y):
        if self.find(x) != self.find(y):
            return -1.0
        return self.weights[x] / self.weights[y]


class SolutionUF(object):
    def calcEquation(self, equations, values, queries):
        """
        :type equations: List[List[str]]
        :type values: List[float]
        :type queries: List[List[str]]
        :rtype: List[float]
        """

        nodes = set([node for eq in equations for node in eq])
        nodes = sorted(nodes)

        graph = UnionFind(len(nodes))
        for (src, tgt), val in zip(equations, values):
            graph.union(nodes.index(src), nodes.index(tgt), val)

        results = []
        for src, tgt in queries:
            if src not in nodes or tgt not in nodes:
                results.append(-1.0)
            elif src == tgt:
                results.append(1.0)
            else:
                results.append(graph.divide(nodes.index(src), nodes.index(tgt)))

        return results


if __name__ == "__main__":
    equations = [["a", "b"], ["e", "f"], ["b", "e"]]
    values = [3.4, 1.4, 2.3]
    queries = [["b", "a"], ["a", "f"], ["f", "f"], ["e", "e"], ["c", "c"], ["a", "c"], ["f", "e"]]
    print SolutionDFS().calcEquation(equations, values, queries)
    print SolutionUF().calcEquation(equations, values, queries)