# -*- coding: utf-8 -*-

class Solution(object):
    def sortColors(self, nums):
        """
        :type nums: List[int]
        :rtype: void Do not return anything, modify nums in-place instead.
        """

        # one-pass two-pointers
        # 这里要求one pass完成排序，需要利用只有数组元素只有3个数的特性，否则无法完成。
        # 排序完成后一定是0...01...12....2，所以可以扫描数组，当遇到0时，交换到前部，当遇到1时，交换到后部。
        # 用双指针left, right来记录当前已经就位的0序列和2序列的边界位置。

        # 假设已经完成到如下所示的状态：

        # 0......0   1......1  x1 x2 .... xm   2.....2
        #            |         |           |
        #           left      cur        right
        # (1) A[cur] = 1：已经就位，cur++即可
        # (2) A[cur] = 0：交换A[cur]和A[left]。由于A[left]=1或left=cur，所以交换以后A[cur]已经就位，cur++，left++
        # (3) A[cur] = 2：交换A[cur]和A[right]，right--。由于xm的值未知，cur不能增加，继续判断xm。
        # cur > right扫描结束。

        n = len(nums)

        red, white, blue = 0, 1, 2
        left, right = 0, n - 1
        i = 0
        while i <= right:
            if nums[i] == red:
                nums[i], nums[left] = nums[left], nums[i]
                left += 1
                i += 1
            elif nums[i] == white:
                i += 1
            elif nums[i] == blue:
                nums[i], nums[right] = nums[right], nums[i]
                right -= 1


if __name__ == "__main__":
    nums = [0, 1]
    Solution().sortColors(nums)
    print nums
