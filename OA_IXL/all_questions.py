import collections

class Solution(object):
    def puzzle(self, inputs):
        if not inputs:
            return []

        circles = {1: 0, 2: 0, 3: 0, 4: 1, 5: 0, 6: 1, 7: 0, 8: 2, 9: 1, 0: 1}
        res = []
        for number in inputs:
            this_count = 0
            while number > 0:
                this_count += circles[number % 10]
                number /= 10
            res.append([this_count])

        return res

    def anagram_difference(self, a, b):
        if len(a) != len(b):
            return -1

        count_a = collections.Counter(a)

        for char in b:
            if char not in count_a:
                count_a[char] = 0
            count_a[char] -= 1

        return sum([cnt if cnt > 0 else 0 for cnt in count_a.values()])

    def grid_game(self, m, n, ops):
        min_x, min_y = m, n
        for x, y in ops:
            min_x = min(min_x, x)
            min_y = min(min_y, y)

        return min_x * min_y

    def fractionAdditionList(self, expression):
        prev_num, prev_den = 0, 1
        numbers = expression.replace("-", "+-").split("+")
        if numbers[0] == "":
            numbers = numbers[1:]
        for i, number in enumerate(numbers):
            num, den = number.split("/")

            # calculate: signs[i-1]*prev_num/prev_den + signs[i] * num/den
            this_gcd = self.gcd(prev_den, int(den))
            prev_num = (prev_num * int(den) + int(num) * prev_den) / this_gcd
            prev_den = prev_den * int(den) / this_gcd

            this_gcd = self.gcd(prev_num, prev_den)
            prev_num /= this_gcd
            prev_den /= this_gcd

        return str(prev_num) + "/" + str(prev_den)


    def fractionAddition(self, expression):
        prev_num, prev_den = 0, 1
        numbers = expression.replace("-", "+-").split("+")
        if numbers[0] == "":
            numbers = numbers[1:]

        left_num, left_den = numbers[0].split("/")
        right_num, right_den = numbers[1].split("/")
        left_num, left_den, right_num, right_den = int(left_num), int(left_den), int(right_num), int(right_den)

        # calculate: signs[i-1]*prev_num/prev_den + signs[i] * num/den
        num = (left_num * right_den + right_num * left_den)
        den = left_den * right_den

        this_gcd = self.gcd(num, den)
        num /= this_gcd
        den /= this_gcd

        return str(num) + "/" + str(den)

    def gcd(self, a, b):
        while b > 0:
            a, b = b, a % b
        return a


    def jungle_book(self, predator):
        if not predator:
            return 0

        lowest_species = []
        graph = collections.defaultdict(list)
        for i, tgt in enumerate(predator):  # i eats tgt
            if tgt == -1:
                lowest_species.append(i)
            else:
                graph[tgt].append(i)

        # level-order graph traversal -- determine the highest rank of species
        # we can put species to its corresponding ranks
        # so num_ranks = num_groups
        highest_rank = 0
        visited = set()
        while lowest_species:
            src = lowest_species[0]
            lowest_species.remove(src)

            # BFS
            queue = collections.deque([src])
            rank = 0
            while queue:
                rank += 1
                size = len(queue)
                for _ in xrange(size):
                    node = queue.popleft()
                    visited.add(node)

                    for nei in graph[node]:
                        if nei not in visited:
                            queue.append(nei)

            highest_rank = max(highest_rank, rank)

        return highest_rank


    def findShortestSubArray(self, nums):
        counts = collections.Counter(nums)
        degree = max(counts.values())

        # for each num, save the left-most and right-most index
        left_idx, right_idx = {}, {}
        for i, num in enumerate(nums):
            if num not in left_idx:
                left_idx[num] = i
            right_idx[num] = i

        shortest_length = len(nums)
        for num in counts:
            if counts[num] == degree:
                shortest_length = min(shortest_length, right_idx[num] - left_idx[num] + 1)

        return shortest_length


if __name__ == "__main__":
    # inputs = [1234, 5678, 9000]
    # print Solution().puzzle(inputs)

    # a = "abc"
    # b = "ccc"
    # print Solution().anagram_difference(a, b)

    # print Solution().grid_game(m=3, n=3, ops=[])


    print Solution().fractionAddition(expression="-1/2+2/3"), Solution().fractionAdditionList(expression="-1/2+2/3")
    print Solution().fractionAddition(expression="-1/2-2/3"), Solution().fractionAdditionList(expression="-1/2-2/3")
    print Solution().fractionAddition(expression="-1/2+1/2"), Solution().fractionAdditionList(expression="-1/2+1/2")
    print Solution().fractionAddition(expression="3/18+4/24"), Solution().fractionAdditionList(expression="3/18+4/24")
    print Solution().fractionAddition(expression="1/2+1/2"), Solution().fractionAdditionList(expression="1/2+1/2")

    # print Solution().jungle_book(predator=[1, -1, 3, 1, 5, 6, 7, -1])

    # print Solution().findShortestSubArray(nums=[1,2,2,3,1,4])
    # print Solution().findShortestSubArray(nums=[1,2,2,3,1,4,2])