# -*- coding: utf-8 -*-

class Solution(object):
    def lengthOfLongestSubstring(self, s):
        """
        :type s: str
        :rtype: int
        """
        # sliding window: [begin, i] 窗口内都是不重复的字符
        begin = 0  # sliding window的开始
        pos = dict()  # pos记录每个字符的最后出现位置
        max_len = 0
        for i in xrange(len(s)):
            # 若s[i]已经遇见过了 -- 更新begin为pos[s[i]] + 1
            # 注意: 若begin > pos[s[i]] + 1, 说明虽然之前遇见过s[i], 但是已经不在sliding window [begin, i]的范围内了, e.g, abba
            if s[i] in pos:
                begin = max(begin, pos[s[i]] + 1)

            max_len = max(max_len, i - begin + 1)
            pos[s[i]] = i

        return max_len


if __name__ == "__main__":
    s = "abba"
    print Solution().lengthOfLongestSubstring(s)

    s = "pwxwkew"
    print Solution().lengthOfLongestSubstring(s)