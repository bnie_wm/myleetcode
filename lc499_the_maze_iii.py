# -*- coding: utf-8 -*-
import heapq

up, down, left, right = "u", "d", "l", "r"
impossible = "impossible"
directions = {left: (0, -1), right: (0, 1), up: (-1, 0), down: (1, 0)}


class Solution(object):
    def findShortestWay(self, maze, ball, hole):
        """
        :type maze: List[List[int]]
        :type ball: List[int]
        :type hole: List[int]
        :rtype: str
        """
        if not maze:
            return impossible

        m, n = len(maze), len(maze[0])
        ball = (ball[0], ball[1])  # change list to tuple -- to be hashable
        hole = (hole[0], hole[1])

        # Dijkstra 求出最短路径的长度
        visited = set()  # key:value = pos: dist
        queue = [(0, "", ball)]  # (dist, path, pos): min heap sort by dist + path (lexicographically)
        while queue:
            cur_dist, cur_path, cur_pos = heapq.heappop(queue)
            if cur_pos in visited:  # must check if visited or not, as the same node can be added to queue multiple times
                continue
            if cur_pos == hole:
                return cur_path
            visited.add(cur_pos)

            for key in directions:
                (x, y), this_dist = self.rolling(cur_pos, key, hole, maze, m, n)
                if this_dist == 0:  # no move along this direction
                    continue
                heapq.heappush(queue, (cur_dist + this_dist, cur_path + key, (x, y)))

        return impossible

    def rolling(self, cur, key, hole, maze, m, n):
        dx, dy = directions[key]

        # keep rolling until reach wall or fall into the hole
        x, y = cur[0], cur[1]
        while 0 <= x < m and 0 <= y < n and maze[x][y] == 0:
            if x == hole[0] and y == hole[1]:
                break
            x, y = x + dx, y + dy

        # if reach the wall, roll back one step, to the last vaild position
        if not (x == hole[0] and y == hole[1]):
            x, y = x - dx, y - dy

        this_dist = abs(x - cur[0]) + abs(y - cur[1])

        return (x, y), this_dist


class SolutionBFS_DFS(object):
    def findShortestWay(self, maze, ball, hole):
        """
        :type maze: List[List[int]]
        :type ball: List[int]
        :type hole: List[int]
        :rtype: str
        """
        if not maze:
            return impossible

        m, n = len(maze), len(maze[0])
        ball = (ball[0], ball[1])  # change list to tuple -- to be hashable
        hole = (hole[0], hole[1])

        # step1: BFS求出最短路径的长度
        distance = dict()
        queue = [(0, ball)]  # (dist, pos)
        while queue:
            cur_dist, cur_pos = heapq.heappop(queue)
            if cur_pos in distance:  # must check if visited or not, as the same node can be added to queue multiple times
                continue
            distance[cur_pos] = cur_dist

            for key in directions:
                (x, y), this_dist = self.rolling(cur_pos, key, hole, maze, m, n)
                if this_dist == 0:  # no move along this direction
                    continue
                heapq.heappush(queue, (cur_dist + this_dist, (x, y)))

        shortest_length = distance[hole] if hole in distance else -1
        if shortest_length == -1:
            return impossible
        # print "shortest_length =", shortest_length

        # step2: DFS输出路径
        distance = {ball: 0}
        final_path = ["z"]
        self.dfs(ball, hole, [], final_path, shortest_length, distance, maze, m, n)
        return final_path[0]

    def dfs(self, cur, hole, res, final_path, shortest_length, distance, maze, m, n):
        # if reach hole and is the shortest path
        if cur[0] == hole[0] and cur[1] == hole[1] and distance[cur] == shortest_length:
            # print "".join(res), final_path[0]
            final_path[0] = min(final_path[0], "".join(res))

        for key in directions:
            if res and res[-1] == key:  # skip the previous direction
                continue
            (x, y), this_dist = self.rolling(cur, key, hole, maze, m, n)
            if this_dist == 0:
                continue

            # if reach an unvisited position or a shorter path
            if (x, y) not in distance or distance[cur] + this_dist <= distance[(x, y)]:
                distance[(x, y)] = distance[cur] + this_dist  # update distance
                self.dfs((x, y), hole, res + [key], final_path, shortest_length, distance, maze, m, n)

    def rolling(self, cur, key, hole, maze, m, n):
        dx, dy = directions[key]

        # keep rolling until reach wall or fall into the hole
        x, y = cur[0], cur[1]
        while 0 <= x < m and 0 <= y < n and maze[x][y] == 0:
            if x == hole[0] and y == hole[1]:
                break
            x, y = x + dx, y + dy

        # if reach the wall, roll back one step, to the last vaild position
        if not (x == hole[0] and y == hole[1]):
            x, y = x - dx, y - dy

        this_dist = abs(x - cur[0]) + abs(y - cur[1])

        return (x, y), this_dist
