import numpy as np
from collections import defaultdict


def main():
    x = [2, 0, 2, 0]
    n = len(x)

    ### O(n^3)
    for i in xrange(n):
        for j in xrange(i+1, n):
            this_sum = x[i] + x[j]
            print "***", x[i], "+", x[j], "=", this_sum
            # key:remaining_sum, value: idx --> x[idx] + remaining_sum = this_sum
            two_sum = defaultdict(list)
            for k in xrange(i+1, n):
                if k == j:
                    continue
                if x[k] in two_sum:
                    generate_pairs(i, j, k, two_sum[x[k]], x)
                    # generate_pairs_idx(i, j, k, two_sum[x[k]])
                else:
                    two_sum[this_sum - x[k]].append(k)


def generate_pairs(i, j, k, idx_list, x):
    for p1 in [(x[i], x[j]), (x[j], x[i])]:
        for m in idx_list:
            print p1, (x[k], x[m])
            print p1, (x[m], x[k])


def generate_pairs_idx(i, j, k, idx_list):
    for p1 in [(i, j), (j, i)]:
        for m in idx_list:
            print p1, (k, m)
            print p1, (m, k)




if __name__ == "__main__":
    main()