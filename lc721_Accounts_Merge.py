# email -- node
# same email from different accounts -- share the same email_id (same node)
# account: add edge between each pair of emails in the same account
# different accounts are linked by the shared email (node)
# the problem is converted to find connected commponents in the graph
# methods: [1] Union&Find [2] BFS/DFS
class UnionFind(object):
    def __init__(self):
        max_num_emails = 1000 * 10
        self.parents = range(max_num_emails)  # save email ids
        self.ranks = [0] * max_num_emails

    def find(self, x):
        if x != self.parents[x]:
            self.parents[x] = self.find(self.parents[x])
        return self.parents[x]

    def union(self, x, y):
        px, py = self.find(x), self.find(y)
        if px == py:
            return False
        if self.ranks[px] < self.ranks[py]:
            self.parents[px] = py
        elif self.ranks[px] > self.ranks[py]:
            self.parents[py] = px
        else:
            self.parents[py] = px
            self.ranks[px] += 1


class Solution(object):
    def accountsMerge(self, accounts):
        """
        :type accounts: List[List[str]]
        :rtype: List[List[str]]
        """
        n = len(accounts)
        if n <= 1:
            return accounts

        uf = UnionFind()  # init: n=1000*10 (max # of emails)
        email_to_name, email_to_id = dict(), dict()
        email_id = 0
        for act in accounts:
            act_name = act[0]
            for email in act[1:]:
                if email not in email_to_id:
                    email_to_name[email] = act_name
                    email_to_id[email] = email_id
                    email_id += 1
                # merge emails in the same account (all union to act[1])
                # if two accounts share the same email, they will be connected as they use the same email id
                uf.union(email_to_id[act[1]], email_to_id[email])

        res = dict()
        for email in email_to_id:
            parent_id = uf.find(email_to_id[email])
            if parent_id not in res:
                res[parent_id] = [email_to_name[email], email]
            else:
                res[parent_id].append(email)

        merged_accounts = []
        for parent_id in res:
            merged_accounts.append(sorted(res[parent_id]))
        return merged_accounts


if __name__ == "__main__":
    s = Solution()
#     accounts = [["John","johnsmith@mail.com","john_newyork@mail.com"],
# ["John","johnsmith@mail.com","john00@mail.com"],
# ["Mary","mary@mail.com"],
# ["John","johnnybravo@mail.com"]]
    accounts = [["David","David0@m.co","David1@m.co"],
                ["David","David3@m.co","David4@m.co"],
                ["David","David4@m.co","David5@m.co"],
                ["David","David2@m.co","David3@m.co"],
                ["David","David1@m.co","David2@m.co"]]
    merged = s.accountsMerge(accounts)
    for account in merged:
        print(account)