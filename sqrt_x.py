# Implement int sqrt(int x).
#
# Compute and return the square root of x.


class Solution(object):
    def mySqrt(self, x):
        """
        :type x: int
        :rtype: int
        """

        left, right = 0, x/2+1
        while left <= right:
            mid = left + (right - left) / 2
            product = mid * mid
            if x == product:
                return mid
            elif x < product:
                right = mid - 1
            else:
                left = mid + 1
        print n, left, right
        return (left+right)/2


if __name__ == "__main__":
    for n in xrange(26):
        print n, Solution().mySqrt(n)