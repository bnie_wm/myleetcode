
def main():
    key = "hello"
    print hashfunc(key)


# djb2
# this algorithm (k=33) was first reported by dan bernstein many years ago in comp.lang.c. another version of this algorithm (now favored by bernstein) uses xor: hash(i) = hash(i - 1) * 33 ^ str[i]; the magic of number 33 (why it works better than many other constants, prime or not) has never been adequately explained.
#     unsigned long
#     hash(unsigned char *str)
#     {
#         unsigned long hash = 5381;
#         int c;
#
#         while (c = *str++)
#             hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
#
#         return hash;
#     }

def hashfunc(key, hash_size=10):
    _sum = 0
    for i in xrange(len(key)):
        _sum = _sum * 33 + ord(key[i])
        print i, key[i], _sum, _sum%hash_size
        _sum = _sum % hash_size
    return _sum




if __name__ == "__main__":
    # main()

    x = "dog"
    y = "log"
    n = len(x)
    for i in xrange(n):
        print i, x[:i] + x[i+1:], y[:i] + y[i+1:], x[:i] + x[i+1:] == y[:i] + y[i+1:]