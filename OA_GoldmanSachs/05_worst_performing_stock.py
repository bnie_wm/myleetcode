class Solution(object):
    def worstStock(self, stock_list):
        if not stock_list:
            return 0

        worst = (None, float("inf"))  # (stock_id, rate)

        for stock_id, opening, closing in stock_list:
            rate = (closing - opening) * 1.0 / opening
            if rate < worst[1]:
                worst = (stock_id, rate)
            """
            This case may or may not exist!!!
            """
            # elif rate == worst[1]:
            #     worst = (min(stock_id, worst[0]), rate)

        return worst[0]


if __name__ == "__main__":
    stock_list = []
    stock_list.append([1200, 100, 105])
    stock_list.append([1400, 50, 55])
    print Solution().worstStock(stock_list)