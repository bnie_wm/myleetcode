class Solution(object):
    def pressA(self, sentence):
        if not sentence:
            return sentence

        cap = False
        res = []
        for char in sentence:
            if char in ['a', 'A']:
                cap = not cap
            elif char.isalpha():
                if not cap:
                    res.append(char)
                else:
                    # reverse the capitalization
                    if char.isupper():
                        res.append(char.lower())
                    else:
                        res.append(char.upper())
            else:
                res.append(char)
        return "".join(res)


if __name__ == "__main__":
    sentence = "My keyboard is broken!"
    print sentence
    print Solution().pressA(sentence)
    print

    sentence = '"Baa, Baa!" said the sheep. HELLO! Welcome to Apple Store. Ahh Hey!'
    print sentence
    print Solution().pressA(sentence)
