import collections
import math

class Solution(object):
    def prob(self, avg_goals):
        if not avg_goals:
            return "0.00"

        n = len(avg_goals)
        max_goal = 0
        goal_dict = collections.defaultdict(int)
        for i in xrange(n):
            for j in xrange(i+1, n):
                this_goal = avg_goals[i] + avg_goals[j]
                if this_goal > max_goal:
                    max_goal = this_goal
                goal_dict[this_goal] += 1

        this_prob = goal_dict[max_goal] * 1.0 / len(goal_dict)
        this_prob = self.truncate(this_prob, 2)
        return this_prob

    def truncate(self, number, precision):
        tr_num = math.floor(number * 10 ** precision) / 10 ** precision
        return "{:.2f}".format(tr_num)
    

if __name__ == "__main__":
    avg_goals = [1, 2, 2, 3]
    print Solution().prob(avg_goals)