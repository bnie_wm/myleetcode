
class Solution(object):
    def reverse(self, expression):
        if not expression:
            return ""

        signs = {'+', '-', '*', "/"}
        n = len(expression)

        # First, reverse expression
        expression = expression[::-1]

        # Second, reverse each number
        res = []
        cur = ""
        for i in xrange(n):
            char = expression[i]
            # char belongs to a number
            if char.isdigit() or char == ".":
                cur += char
            # char is the negative sign, instead of the minus operation
            elif char == "-" and ((i + 1 < n and expression[i+1] in signs) or (i == n - 1)):
                cur += char
            # char is an operation
            else:
                res.append(cur[::-1])
                res.append(char)
                cur = ""

        # append the last number
        res.append(cur[::-1])
        return "".join(res)


if __name__ == "__main__":
    expression = "23.89"
    print expression
    print Solution().reverse(expression)
    print

    expression = "-23.89"
    print expression
    print Solution().reverse(expression)
    print

    expression = "1*2.4+9.6-23.89"
    print expression
    print Solution().reverse(expression)
    print

    expression = "2*3.5+15/-2.1"
    print expression
    print Solution().reverse(expression)
    print

    expression = "-2*-3.5+-15/-2.1--0.5"
    print expression
    print Solution().reverse(expression)

