import collections

class Solution(object):
    def perfectTeam(self, skills):
        if not skills:
            return 0

        counts = collections.Counter(skills)
        """
        If counts contain courses not in the five listed?????
        """

        if len(counts) < 5:
            return 0
        else:
            return min(counts.values())


if __name__ == "__main__":
    print Solution().perfectTeam(skills="mcmz")
    print Solution().perfectTeam(skills="pcmbz")
    print Solution().perfectTeam(skills="pcmpp")
    print Solution().perfectTeam(skills="pcmpcmbbbzz")