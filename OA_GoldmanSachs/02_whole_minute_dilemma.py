import collections

class Solution(object):
    def play(self, songs):
        if not songs:
            return 0

        # count the frequency of each songs (% 60)
        counter = collections.defaultdict(int)
        for duration in songs:
            counter[duration % 60] += 1

        num_pairs = 0
        # special cases for 60s, 120s, ... and 30s, 90s, ...
        for key in [0, 30]:
            num_pairs += counter[key] / 2
            del counter[key]

        # count other pairs
        for key in counter:
            if counter[key] == -1:  # visited
                continue
            if 60 - key in counter:
                num_pairs += min(counter[key], counter[60 - key])
                counter[60 - key] = -1
            counter[key] = -1

        return num_pairs


if __name__ == "__main__":
    songs = [40, 60, 20]
    print Solution().play(songs)
    print

    songs = [40, 20, 20, 20, 60, 60, 60, 30, 30, 90, 30, 30, 23, 60-23, 38, 23]
    print Solution().play(songs)

