import collections
import math


class Solution(object):
    def format(self, inputs):
        if not inputs:
            return ""

        delimeter = ","

        column_max_width = collections.defaultdict(int)
        num_columns = len(inputs[0].split(delimeter))
        for line in inputs:
            cols = line.split(delimeter)
            """"""
            assert len(cols) == num_columns
            """"""
            for i in xrange(num_columns):
                column_max_width[i] = max(column_max_width[i], len(cols[i]))

        results = ""
        for line in inputs:
            newline = ""
            for i, col in enumerate(line.split(delimeter)):
                space_holder = [" "] * (column_max_width[i] - len(col))
                newline += "".join(space_holder) + col + delimeter + " "  # delimited by ", "
            results += newline[:-2] + "\n"
        return results


if __name__ == "__main__":
    inputs = []
    inputs += ["name,id,city,job,date of birth"]
    inputs += ["mary,20089,nyc,student,89/10/9"]
    inputs += [",,,,"]
    inputs += ["bob,,,,90/1/2"]
    inputs += [",,nyc,teacher,"]
    for line in inputs:
        print line

    print "---------------------"
    print Solution().format(inputs)