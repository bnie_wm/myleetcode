import collections

class Solution(object):
    def whoWin(self, bob, erica):
        bob, erica = bob.upper(), erica.upper()

        levels = ['S', 'E', 'M', 'H']
        board = {'S': 0, 'E': 1, 'M': 3, 'H': 5}

        bob_tot = sum(board[level] for level in bob)
        erica_tot = sum(board[level] for level in erica)
        if bob_tot > erica_tot:
            return "Bob"
        elif bob_tot < erica_tot:
            return "Erica"
        else:
            freq_bob = self.calculateLeveLCount(bob, levels)
            freq_erica = self.calculateLeveLCount(erica, levels)
            if freq_bob > freq_erica:
                return "Bob"
            elif freq_bob < freq_erica:
                return "Erica"
            else:
                return "Tie"

    def calculateLeveLCount(self, questions, levels):
        counter = collections.Counter(questions)
        level_freq = tuple([counter[level] for level in levels])
        return level_freq


if __name__ == "__main__":
    print Solution().whoWin(bob="EHME", erica="HMEE")
    print Solution().whoWin(bob="EHMEEE", erica="HMEEM")
    print Solution().whoWin(bob="EHMEEEE", erica="HMEEM")
    print Solution().whoWin(bob="", erica="")
