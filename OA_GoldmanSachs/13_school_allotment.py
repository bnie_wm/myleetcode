import heapq

class Solution(object):
    def allocate(self, schoolSeatArray, studentScoreArray, studentSchoolPrefArray):
        if not schoolSeatArray:
            return 0, len(studentScoreArray)

        num_students = min(len(studentScoreArray), len(studentSchoolPrefArray))
        if num_students == 0:
            return sum(schoolSeatArray), 0

        school_seats = {i: cnt for i, cnt in enumerate(schoolSeatArray)}

        student_scores = []
        student_preferences = dict()
        for stu_id in xrange(num_students):
            heapq.heappush(student_scores, (-studentScoreArray[stu_id], stu_id))
            student_preferences[stu_id] = studentSchoolPrefArray[stu_id]

        num_unallocated_students = 0
        while student_scores:
            score, stu_id = heapq.heappop(student_scores)  # get the student with the highest score
            allocate_res = self.assign_student_to_school(student_preferences[stu_id], school_seats)
            if not allocate_res:
                num_unallocated_students += 1

        num_remaining_seats = sum(school_seats.values())

        return num_remaining_seats, num_unallocated_students

    def assign_student_to_school(self, preferences, school_seats):
        for choice in preferences:
            if school_seats[choice] > 0:
                school_seats[choice] -= 1
                return True
        return False


if __name__ == "__main__":
    schoolSeatArray = [1, 3, 1, 2]
    studentScoreArray = [990, 780, 830, 860, 920]
    studentSchoolPrefArray = []
    studentSchoolPrefArray += [[3, 2, 1]]
    studentSchoolPrefArray += [[3, 0, 0]]
    studentSchoolPrefArray += [[2, 0, 1]]
    studentSchoolPrefArray += [[0, 1, 3]]
    studentSchoolPrefArray += [[0, 2, 3]]
    # studentSchoolPrefArray += [[3, 0, 0]]

    print Solution().allocate(schoolSeatArray, studentScoreArray, studentSchoolPrefArray)