class Solution(object):
    def maxDif(self, a):
        if not a:
            return 0

        min_odd, max_diff = float("inf"), 0
        for x in a:
            if x % 2 == 1:
                min_odd = min(min_odd, x)
            elif x > min_odd:
                max_diff = max(max_diff, x - min_odd)
        return max_diff


if __name__ == "__main__":
    print Solution().maxDif(a=[7, 8, 5, 4, 3, 12, 18, 5, 10])