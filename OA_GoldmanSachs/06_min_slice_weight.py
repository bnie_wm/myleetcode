class Solution(object):
    def minSliceWeight(self, matrix):
        if not matrix:
            return 0

        m, n = len(matrix), len(matrix[0])
        dp = [[0 for _ in xrange(n)] for _ in xrange(m)]

        # init: 1st row
        for j in xrange(n):
            dp[0][j] = matrix[0][j]

        # update dp[i][j]
        for i in xrange(1, m):
            for j in xrange(n):
                upper_left = dp[i-1][j-1] if j - 1 >= 0 else float("inf")
                upper_right = dp[i-1][j+1] if j + 1 < n else float("inf")
                dp[i][j] = min(upper_left, dp[i-1][j], upper_right) + matrix[i][j]
        return min(dp[n-1])


if __name__ == "__main__":
    matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    print Solution().minSliceWeight(matrix)
