import collections
import numpy


class Solution(object):
    def latestStudent(self, attendance):
        if not attendance:
            return ""

        # calculate the average lateness on each day
        day_avg_lateness, day_count = dict(), dict()
        for day, student, start, arrive in attendance:
            this_lateness = max(int(arrive) - int(start), 0)
            if day in day_count:
                day_avg_lateness[day] = (day_avg_lateness[day] * day_count[day] + this_lateness) * 1.0 / (day_count[day] + 1)
                day_count[day] += 1
            else:
                day_avg_lateness[day] = this_lateness
                day_count[day] = 1

        # calculate the sum of relative lateness for each student
        student_total_relative_lateness = collections.defaultdict(int)
        worst_student = (None, 0)  # (student, sum of relative lateness)
        for day, student, start, arrive in attendance:
            this_lateness = max(int(arrive) - int(start), 0)
            this_relative_lateness = max(this_lateness - day_avg_lateness[day], 0)
            student_total_relative_lateness[student] += this_relative_lateness

            # update worst student
            if student_total_relative_lateness[student] > worst_student[1]:
                worst_student = (student, student_total_relative_lateness[student])
            elif student_total_relative_lateness[student] == worst_student[1]:
                worst_student = (min(student, worst_student[0]), worst_student[1])

        return worst_student[0]


if __name__ == "__main__":
    attendance = []
    attendance.append(["08-21", "Mary", "540", "545"])
    attendance.append(["08-22", "Mary", "540", "530"])
    attendance.append(["08-23", "Mary", "540", "568"])
    attendance.append(["08-23", "Bob", "540", "541"])
    attendance.append(["08-25", "Bob", "540", "543"])
    attendance.append(["08-25", "Mary", "540", "547"])
    attendance.append(["08-22", "Tom", "540", "543"])
    attendance.append(["08-22", "Apple", "540", "543"])
    attendance.append(["08-22", "Zelda", "540", "543"])
    attendance.append(["08-27", "Tom", "540", "600"])
    attendance.append(["08-27", "Apple", "540", "600"])
    attendance.append(["08-27", "Zelda", "540", "600"])
    attendance.append(["08-27", "Mary", "540", "540"])
    attendance.append(["08-27", "Bob", "540", "545"])
    print Solution().latestStudent(attendance)
