class Solution(object):
    def removeComment(self, inputs):
        results = ""  # save the codes
        comment_buffer = ""  # buffer multiple-line comments when met "/*"
        commented = False  # turn it on when met "/*" and turn it off when commented is True and met "*/"

        # handle line by line, then for each line, char by char
        for line in inputs:
            n = len(line)

            # handle an empty line
            if n == 0:
                if commented:
                    comment_buffer += "\n"
                else:
                    results += "\n"
                continue

            i = 0
            while i < n:
                if i == n - 1:  # the last char in line, can always be added
                    if commented:
                        comment_buffer += line[i] + "\n"  # cannot be "*/"
                    else:
                        results += line[i] + "\n"
                else:
                    if line[i:i+2] == "/*":
                        # if this is the first "/*": commented: False --> True
                        # else: say "/* /*" -- for the second "/*": commented: True --> True
                        comment_buffer += "/*"  # in case there is only "/*", but no corresponding "*/"
                        commented = True
                        i += 1
                    elif line[i: i+2] == "*/" and commented:
                        if i + 2 == n:  # if "*/" is the end of this line
                            comment_buffer += "\n"
                        # convert multi-line comments into several space holders (splitted by "\n")
                        num_commented_rows = len(comment_buffer.split("\n")) - 1
                        if num_commented_rows == 0:
                            results += " "
                        else:
                            for _ in xrange(num_commented_rows):
                                results += " \n"
                        comment_buffer = ""  # clear comment buffer

                        commented = False  # toggle off commented
                        i += 1
                    elif line[i: i+2] == "//":
                        if commented:
                            comment_buffer += " \n"
                        else:
                            results += " \n"
                        break  # break the line when meet "//"
                    else:
                        if commented:
                            comment_buffer += line[i]
                        else:
                            results += line[i]

                i += 1

        # if comment_buffer != "" --> means there is unterminated /* */
        return results + comment_buffer


if __name__ == "__main__":
    inputs = []
    inputs.append("public class HelloWorld {")
    inputs.append(' // This program will print "Hello, World"')
    inputs.append(" public static void main(String[] args) {")
    inputs.append('     System.out.println("Hello/*, World*/"); // A single line comment')
    inputs.append("     /* A sample comment")
    inputs.append("           ")
    inputs.append("        spanning multiple")
    inputs.append("        lines*/")
    inputs.append("            ")
    inputs.append(" /* /* }")
    inputs.append("}")

    for line in inputs:
        print line

    print "------------------------------"
    print Solution().removeComment(inputs)
    print "------------------------------"
