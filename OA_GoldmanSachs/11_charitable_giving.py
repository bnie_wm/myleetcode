import heapq

class Solution(object):
    def allocation(self, donates):
        if not donates:
            return []

        queue = [(0, "A"), (0, "B"), (0, "C")]
        heapq.heapify(queue)
        res = []
        for grant in donates:
            # get the company with least money
            money, company = heapq.heappop(queue)
            res.append(company)
            heapq.heappush(queue, (money + grant, company))
        return res


if __name__ == "__main__":
    donates = [25, 8, 2, 35, 15, 120, 55, 42]
    print Solution().allocation(donates)