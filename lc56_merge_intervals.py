"""
Time: O(nlogn)
Space: O(n)
"""

# Definition for an interval.
# class Interval(object):
#     def __init__(self, s=0, e=0):
#         self.start = s
#         self.end = e

class Solution(object):
    def merge(self, intervals):
        """
        :type intervals: List[Interval]
        :rtype: List[Interval]
        """
        intervals.sort(key=lambda x: x.start)  # O(nlogn)
        res = []
        for interval in intervals:  # O(n)
            if not res or interval.start > res[-1][1]:  # new interval
                res.append([interval.start, interval.end])
            else:  # merge with previous interval
                if interval.end > res[-1][1]: ## update only when current interval expands res[-1]
                    res[-1][1] = interval.end
                # else: covered by res[-1]
        return res