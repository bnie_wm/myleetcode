class Solution(object):
    def wordsTyping(self, sentence, rows, cols):
        """
        :type sentence: List[str]
        :type rows: int
        :type cols: int
        :rtype: int
        """
        n = len(sentence)
        # dp: if current row starts with word i, then
        # (1) current row contains dp[i][0] number of full sentences (indicated by number of last words)
        # (2) next row starts with dp[i][1]
        dp = [(0, 0) for _ in xrange(n)]
        for wid in xrange(n):
            i = wid
            # fill one row, get repeats[wid] and nextWord[wid]
            nextCol, occur = 0, 0
            while nextCol + len(sentence[i]) <= cols:  # while cur word can fit in the row
                nextCol += len(sentence[i]) + 1  # pad one space
                i += 1  # next word
                if i == n:  # if meet the last word in sentence
                    i = 0  # reset word idx
                    occur += 1  # increment occur
            dp[wid] = (occur, i)

        # for (repeats, wid) in sorted(dp, key=lambda x:x[1]):
        #     print wid, repeats

        res, wid = 0, 0
        row = 0
        while row < rows:
            res += dp[wid][0]
            wid = dp[wid][1]  # next word
            row += 1

        return res


class SolutionTLE(object):  # TLE
    def wordsTyping(self, sentence, rows, cols):
        """
        :type sentence: List[str]
        :type rows: int
        :type cols: int
        :rtype: int
        """
        if not sentence:
            return 0

        remaining_rows_to_occur = {0: 0}
        num_rows_per_iteration, repeat_times_per_iteration = self.oneIteration(sentence, rows, cols,
                                                                               remaining_rows_to_occur)
        if num_rows_per_iteration == -1:
            return 0

        occurrence = (rows / num_rows_per_iteration) * repeat_times_per_iteration
        remaining_rows = rows % num_rows_per_iteration
        occurrence += remaining_rows_to_occur[remaining_rows]

        return occurrence

    def oneIteration(self, sentence, rows, cols, remaining_rows_to_occur):
        num_words = len(sentence)
        total_len = sum(map(len, sentence)) + num_words
        num_full_repeats_per_row = cols / total_len

        next_row, next_col = 0, 0
        wid = 0
        while True:
            cur_word = sentence[wid % num_words]
            if len(cur_word) > cols:
                return -1, -1

            # skip full repeats in the row
            # but only pad once per row
            # padding occurs when first enter a new row
            if num_full_repeats_per_row > 0 and next_col < total_len:
                wid += num_full_repeats_per_row * num_words
                next_col += num_full_repeats_per_row * total_len
                if next_col >= cols:
                    wid -= num_words
                    next_col -= total_len

            next_col += len(cur_word)
            if next_col > cols:  # if cur_word cannot fit in cur_row
                next_row += 1
                next_col = len(cur_word)
                if next_col < cols:
                    next_col += 1  # pad one space
            elif next_col == cols or next_col + 1 == cols:
                next_row += 1
                next_col = 0
            else:
                next_col += 1

            # only recode when finish one row --> that is we first enter the next row
            if next_row not in remaining_rows_to_occur:
                remaining_rows_to_occur[next_row] = (wid + 1) / num_words

            if next_row >= rows:
                if (wid + 1) % num_words == 0 and next_col != 0:  # if failed to output last word
                    return next_row, remaining_rows_to_occur[next_row] - 1
                else:
                    return next_row, remaining_rows_to_occur[next_row]

            # if the first word becomes the first one in the row again
            # finish one iteration
            if (wid + 1) % num_words == 0 and next_col == 0:
                break

            wid += 1

        num_rows_per_iteration = next_row
        repeat_times_per_iteration = (wid + 1) / num_words

        return num_rows_per_iteration, repeat_times_per_iteration


if __name__ == "__main__":
    # rows = 8
    # cols = 13  # 18
    # sentence = ["f","p","a"]

    # sentence = ["hello", "world"]
    # # rows, cols = 9, 10000   # 7497
    # rows, cols = 2, 8

    # sentence = ['a']
    # rows, cols = 2, 3

    # sentence = ['a', 'bcd', 'e']
    # rows, cols = 3, 6

    # sentence = ["hello", "leetcode"]
    # rows, cols = 1, 10

    # sentence = ["try", "to", "be", "better"]
    # rows = 10000
    # cols = 9001  # 5293333

    # sentence = ["I", "had", "apple", "pie"]
    # rows, cols = 4, 5

    sentence = ["abcdef", "ghijkl", "mnop", "qrs", "tuv", "wxyz", "asdf", "ogfd", "df", "r", "abcdef", "ghijkl", "mnop", "qrs",
     "tuv", "wxyz", "asdf", "ogfd", "df", "r", "abcdef", "ghijkl", "mnop", "qrs", "tuv", "wxyz", "asdf", "ogfd", "df",
     "r", "abcdef", "ghijkl", "mnop", "qrs", "tuv", "wxyz", "asdf", "ogfd", "df", "r", "abcdef", "ghijkl", "mnop",
     "qrs", "tuv", "wxyz", "asdf", "ogfd", "df", "r", "abcdef", "ghijkl", "mnop", "qrs", "tuv", "wxyz", "asdf", "ogfd",
     "df", "r", "abcdef", "ghijkl", "mnop", "qrs", "tuv", "wxyz", "asdf", "ogfd", "df", "r", "abcdef", "ghijkl", "mnop",
     "qrs", "tuv", "wxyz", "asdf", "ogfd", "df", "r", "abcdef", "ghijkl", "mnop", "qrs", "tuv", "wxyz", "asdf", "ogfd",
     "df", "r", "abcdef", "ghijkl", "mnop", "qrs", "tuv", "wxyz", "asdf", "ogfd", "df", "r"]
    sentence = ["tpgl","brqe","wvvquojeh","vehbhsj","nicowu","fejqky","elg","vwpbsm","wsmplm","tijf","gtuujjesu","zatkqbmeq","qerucwer","pevdflxvo","ndagbkbqft","gbm","dqnunyiw","qa","yhumy","qh","nfprt","dpj","bkctxwbbgc","mbqqnkyi","kdn","mzunp","xrgegcu","vxfxmmqf","ruqwclx","bbauvgyaru","djpyxbbt","vmgeld","mwvvspgklq","tmqtq","nubymta","qqzeqg","wh","tul","imawmqasy","ovxoihtuz","c","tvlrtkk","g","mnqp","fscwh","ou","oq","vnghd","zptoo","cl","huhjecx","iurshssq","fijpr","xmusn","stscdep","xoyywgn","sxvgynnb","b","lnlueyomlg","f","j","exn","wltqouopqv","ulo","v","ud","vucvtms","hzkbswn","ix","tovtl","lbrqmx","vg","m","hynilg","juqa","gxrjmxzbc","pcydkl","vqsinyfb","lxcg","kjulwo","pwfpjtiw","cefvbkogi","rjxvcximeo","xxgx","osda","ocvw","aydcul","xmgva","heiuq","a","rlbncaa","kf","i","uwyajpdeb","yqnis","glrhniazyf","ixslvy","neaqh","psxpdzrq","y"]
    rows, cols = 20000, 20000  # 851000
    # --> until rows == 20000, not finish one iteration

    # sentence = ['a']
    # rows, cols = 20000, 20000

    print Solution().wordsTyping(sentence, rows, cols)

