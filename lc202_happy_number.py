class Solution(object):
    def isHappy(self, n):
        """
        :type n: int
        :rtype: bool
        """
        slow = fast = n
        while slow != 1 and fast != 1:
            slow = self.next_number(slow)
            fast = self.next_number(self.next_number(fast))

            if slow == fast and slow != 1 and fast != 1:  # detect a cycle, but not equal to 1
                return False

        return True

    def next_number(self, cur_num):
        next_num = 0
        while cur_num > 0:
            next_num += (cur_num % 10) ** 2
            cur_num /= 10
        return next_num


if __name__ == "__main__":
    print Solution().isHappy(n=19)
    print Solution().isHappy(n=37)
