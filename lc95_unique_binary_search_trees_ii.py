# -*- coding: utf-8 -*-
# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


#### Time Complexity: O(3^n)
#### Space Complexity: O(3^n)
class Solution(object):
    def generateTrees(self, n):
        """
        :type n: int
        :rtype: List[TreeNode]
        """
        return self.dfs(1, n)

    def dfs(self, left, right):
        if left == right:
            return [TreeNode(left)]

        subtrees = []
        for i in xrange(left, right + 1):
            if i == left:
                left_trees = [None]
                right_trees = self.dfs(i + 1, right)
            elif i == right:
                left_trees = self.dfs(left, i - 1)
                right_trees = [None]
            else:
                left_trees = self.dfs(left, i - 1)
                right_trees = self.dfs(i + 1, right)

            # 左/右子树都可以是有多种组合的
            # 先, Divide&Conqur 分别获取所有的左子树和右子树
            # 然后, 拼起来组成左右以当前i (left <= i <= right)为root的树
            # 返回subtrees列表
            for tl in left_trees:
                for tr in right_trees:
                    root = TreeNode(i)
                    root.left = tl
                    root.right = tr

                    subtrees.append(root)

        # 返回List, 而不是root, 因为一个root下可以对应多个subtrees!!!
        return subtrees
