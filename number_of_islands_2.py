"""
Union Find
Time: ~O(n)
Space: O(k)
"""
class UnionFind(object):
    def __init__(self, k):  # k = m*n, total num of nodes in the 2D grid
        self.parent = [-1] * k  # -1 means it is water not land
        self.num_islands = 0

    def find(self, x):
        if self.parent[x] != x:
            self.parent[x] = self.find(self.parent[x])
        return self.parent[x]

    def union(self, x, y):
        rx, ry = self.find(x), self.find(y)
        if rx != ry:
            self.parent[min(rx, ry)] = max(rx, ry)
            self.num_islands -= 1

    def addLand(self, x):
        if self.parent[x] == -1:
            self.parent[x] = x  # change water to land
            self.num_islands += 1

    def isLand(self, x):
        return self.parent[x] != -1


class Solution(object):
    def numIslands2(self, m, n, positions):
        """
        :type m: int
        :type n: int
        :type positions: List[List[int]]
        :rtype: List[int]
        """
        res = []
        uf = UnionFind(m * n)
        directions = [(-1, 0), (1, 0), (0, 1), (0, -1)]
        for pos in positions:
            x, y = pos[0], pos[1]
            uf.addLand(x * n + y)
            # check if add any edge
            for d in directions:
                i, j = x + d[0], y + d[1]
                if 0 <= i < m and 0 <= j < n and uf.isLand(i * n + j):  # if its neighbour is also a land --> add one edge
                    uf.union(x * n + y, i * n + j)
            res.append(uf.num_islands)

        return res


if __name__ == "__main__":
    m, n = 3, 3
    positions = [[0, 0], [0, 1], [1, 2], [2, 1]]

    m, n = 8, 2
    positions = [[7, 0]]
    print Solution().numIslands2(m, n, positions)