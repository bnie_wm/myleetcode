# -*- coding: utf-8 -*-


def binary_search_left(nums, x):
    """
    即: bisect.bisect_left
    return the index of 1st occurrence of x in nums
    e.g., nums=[0, 1, 1, 2], x=1 --> return 1
    """
    if not nums:
        return -1

    """
    注意:
    1. while终止条件: left + 1 < right --> 至少间隔一个元素
    2. 不用 mid++ 或 mid-- !!!
    3. 最后比较left和right, 判断返回哪个
    """
    left, right = 0, len(nums) - 1
    while left + 1 < right:
        mid = left + (right - left) / 2
        if nums[mid] == x:
            right = mid    # find 1st occur, right=mid; find last occur, left=mid
        elif nums[mid] < x:
            left = mid
        else:
            right = mid

    if nums[left] == x:
        return left
    if nums[right] == x:
        return right

    return -1


def binary_search(nums, x):
    """
    不是 bisect.bisect / bisect.bisect_right
    return the index of the last occurrence of x in nums
    e.g., nums=[0, 1, 1, 2], x=1 --> return 2 (而bisect.bisect返回3)
    """
    if not nums:
        return -1

    left, right = 0, len(nums) - 1
    while left + 1 < right:
        mid = left + (right - left) / 2
        if nums[mid] == x:
            left = mid  # find last occurrence
        elif nums[mid] < x:
            left = mid
        else:
            right = mid

    # 先查right, 再查left -- 访问顺序很重要
    # e.g., x = [0, 1, 1, 1, 1] -- exit时, left=4, right=5, 值都为1, 要先返回right
    if nums[right] == x:
        return right
    if nums[left] == x:
        return left

    return -1


if __name__ == "__main__":
    nums = [0, 1, 1, 3]
    print nums
    print binary_search_left(nums, 1)
    print binary_search(nums, 1)
