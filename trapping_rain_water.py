# Given n non-negative integers representing an elevation map where the width of each bar is 1,
# compute how much water it is able to trap after raining.
#
# For example,
# Given [0,1,0,2,1,0,1,3,2,1,2,1], return 6.


"""
Greedy
Time: O(n)
Space: O(1)
"""


class Solution(object):
    def trap(self, height):
        """
        :type height: List[int]
        :rtype: int
        """
        area = 0
        # find top
        idx_top = 0
        for i in xrange(len(height)):
            if height[idx_top] < height[i]:
                idx_top = i

        # left --> top
        idx_2top = 0  # init: assume, left most is the second top
        for i in xrange(idx_top):
            if height[idx_2top] < height[i]:
                idx_2top = i
            area += height[idx_2top] - height[i]  # add the amount of water trapped by this bar

        # right --> top
        idx_2top = len(height) - 1  # init: assume, right most is the second top
        for i in xrange(len(height) - 1, idx_top, -1):
            if height[idx_2top] < height[i]:
                idx_2top = i
            area += height[idx_2top] - height[i]

        return area


if __name__ == "__main__":
    height = [0,1,0,2,1,0,1,3,2,1,2,1]
    print Solution().trap(height)