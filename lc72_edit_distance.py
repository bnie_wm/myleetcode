class Solution(object):
    def minDistance(self, word1, word2):
        """
        :type word1: str
        :type word2: str
        :rtype: int
        """
        m, n = len(word1), len(word2)
        if m == 0 or n == 0:
            return m if n == 0 else n

        # dp[i][j]: word1的前i个字符匹配上word2的前j个字符最少要用的编辑次数
        dp = [[0 for _ in xrange(n + 1)] for _ in xrange(m + 1)]
        # init 1st row
        for j in xrange(n + 1):
            dp[0][j] = j
        # init 1st column
        for i in xrange(m + 1):
            dp[i][0] = i

        for i in xrange(1, m + 1):
            for j in xrange(1, n + 1):
                dp[i][j] = min(dp[i - 1][j] + 1,  # i-1与j匹配了，insert word1[i-1]
                               dp[i][j - 1] + 1)  # i与j-1匹配了，remove word1[i-1]
                # 再分情况考虑replace
                if word1[i - 1] == word2[j - 1]:
                    dp[i][j] = min(dp[i][j], dp[i - 1][j - 1])  # i-1与j-1已经匹配了，无需+1
                else:
                    dp[i][j] = min(dp[i][j], dp[i - 1][j - 1] + 1)  # i-1与j-1匹配了，replace word1[i-1] with word2[j-1]
        return dp[m][n]
