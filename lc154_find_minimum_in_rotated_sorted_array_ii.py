# -*- coding: utf-8 -*-


class Solution(object):
    def findMin(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """

        if not nums:
            return None

        left, right = 0, len(nums) - 1
        while left + 1 < right:
            mid = left + (right - left) / 2

            # 无法判断哪边是有序的 -- 最小值可能在任意一边
            if nums[left] == nums[mid] == nums[right]:
                left += 1
                right -= 1

            # 从left到right, 已经是递增的了
            elif nums[left] <= nums[mid] <= nums[right]:
                return nums[left]

            # 只有左边有序, 则minimum在右边
            elif nums[left] <= nums[mid]:
                left = mid

            # 只有右边有序, 则minimum在左边
            else:
                right = mid

        return nums[left] if nums[left] < nums[right] else nums[right]


if __name__ == "__main__":
    nums = [2, 2, 2, 0, 1]
    print Solution().findMin(nums)