class Solution(object):
    def firstBadVersion(self, n):
        """
        :type n: int
        :rtype: int
        """
        left, right = 1, n
        while left <= right:
            mid = (left + right) / 2
            if isBadVersion(mid):
                right = mid - 1
            else:
                left = mid + 1
        return left


def isBadVersion(x):
    if x <= 1:
        return True
    else:
        return False


if __name__ == "__main__":
    print Solution().firstBadVersion(2)