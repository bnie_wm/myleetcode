# -*- coding: utf-8 -*-

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

#### 同LC124 -- 共同点: 可不含root, 所以D&G不能直接返回题目所求
#### 而是返回必须含有root的所求
class Solution(object):
    def longestUnivaluePath(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        self.path_length = 0
        self.dfs(root)
        return self.path_length

    # 返回含有root的最长的path
    def dfs(self, root):
        if not root:
            return 0

        left = self.dfs(root.left)
        right = self.dfs(root.right)

        # 跟新含有root的最大长度
        left_val = root.left.val if root.left else None
        right_val = root.right.val if root.right else None
        from_left = left + 1 if left_val == root.val else 0
        from_right = right + 1 if right_val == root.val else 0
        path_length_include_root = max(from_left, from_right)

        # if left_val == root.val == right_val:  # 不加, 因为计算from_left/from_right时已经考虑过了
        self.path_length = max(from_left + from_right, self.path_length)

        return path_length_include_root

