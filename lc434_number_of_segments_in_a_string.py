class Solution(object):
    def countSegments(self, s):
        """
        :type s: str
        :rtype: int
        """

        s = s.strip()
        if not s:
            return 0

        num_seg = 0
        prev_char = True
        for char in s:
            if char == " ":
                prev_char = True
            else:
                if prev_char:
                    num_seg += 1
                prev_char = False
        return num_seg


if __name__ == "__main__":
    s = Solution()
    print s.countSegments(s="Hello, my name is John")
    print s.countSegments(s="Hello, my name is John.")
    print s.countSegments(s="Hello,")
    print s.countSegments(s="Hello")
    print s.countSegments(s="love live! mu'sic forever")