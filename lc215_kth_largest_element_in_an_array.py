import heapq


class Solution(object):
    def findKthLargest(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: int
        """

        ##### min-heap of size k
        ##### Time: O(nlogk) Space: O(k)
        heap = [float("-inf")]
        for num in nums:
            if num > heap[0]:
                if len(heap) < k:
                    heapq.heappush(heap, num)
                else:
                    heapq.heappushpop(heap, num)

        return heap[0]

#         #### Divide and Conquer -- 即快排, 返回nums[-k]
#         #### Time: O(nlogn), Space: O(logn)
#         self.quick_sort(nums, 0, len(nums) - 1)
#         return nums[-k]


#     def quick_sort(self, nums, left, right):
#         if left >= right:
#             return

#         pos = self.partition(nums, left, right)
#         self.quick_sort(nums, left, pos - 1)
#         self.quick_sort(nums, pos + 1, right)


#     def partition(self, nums, left, right):
#         pivot = nums[left]
#         while left < right:
#             while left < right and nums[right] > pivot:
#                 right -= 1
#             nums[left] = nums[right]

#             while left < right and nums[left] <= pivot:
#                 left += 1
#             nums[right] = nums[left]

#         nums[left] = pivot
#         return left



