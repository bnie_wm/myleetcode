# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
    def sortedArrayToBST(self, nums):
        """
        :type nums: List[int]
        :rtype: TreeNode
        """
        if not nums:
            return None

        return self.dfs(0, len(nums) - 1, nums)

    def dfs(self, left, right, nums):
        if left == right:
            return TreeNode(nums[left])
        elif left + 1 == right:
            root = TreeNode(nums[right])
            root.left = TreeNode(nums[left])
            return root

        mid = (right + left) / 2
        root = TreeNode(nums[mid])
        root.left = self.dfs(left, mid - 1, nums)
        root.right = self.dfs(mid + 1, right, nums)
        return root
