class Solution(object):
    def countBattleships(self, board):
        """
        :type board: List[List[str]]
        :rtype: int
        """
        if not board:
            return 0

        cnt = 0
        for i in xrange(len(board)):
            for j in xrange(len(board[0])):
                # cnt += int(board[i][j] == 'X' and \
                #        (i == 0 or board[i - 1][j] != 'X') and \
                #        (j == 0 or board[i][j - 1] != 'X'))
                isValid = False
                if board[i][j] == 'X' \
                    and (i == 0 or board[i-1][j] != 'X') \
                    and (j == 0 or board[i][j-1] != 'X'):
                    isValid = True
                if isValid:
                    cnt += 1
        return cnt

if __name__ == "__main__":
    board = ["...X", "XXXX", "...X"]
    print Solution().countBattleships(board)