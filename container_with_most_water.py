## Given n non-negative integers a1, a2, ..., an,
## where each represents a point at coordinate (i, ai).
## n vertical lines are drawn such that the two endpoints of line i is at (i, ai) and (i, 0).
## Find two lines, which together with x-axis forms a container,
##  such that the container contains the most water.

"""
Greedy
Time: O(n)
Space: O(1)
"""


class Solution(object):

    # @param heights: a list of integers
    # @return: an integer
    def maxArea(self, heights):
        max_area, i ,j = 0, 0, len(heights)-1

        while i < j:
            ### by default, assume the most outside container is the largest
            ### min(heights[0], heights[len(heights)-1]) * len(heights)
            max_area = max(max_area, min(heights[i], heights[j]) * (j-i))

            ### move right if left height is shorter; otherwise, move left
            ### seek if there is container with higher height though with reduced width, but lead to larger space
            if heights[i] < heights[j]:
                i += 1
            else:
                j -= 1
        return max_area


if __name__ == "__main__":
    heights = [1,3,2]
    heights = [1, 2, 3, 4, 3, 2, 1, 5]
    print Solution().maxArea(heights)