# -*- coding: utf-8 -*-
# ### Extension of "Largest Rectangle in Histogram"
# Given a 2D binary matrix filled with 0's and 1's, find the largest rectangle containing only 1's and return its area.
#
# For example, given the following matrix:
#
# 1 0 1 0 0
# 1 0 1 1 1
# 1 1 1 1 1
# 1 0 0 1 0
# Return 6.


"""
Ascending stack
Iterate through each row, if 'base'=='1', increase height by 1; otherwise, clear height
Then, for the heights of each row, use "largest_rectange_in_histogram" to find max rectangle

Time: O(m*n)
Space: O(n)
"""

#### O(m*n): O(m) iterate each row * O(n) find the max_area using each row as base
class Solution(object):
    def maximalRectangle(self, matrix):
        """
        :type matrix: List[List[str]]
        :rtype: int
        """
        if not matrix:
            return 0

        m, n = len(matrix), len(matrix[0])

        max_area = 0
        heights = [0] * n
        for i in xrange(m):  # build heights row by row
            heights = [heights[j] + 1 if matrix[i][j] == "1" else 0 for j in xrange(n)]  # don't forget "else 0"
            max_area = max(max_area, self.largestRectangleArea(heights))
        return max_area

    ### O(n) find the largest histogram given heights
    ### This is lc84_Largest_Rectangle_in_Histogram
    def largestRectangleArea(self, heights):
        heights = [0] + heights + [0]  # add dummy boundaries

        ## ascending_stack[-1] is bounded by ascending_stack[-2] and i
        ascending_stack = []

        i = 0
        max_area = 0
        while i < len(heights):
            if i == 0 or heights[i] >= heights[ascending_stack[-1]]:
                ascending_stack.append(i)
                i += 1
            else:
                cur_idx = ascending_stack.pop()
                left_bound, right_bound = ascending_stack[-1], i
                this_area = heights[cur_idx] * (right_bound - left_bound - 1)
                if this_area > max_area:
                    max_area = this_area
        return max_area


if __name__ == "__main__":
    matrix = ["10100",
              "10111",
              "11111",
              "10010"]
    matrix = ["01","10"]
    print Solution().maximalRectangle(matrix)
