# -*- coding: utf-8 -*-


class Solution(object):
    def decodeString(self, s):
        """
        :type s: str
        :rtype: str
        """

        # using stack
        # init with [1, ""] -- serve as dummy, so we don't need to handle empty stack
        stack = [[1, ""]]  # [cnt, word]  --> must be list, rather than tuple (as tuple cannot use index)
        i = 0
        while i < len(s):
            if s[i].isdigit():
                cnt = ""
                while i < len(s) and s[i].isdigit():
                    cnt += s[i]
                    i += 1
                stack.append([int(cnt), ""])

            elif s[i] == "[":
                pass

            elif s[i] == "]":
                # construct the last item in stack, append to the second last
                last = stack.pop()
                stack[-1][1] += "".join([last[1]] * last[0])

            else:  # append cur char to the last item in stack
                stack[-1][1] += s[i]

            i += 1

        return stack[-1][1]


if __name__ == "__main__":
    # s = "3[a2[c]]4[e]"
    # s = "2[2[b]]"
    # s = "2[abc]3[cd]ef"
    # s = "100[ac]"
    # s = "3a[2[c]]"
    # s = "sd2[f2[e]g]in2[bc]end"
    # s = "sd"
    # s = "3[a]2[bc]"
    s = "aa3[3[bc]4[xy]]xd5[rt]end"
    print Solution().decodeString(s)
