# Given a digit string, return all possible letter combinations that the number could represent.
#
# A mapping of digit to letters (just like on the telephone buttons) is given below.
#
# lookup = ["", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"]
#
# Input:Digit string "23"
# Output: ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"].
# Note:
# Although the above answer is in lexicographical order, your answer could be in any order you want.
#


"""
Backtracking
Time: O(n * 4^n)  --> n: num of digits, 4: at most 4 letters per digit
Space: O(n)
"""
class Solution:
    def letterCombinations(self, digits):
        """
        :type digits: str
        :rtype: List[str]
        """
        if digits == "":
            return []

        lookup = ["", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"]
        results = []
        self.letterCombinationsHelper(results, [], list(digits), lookup, 0)
        return results

    def letterCombinationsHelper(self, results, res, digits, lookup, cur_idx):
        if cur_idx == len(digits):
            results.append("".join(res))
            return results

        for letter in lookup[int(digits[cur_idx])]:
            res.append(letter)
            self.letterCombinationsHelper(results, res, digits, lookup, cur_idx + 1)
            res.pop()


if __name__ == "__main__":
    print Solution().letterCombinations("23")