class Solution2(object):
    """
    Time: O(n^2)
    Space: O(1)
    """
    def longestPalindrome(self, s):
        """
        :type s: str
        :rtype: str
        """
        if len(s) <= 1:
            return s

        maxLen = 0
        mid_idx = 0
        # if len of palindrome is odd
        for i in xrange(1, len(s)):
            for l in xrange(1, len(s) / 2 + 1):  # len(s)/2+1!, i.e., "aba"
                if i - l >= 0 and i + l < len(s) and s[i - l] == s[i + l]:
                    if l > maxLen:
                        maxLen = l
                        mid_idx = i
                else:
                    break
        res = s[mid_idx - maxLen: mid_idx + maxLen + 1]

        # if len of palindrome is even
        longer = False
        for i in xrange(1, len(s)):
            for l in xrange(0, len(s) / 2 + 1):  # if even, start from 0!
                if i - 1 - l >= 0 and i + l < len(s) and s[i - 1 - l] == s[i + l]:
                    if l >= maxLen:
                        longer = True
                        maxLen = l
                        mid_idx = i
                else:
                    break
        if longer:
            res = s[mid_idx - 1 - maxLen: mid_idx + maxLen + 1]

        return res


"""
DP
base: dp[i][j] = True
p[i][j] = (s[i] == s[j]) and (p[i+1][j-1] == True)
Time: O(n^2)
Space: O(n^2)
"""
class Solution(object):
    def longestPalindrome(self, s):
        """
        :type s: str
        :rtype: str
        """
        if len(s) <= 1:
            return s

        dp = [[False for _ in xrange(len(s))] for _ in xrange(len(s))]

        # build strLen=1: dp[i][i]
        for i in xrange(len(s)):
            dp[i][i] = True

        maxLen = 0
        begin_idx = 0
        # build strLen=2: dp[i][i+1]
        for i in xrange(len(s) - 1):
            if s[i] == s[i + 1]:
                dp[i][i + 1] = True
                maxLen = 2
                begin_idx = i

        # build strlen>=3: dp[i][i+k], where k >= 3-1
        for curLen in xrange(3, len(s) + 1):  # possible str len: [3, len(s)]
            for i in xrange(0, len(s) - curLen + 1):
                j = i + curLen - 1
                if s[i] == s[j] and dp[i+1][j-1]:
                    dp[i][j] = True
                    maxLen = curLen
                    begin_idx = i

        return s[begin_idx: begin_idx + maxLen]


"""
Brute Force
Time: O(n^3)
Space: O(1)
"""
class Solution1(object):
    def longestPalindrome(self, s):
        """
        :type s: str
        :rtype: str
        """
        max_len = 0
        max_substr = (-1, -1)
        for i in xrange(len(s)):
            for j in xrange(i, len(s)):   # j starts from i
                if self.isPalindrome(s, i, j):
                    if j-i+1 > max_len:
                        max_len = j-i+1
                        max_substr = (i, j)

        return s[max_substr[0]: max_substr[1]+1]

    def isPalindrome(self, s, i, j):
        while i < j:
            if s[i] != s[j]:
                return False
            i += 1
            j -= 1
        return True


if __name__ == "__main__":
    str = "bananas"
    # str = "ccc"
    print Solution2().longestPalindrome(str)
    print Solution1().longestPalindrome(str)
    print Solution().longestPalindrome(str)