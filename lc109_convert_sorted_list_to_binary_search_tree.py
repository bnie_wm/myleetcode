# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
    def sortedListToBST(self, head):
        """
        :type head: ListNode
        :rtype: TreeNode
        """

        if not head:
            return None

        tail = head
        while tail.next:
            tail = tail.next

        return self.dfs(head, tail.next)

    def dfs(self, head, tail):  # List starts from head, ends in tail (excluded)
        if head.next == tail:  # only one element
            return TreeNode(head.val)
        elif head.next.next == tail:  # only two elements
            root = TreeNode(head.next.val)
            root.left = TreeNode(head.val)
            return root

        mid = self.findMid(head, tail)
        root = TreeNode(mid.val)
        root.left = self.dfs(head, mid)
        root.right = self.dfs(mid.next, tail)
        return root

    def findMid(self, head, tail):
        slow, fast = head, head
        while fast.next and fast.next.next:
            slow = slow.next
            fast = fast.next.next
            if fast.next == tail or fast.next.next == tail:
                break
        return slow