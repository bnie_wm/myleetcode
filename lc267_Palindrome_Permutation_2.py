class Solution(object):
    def generatePalindromes(self, s):
        """
        :type s: str
        :rtype: List[str]
        """

        if not s:
            return []

        # if contain two or more odd occurrences -- no palindrome permutation
        count = collections.Counter(s)
        odd_char = ""  # if there is one, this odd_char must in the middle
        even_chars = []  # save half occurrence of every evenly occurred chars
        for char in count:
            if count[char] % 2 == 1:
                if odd_char == "":
                    odd_char = char
                    if count[char] > 1:
                        even_chars.extend([char] * ((count[char] - 1) / 2))
                else:  # if contain >=1 odd occurred chars --> no palindrome permutation
                    return []
            else:
                even_chars.extend([char] * (count[char] / 2))

        results = []
        visited = [False] * len(even_chars)
        self.dfs([], results, even_chars, odd_char, visited)
        return results

    # get all permutations for even_chars
    def dfs(self, res, results, even_chars, odd_char, visited):
        if len(res) == len(even_chars):
            # build palindrome
            pstr = "".join(res) + odd_char + "".join(res[::-1])
            results.append(pstr)

        prev = None
        for i in xrange(len(even_chars)):
            if (i == 0 or prev != even_chars[i]) and (not visited[i]):
                visited[i] = True
                self.dfs(res + [even_chars[i]], results, even_chars, odd_char, visited)
                visited[i] = False
                prev = even_chars[i]




