# -*- coding: utf-8 -*-

#### O(logn) ~ O(n)
class Solution1(object):
    def findPeakElement(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        n = len(nums)
        if n <= 1:
            return 0

        pos = self.binary_search(0, n - 1, nums)

        return pos

    def binary_search(self, left, right, nums):
        if left > right:  # >, not >= e.g., [1, 2] --> when left == right, still need to check if left/right is the peak
            return -1

        mid = left + (right - left) / 2

        left_val = nums[mid - 1] if mid > 0 else float("-inf")
        right_val = nums[mid + 1] if mid < len(nums) - 1 else float("-inf")
        if left_val < nums[mid] and nums[mid] > right_val:
            return mid

        pos = self.binary_search(left, mid - 1, nums)
        if pos != -1:
            return pos
        else:
            return self.binary_search(mid + 1, right, nums)


class Solution(object):
    def findPeakElement(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        # 由于只是需要找到任意一个峰值，那么我们在确定二分查找折半后中间那个元素后，和紧跟的那个元素比较下大小，
        # 如果大于，则说明峰值在前面，如果小于则在后面。这样就可以找到一个峰值了
        n = len(nums)
        if n <= 1:
            return 0

        left, right = 0, n - 1
        while left + 1 < right:
            mid = left + (right - left) / 2

            right_val = nums[mid + 1] if mid < n - 1 else float("-inf")
            if nums[mid] > right_val:
                right = mid
            else:
                left = mid

        if nums[left] > nums[right]:
            return left
        else:
            return right


if __name__ == "__main__":
    nums = [1, 2]
    print Solution().findPeakElement(nums)

    nums = [1,2,1,3,5,6,4]
    print Solution().findPeakElement(nums)

