class TrieNode(object):
    def __init__(self):
        self.is_word = False
        self.children = {}

    def insert(self, word):
        cur = self
        for c in word:
            if c not in cur.children:
                cur.children[c] = TrieNode()
            cur = cur.children[c]
        cur.is_word = True


####
# Time Complexity: O(k*l + m*n*l*4^l).
#       k*l: build Trie.
#       m*n: loop in findWords,
#       l: search Trie time,
#       4^l: recursion + backtracking (4 directions, l is word length)
# Space Complexity: O(k*l + l).
#       k*l: Trie
#       l: stack (recursion/dfs)
####
class Solution(object):
    def findWords(self, board, words):
        """
        :type board: List[List[str]]
        :type words: List[str]
        :rtype: List[str]
        """

        if not board:
            return []

        trie = TrieNode()
        for word in words:
            trie.insert(word)

        m, n = len(board), len(board[0])
        results = []
        # each cell in one dfs path only be visited once -- avoid infinity loop
        # avoid duplicates: i.e., boards = [['a', 'a']] words=['aaa']
        visited = [[False for _ in xrange(n)] for _ in xrange(m)]
        for i in xrange(m):
            for j in xrange(n):
                if board[i][j] in trie.children:
                    visited[i][j] = True
                    self.dfs_search(i, j, trie.children[board[i][j]], board[i][j], results, board, m, n, visited)
                    visited[i][j] = False
        return results

    def dfs_search(self, i, j, trie, cur_word, results, board, m, n, visited):
        if trie.is_word:
            if cur_word not in results:
                results.append(cur_word)
            # cannot return: i.e., words = ["abc", "abcd"]. if return, will never find "abcd"

        directions = [(0, 1), (0, -1), (1, 0), (-1, 0)]
        for dir in directions:
            x, y = i + dir[0], j + dir[1]
            if x < 0 or x >= m or y < 0 or y >= n or visited[x][y]:  # invalid
                continue
            if board[x][y] in trie.children:
                visited[x][y] = True
                self.dfs_search(x, y, trie.children[board[x][y]], cur_word + board[x][y], results, board, m, n, visited)
                visited[x][y] = False


if __name__ == "__main__":
    board = ["oaan", "etae", "ihkr", "iflv"]
    words = ["oath", "pea", "eat", "rain"]
    print Solution().findWords(board, words)
