class Solution(object):
    def minimum_repeating(self, n):
        if n == 1:
            return "0"

        remainder_list = []
        repeating = ""  # quotient

        r = 1 % n
        while True:
            if r == 0:
                return "0"

            if r in remainder_list:
                break
            remainder_list.append(r)

            r *= 10
            q = r / n
            repeating += str(q)
            r %= n

        # get the next quotient -- to detect the circle begin
        q = str(r * 10 / n)
        begin_index = repeating.index(q)
        return repeating[begin_index:]


if __name__ == "__main__":
    s = Solution()
    for n in xrange(1, 20):
        print n, s.minimum_repeating(n)

    for n in xrange(90, 100):
        print n, s.minimum_repeating(n)
