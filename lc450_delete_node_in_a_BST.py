# -*- coding: utf-8 -*-
# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
    def deleteNode(self, root, key):
        """
        :type root: TreeNode
        :type key: int
        :rtype: TreeNode
        """
        if not root:
            return root
        # 递归找到root.val == key
        if key > root.val:
            root.right = self.deleteNode(root.right, key)
            return root  # 返回root, 而不是root.right
        elif key < root.val:
            root.left = self.deleteNode(root.left, key)
            return root

        # 找到了root.val == key
        else:
            # 如果root有左子树也有右子树
            if root.left and root.right:
                # O(h) 找 右子树中的最小的点
                succ = root.right  # min in right tree, inorder succesor
                while succ.left:
                    succ = succ.left
                root.val = succ.val
                root.right = self.deleteNode(root.right, succ.val)  # 递归删除右子树中的succ (即, O(h)走下边"else"的情况)
                return root
            # 如果root是叶子, 或者只有左子树,或者只有右子树
            else:
                new_root = root.left if root.left else root.right
                root.left = root.right = None
                del root
                return new_root

