class UnionFind(object):
    def __init__(self, nums):
        n = len(nums)
        self.parents = range(n)  # save the idx of numbers
        self.ranks = [0] * n
        self.counts = [1] * n  # initially, each sinlge node forms a LCS with len=1

    def find(self, x):
        if x != self.parents[x]:
            self.parents[x] = self.find(self.parents[x])  # data compression
        return self.parents[x]

    def union(self, x, y):
        x_parent, y_parent = self.find(x), self.find(y)
        if x_parent == y_parent:  # x, y already from the same tree
            return False
        if self.ranks[x] > self.ranks[y]:
            self.parents[y_parent] = x_parent  # merge small tree to large tree
            self.counts[x_parent] += self.counts[y_parent]  # add count of small tree to large tree
        elif self.ranks[x] < self.ranks[y]:
            self.parents[x_parent] = y_parent
            self.counts[y_parent] += self.counts[x_parent]
        else:
            self.parents[x_parent] = y_parent
            self.ranks[y_parent] += 1
            self.counts[y_parent] += self.counts[x_parent]
        return True

    def getCount(self, x):
        return self.counts[x]


#### Union Find: ~O(n)
class Solution(object):
    def longestConsecutive(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """

        nums = list(set(nums))  # remove duplicates

        uf = UnionFind(nums)
        for x_id, x in enumerate(nums):
            for diff in [-1, +1]:
                if x + diff in nums:
                    y_id = nums.index(x + diff)
                    uf.union(x_id,
                             y_id)  # no need to check if x_id and y_id already from the same tree, checked by uf.union

        maxLen = 0
        for x_id in xrange(len(nums)):
            maxLen = max(uf.getCount(x_id), maxLen)

        return maxLen


### HashTable: O(n)
class Solution2(object):
    def longestConsecutive(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) == 0:
            return 0

        nums = set(nums)  # remove duplicates

        # save the max len of LCS
        maxLen = {num: 1 for num in nums}

        for num in nums:
            if maxLen[num] == 0:  # visited
                continue
            nei = num - 1
            while nei in nums:
                maxLen[num] += 1
                maxLen[nei] = 0  # mark as visited
                nei -= 1
            nei = num + 1
            while nei in nums:
                maxLen[num] += 1
                maxLen[nei] = 0  # mark as visited
                nei += 1

        return max(maxLen.values())


if __name__ == "__main__":
    nums = [100, 4, 200, 1, 3, 2]
    # nums = [0, 0, -1]
    # s = Solution()
    s = Solution2()
    print s.longestConsecutive(nums)