# -*- coding: utf-8 -*-
# Given n non-negative integers representing the histogram's bar height where the width of each bar is 1,
# find the area of largest rectangle in the histogram.

#### (1) very bad: O(n^3)
class Solution1(object):
    def largestRectangleArea(self, heights):
        """
        :type heights: List[int]
        :rtype: int
        """
        n = len(heights)
        max_area = 0
        for i in xrange(n):
            for j in xrange(i, n):
                this_area = (j-i+1) * min(heights[i:j+1])
                max_area = max(this_area, max_area)
        return max_area


#### (2) optimized: O(n^2)
#### use DP to save min heights
class Solution2(object):
    def largestRectangleArea(self, heights):
        n = len(heights)
        minHeights = [0] * n
        max_area = 0
        for i in xrange(n):
            for j in xrange(i, n):
                if i == j:
                    minHeights[j] = heights[j]
                else:
                    minHeights[j] = min(minHeights[j-1], heights[j])
                this_area = (j - i + 1) * minHeights[j]
                max_area  = max(this_area, max_area)
        return max_area


#### (3) using ascending stack: O(n)  --> stack helps to find the left and right boundaries (min height)
class Solution3(object):
    def largestRectangleArea(self, heights):
        """
        :type heights: List[int]
        :rtype: int
        """
        heights = [0] + heights + [0]  # add dummy boundaries

        ## ascending_stack[-1] is bounded by ascending_stack[-2] and i
        ascending_stack = []

        i = 0
        max_area = 0
        while i < len(heights):
            if i == 0 or heights[i] >= heights[ascending_stack[-1]]:
                ascending_stack.append(i)
                i += 1
            else:
                cur_idx = ascending_stack.pop()
                left_bound, right_bound = ascending_stack[-1], i
                this_area = heights[cur_idx] * (right_bound - left_bound - 1)
                if this_area > max_area:
                    max_area = this_area
        return max_area


if __name__ == "__main__":
    # heights = [2,1,5,6,2,3]
    heights = [2, 1, 2, 5, 6, 3]
    print Solution1().largestRectangleArea(heights)
    print Solution2().largestRectangleArea(heights)
    print Solution3().largestRectangleArea(heights)