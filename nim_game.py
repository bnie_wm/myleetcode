# -*- coding: utf-8 -*-
# You are playing the following Nim Game with your friend: There is a heap of stones on the table, each time one of you take turns to remove 1 to 3 stones. The one who removes the last stone will be the winner. You will take the first turn to remove the stones.
#
# Both of you are very clever and have optimal strategies for the game. Write a function to determine whether you can win the game given the number of stones in the heap.
#
# For example, if there are 4 stones in the heap, then you will never win the game: no matter 1, 2, or 3 stones you remove, the last stone will always be removed by your friend.
#
# Hint:
#
# If there are 5 stones in the heap, could you figure out a way to remove the stones such that you will always be the winner?


"""
DP
Time: O(n)
Space: O(n)
"""
class Solution3(object):
    def canWinNim(self, n):
        """
        :type n: int
        :rtype: bool
        """

        res = [False] * (n+1)

        # init, when n <= 3, A always wins!
        # first 0 is just to fill in index=0
        for i in xrange(1, n+1):
            if n <= 3:
                res[i] = True

            canAWin = False
            for a in [1, 2, 3]:  # A's move
                if not res[i - a]:  # B's move: if res[i-a]=F, indicating that B must lose, then, A must win
                    canAWin = True
                    break
            res[i] = canAWin

        return res[n]




"""
Math:
Time: O(1)
Space: O(1)

原理:
因为A先出, 所以当n=1,2,3时, A一定赢; 当n=4时, B一定赢。
所以,制胜关键是, 想办法总是留下4个石头给B,这样无论B怎样拿,A都一定赢。

所以,第一次A取(n%4)块石头, 之后若B取x块石头, 则A取(4-x)块, 从而保持每次A+B=4,使得留给B的石头数量永远为4的倍数。
而只有当初始石头数量为4的倍数是,A才一定不会赢。

"""
class Solution1(object):
    def canWinNim(self, n):
        """
        :type n: int
        :rtype: bool
        """
        return n % 4 != 0


class Solution2(object):
    def canWinNim(self, n):
        """
        :type n: int
        :rtype: bool
        """

        # n >> 2: n/4
        # n << 2: n * 4
        return n >> 2 << 2 != n


if __name__ == "__main__":
    n = 13
    print Solution1().canWinNim(n)
    print Solution2().canWinNim(n)
    print Solution3().canWinNim(n)

    for n in xrange(5000):
        # print Solution1().canWinNim(n), Solution3().canWinNim(n)
        s1 = Solution1().canWinNim(n)
        s3 = Solution3().canWinNim(n)
        if s1 != s3:
            print n, "Sol1=" + str(s1), "Sol3=" + str(s3)