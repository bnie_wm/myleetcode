# -*- coding: utf-8 -*-

class Solution(object):
    def kthSmallest(self, matrix, k):
        """
        :type matrix: List[List[int]]
        :type k: int
        :rtype: int
        """

        if not matrix:
            return None

        n = len(matrix)
        if n == 1 or k == 1:
            return matrix[0][0]

        # Binary Search:
        # matrix左上角和右下角的数是min和max.
        # 每次判断 mid=(max-min)/2是matrix的第几个数  -- 使用get_rank(), O(n)时间
        # O(nlogK), where K=max-min

        min_num, max_num = matrix[0][0], matrix[n - 1][n - 1]
        while min_num + 1 < max_num:
            mid = min_num + (max_num - min_num) / 2
            cnt, num = self.get_rank(mid, matrix, n)

            if cnt == k:
                min_num = mid
            elif cnt < k:
                min_num = mid
            else:
                max_num = mid

        # find out which is the correct one: min_num or max_num
        cnt, num = self.get_rank(min_num, matrix, n)
        if cnt >= k:  # here >= , not == !!!, E.g., [[1,2],[1,3]], k=1
            return num
        else:
            return self.get_rank(max_num, matrix, n)[1]

    def get_rank(self, x, matrix, n):
        # 从左下角的数开始与x比较, 判断x是matrix中第几大的数
        cnt = 0
        i, j = n - 1, 0

        # x可能不在matrix中, 找到最大的那个小于或等于x的数
        num = float("-inf")  # NOT num=0, 因为matrix可以有负数
        while i >= 0 and j < n:
            if matrix[i][j] <= x:
                cnt += (i + 1)
                num = max(num, matrix[i][j])
                j += 1  # go right
            else:
                i -= 1  # go up

        return cnt, num


if __name__ == "__main__":
    matrix = [[1, 2], [1, 3]]
    k = 2
    print Solution().kthSmallest(matrix, k)

    matrix = [[-5]]
    k = 1
    print Solution().kthSmallest(matrix, k)
