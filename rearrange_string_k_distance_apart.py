# Given a non-empty string str and an integer k, rearrange the string such that the same characters are at least distance k from each other.
#
# All input strings are given in lowercase letters. If it is not possible to rearrange the string, return an empty string "".
#
# Example 1:
# str = "aabbcc", k = 3
#
# Result: "abcabc"
#
# The same letters are at least distance 3 from each other.
# Example 2:
# str = "aaabc", k = 3
#
# Answer: ""
#
# It is not possible to rearrange the string.
# Example 3:
# str = "aaadbbcc", k = 2
#
# Answer: "abacabcd"
#
# Another possible answer is: "abcabcda"
#
# The same letters are at least distance 2 from each other.
# Credits:
# Special thanks to @elmirap for adding this problem and creating all test cases.

import collections
import heapq


# Time:  O(nlogc), c is the count of unique characters.
# Space: O(c)
class Solution(object):
    def rearrangeString(self, str, k):
        """
        :type str: str
        :type k: int
        :rtype: str
        """
        if k == 0:
            return str

        cnts = collections.Counter(str)
        pq = [(-cnts[c], c) for c in cnts]
        heapq.heapify(pq)

        # res = ""  --> very inefficient when res += c and len(res)
        res = []  # use list, instead of str!
        while pq:
            used = []  # store used chars in one loop
            for _ in xrange(min(k, len(str) - len(res))):  # max num of chars need to arrange
                if not pq:
                    return ""  # if invalid
                cnt, c = heapq.heappop(pq)  # remember that cnt is neg freq
                res += [c]
                cnt += 1
                if cnt < 0:
                    used.append((cnt, c))
            for item in used:
                heapq.heappush(pq, item)  # heappush, instead of append
        return "".join(res)


if __name__ == "__main__":
    # str, k = "aabbcc", 3
    # str, k = "aabbcc", 2
    str, k = "aaabc", 2
    print Solution().rearrangeString(str, k)