import os

# dash_line = "".join(["="]*50)
dash_line = "=================================================="


def main():
    folder = "kamyu104_LeetCode"
    fout = open(os.path.join(folder, "_" + folder + ".txt"), "w")

    # folder = "."
    # fout = open("myLeetCode.txt", "w")

    cnt = 0
    for fn in os.listdir(folder):
        if folder == "." and (".py" not in fn or "toPDF" in fn):
            continue
        if "LeetCode" in fn:
            continue
        cnt += 1
        print cnt, fn
        fout.write(dash_line + "\n")
        fout.write("[" + str(cnt) + "] " + fn + "\n")
        fout.write(dash_line + "\n")
        fdata = open(os.path.join(folder, fn))
        for line in fdata:
            fout.write(line)
        fdata.close()
        fout.write("\n")
    fout.close()


if __name__ == "__main__":
    main()