# -*- coding: utf-8 -*-

class Solution:
    """
    @param nums: The integer array you should partition
    @param k: An integer
    @return: The index after partition
    """

    def partitionArray(self, nums, k):
        # write your code here

        if not nums:
            return 0

        left, right = 0, len(nums) - 1

        # 从左边开始, 找到第一个坑: 第一个>=k的数
        while left <= right and nums[left] < k:
            left += 1

        # 说明全部的数都小于k
        if left == len(nums):
            return left

        pivot = nums[left]  # 这是初始坑
        while left < right:
            # 先从右边开始, 找到第一个<k的数, 放到左边的坑位上
            while left < right and nums[right] >= k:
                right -= 1
            nums[left] = nums[right]

            # 再从左边开始, 找到第一个<=k的数, 放到右边空出来的坑位上
            while left < right and nums[left] < k:
                left += 1
            nums[right] = nums[left]

        # while exit condition: left==right
        nums[left] = pivot

        # 结果: left左边的数都<k, 右边的数都>=k --> 所以返回left
        return left
