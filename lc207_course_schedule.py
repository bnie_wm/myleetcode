# -*- coding: utf-8 -*-
import collections


class Solution(object):
    def canFinish(self, numCourses, prerequisites):
        """
        :type numCourses: int
        :type prerequisites: List[List[int]]
        :rtype: bool
        """
        if not prerequisites:
            return True

        # 拓扑排序 (DAG):
        # (1) 维护一个入度的dict
        indegrees = {course: 0 for course in xrange(numCourses)}  # 注意: 初始化所有的点, prerequisites
        # (2) 记录每个点的out edges
        graph = collections.defaultdict(list)

        # 初始化: O(|E|)
        for course, prereq in prerequisites:
            indegrees[course] += 1  # increment indegree
            graph[prereq].append(course)  # add out edges

        # 拓扑排序: O(|V|+|E|)
        # 时间复杂度: 获取每个入度为0的点的操作一共执行O(|V|)次, 而将删除每个边的操作(indegree-1)一共执行O(|E|)次
        while indegrees:
            # 找到一个入度为0的点 (没有prereq要求的课程)
            prereq = None
            for p in indegrees:
                if indegrees[p] == 0:
                    prereq = p
                    break
            if prereq is None:  # 虽然indegrees不为空, 但是不存在入度为0的点了! -- 说明DAG中有环
                return False

            # 删除当前入度为0的点,及其所有的边
            # 即将它的每个out edge可达的点的入度减1
            del indegrees[prereq]
            for course in graph[prereq]:
                if course in indegrees:
                    indegrees[course] -= 1

        return True


if __name__ == "__main__":
    numCourses = 8
    prerequisites = [[0,3], [1,3], [1,4], [2,4], [2,7], [3,5], [3,6], [3,7], [4,6]]
    print Solution().canFinish(numCourses, prerequisites)

