class Solution(object):
    def searchMatrix(self, matrix, target):
        """
        :type matrix: List[List[int]]
        :type target: int
        :rtype: bool
        """

        #### O(logn+logn)

        if not matrix:
            return False

        m, n = len(matrix), len(matrix[0])

        if n == 0:
            return False

        # use the last element of each row as guidance -- find the row containing the target
        tgt_row = self.find_row(target, [matrix[i][-1] for i in xrange(m)], m)
        if tgt_row == m:  # since we use the last element (largest) -- it means target not in matrix
            return False

        pos = self.binary_search(target, matrix[tgt_row], n)
        return True if pos != -1 else False

    def find_row(self, target, row, n):
        #### LC35. Search Insert Position
        if target <= row[0]:
            return 0

        if target > row[-1]:
            return n

        left, right = 0, n - 1
        while left + 1 < right:
            mid = left + (right - left) / 2

            if target == row[mid]:
                right = mid
            elif target < row[mid]:
                right = mid
            else:
                left = mid

        return right

    def binary_search(self, target, row, n):
        #### vanila binary search

        left, right = 0, n - 1
        while left + 1 < right:
            mid = left + (right - left) / 2

            if target <= row[mid]:
                right = mid
            elif target < row[mid]:
                right = mid
            else:
                left = mid

        if row[left] == target:
            return left

        if row[right] == target:
            return right

        return -1
