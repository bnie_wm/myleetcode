# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def detectCycle(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """

        # check if there is cycle
        has_cycle = False
        slow = fast = head
        while slow and fast and fast.next:
            slow = slow.next
            fast = fast.next.next

            if slow == fast:
                has_cycle = True
                break

        if not has_cycle:
            return None

        # locate the cycle
        fast = head
        while slow and fast:
            if slow == fast:
                break

            slow = slow.next
            fast = fast.next

        return slow
