# Definition for an interval.
class Interval(object):
    def __init__(self, s=0, e=0):
        self.start = s
        self.end = e

import heapq
class Solution(object):
    def minMeetingRooms(self, intervals):
        """
        :type intervals: List[Interval]
        :rtype: int
        """
        if not intervals:
            return 0

        intervals = sorted(intervals, key=lambda x: x.start)

        meeting_rooms = []  # save the end time of each room
        for i in xrange(len(intervals)):
            if not meeting_rooms or intervals[i].start < meeting_rooms[0]:
                # meeting_rooms.append(intervals[i].end)   # add one room
                heapq.heappush(meeting_rooms, intervals[i].end)
            else:
                # idx = meeting_rooms.index(min(meeting_rooms))  # get the room that ends the earliest
                # meeting_rooms[idx] = intervals[i].end
                meeting_rooms[0] = intervals[i].end
        return len(meeting_rooms)


if __name__ == "__main__":
    intervals = []
    intervals.append(Interval(15, 16))
    intervals.append(Interval(10, 15))
    intervals.append(Interval(16, 25))
    print Solution().minMeetingRooms(intervals)