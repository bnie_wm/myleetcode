# -*- coding: utf-8 -*-
class Solution:
    """
    @param: A: An integer array
    @param: target: An integer
    @return: An integer
    """

    def MinAdjustmentCost(self, A, target):
        # write your code here
        if not A:
            return 0

        max_int = max(A) + target
        # dp[i][j]存调整完i-1之后，再调整i到j大小，所需要的最小cost
        dp = [[float("inf") for _ in xrange(max_int + 1)] for _ in xrange(len(A))]
        # init
        for v in xrange(max_int + 1):
            dp[0][v] = abs(A[0] - v)

        for i in xrange(1, len(A)):
            # find the min cost for changing A[i] to v
            for v in xrange(max_int + 1):
                # we have constraint: |A[i-1]-A[i]|<=target
                # so, possible choices of A[i-1] can only be choosen from A[i]-target~A[i]+target
                # only use those valid A[i-1] to determine min_cost for dp[i][v]
                min_k = max(v - target, 0)
                max_k = min(v + target, max_int)
                for k in xrange(min_k, max_k + 1):
                    dp[i][v] = min(dp[i][v], dp[i - 1][k] + abs(A[i] - v))

        return min(dp[len(A) - 1])


if __name__ == "__main__":
    s = Solution()
    A = [1, 4, 2, 3]
    target = 1
    print s.MinAdjustmentCost(A, target)

    A = [12, 3, 7, 4, 5, 13, 2, 8, 4, 7, 6, 5, 7]
    target = 2
    print s.MinAdjustmentCost(A, target)
