class Solution(object):
    def setZeroes(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: void Do not return anything, modify matrix in-place instead.
        """
        if not matrix:
            return

        m, n = len(matrix), len(matrix[0])
        zrows = [0] * m
        zcols = [0] * n

        # first loop, mark rows and cols with zeros
        for i in xrange(m):
            for j in xrange(n):
                if matrix[i][j] == 0:
                    zrows[i], zcols[j] = 1, 1

        # second loop, set rows and cols with zero to zero
        for i in xrange(m):
            for j in xrange(n):
                if zrows[i] == 1 or zcols[j] == 1:
                    matrix[i][j] = 0


if __name__ == "__main__":
    matrix = [[0]]
    Solution().setZeroes(matrix)
    print matrix