# -*- coding: utf-8 -*-
import collections

"""
拓扑排序: O(|V|+|E|)
时间复杂度: 获取每个入度为0的点的操作一共执行O(|V|)次, 而将删除每个边的操作(indegree-1)一共执行O(|E|)次
作用:(1) 判断有向图中是否有环
     (2) 求出事件的执行顺序 (事件之间是有依赖关系的) -- 顺序可能不止一个
     (若要输出所有的, 则用DFS)
"""

class Solution(object):
    def findOrder(self, numCourses, prerequisites):
        """
        :type numCourses: int
        :type prerequisites: List[List[int]]
        :rtype: List[int]
        """
        if not prerequisites:
            return range(numCourses)  # 若无prerequisites, 可以按任何顺序上课

        # 拓扑排序 (DAG):
        # (1) 维护一个入度的dict (所有课程需要的prereq的数量)
        indegrees = {course: 0 for course in xrange(numCourses)}
        # (2) 记录每个点的out edges (以该课程为prereq的所有课程)
        graph = collections.defaultdict(list)  # number of courses require this course as prereq

        # 初始化: O(|E|)
        for course, prereq in prerequisites:
            indegrees[course] += 1  # increase indegree
            graph[prereq].append(course)  # add one out_edge

        res = []  # 保存课程顺序: res[i]是res[i+1]的prereq
        while indegrees:
            # 找到一个入度为0的点 (没有prereq要求的课程)
            prereq = None
            for p in indegrees:
                if indegrees[p] == 0:
                    prereq = p
                    break
            if prereq is None:  # 虽然indegrees不为空, 但是不存在入度为0的点了! -- 说明DAG中有环
                return []

            # 删除当前入度为0的点,及其所有的边(即将它的每个out edge可达的点的入度减1)
            res.append(prereq)
            del indegrees[prereq]
            for course in graph[prereq]:
                if course in indegrees:
                    indegrees[course] -= 1
        return res


if __name__ == "__main__":
    numCourses = 8
    prerequisites = [[0,3], [1,3], [1,4], [2,4], [2,7], [3,5], [3,6], [3,7], [4,6]]
    print Solution().findOrder(numCourses, prerequisites)