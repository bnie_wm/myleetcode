
#### DFS+memo: TLE
class Solution(object):
    def scheduleCourse(self, courses):
        """
        :type courses: List[List[int]]
        :rtype: int
        """

        if not courses:
            return 0

        #### DFS
        # sorted by end day -- course with early end day goes first
        courses.sort(key=lambda x: x[1])
        n = len(courses)
        max_days = sum([d for (t, d) in courses])
        memo = [[None for _ in xrange(max_days + 1)] for _ in xrange(n)]
        return self.dfs(0, 0, courses, memo)

    def dfs(self, cur, end_day, courses, memo):
        if cur == len(courses):  # recursion return case
            return 0

        if memo[cur][end_day] is not None:
            return memo[cur][end_day]

        num_courses = 0
        for i in xrange(cur, len(courses)):
            # 如果当前课程可以在要求之前完成, 则可以加入
            if end_day + courses[i][0] <= courses[i][1]:
                num_courses = max(num_courses, self.dfs(i + 1, end_day + courses[i][0], courses, memo) + 1)
        memo[cur][end_day] = num_courses
        return num_courses


##### Greedy
import heapq


class Solution(object):
    def scheduleCourse(self, courses):
        """
        :type courses: List[List[int]]
        :rtype: int
        """
        if not courses:
            return 0

        #### Greedy -- O(nlogn)
        #### # 维护一个最大堆, 记录当前上的课程的duration.若遇到一个超过ddl不能上的课, 则优先考虑去掉/不上duration最长的那门
        #### 类似于skyline -- 维护一个记录building height的最大堆, 每次和最高的楼比较
        courses.sort(key=lambda x: x[1])
        schedules = []
        end_day = 0
        for dur, ddl in courses:
            if end_day + dur <= ddl:
                heapq.heappush(schedules, -dur)
                end_day += dur
            else:
                # 将当前课程加入queue中, 然后取出queue中最久的课程去掉
                max_dur = -heapq.heappushpop(schedules, -dur)
                end_day = end_day - max_dur + dur
        return len(schedules)