class Solution:
    """
    @param: chars: The letter array you should sort by Case
    @return: nothing
    """

    def sortLetters(self, chars):
        # write your code here

        if not chars:
            return chars

        n = len(chars)
        left, right = 0, n - 1

        # 找左坑 -- 左边第一个大写字母
        while left <= right and chars[left].islower():
            left += 1
        if left == n:  # all lower cases
            return chars

        tmp = chars[left]  # 暂存第一个坑
        while left < right:
            while left < right and chars[right].isupper():
                right -= 1
            chars[left] = chars[right]

            while left < right and chars[left].islower():
                left += 1
            chars[right] = chars[left]

        chars[left] = tmp

        return chars
