# -*- coding: utf-8 -*-

class Solution(object):
    def findDuplicate(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """

        #  e.g., [2,2,2,2,2]
        # 将题目转换成Linked List Cycle II -- 返回环开始的地方
        # nums[x] = y --> 看成 node(x).next = node(y)

        n = len(nums)

        if n == 1:
            return nums[0]

        # 判断是否有dup的数 -- 依题, 一定有重复的数(有环), 因为n+1个位置放1~n的数,
        slow = fast = nums[0]
        while slow < n and fast < n and nums[fast] < n:
            slow = nums[slow]
            fast = nums[nums[fast]]

            if slow == fast:
                break

        # 如果有(必须有), 找到它
        # Additionally, because 0 cannot appear as a value in nums, nums[0] cannot be part of the cycle.
        # Therefore, traversing the array in this manner from nums[0] is equivalent to traversing a cyclic linked list.
        # Given this, the problem can be solved just like Linked List Cycle II.
        fast = nums[0]
        while slow < n and fast < n:
            if slow == fast:
                break
            slow = nums[slow]
            fast = nums[fast]

        return slow


if __name__ == "__main__":
    nums = [3,1,3,4,2]
    print Solution().findDuplicate(nums)

    nums = [2, 2, 2, 2]
    print Solution().findDuplicate(nums)
