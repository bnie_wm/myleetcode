class UnionFind(object):
    def __init__(self, n):
        self.parents = range(n)
        self.ranks = [0] * (n)

    def find(self, x):
        if x != self.parents[x]:
            self.parents[x] = self.find(self.parents[x])
        return self.parents[x]

    def union(self, x, y):
        px, py = self.find(x), self.find(y)
        if px == py:
            return False

        if self.ranks[px] > self.ranks[py]:
            self.parents[py] = px
        elif self.ranks[px] < self.ranks[py]:
            self.parents[px] = py
        else:
            self.parents[py] = px
            self.ranks[px] += 1
        return True


class Solution(object):
    def findRedundantConnection(self, edges):
        """
        :type edges: List[List[int]]
        :rtype: List[int]
        """

        # find N
        N = 0
        for (u, v) in edges:
            N = max(N, v)

        uf = UnionFind(N)
        for (u, v) in edges:
            if not uf.union(u - 1, v - 1):  # self.parents save the index of u and v!
                return [u, v]


import collections
# BFS
class Solution2(object):
    def findRedundantConnection(self, edges):
        """
        :type edges: List[List[int]]
        :rtype: List[int]
        """
        N = 0
        for (u, v) in edges:
            N = max(N, v)

        graph = collections.defaultdict(list)
        for (u, v) in edges:
            # res = self.bfs(u, v, graph)
            res = self.dfs(u, v, graph)
            if not res:
                return [u, v]
            graph[u].append(v)
            graph[v].append(u)

    def dfs(self, u, v, graph):
        if u not in graph or v not in graph:
            return True

        return self.dfs_helper(u, v, graph, [u])

    def dfs_helper(self, cur, target, graph, visited):
        if cur == target:
            return False

        for node in graph[cur]:
            if node not in visited:
                visited.append(node)
                res = self.dfs_helper(node, target, graph, visited)
                if not res:
                    return res
                visited.remove(node)
        return True

    def bfs(self, u, v, graph):
        if u not in graph or v not in graph:  # unseen nodes -- always can be added
            return True

        # bfs start from u, if go to v -- cannot add (u, v)
        visited = set()
        queue = collections.deque([u])
        while queue:
            cur = queue.popleft()
            if cur == v:
                return False
            visited.add(cur)
            for node in graph[cur]:
                if node not in visited:
                    queue.append(node)
        return True


if __name__ == "__main__":
    edges = [[1,2],[2,3],[1,5],[3,4],[1,4]]

    s = Solution()
    print(s.findRedundantConnection(edges))

    s = Solution2()
    print(s.findRedundantConnection(edges))
