class Solution(object):
    def combinationSum4(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        if not nums:
            return 0

        # dp[v]: number of combinations when target=v
        dp = [0] * (target + 1)
        dp[0] = 1
        for v in xrange(target + 1):
            for num in nums:
                remaining = v - num
                # if remaining >= 0, "num" can be added,
                # then, accumulate dp[remaining]
                if remaining >= 0:
                    dp[v] += dp[remaining]
        return dp[target]


class SolutionDFS(object):
    def combinationSum4(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        if not nums:
            return 0

        # cnt = self.dfs(nums, target)
        # return cnt

        # memo[v]: # combinations when target value is v
        memo = [-1] * (target + 1)
        memo[0] = 1
        self.dfs_memo(nums, target, memo)
        return memo[target]

    def dfs_memo(self, nums, remaining, memo):
        if memo[remaining] != -1:
            return memo[remaining]

        cnt = 0
        for i in xrange(len(nums)):  # as one number can be used multiple times
            if remaining - nums[i] >= 0:
                cnt += self.dfs_memo(nums, remaining - nums[i], memo)
        memo[remaining] = cnt
        return cnt

    def dfs(self, nums, remaining):
        if remaining == 0:
            return 1  # found one valid combination

        cnt = 0
        for i in xrange(len(nums)):
            if remaining - nums[i] >= 0:
                cnt += self.dfs(nums, remaining - nums[i])
        return cnt


class SolutionDFS2(object):
    def combinationSum4(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """

        if not nums:
            return 0

        # memo[v]: number of combinations for target=v
        memo = [-1] * (target+1)
        memo[0] = 1
        self.dfs_hepler([], nums, target, memo)
        print memo
        return memo[target]

    def dfs_hepler(self, res, nums, target, memo):
        remaining = target - sum(res)
        if memo[remaining] != -1:
            return memo[remaining]

        count = 0
        for i in xrange(len(nums)):
            if sum(res) + nums[i] <= target:
                # we can re-use the current element, so it's "cur", not "cur+1"
                count += self.dfs_hepler(res + [nums[i]], nums, target, memo)
        memo[remaining] = count
        return count


if __name__ == "__main__":
    # nums = [2, 3, 4, 5, 6]
    # target = 6

    nums = [1, 2, 3]
    target = 4
    print Solution().combinationSum4(nums, target)
    print SolutionDFS().combinationSum4(nums, target)
    print SolutionDFS2().combinationSum4(nums, target)
    exit()

    nums = [4, 2, 1]
    target = 32
    print Solution().combinationSum4(nums, target)
    print SolutionDFS().combinationSum4(nums, target)