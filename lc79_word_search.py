"""
DFS
Time: O(k * n^k)  k is word length, for each char in word, there are n choices for backtracking
Space: (k)
"""


class Solution(object):
    def exist(self, board, word):
        """
        :type board: List[List[str]]
        :type word: str
        :rtype: bool
        """

        if word == "":
            return True
        if not board:
            return False

        m, n = len(board), len(board[0])
        visited = [[False for _ in xrange(n)] for _ in xrange(m)]
        for i in xrange(m):
            for j in xrange(n):
                if board[i][j] == word[0]:
                    visited[i][j] = True
                    this_found = self.dfs_search(i, j, board[i][j], word, board, m, n, visited)
                    if this_found:
                        return True
                    visited[i][j] = False
        return False

    def dfs_search(self, i, j, cur_word, word, board, m, n, visited):
        if cur_word == word:
            return True

        directions = [(-1, 0), (1, 0), (0, 1), (0, -1)]
        for d in directions:
            x, y = i + d[0], j + d[1]
            if x < 0 or x >= m or y < 0 or y >= n or visited[x][y]:
                continue
            if word[len(cur_word)] == board[x][y]:
                visited[x][y] = True
                if self.dfs_search(x, y, cur_word + board[x][y], word, board, m, n, visited):
                    return True
                visited[x][y] = False

        return False


if __name__ == "__main__":
    board = ["ABCE","SFCS","ADEE"]
    word = "ABCCED"
    print Solution().exist(board, word)