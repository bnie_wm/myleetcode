fdata = open("_questions_by_company_raw.csv")
header = fdata.readline()

xyz = '\xef\xbf\xbd'

difficulty_to_lines = dict()
for key in ['Easy', 'Medium', 'Hard']:
    difficulty_to_lines[key] = []

line = fdata.readline().replace(xyz, '')
cols = line.strip().split(",")
prev_idx, prev_info = cols[0], cols[:-1]
companies = [cols[-1]]

for line in fdata:
    cols = line.strip().replace(xyz, '').split(",")
    print cols
    cur_idx = cols[0]
    if cur_idx == prev_idx:
        companies.append(cols[-1])
    else:
        companies_str = " ".join(companies)
        newline = ",".join(prev_info) + "," + companies_str
        difficulty_to_lines[prev_info[3]].append(newline)

        prev_idx, prev_info = cur_idx, cols[:-1]
        companies = [cols[-1]]

fdata.close()

fout = open("_questions_by_company.csv", "w")
fout.write("idx," + header)
line_idx = 1
for key in ['Easy', 'Medium', 'Hard']:
    idx = 0
    for line in difficulty_to_lines[key]:
        fout.write(str(line_idx) + "," + line + "\n")
        idx += 1
        line_idx += 1
    print key, idx
fout.close()

