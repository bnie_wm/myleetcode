class Solution(object):
    def productExceptSelf(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        if not nums:
            return []

        n = len(nums)

        product_from_left = [nums[0]]  # product from 0 to i
        for i in xrange(1, n):
            product_from_left.append(product_from_left[i - 1] * nums[i])

        product_from_right = [0] * n
        product_from_right[n - 1] = nums[n - 1]
        for i in xrange(n - 2, -1, -1):
            product_from_right[i] = product_from_right[i + 1] * nums[i]

        output = [1] * n
        output[0], output[n - 1] = product_from_right[1], product_from_left[n - 2]
        for i in xrange(1, n - 1):
            output[i] = product_from_left[i - 1] * product_from_right[i + 1]
        return output