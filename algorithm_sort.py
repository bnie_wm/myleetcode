# -*- coding: utf-8 -*-

import random
import copy
import math

random.seed(123)


class Solution(object):
    def __init__(self):
        n = 10
        self.x = range(n)
        random.shuffle(self.x)
        self.x = self.x + self.x[:n/2] + self.x[:n/3]
        print self.x
        print "".join(["-"] * 30)

    """
    Bubble Sort:
    * 已排序部分定义在右端
    * 在遍历未排序部分(左边)的过程执行交换(每次比较相邻的两个元素), 将最大元素交换到最右端 -- bubble up
    Time: O(n^2) average and worst
    Space: O(1)
    """
    def bubble_sort(self):
        x = copy.deepcopy(self.x)
        for i in xrange(len(x), 0, -1):
            for j in xrange(i - 1):
                if x[j] > x[j+1]:
                    x[j], x[j+1] = x[j+1], x[j]
        return x

    """
    Insertion Sort:
    * 已排序部分定义在左端(x[:i])
    * 将未排序部分元的第一个元素(x[i])插入到已排序部分合适的位置。
    Time: O(n^2) average and worst
    Space: O(1)
    """
    def insertion_sort(self):
        x = copy.deepcopy(self.x)
        for i in xrange(1, len(x)):
            j = i
            tgt = x[i]  # 每次在x[:i](已排好序的)中, 查找tgt=x[i]要插入的位置
            while j > 0 and x[j-1] > tgt:
                x[j] = x[j-1]
                j -= 1
            x[j] = tgt  # j为x[i]所在的位置
        return x

    """
    Selection Sort:
    * 已排序部分定义在左端，
    * 然后选择未排序部分的最小元素和未排序部分的第一个元素交换。
    * 适用于基本有序的序列
    Time: O(n^2) average and worst
    Space: O(1)
    """
    def selection_sort(self):
        x = copy.deepcopy(self.x)
        for i in xrange(len(x)):
            min_idx = i
            for j in xrange(i+1, len(x)):
                if x[j] < x[min_idx]:
                    min_idx = j
            if min_idx != i:
                x[i], x[min_idx] = x[min_idx], x[i]
        return x

    """
    Heap Sort
    * 基于Bubble Sort的优化
    * 维护一个最大堆 -- 若当前节点的index为i:
    *   它的parent_idx=floor((i-1)/2)
    *   它的left_child_idx=2*i+1, right_child_idx=2*i+2
    Time: O(nlogn) average and worst
    Space: O(1) -- in-place sort, no additional space
    """

    def heap_sort(self):
        x = copy.deepcopy(self.x)
        ### 第一步: heapify -- 建立最大堆
        self._heapify(x)

        ### 第二步: 从n-1到0
        for i in xrange(len(x) - 1, -1, -1):
            x[0], x[i] = x[i], x[0]  # 将栈顶元素x[0](即最大值)归位 (与最后一个为排序的元素x[i]交换)
            self._siftdown(x, 0, i)

        return x

    ### O(n): while从一半的位置开始, 而且每次siftdown都少一个元素
    def _heapify(self, x):
        # 找到第一个非叶子节点x[i]到根节点x[0] --> 一次siftdown, 对x[i:n]进行调整
        i = int(math.floor((len(x) - 1) / 2.))
        for cur in xrange(i, -1, -1):
            self._siftdown(x, cur, len(x))


    #### O(logn): 一次从cur到end, 进行向下交换
    def _siftdown(self, x, cur, end):
        while cur < end:
            i = cur  # 现在要交换的位置
            left, right = 2 * i + 1, 2 * i + 2

            # 若cur比它的孩子还小, 则与其孩子交换并将cur移到它的孩子,
            # 继续这样的交换, 直到cur比他的两个孩子都大或者到底了
            # 注意: 若只有一个孩子比cur小, 则与之交换; 若两个都比cur小, 则与较大的那个交换
            if left < end and x[i] < x[left]:
                i = left
            if right < end and x[i] < x[right]:
                i = right

            if i == cur:  # 不用交换了
                return
            x[i], x[cur] = x[cur], x[i]

            cur = i  # cur移到与其交换的孩子的位置上

    """
    Quick Sort:
    * 基于Selection Sort的优化
    * parition -- 挖坑填数 
    Time: O(nlogn) average and O(n^2) worst
    Space: O(logn) -- stack
    """
    def quick_sort(self):
        x = copy.deepcopy(self.x)
        self._quick_sort_helper(x, 0, len(x) - 1)
        return x

    def _quick_sort_helper(self, x, left, right):
        if left >= right:
            return

        pos = self._partition(x, left, right)
        self._quick_sort_helper(x, left, pos - 1)
        self._quick_sort_helper(x, pos + 1, right)

    #### 挖坑填数
    #### 初始: pivot=x[left].
    #### 先从右找到小于等于pivot的填左坑, 再从左找大于pivot的填右坑, 直到left==right
    #### 返回分界: 左边都小于等于它, 右边都大于它
    def _partition(self, x, left, right):
        pivot = x[left]  # 这是第一个坑
        while left < right:
            #### 因为第一个坑在左, 所以先从右找. 当右边出坑后, 再从左找

            # 从右边找到第一个小于等于pivot的数, 填到left上
            # 此时, right为下一个要填的坑
            while left < right and x[right] > pivot:  # 右边的数都大于pivot
                right -= 1
            x[left] = x[right]

            # 从左边找到第一个大于pivot的数, 填到right上
            # 此时, left成为下一个要填的坑 -- 继续while
            while left < right and x[left] <= pivot:  # 左边的数都小于等于pivot
                left += 1
            x[right] = x[left]

        # 当left==right时, 跳出while循环 -- 将pivot填到left/right上
        x[left] = pivot

        return left

    #### CC189的quick sort实现
    # def _quick_sort_helper(self, x, left, right):
    #     if left >= right:
    #         return
    #
    #     pos = self._partition(x, left, right)
    #     self._quick_sort_helper(x, left, pos - 1)  # x[left: pos]都小于等于pivot
    #     self._quick_sort_helper(x, pos, right)  # x[pos:right+1]都大于等于pivot
    #
    #
    # def _partition(self, x, left, right):
    #     pivot = x[left + (right - left) / 2]
    #     while left <= right:
    #         while x[left] < pivot:
    #             left += 1
    #         while x[right] > pivot:
    #             right -= 1
    #
    #         if left <= right:  # <=, as if left=right, still need modify the pointer, otherwise, infinite
    #             x[left], x[right] = x[right], x[left]
    #             left += 1
    #             right -= 1
    #
    #     return left


    """
    Merge Sort:
    Time: O(nlogn) average and worst
    Space: O(n) -- merge, copy helper_array -- inefficient
    """
    def merge_sort(self):
        x = copy.deepcopy(self.x)
        return self._merge_sort_helper(x)

    def _merge_sort_helper(self, x):
        if len(x) <= 1:  # 边界条件: 只有一个数
            return x

        # Divide: 将x平均分成两个list, 分别对left, right进行排序
        mid = len(x) / 2
        left = self._merge_sort_helper(x[:mid])
        right = self._merge_sort_helper(x[mid:])

        # Conqur: 将排好序的left, right合并成x
        return self._merge(left, right)

    def _merge(self, left, right):
        results = []  # 需要额外的O(n)空间存归并后的数组!
        i, j = 0, 0  # i, j分别为left, right的index
        while i < len(left) and j < len(right):
            if left[i] < right[j]:
                results.append(left[i])
                i += 1
            else:
                results.append(right[j])
                j += 1

        # append剩余的数组(其实either left or right)
        results += left[i:]
        results += right[j:]
        return results

    """
    Counting Sort:
    * 记录每个数出现的次数, 再以此输出即可 -- 对数据有要求: 像有固定范围的整数, e.g., 年龄
    * 一次遍历, 数个数; 再一次遍历输出
    Time: O(n+k) average and worst
    Space: O(k) (k是count数组的长度)
    """
    def counting_sort(self):
        x = copy.deepcopy(self.x)

        count = [0] * (max(x) + 1)
        for i in xrange(len(x)):
            count[x[i]] += 1

        results = []
        for i in xrange(len(count)):
            results += [i] * count[i]
        return results

    """
    Bucket Sort:
    * 桶排序相当于Counting Sort的推广 (counting sort: 每个数据是一个桶)
    * 将n个数按一定的函数关系放入k个桶, 桶内采用快速排序
    * 参考: LC683. K Empty Slots: 
    *       每个桶有k+1个数: buck_id = pos / (k + 1). 
    *       桶内不需要排序, 只需要O(1)时间维护最大值和最小值
    Time: O(n+k) average and O(n^2) worst
    Space: O(n)
    """

    """
    Radix Sort:
    * 按位比较排序 -- 先按个位排, 再按十位排, ...
    * e.g., 按照个位, 将数分别装入10个"桶"中. 桶是有序的, 按序输出
    Time: O(nk) average and worst
    Space: O(n+k) -- k为最大数的位数
    """


if __name__ == "__main__":
    s = Solution()

    # O(n^2)
    print s.bubble_sort()
    print s.insertion_sort()
    print s.selection_sort()

    # O(nlogn)
    print s.heap_sort()
    print s.quick_sort()
    exit()
    print s.merge_sort()

    # O(n)
    print s.counting_sort()


