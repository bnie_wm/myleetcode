# -*- coding: utf-8 -*-

class Solution(object):
    def backspaceCompare(self, S, T):
        """
        :type S: str
        :type T: str
        :rtype: bool
        """
        i, j = len(S) - 1, len(T) - 1
        while i >= 0 and j >= 0:
            while i >= 0 and S[i] == "#":
                i = self.delete_chars(i, S)
            while j >= 0 and T[j] == "#":  # e.g, #a#c
                j = self.delete_chars(j, T)
            if S[i] != T[j]:
                return False
            i -= 1
            j -= 1

        # 若S or T 还有剩余 -- 继续消除
        # e.g., "bbbextm" and "bbb#extm" -- False
        # e.g., "nzp#o#g" and "b#nzp#o#g" -- True
        while i >= 0 and S[i] == "#":
            i = self.delete_chars(i, S)
        while j >= 0 and T[j] == "#":
            j = self.delete_chars(j, T)

        return i == j  # 消除之后 len(S)==len(T)

    def delete_chars(self, i, s):
        cnt = 1  # as s[i] == "#"
        i -= 1
        while i >= 0 and cnt > 0:
            if s[i] == "#":
                cnt += 1
            else:
                cnt -= 1
            i -= 1
        return i

if __name__ == "__main__":
    print Solution().backspaceCompare(S="bbbextm", T="bbb#extm")
    print Solution().backspaceCompare(S="nzp#o#g", T="b#nzp#o#g")
    print Solution().backspaceCompare(S="#a#c", T="a##c")