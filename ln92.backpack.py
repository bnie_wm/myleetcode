class Solution:
    """
    @param m: An integer m denotes the size of a backpack
    @param A: Given n items with size A[i]
    @return: The maximum size
    """

    def backPack(self, m, A):
        # write your code here

        if not A:
            return 0

        # #### Method-1: DP: time: O(n^2) space: O(n^2)
        # n =  len(A)
        # # dp[i][k]=True if first i items can make size k
        # dp = [[False for _ in xrange(m+1)] for _ in xrange(n+1)]
        # # init 1st column: k=0 can always be satisfied
        # for i in xrange(n+1):
        #     dp[i][0] = True

        # for i in xrange(1, n+1):
        #     for k in xrange(1, m+1):
        #         # must make sure k-A[i-1]>=0!!!
        #         if (k >= A[i-1] and dp[i-1][k-A[i-1]]) or (dp[i-1][k]):
        #             dp[i][k] = True

        # # get the last index of dp[n][k]=1
        # for k in xrange(m, -1, -1):
        #     if dp[n][k] == True:
        #         break

        # return k

        # ##### Method-2: DP: time: O(n^2) space: O(2*n) -- dp and cur_dp
        # n =  len(A)
        # # dp[k]=1 if first i-1 items can make size k
        # dp = [False] * (m+1)  # to calculate i, as only need the info of item i-1 (the previous layer)
        # dp[0] = True
        # # use dp (for i-1) to update cur_dp (for i)
        # for i in xrange(n):
        #     cur_dp = [v for v in dp]
        #     for k in xrange(1, m+1):
        #         # must make sure k-A[i]>=0!!!
        #         if (k >= A[i] and dp[k-A[i]]) or (dp[k]):
        #             cur_dp[k] = True
        #     dp = [v for v in cur_dp]

        # # get the last index of dp[k]=1
        # for k in xrange(m, -1, -1):
        #     if dp[k] == True:
        #         break

        # return k

        ##### Method-3: DP: time: O(n^2) space: O(n) -- dp
        ##### 若不使用cur_dp区分上一次和这一次的dp，则应该***从后往前***更新dp
        ##### 使得dp[k] 都是通过上层的数(dp[k-A[i])算的(未更新过的)
        n = len(A)
        # dp[k]=1 if first i-1 items can make size k
        dp = [False] * (m + 1)  # to calculate i, as only need the info of item i-1 (the previous layer)
        dp[0] = True
        # use dp (for i-1) to update cur_dp (for i)
        for i in xrange(n):
            for k in xrange(m, -1, -1):  # update: end-->front
                # must make sure k-A[i]>=0!!!
                if (k >= A[i] and dp[k - A[i]]) or (dp[k]):
                    dp[k] = True

        # get the last index of dp[k]=1
        for k in xrange(m, -1, -1):
            if dp[k] == True:
                break

        return k



