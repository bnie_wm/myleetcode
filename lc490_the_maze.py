class Solution(object):
    def hasPath(self, maze, start, destination):
        """
        :type maze: List[List[int]]
        :type start: List[int]
        :type destination: List[int]
        :rtype: bool
        """

        if not maze:
            return False
        m, n = len(maze), len(maze[0])

        visited = [[False for _ in xrange(n)] for _ in xrange(m)]
        return self.dfs(start, destination, maze, m, n, visited)

    def dfs(self, cur, dst, maze, m, n, visited):
        if cur[0] == dst[0] and cur[1] == dst[1]:
            return True

        # only set visit=True for the stop position
        # not including positions along the path!
        visited[cur[0]][cur[1]] = True

        directions = [(0, -1), (0, 1), (-1, 0), (1, 0)]
        for (dx, dy) in directions:
            x, y = cur[0], cur[1]
            # keep rolling in this direction util reach wall
            while 0 <= x < m and 0 <= y < n and maze[x][y] == 0:
                x, y = x + dx, y + dy
            x, y = x - dx, y - dy  # roll back one step: the valid position before the wall
            if not visited[x][y]:
                reachable = self.dfs((x, y), dst, maze, m, n, visited)
                if reachable:
                    return True
        return False


if __name__ == "__main__":
    maze = [[0, 0, 1, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 1, 0], [1, 1, 0, 1, 1], [0, 0, 0, 0, 0]]
    start = [0, 4]
    destination = [4, 4]
    print Solution().hasPath(maze, start, destination)