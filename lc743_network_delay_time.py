import collections
import heapq

#### DFS -- TLE (as revist the same nodes a lot of times to update for shorter path)
# class Solution(object):
#     def networkDelayTime(self, times, N, K):
#         """
#         :type times: List[List[int]]
#         :type N: int
#         :type K: int
#         :rtype: int
#         """
#         graph = collections.defaultdict(list)
#         for (u, v, w) in times:
#             graph[u].append((v, w))

#         # DFS -- get the shortest path to each node
#         visited = {i: float("inf") for i in range(1, N+1)}
#         self.dfs(K, 0, visited, graph)  # start from source node

#         longest_path = 0
#         for nid in visited:
#             if visited[nid] == float("inf"):  # some nodes cannot be reached from source
#                 return -1
#             longest_path = max(longest_path, visited[nid])
#         return longest_path


#     def dfs(self, cur_nid, cur_dist, visited, graph):
#         # visit current node
#         visited[cur_nid] = cur_dist

#         # visit neighbours
#         for nei_nid, nei_dist in graph[cur_nid]:
#             # if visited[nei_nid] != float("inf"):  # normal DFS -- skip visited nodes
#             # but, this question -- we need to update the path length
#             # so, only longest path
#             if cur_dist + nei_dist < visited[nei_nid]:
#                 self.dfs(nei_nid, cur_dist + nei_dist, visited, graph)

#### BFS + Dijkstra -- O(nlogn)
class Solution(object):
    def networkDelayTime(self, times, N, K):
        """
        :type times: List[List[int]]
        :type N: int
        :type K: int
        :rtype: int
        """

        # build graph
        graph = collections.defaultdict(list)
        for (u, v, w) in times:
            graph[u].append((v, w))

        # BFS + Dijkstra (shortest path)

        # normal BFS: use collections.deque
        # Dijkstra: use heapq
        queue = [(0, K)]  # (dist, nid)
        visited = dict()
        while queue:
            cur_dist, cur_nid = heapq.heappop(queue)
            # must double check here: avoid revisit
            if cur_nid in visited:
                continue
            visited[cur_nid] = cur_dist
            # update neighbour nodes' distances
            for nei_nid, nei_dist in graph[cur_nid]:
                if nei_nid in visited:
                    continue
                # using heapq (min heap)
                # so there is no need to replace the longer dist with shorter dist
                # as shorter dist of the same unvisited node will be visited first
                heapq.heappush(queue, (cur_dist + nei_dist, nei_nid))

        if len(visited) == N:
            return max(visited.values())  # reture the longest path
        else:
            return -1  # cannot reach all nodes

