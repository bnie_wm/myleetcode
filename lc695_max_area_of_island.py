class Solution(object):
    def maxAreaOfIsland(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        if not grid:
            return 0

        m, n = len(grid), len(grid[0])

        max_area = 0
        visited = [[False for _ in xrange(n)] for _ in xrange(m)]
        for i in xrange(m):
            for j in xrange(n):
                if grid[i][j] == 1 and not visited[i][j]:
                    this_area = self.dfs(i, j, visited, grid, m, n)
                    max_area = max(max_area, this_area)
        return max_area

    def dfs(self, i, j, visited, grid, m, n):
        visited[i][j] = True

        directions = [(0, -1), (0, 1), (1, 0), (-1, 0)]
        area = 1  # visited current grid[i][j] == 1, so area is at least 1
        for dx, dy in directions:
            x, y = i + dx, j + dy
            if 0 <= x < m and 0 <= y < n and grid[x][y] == 1 and not visited[x][y]:
                area += self.dfs(x, y, visited, grid, m, n)
        return area


if __name__ == "__main__":
    grid = [[1,1,0,0,0],[1,1,0,0,0],[0,0,0,1,1],[0,0,0,1,1]]
    for i in xrange(len(grid)):
        print grid[i]

    print Solution().maxAreaOfIsland(grid)