class Solution(object):
    def isInterleave(self, s1, s2, s3):
        """
        :type s1: str
        :type s2: str
        :type s3: str
        :rtype: bool
        """
        l1, l2, l3 = len(s1), len(s2), len(s3)
        if l1 + l2 != l3:
            return False

        # dp[i][j]: s3[0,i+j] is a interleaving of s1[0,i] and s2[0,j]
        dp = [[False for _ in xrange(l2 + 1)] for _ in xrange(l1 + 1)]
        dp[0][0] = True
        # init 1st column
        for i in xrange(1, l1 + 1):
            dp[i][0] = True if (dp[i - 1][0] and s1[i - 1] == s3[i - 1]) else False
        # init 1st row:
        for j in xrange(1, l2 + 1):
            dp[0][j] = True if (dp[0][j - 1] and s2[j - 1] == s3[j - 1]) else False

        for i in xrange(1, l1 + 1):
            for j in xrange(1, l2 + 1):
                if (dp[i - 1][j] and s1[i - 1] == s3[i + j - 1]) or (dp[i][j - 1] and s2[j - 1] == s3[i + j - 1]):
                    dp[i][j] = True

        return dp[l1][l2]


