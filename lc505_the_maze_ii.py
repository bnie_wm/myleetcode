# -*- coding: utf-8 -*-
# Complexity Analysis
# Time complexity : O(m∗n∗max(m,n)). Complete traversal of maze will be done in the worst case.
#                   For every current node chosen, we can travel upto a maximum depth of max(m,n) in any direction.
# Space complexity : O(mn). distancedistance array of size m*nm∗n is used.
class SolutionDFS(object):
    def shortestDistance(self, maze, start, destination):
        """
        :type maze: List[List[int]]
        :type start: List[int]
        :type destination: List[int]
        :rtype: int
        """
        if not maze:
            return -1
        m, n = len(maze), len(maze[0])

        visited = [[float("inf") for _ in xrange(n)] for _ in xrange(m)]
        visited[start[0]][start[1]] = 0
        self.dfs(start, destination, maze, m, n, visited)
        shortest_length = visited[destination[0]][destination[1]]
        return shortest_length if shortest_length != float("inf") else -1

    def dfs(self, cur, dst, maze, m, n, visited):
        prev_dist = visited[cur[0]][cur[1]]

        directions = [(0, -1), (0, 1), (-1, 0), (1, 0)]
        for (dx, dy) in directions:
            x, y = cur[0], cur[1]

            # keep rolling in this direction util reach wall
            while 0 <= x < m and 0 <= y < n and maze[x][y] == 0:
                x, y = x + dx, y + dy
            x, y = x - dx, y - dy  # roll back one step: the valid position before the wall

            cur_dist = abs(x - cur[0]) + abs(y - cur[1])
            if prev_dist + cur_dist < visited[x][y]:
                visited[x][y] = prev_dist + cur_dist
                self.dfs((x, y), dst, maze, m, n, visited)

import heapq


#### Dijkstra: O(mn * log(mn)) -- mn为position的个数 (即图的节点个数)

class Solution(object):
    def shortestDistance(self, maze, start, destination):
        """
        :type maze: List[List[int]]
        :type start: List[int]
        :type destination: List[int]
        :rtype: int
        """
        m, n = len(maze), len(maze[0])
        start = (start[0], start[1])  # only tuple is hashable, list is NOT
        destination = (destination[0], destination[1])

        visited = set()  # record visited positions
        queue = [(0, start)]  # use heapq to maintain a min heap
        while queue:
            cur_dist, cur_pos = heapq.heappop(queue)  # O(logn)
            if cur_pos in visited:  # !!!! remeber to skip visited nodes, as one node might be added to queue multiple times
                continue
            if cur_pos == destination:
                return cur_dist
            visited.add(cur_pos)

            directions = [(-1, 0), (1, 0), (0, -1), (0, 1)]
            for (dx, dy) in directions:
                # keep rolling until reach the wall
                x, y = cur_pos
                while 0 <= x < m and 0 <= y < n and maze[x][y] == 0:
                    x, y = x + dx, y + dy
                x, y = x - dx, y - dy  # "while" stops util reach a wall, so roll back one step, to the last valid position
                new_distance = cur_dist + abs(x - cur_pos[0]) + abs(y - cur_pos[1])
                heapq.heappush(queue, (new_distance, (x, y)))

        return -1


if __name__ == "__main__":
    maze = [[0, 0, 1, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 1, 0], [1, 1, 0, 1, 1], [0, 0, 0, 0, 0]]
    start = [0, 4]
    destination = [4, 4]
    print SolutionDFS().shortestDistance(maze, start, destination)
    print Solution().shortestDistance(maze, start, destination)  # Dijkstra

