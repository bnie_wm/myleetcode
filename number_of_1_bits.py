class Solution(object):
    def hammingWeight(self, n):
        """
        :type n: int
        :rtype: int
        """

        # Time: O(logn)=O(32)
        # Space: O(1)

        cnt = 0
        for _ in xrange(32):
            if n&1 == 1:  # get right most bit, check if it is one
                cnt += 1
            n >>= 1  # n: shift right 1 bit
        return cnt


if __name__ == "__main__":
    print Solution().hammingWeight(11)