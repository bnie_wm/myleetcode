# -*- coding: utf-8 -*-

import bisect


class Trie(object):
    def __init__(self, topk):
        self.topk = topk
        self.children = dict()
        # topk most frequent sentences with this prefix -- maintained with bisect
        self.sentences = []
        self.occur_cnt = 0  # =0, 不是sentence; >0, 是sentence, 且其出现的次数

    def add_sentence(self, sentence, freq, init=False):
        # 注意: Trie的root为空, 每个节点最多有26+1个孩子
        cur = self
        for i, char in enumerate(sentence):
            if char not in cur.children:
                cur.children[char] = Trie(cur.topk)
            cur = cur.children[char]

            # 先加, 如果发现len>topk, 再删 -- 顺序不能颠倒!
            # 如果freq相同, 应该删字母序大的那个 -- 所以按(-freq, 字母序)排列, 先删最后一个
            # 每个节点只存sentence[i+1:] -- 节省空间. 因为prefix已经有记录了
            if not init and (-(freq - 1), sentence[i + 1:]) in cur.sentences:  # O(n)
                cur.sentences.remove((-(freq - 1), sentence[i + 1:]))
            bisect.insort(cur.sentences, (-freq, sentence[i + 1:]))
            if len(cur.sentences) > cur.topk:
                cur.sentences = cur.sentences[:-1]

        cur.occur_cnt = freq  # 标记这是一个sentence的结尾, 且记录这个sentence出现的次数


class AutocompleteSystem(object):

    def __init__(self, sentences, times):
        """
        :type sentences: List[str]
        :type times: List[int]
        """
        self.topk = 3
        self.trie = Trie(self.topk)
        # initialize trie
        for sen, freq in zip(sentences, times):
            self.trie.add_sentence(sen, freq, init=True)

            # 记录input history, reset when got "#"
        self.cur = self.trie
        self.prefix = ""
        # 判断self.prefix在不在Trie中. 如果prefix都不在,则后边的输入都不用判断了, 只能等到"#"重置以后再次判断
        self.in_trie = True

    def input(self, c):
        """
        :type c: str
        :rtype: List[str]
        """
        if c == "#":
            # update Trie
            # if prev_cnt>0, means it is already a sentence in Trie
            # 只有当self.in_trie=True时才判断, 不然self.cur的位置在第一次发现input不在Trie的地方, 那个prev_cnt不作数
            prev_cnt = self.cur.occur_cnt if self.in_trie else 0
            self.trie.add_sentence(self.prefix, prev_cnt + 1)

            # reset
            self.cur = self.trie
            self.prefix = ""
            self.in_trie = True
            return []

        self.prefix += c
        if self.in_trie and c in self.cur.children:
            self.cur = self.cur.children[c]
            return [self.prefix + sen[1] for sen in self.cur.sentences][:self.topk]
        else:
            self.in_trie = False
            return []


if __name__ == "__main__":
    sentences = ["i love you", "island", "iroman", "i love leetcode", "i a"]
    times = [5, 3, 2, 2, 2]
    sys = AutocompleteSystem(sentences, times)
    inputs = [["i"], [" "], ["a"], ["#"]]
    for ipt in inputs:
        print sys.input(ipt[0])