from collections import defaultdict

class Solution1(object):
    '''
    solution1: two pointers. Time O(n^3), Space O(1)
    solution2: hashtable. Time O(n^2), Spcae O(n^2)
    '''


    def fourSum(self, numbers, target):
        '''
        Time: O(n^3) ---> Time Limit Exceeded!!!
        Space: O(1) --> Two Pointers
        '''

        numbers, num_len = sorted(numbers), len(numbers)
        res = set()
        for i in range(num_len):
            for j in range(i+1, num_len):
                m, n = j+1, num_len-1
                while m < n:
                    this_sum = numbers[i] + numbers[j] + numbers[m] + numbers[n]
                    if this_sum > target:
                        n -= 1
                    elif this_sum < target:
                        m += 1
                    else:
                        res.add((numbers[i], numbers[j], numbers[m], numbers[n]))
                        if m+1<n and numbers[m+1] == numbers[m]:
                            res.add((numbers[i], numbers[j], numbers[m+1], numbers[n]))
                        if m<n-1 and numbers[n-1] == numbers[n]:
                            res.add((numbers[i], numbers[j], numbers[m], numbers[n-1]))
                        m += 1
                        n -= 1
        return list(res)

class Solution2(object):
    '''
    solution2: hashtable. Time O(n^2), Space O(n^2)
    '''

    def fourSum(self, numbers, target):
        '''
        Time: O(n^2 * p) -- any time greater than O(n^2) will to TLE
        Space: O(n^2 * p) --> HashTable
        *** p --> (1) extra boolean (saved) and (2) extra set (saved_res) to avoid duplicates
            --> so that the 3rd iteration reduced from n to p!!! ***
        '''

        numbers = sorted(numbers)
        two_sum = defaultdict(list)
        for i in xrange(0, len(numbers)):
            for j in xrange(i+1, len(numbers)):
                saved = False
                for (m, n) in two_sum[numbers[i]+numbers[j]]:
                    if numbers[m] == numbers[i]:
                        saved = True
                        break
                if not saved:
                    two_sum[numbers[i] + numbers[j]].append((i, j))

        res, saved_res = [], set()
        for m in xrange(2, len(numbers)):
            for n in xrange(m+1, len(numbers)):
                if target - numbers[m] - numbers[n] in two_sum:
                    for (idx1, idx2) in two_sum[target - numbers[m] - numbers[n]]:
                        if idx2 < m:
                            this_quad = (numbers[idx1], numbers[idx2], numbers[m], numbers[n])
                            if " ".join(str(this_quad)) not in saved_res:
                                res.append(this_quad)
                                saved_res.add(" ".join(str(this_quad)))
        return res


if __name__ == "__main__":
    # numbers, target = [1, 0, -1, 0, -2, 2], 0
    numbers, target = [1,0,-1,-1,-1,-1,0,1,1,1,2], 2
    print Solution1().fourSum(numbers, target)
    print Solution2().fourSum(numbers, target)