# -*- coding: utf-8 -*-
import heapq

class Solution(object):
    def mincostToHireWorkers(self, quality, wage, K):
        """
        :type quality: List[int]
        :type wage: List[int]
        :type K: int
        :rtype: float
        """
        # Greedy:
        #### 思路:
        #### 令ratio=wage[i]/quality[i]. 并不是ratio越小越好, 也不是ratio越大越好!
        #### 因为有条件1, 需要将同样的ratio应用到所有人 -- 即公司会对所有K个人match最差的ratio!!!
        #### best_ratio: quality好但wage要求低 -- 但是公司会按照worst_ratio match给他. 所以他的pay还是会很高.
        #### worst_ratio: quality低但是wage要求高 -- 公司要按照他的高要求match给所有人. 所以整体大家都overpay了.
        #### e.g., quality = [10,20,5], wage = [180,50,30], K = 2  (ratio=[18, 2.5, 6])
        #### pick(0,1)= max(18, 2.5)*(10+20)=540, pick(0, 2)=max(18,6)*(10+5)=270, pick(1,2)=max(2.5, 6)*(20+5)=150
        #### 所以,贪心的策略应该是平衡了quality和wage的ratio -- 即ratio[i] * tot_quality最小的那个!! -- 这里为pick(1,2), 以ratio=6为标准

        # 为了满足条件-2: match最低工资要求
        # 按wage[i]/quality[i], 从小到大排. 这样依次选择worker, 使用后一个人的ratio总能match前面所有人的最低工资要求!
        # e.g., i < j --> workers[i].ratio < workers[j].ratio
        # --> workers[i].ratio * workers[i].quality < workers[j].ratio * workers[i].quality
        workers = [(w * 1.0 / q, q) for w, q in zip(wage, quality)]
        workers.sort()

        # 为了满足条件-1: 按quality比例发工资
        # 当ratio确定时, tot_paid = ratio * tot_quality. 所以, quality越小越好.
        heap = []  # max-heap: 选中的K个人的quality
        tot_quality, tot_paid = 0, float("inf")
        for ratio, q in workers:
            tot_quality += q
            heapq.heappush(heap, -q)
            if len(heap) > K:  # 永远先排除quality最大的人, 这样才能付的总钱数最少.
                top_q = -heapq.heappop(heap)
                tot_quality -= top_q
            if len(heap) == K:
                tot_paid = min(tot_paid, ratio * tot_quality)
        return tot_paid


if __name__ == "__main__":
    quality = [10, 20, 5]
    wage = [180, 50, 30]
    K = 2
    print Solution().mincostToHireWorkers(quality, wage, K)
