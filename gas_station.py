# Time:  O(n)
# Space: O(1)
#
# There are N gas stations along a circular route, where the amount of gas at station i is gas[i].
#
# You have a car with an unlimited gas tank and it costs cost[i] of gas to travel from station i to its next station (i+1).
# You begin the journey with an empty tank at one of the gas stations.
#
# Return the starting gas station's index if you can travel around the circuit once, otherwise return -1.
#
# Note:
# The solution is guaranteed to be unique.
#

class Solution:
    def canCompleteCircuit(self, gas, cost):
        """
        :type gas: List[int]
        :type cost: List[int]
        :rtype: int
        """
        start_idx, cur_gas, total_gas = 0, 0, 0
        for i in xrange(len(gas)):
            cur_gas += gas[i] - cost[i]
            total_gas += gas[i] - cost[i]
            if cur_gas < 0:  # cannot reach the next stop, then, this stop cannot be the start index
                start_idx = i + 1  # since retrun non-zero start index
                cur_gas = 0  # since start index changes, reset cur_gas

        if total_gas >= 0:
            return start_idx
        else:
            return -1

        # Note that, we do not need to consider if we can drive from Stop n to Stop 1
        # since total_gsa >= 0 indicates that sum(gas) >= sum(cost), then we can definitely cycle!


if __name__ == "__main__":
    print Solution().canCompleteCircuit([1, 2, 3], [3, 2, 1])
    print Solution().canCompleteCircuit([1, 2, 3], [2, 2, 2])
    print Solution().canCompleteCircuit([1, 2, 3], [1, 2, 3])
    print Solution().canCompleteCircuit([1, 2, 3], [1, 2, 4])