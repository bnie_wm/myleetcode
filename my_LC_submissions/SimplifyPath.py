class Solution(object):
    def simplifyPath(self, path):
        """
        :type path: str
        :rtype: str
        """
        if path == "" or path == "/":
            return path
        
        if path[-1] == "/":
            path = path[:-1]
            
        # "/home/a/c/d/"
        # split: ['', 'home', 'a', 'c', 'd', '']
        
        # corner cases: "///"
        dirs = path.split("/")
        
        stack = []
        for dir in dirs:
            if dir == "" or dir == ".":
                continue
            elif dir == ".." and stack:
                stack.pop()
            else:
                stack.append(dir)
                
        if stack and stack[0] == "..":  # cannot be "/../xxxx"
            stack = stack[1:]
            
        return "/" + "/".join(stack)