# Definition for a undirected graph node
# class UndirectedGraphNode(object):
#     def __init__(self, x):
#         self.label = x
#         self.neighbors = []
"""
Time: O(n)
Space: O(n)
"""
class Solution(object):
    def cloneGraph(self, node):
        """
        :type node: UndirectedGraphNode
        :rtype: UndirectedGraphNode
        """
        if node is None:
            return None
            
        graph_entry = UndirectedGraphNode(node.label)
        cloned = {node.label:graph_entry}   # visited && maintain old node --> new node
        q = collections.deque([node])  # BFS queue for old graph
        
        while q:
            node = q.popleft()
            new_node = cloned[node.label]
            for nei in node.neighbors:
                # get or create new_nei
                visited = False
                if nei.label in cloned:
                    new_nei = cloned[nei.label]
                    if new_nei.neighbors is not None:  # it has been visited before
                        visited = True
                else:
                    new_nei = UndirectedGraphNode(nei.label)
                    
                if not visited:
                    q.append(nei)  # append to old graph's BFS queue
                    
                cloned[nei.label] = new_nei  # add old --> new relation
                new_node.neighbors.append(new_nei)
        return graph_entry
                
                
            
            
            