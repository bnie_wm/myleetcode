/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */
public class Solution {
    public ListNode partition(ListNode head, int x) {
        ListNode small_head = new ListNode(0);  // list head for nodes smaller than x
        ListNode big_head = new ListNode(0); // list head for nodes no smaller than x
        ListNode small = small_head, big = big_head, cur = head;
        while(cur != null) {
            if(cur.val < x) {
                small.next = cur;
                small = small.next;
                cur = cur.next; // cannot move out of the if-else!!!
                small.next = null; // in case of cycle!!!
            }else {
                big.next = cur;
                big = big.next;
                cur = cur.next;
                big.next = null;
            }
            
        }
        small.next = big_head.next; // connect two lists
        return small_head.next;
    }
}