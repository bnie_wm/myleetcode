/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; next = null; }
 * }
 */
/**
 * Definition for binary tree
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    /**
     * careercup 4.3 similar
     * O(N)
     * */
    public TreeNode sortedListToBST(ListNode head) {
        // IMPORTANT: Please reset any member data you declared, as
        // the same Solution instance will be reused for each test case.
        if(head == null) return null;
        ListNode cur = head;
        return buildBST(cur, 0, listLength(head)-1);
    }
    
    private TreeNode buildBST(ListNode head, int start, int end){
        if(start > end) return null;
        int mid = start + (end-start)/2;
        TreeNode left = buildBST(head, start, mid-1);
        TreeNode root = new TreeNode(head.val);
        root.left = left;
        if(head.next != null){
            head.val = head.next.val;
            head.next = head.next.next;
        }
        root.right = buildBST(head, mid+1, end);
        return root;
    }
    
    private int listLength(ListNode head){
        if(head == null) return 0;
        int len = 1;
        ListNode cur = head;
        while(cur.next != null){
            len++;
            cur = cur.next;
        }
        return len;
    }
}