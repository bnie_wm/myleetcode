/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */
public class Solution {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        // Note: The Solution object is instantiated only once and is reused by each test case.
		if (l1 == null || l2 == null) return null;
		ListNode head = new ListNode(-1);
		ListNode cur = head;
		boolean carry = false;
		while (true) {
			int curval = l1.val + l2.val;
			if (carry == true)
				curval += 1;
			if (curval >= 10) {
				curval = curval % 10;
				carry = true;
			} else
				carry = false;
			cur.next = new ListNode(curval);
			cur = cur.next;
			if (l1.next == null && l2.next == null) break;
			// make two linkedlists the same length
			l1 = (l1.next == null) ? new ListNode(0) : l1.next;
			l2 = (l2.next == null) ? new ListNode(0) : l2.next;
		}
		if (carry == true) {
			cur.next = new ListNode(1);
		}
		return head.next;
	}

}