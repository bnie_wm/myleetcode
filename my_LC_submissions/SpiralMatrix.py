# Time: O(m*n)
# Space: O(1)

class Solution(object):
    def spiralOrder(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: List[int]
        """
        if not matrix:
            return []
        
        left, right, top, bottom = 0, len(matrix[0])-1, 0, len(matrix)-1
        
        results = []
        while left <= right and top <= bottom:
            for i in xrange(left, right+1): # top row
                results.append(matrix[top][i])
            
            for i in xrange(top+1, bottom+1): # right col
                results.append(matrix[i][right])
            
            if top != bottom:
                for i in reversed(xrange(left, right)):  # bottom row
                    results.append(matrix[bottom][i])
            
            if left != right:   
                for i in reversed(xrange(top+1, bottom)):  # left col
                    results.append(matrix[i][left])
                
            left, right, top, bottom = left+1, right-1, top+1, bottom-1
            
        return results