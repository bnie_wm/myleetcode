"""
Binary Search
Time: O(logn)
Space: O(1)
"""
class Solution(object):
    def search(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: bool
        """
        
        if not nums:
            return -1
        
        res = self.searchHelper(nums, 0, len(nums)-1, target)
        return True if res != -1 else False

    def searchHelper(self, nums, left, right, target):
        mid = (left + right) / 2
        if nums[mid] == target:
            return mid
        
        while left <= right:
            if nums[left] < nums[mid]:  # left is normally ordered
                if nums[left] <= target < nums[mid]:
                    return self.searchHelper(nums, left, mid-1, target)
                else:
                    return self.searchHelper(nums, mid+1, right, target)
            elif nums[left] > nums[mid]: # right is normally ordered
                if nums[mid] < target <= nums[right]:
                    return self.searchHelper(nums, mid+1, right, target)
                else:
                    return self.searchHelper(nums, left, mid-1, target)
            elif nums[left] == nums[mid]:  # left half is all repeats
                if nums[mid] != nums[right]: # if right is diff. search it
                    return self.searchHelper(nums, mid+1, right, target)
                else:   # else, we have to search both halves
                    left_res = self.searchHelper(nums, left, mid-1, target)
                    if left_res == -1:
                        return self.searchHelper(nums, mid+1, right, target)
                    else:
                        return left_res
        return -1
                
                
                
            