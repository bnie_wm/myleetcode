class Solution(object):
    def wordsTyping(self, sentence, rows, cols):
        """
        :type sentence: List[str]
        :type rows: int
        :type cols: int
        :rtype: int
        """
        n = len(sentence)
        # dp: if current row starts with word i, then
        # (1) current row contains dp[i][0] number of full sentences (indicated by number of last words)
        # (2) next row starts with dp[i][1]
        dp =[(0, 0) for _ in xrange(n)]
        for wid in xrange(n):
            i = wid
            # fill one row, get repeats[wid] and nextWord[wid]
            nextCol, occur = 0, 0
            while nextCol + len(sentence[i]) <= cols:  # while cur word can fit in the row
                nextCol += len(sentence[i]) + 1  # pad one space
                i += 1  # next word
                if i == n:  # if meet the last word in sentence
                    i = 0  # reset word idx
                    occur += 1  # increment occur
            dp[wid] = (occur, i)
        
        res, wid = 0, 0
        row = 0
        while row < rows:
            res += dp[wid][0]
            wid = dp[wid][1]  # next word
            row += 1
            
        return res