# Time:  O(k * n^k)
# Space: O(k)
class Solution(object):
    def combinationSum(self, candidates, target):
        """
        :type candidates: List[int]
        :type target: int
        :rtype: List[List[int]]
        """
        candidates.sort()
        
        results = []
        self.combinationSumHelper(results, [], candidates, target, 0)
        return results
        
    def combinationSumHelper(self, results, res, candidates, target, start):
        if target == 0:
            results.append(res)
            return
        
        for i in xrange(start, len(candidates)):  # since allow duplicates, i starts from start, instead of start+1
            if target - candidates[i] >= 0:
                self.combinationSumHelper(results, res + [candidates[i]], candidates, target-candidates[i], i)
        
 