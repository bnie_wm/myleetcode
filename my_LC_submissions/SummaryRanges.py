class Solution(object):
    def summaryRanges(self, nums):
        """
        :type nums: List[int]
        :rtype: List[str]
        """
        if len(nums) < 1:
            return []
        elif len(nums) == 1:
            return [str(nums[0])]
            
        res = []
        start = nums[0]
        for i in xrange(1, len(nums)):
            if nums[i] > nums[i-1] + 1:
                # ranges.append((start, nums[i-1]))
                if nums[i-1] == start:
                    res.append(str(start))
                else:
                    res.append(str(start) + "->" + str(nums[i-1]))
                start = nums[i]
                
        # add the last range
        if nums[-1] == start:
            res.append(str(start))
        else:
            res.append(str(start) + "->" + str(nums[-1]))
        return res
            
        
                