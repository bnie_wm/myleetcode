# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

"""
Time: n*log(n) = O(nlogn)
Space: O(h)
"""
class Solution(object):
    def largestBSTSubtree(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        if not root:
            return 0
            
        q = [root]
        largest = -1
        while q:
            node = q.pop()
            tree_size = self.isValidBST(node, float("-inf"), float("inf"))
            largest = max(tree_size, largest)
            if node.left:
                q.append(node.left)
            if node.right:
                q.append(node.right)
        return largest

    
    # return tree size, if not BST, return -1
    def isValidBST(self, root, low, high):
        if root is None:
            return 0
        
        if low < root.val < high:
            left_size = self.isValidBST(root.left, low, root.val)
            right_size = self.isValidBST(root.right, root.val, high)
            if left_size == -1 or right_size == -1:
                return -1
            else:
                return left_size + right_size + 1
        else:
            return -1

        