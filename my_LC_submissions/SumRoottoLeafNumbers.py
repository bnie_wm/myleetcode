# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def sumNumbers(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        results = []
        self.sumNumbersHelper(results, 0, root)
        return sum(results)
        
    def sumNumbersHelper(self, results, res, root):
        if root is None:
            return results
        
        if root.left is None and root.right is None:
            results.append((res * 10) + root.val)
            return results
            
        self.sumNumbersHelper(results, res*10+root.val, root.left)
        self.sumNumbersHelper(results, res*10+root.val, root.right)
        
        
            