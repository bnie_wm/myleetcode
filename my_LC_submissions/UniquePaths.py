"""
DP: f(m,n) = f(m-1,n) + f(m, n-1)
Time: O(m*n)
Space: O(m+n)
"""

class Solution(object):
    def uniquePaths(self, m, n):
        """
        :type m: int
        :type n: int
        :rtype: int
        """
        num = [[1]*n]*m
        for i in range(1, m):
            for j in range(1, n):
                num[i][j] = num[i-1][j] + num[i][j-1]
        return num[m-1][n-1]
        