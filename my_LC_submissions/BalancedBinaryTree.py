# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

"""
Time: O(n)
Space: O(h)
"""
class Solution(object):
    def isBalanced(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        if not root:
            return True
        return self.isBalancedHelper(root) != -1
        
    def isBalancedHelper(self, root):
        if not root:
            return 0
        leftH = self.isBalancedHelper(root.left)
        if leftH == -1:
            return -1
        rightH = self.isBalancedHelper(root.right)
        if rightH == -1:
            return -1
        return max(leftH, rightH) + 1 if abs(leftH - rightH) <= 1 else -1
        
        