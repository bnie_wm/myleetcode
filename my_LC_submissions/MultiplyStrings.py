class Solution(object):
    def multiply(self, num1, num2):
        """
        :type num1: str
        :type num2: str
        :rtype: str
        """
        if num1 == '0' or num2 == '0':
            return '0'
        
        num1, num2 = num1[::-1], num2[::-1]
        res = [0] * (len(num1) + len(num2))  # save results in reversed order
        for i in xrange(len(num1)):
            for j in xrange(len(num2)):
                p = int(num1[i]) * int(num2[j])
                res[i+j] += p%10
                # handle carry bit in res[i+j]
                carry = 0
                if res[i+j] > 9:
                    carry = res[i+j]/10
                    res[i+j] = res[i+j]%10
                res[i+j+1] += p/10 + carry
        
        # clear redundant 0s, i.e. 100*1 = '0100'
        if res[-1] == 0:
            res = res[:-1]
        
        return "".join([str(x) for x in res[::-1]])
                
                
                