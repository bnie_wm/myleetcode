# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def deleteDuplicates(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        
        # Time: O(n)
        # Space: O(1)
        
        cur = head
        while cur != None and cur.next != None:
            next = cur.next
            if cur.val == next.val:
                cur.next = next.next
            else:
                cur = next
        return head