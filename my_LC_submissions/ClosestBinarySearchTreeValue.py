# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

"""
BFS
Time: O(n)
Space: O(1)  -- utilize BST feature!
"""
class Solution(object):
    def closestValue(self, root, target):
        """
        :type root: TreeNode
        :type target: float
        :rtype: int
        """
        diff, closest = float("inf"), float("inf")
        while root:
            this_diff = abs(root.val - target)
            if this_diff < diff:
                diff = this_diff
                closest = root.val
            if root.val == target:
                break
            elif root.val < target:
                root = root.right
            else:
                root = root.left
        return closest
        
            
        
        