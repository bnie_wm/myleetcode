class Solution(object):
    def wiggleSort(self, nums):
        """
        :type nums: List[int]
        :rtype: void Do not return anything, modify nums in-place instead.
        """
        if len(nums) <= 1:
            return
        
        for i in xrange(1, len(nums)):
            if (i % 2 == 0 and nums[i-1] >= nums[i]) \
                or (i % 2 == 1 and nums[i-1] <= nums[i]):  # valid sequence
                    continue
            else:
                # if invalid
                # if i is odd: nums[i-2] >= nums[i-1] > nums[i] --> nums[i-2] >= nums[i] < nums[i-1]
                # if i is even: nums[i-2] <= nums[i-1] < nums[i] --> nums[i-2] <= nums[i] > nums[i-1]
                nums[i-1], nums[i] = nums[i], nums[i-1]