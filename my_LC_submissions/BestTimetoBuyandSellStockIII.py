public class Solution {
    public int maxProfit(int[] prices) {
        if(prices.length < 2)
            return 0;
        
        //step1: stores max profit in [0,1,...i] subarray in pricess
        int[] curMaxProfits = new int[prices.length];
        int minPrice = prices[0];
        int maxProfit = 0;
        curMaxProfits[0] = maxProfit;
        for(int i=1; i<prices.length; i++){
            int profit = prices[i] - minPrice;
            if(profit > maxProfit)
                maxProfit = profit;
            curMaxProfits[i] = maxProfit;
            if(prices[i] < minPrice)
                minPrice = prices[i];
        }
        
        // step2:
        int maxFinalProfit = curMaxProfits[prices.length-1];
        int maxPrice = prices[prices.length-1];
        maxProfit = 0;
        for(int i = prices.length-2; i>=0; i--){
            int profit = maxPrice - prices[i];
            if(profit > maxProfit)
                maxProfit = profit;
            int finalProfit = maxProfit + curMaxProfits[i];
            if(finalProfit > maxFinalProfit)
                maxFinalProfit = finalProfit;
            if(prices[i] > maxPrice)
                maxPrice = prices[i];
        }
        return maxFinalProfit;
    }
}