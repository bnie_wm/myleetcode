# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None



"""
Iterative with Stack

"""
class Solution(object):
    def inorderTraversal(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        path = []
        stack = []
        cur = root
        while cur or stack:
            while cur:
                stack.append(cur)
                cur = cur.left
            cur = stack.pop()
            path.append(cur.val)
            cur = cur.right
            
        return path
        



# """
# Recursive
# Time: O(n)
# Space: O(h)
# """
# class Solution2(object):
#     def inorderTraversal(self, root):
#         """
#         :type root: TreeNode
#         :rtype: List[int]
#         """
        
#         path = []
#         self.inorderTraversalHelper(path, root)
#         return path
        
#     def inorderTraversalHelper(self, path, root):
#         if root is None:
#             return
        
#         self.inorderTraversalHelper(path, root.left)
#         path.append(root.val)
#         self.inorderTraversalHelper(path, root.right)
            