# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

"""
Time: O(h)
Space: O(1)
"""
class Solution(object):
    def inorderSuccessor(self, root, p):
        """
        :type root: TreeNode
        :type p: TreeNode
        :rtype: TreeNode
        """
        if not root or not p:
            return None
        
        # if p has right tree
        # then, its successor is the left most leaf of its right subtree
        if p.right:
            cur = p.right
            while cur.left:
                cur = cur.left
            return cur
        
        # if p doesn't have right tree
        # # then, it's successor is the first root whose value is greater than it
        successor = None
        cur = root
        while cur:
            if cur.val > p.val:
                successor = cur
                cur = cur.left
            else:
                cur = cur.right
        return successor

            