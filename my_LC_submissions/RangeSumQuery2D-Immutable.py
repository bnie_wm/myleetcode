class NumMatrix(object):
    def __init__(self, matrix):
        """
        initialize your data structure here.
        :type matrix: List[List[int]]
        """
        m = len(matrix)
        n = len(matrix[0]) if matrix else 0
        # 1st row and 1st col: dummy border -- 0
        self.__dp = [[0 for _ in xrange(n+1)] for _ in xrange(m+1)]
        for i in xrange(1, m+1):
            for j in xrange(1, n+1):
                self.__dp[i][j] = matrix[i-1][j-1] + self.__dp[i-1][j] + self.__dp[i][j-1] - self.__dp[i-1][j-1]
        

    def sumRegion(self, row1, col1, row2, col2):
        """
        sum of elements matrix[(row1,col1)..(row2,col2)], inclusive.
        :type row1: int
        :type col1: int
        :type row2: int
        :type col2: int
        :rtype: int
        """
        if not self.__dp:
            return 0
        
        return self.__dp[row2+1][col2+1] - self.__dp[row1][col2+1] - self.__dp[row2+1][col1] + self.__dp[row1][col1]
        

# Your NumMatrix object will be instantiated and called as such:
# numMatrix = NumMatrix(matrix)
# numMatrix.sumRegion(0, 1, 2, 3)
# numMatrix.sumRegion(1, 2, 3, 4)