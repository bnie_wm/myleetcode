class Solution(object):
    def findMissingRanges(self, nums, lower, upper):
        """
        :type nums: List[int]
        :type lower: int
        :type upper: int
        :rtype: List[str]
        """
        if lower > upper:
            return []

        if not nums:
            return [self.buildStr(lower, upper)]

        res = []
        for i in xrange(len(nums)):
            if i == 0:
                if nums[0] > lower:
                    res.append(self.buildStr(lower, nums[0] - 1))
            elif nums[i] > nums[i - 1] + 1:
                res.append(self.buildStr(nums[i - 1] + 1, nums[i] - 1))
        if nums[-1] < upper:
            res.append(self.buildStr(nums[-1] + 1, upper))
        return res

    def buildStr(self, n1, n2):
        if n1 == n2:
            return str(n1)
        else:
            return str(n1) + "->" + str(n2)
