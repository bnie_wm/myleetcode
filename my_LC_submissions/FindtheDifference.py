"""
Bit Manipulation: XOR
Time: O(n)
Space: O(1)
"""
class Solution(object):
    def findTheDifference(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: str
        """
        res = 0
        for x in s:
            res ^= ord(x)
        for x in t:
            res ^= ord(x)
        return chr(res)