class Solution(object):
    def canCompleteCircuit(self, gas, cost):
        """
        :type gas: List[int]
        :type cost: List[int]
        :rtype: int
        """
        
        start_idx, cur_gas, total_gas = 0, 0, 0
        for i in xrange(len(gas)):
            cur_gas += gas[i] - cost[i]
            total_gas += gas[i] - cost[i]
            if cur_gas < 0:  # cannot reach the next stop, then, this stop cannot be the start index
                start_idx = i + 1  # since retrun non-zero start index
                cur_gas = 0  # since start index changes, reset cur_gas
        if total_gas >= 0:
            return start_idx
        else:
            return -1
        
                