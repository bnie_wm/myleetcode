public class Solution {
    public boolean isPalindrome(int x) {
        // Note: The Solution object is instantiated only once and is reused by each test case.
        if(x < 0) return false;
        if(x == 0) return true;
		int length = (int)(Math.log10(x) + 1);
		int[] digits = new int[length];
        for(int i = 0; i < length; i++){
        	digits[i] = x%10;
        	x = x/10;
        }
        for(int i = 0; i < length/2; i++)
        	if(digits[i] != digits[length-i-1])
        		return false;
		return true;
    }
}