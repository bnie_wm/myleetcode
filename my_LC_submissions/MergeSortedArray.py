# Time:  O(n)
# Space: O(1)
class Solution(object):
    def merge(self, nums1, m, nums2, n):
        """
        :type nums1: List[int]
        :type m: int
        :type nums2: List[int]
        :type n: int
        :rtype: void Do not return anything, modify nums1 in-place instead.
        """
        if n == 0:
            return
        
        idx, i, j = m+n-1, m-1, n-1
        while i>=0 and j >=0:
            while i >= 0 and nums1[i] >= nums2[j]:
                nums1[idx] = nums1[i]
                i -= 1
                idx -= 1
            while j >= 0 and nums1[i] < nums2[j]:
                nums1[idx] = nums2[j]
                j -= 1
                idx -= 1
        
        while j >= 0:  # in case more numbers in nums2
            nums1[idx] = nums2[j]
            idx -= 1
            j -= 1
                
        