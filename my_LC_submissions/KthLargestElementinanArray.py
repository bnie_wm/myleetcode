class Solution(object):
    def findKthLargest(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: int
        """
        
        # quick sort --> partition
        # Time: O(n) ~ O(n^2)
        # Space: O(1)
        
        left, right = 0, len(nums)-1
        while left <= right:   # must have =, since base case is only ONE element
            p = (left + right) / 2
            new_p = self.partition(nums, left, right, p)
            # after partition, all numbers in nums[:new_p] > all numbers in nums[new_p:]
            if new_p == k-1:
                return nums[new_p]
            elif new_p < k-1:
                left = new_p + 1
            else:
                right = new_p - 1
        
    def partition(self, nums, left, right, p):
        p_val = nums[p]
        nums[right], nums[p] = nums[p], nums[right]  # save p_val in the right most pos
        # new_p is the first index that is smaller than p_val!!!
        # after later swap with p_val (saved in nums[right]), new_p becomes the last index that is > p_val
        new_p = left  
        for i in xrange(left, right): # skip nums[-1], since it stores cur p_val
            if nums[i] > p_val:
                nums[i], nums[new_p] = nums[new_p], nums[i]  # swap larger val before new_p, then new_p
                new_p += 1
        nums[right], nums[new_p] = nums[new_p], nums[right]
        return new_p
        
    