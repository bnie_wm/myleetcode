"""
Time: O(logn)
Space: O(1)
"""
class Solution(object):
    def convertToTitle(self, n):
        """
        :type n: int
        :rtype: str
        """
        title, dvd = "", n
        while dvd:
            title += chr((dvd-1)%26 + ord('A'))
            dvd = (dvd-1)/26
        return title[::-1]
        