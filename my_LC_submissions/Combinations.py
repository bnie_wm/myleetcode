"""
Time: O(n!)
Space: O(n)
"""
class Solution(object):
    def combine(self, n, k):
        """
        :type n: int
        :type k: int
        :rtype: List[List[int]]
        """

        results = []
        self.combineHelper(results, [], n, k, 0)
        return results
        
    def combineHelper(self, results, res, n, k, start):
        if len(res) == k:
            results.append(res)
            return
        for i in xrange(start, n):
            self.combineHelper(results, res + [i+1], n, k, i+1)  # note that, next start should greater than i