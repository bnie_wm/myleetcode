"""
using BIT since BIT use less space than Segment Tree and is easier to perform the 2D problem
ctro: O(n)
update/query: O(logn)
"""
class NumArray(object):
    def __init__(self, nums):
        """
        initialize your data structure here.
        :type nums: List[int]
        """
        self.__nums = nums
        
        # segment tree: len of 4n
        # build BIT: len of (n+1)
        self.__bit = [0]  # bit[0] is dummy
        # 1st iteration: begin -> end, up -> bottom
        # bit[i] = sum(nums[:i]); bit[0] = 0
        for i in xrange(1, len(self.__nums) + 1):
            self.__bit.append(self.__bit[i-1] + self.__nums[i-1])
        # 2nd iteration: end -> begin, bottom -> up
        # bit[i] = sum(nums[:i]) - sum(nums[:parent(i)])
        for i in xrange(len(self.__nums), 0, -1):
            pid = i - (i & -i)  # i's parent
            self.__bit[i] -= self.__bit[pid]
    
    def _lsb(self, i):
        return i & -i

    # query range [1, i] = bit[i] + bit[p(i)] + bit[p(p(i))] + ..., util reach dummy root
    # bottom -> up -- O(logn)
    def _queryBIT(self, i):
        s = 0
        while i > 0:
            s += self.__bit[i]
            i -= self._lsb(i)  # travel up to parent node
        return s
    
    # update bit[i]: affected nodes: bit[i], bit[child(i)], bit[child(child(i))], ..., util reach leaf
    # up -> bottom -- O(logn)
    def _updateBIT(self, i, dif):
        while i < len(self.__bit):
            self.__bit[i] += dif
            i += self._lsb(i)  # travel down to its descendant node

    def update(self, i, val):
        """
        :type i: int
        :type val: int
        :rtype: int
        """
        dif = val - self.__nums[i]
        if dif:
            self.__nums[i] = val
            bit_idx = i + 1  # BIT index starts from 1
            self._updateBIT(bit_idx, dif)

    def sumRange(self, i, j):
        """
        sum of elements nums[i..j], inclusive.
        :type i: int
        :type j: int
        :rtype: int
        """
        # BIT index starts from 1
        # self._queryBIT(j+1) returns sum from nums[0] ~ nums[j], inclusive
        return self._queryBIT(j+1) - self._queryBIT(i)
    


# Your NumArray object will be instantiated and called as such:
# numArray = NumArray(nums)
# numArray.sumRange(0, 1)
# numArray.update(1, 10)
# numArray.sumRange(1, 2)