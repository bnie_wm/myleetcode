# Definition for binary tree with next pointer.
# class TreeLinkNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
#         self.next = None

# level order tree traversal
# Time:  O(n)
# Space: O(1)
class Solution:
    # @param root, a tree link node
    # @return nothing
    def connect(self, root):
        cur_head = root
        while cur_head:
            cur = cur_head
            # two conidtions, cur != None and cur has children (since perferct tree, each node must have two children!)
            while cur and cur.left:   
                cur.left.next = cur.right
                if cur.next:
                    cur.right.next = cur.next.left
                cur = cur.next
            cur_head = cur_head.left  # move to next level
        