class Solution(object):
    def rob(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if not nums:
            return 0
            
        if len(nums) == 1:
            return nums[0]
        
        profit_0 = self.oneRob(nums[:-1])
        profit_1 = self.oneRob(nums[1:])
        return max(profit_0, profit_1)
        
    def oneRob(self, nums):
        if not nums:
            return 0
        if len(nums) == 1:
            return nums[0]
        
        # DP -- space: O(1)
        prev, cur = nums[0], max(nums[0], nums[1])
        for i in xrange(2, len(nums)):
            prev, cur = cur, max(prev+nums[i], cur)
        return cur  