class Solution(object):
    def mySqrt(self, x):
        """
        :type x: int
        :rtype: int
        """

        left, right = 0, x/2+1
        while left <= right:   # important to have =!!!
            mid = left + (right - left) / 2
            product = mid * mid
            if x == product:
                return mid
            elif x < product:
                right = mid - 1
            else:
                left = mid + 1
        # return (left+right)/2
        return right