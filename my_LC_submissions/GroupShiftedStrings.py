"""
Time: O(n*k)
Space: O(1)
"""
class Solution(object):
    def groupStrings(self, strings):
        """
        :type strings: List[str]
        :rtype: List[List[str]]
        """
        groups = dict()
        for word in strings:
            if len(word) == 1:
                gid = ""
            else:
                gid = ""
                for i in xrange(len(word)-1):
                    if ord(word[i+1]) >= ord(word[i]):
                        gid += str(ord(word[i+1]) - ord(word[i]))
                    else:
                        gid += str(ord(word[i+1]) + 26 - ord(word[i]))
            if gid not in groups:
                groups[gid] = []
            groups[gid].append(word)
            
        res = []
        for gid in groups:
            res.append(groups[gid])
        return res