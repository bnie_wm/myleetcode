class Solution(object):
    def plusOne(self, digits):
        """
        :type digits: List[int]
        :rtype: List[int]
        """
        digits[-1] += 1
        carry = 0
        for i in xrange(len(digits)-1, -1, -1):
            tmp = digits[i] + carry
            digits[i], carry = tmp%10, tmp/10
        if carry > 0:
            digits = [carry] + digits
        return digits