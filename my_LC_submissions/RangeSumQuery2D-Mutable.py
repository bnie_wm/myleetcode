"""
BIT:
ctor: O(m * n)
update/query: O(logm * logn)
space: O(m*n)
"""
class NumMatrix(object):
    def __init__(self, matrix):
        """
        initialize your data structure here.
        :type matrix: List[List[int]]
        """
        if not matrix:
            return
        self.__matrix = matrix
        self.__m = len(self.__matrix)
        self.__n = len(self.__matrix[0]) if self.__matrix else 0
        
        # build BIT -- matrix
        # 1st and 1st col: dummy zeros -- same as done 1D version (bit[0] = 0)
        self.__bit = [[0 for _ in xrange(self.__n + 1)] for _ in xrange(self.__m + 1)]
        # 1st iteration: 
        # bit[i][j] = sum of cells on the left and upper side of matrix[i-1][j-1] (including row i-1 and col j-1)
        for i in xrange(1, self.__m+1):
            for j in xrange(1, self.__n+1):
                self.__bit[i][j] = self.__matrix[i-1][j-1] \
                                + self.__bit[i-1][j] + self.__bit[i][j-1] \
                                - self.__bit[i-1][j-1]
        # 2nd iteration:
        # exclude sum in bit[parent(i)][parent(j)] from bit[i][j]  -- same as done in 1D version
        for i in xrange(self.__m, 0, -1):
            for j in xrange(self.__n, 0, -1):
                pi, pj = i - self._lsb(i), j - self._lsb(j) # to parent
                self.__bit[i][j] = self.__bit[i][j] \
                                - self.__bit[pi][j] - self.__bit[i][pj] \
                                + self.__bit[pi][pj]
        
    def _lsb(self, i):
        return i & -i
    
    def _queryBIT(self, row, col):
        s = 0
        i = row
        while i > 0:
            j = col
            while j > 0:
                s += self.__bit[i][j]
                j -= self._lsb(j)
            i -= self._lsb(i)  # to parent
        return s
    
    def _updateBIT(self, row, col, dif):
        i = row
        while i <= self.__m:
            j = col
            while j <= self.__n:
                self.__bit[i][j] += dif
                j += self._lsb(j)
            i += self._lsb(i) # to child
        
    def update(self, row, col, val):
        """
        update the element at matrix[row,col] to val.
        :type row: int
        :type col: int
        :type val: int
        :rtype: void
        """
        dif = val - self.__matrix[row][col]
        if dif:
            self.__matrix[row][col] = val
            # BIT index starts from 1
            self._updateBIT(row+1, col+1, dif)

    def sumRegion(self, row1, col1, row2, col2):
        """
        sum of elements matrix[(row1,col1)..(row2,col2)], inclusive.
        :type row1: int
        :type col1: int
        :type row2: int
        :type col2: int
        :rtype: int
        """
        # BIT index starts from 1
        return self._queryBIT(row2+1, col2+1) \
            - self._queryBIT(row1, col2+1) - self._queryBIT(row2+1, col1) \
            + self._queryBIT(row1, col1)
        

# Your NumMatrix object will be instantiated and called as such:
# numMatrix = NumMatrix(matrix)
# numMatrix.sumRegion(0, 1, 2, 3)
# numMatrix.update(1, 1, 10)
# numMatrix.sumRegion(1, 2, 3, 4)