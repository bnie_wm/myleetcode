class Solution(object):
    def rob(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        
        if not nums:
            return 0
        if len(nums) == 1:
            return nums[0]
        
        # DP2 -- space: O(1)
        prev, cur = nums[0], max(nums[0], nums[1])
        for i in xrange(2, len(nums)):
            prev, cur = cur, max(prev+nums[i], cur)
        return cur    

        
        # # DP1 -- space: O(n)
        # # dp[i] = max(dp[i-2] + nums[i], dp[i-1])
        # dp = [0] * len(nums)
        # dp[0], dp[1] = nums[0], max(nums[0], nums[1])  # dp[1] != nums[1]!, but = max(nums[0], nums[1])!!!!
        # for i in xrange(2, len(nums)):
        #     dp[i] = max(dp[i-2] + nums[i], dp[i-1])
        # return dp[-1]
        