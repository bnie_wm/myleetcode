class Solution(object):
    def combinationSum2(self, candidates, target):
        """
        :type candidates: List[int]
        :type target: int
        :rtype: List[List[int]]
        """
        candidates.sort()
        results = []
        self.combinationSumHelper(results, [], candidates, target, 0)
        return results
        
        
    def combinationSumHelper(self, results, res, candidates, target, start):
        if target == 0:
            results.append(res)
            return

        for i in xrange(start, len(candidates)):
            if i > start and candidates[i] == candidates[i-1]:
                # i.e., [1, 1, 2, 3]
                # skip the second 1, since the result will be the same as the first one
                continue
            if target - candidates[i] >= 0:
                # next start: i+1, (since no duplicates)
                self.combinationSumHelper(results, res + [candidates[i]], candidates, target - candidates[i], i+1)