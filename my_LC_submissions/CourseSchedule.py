"""
Time: O(|V|+|E|)
Space: O(|V|*|E|)
"""

class Solution(object):
    def canFinish(self, numCourses, prerequisites):
        """
        :type numCourses: int
        :type prerequisites: List[List[int]]
        :rtype: bool
        """
        indegrees = [0] * numCourses    
        s = collections.deque()
        l = []
        
        # build indegress
        edge_list = dict()
        for nid in xrange(numCourses):
            edge_list[nid] = []
        for (m, n) in prerequisites:
            edge_list[m].append(n)
            indegrees[n] += 1
        
        for nid in xrange(numCourses):
            if indegrees[nid] == 0:
                s.append(nid)
    
        while s:
            m = s.popleft()
            l.append(m)
            for n in edge_list[m]:
                prerequisites.remove([m, n])
                indegrees[n] -= 1
                if indegrees[n] == 0:
                    s.append(n)
                    
        if len(prerequisites) == 0:
            return True
        else:
            return False
                
            