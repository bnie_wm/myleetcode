class Solution(object):
    def generateParenthesis(self, n):
        """
        :type n: int
        :rtype: List[str]
        """
        # Time:  O(4^n / n^(3/2)) ~= Catalan numbers
        # h(n) = h(0)*h(n-1) + h(1)*h(n-2) + ... + h(n-1)*h(0) = 
        # Space: O(n)
        
        results = []
        self.helper(results, n, n, "")
        return results;
        
    def helper(self, results, leftRem, rightRem, curStr):
        if leftRem == 0 and rightRem == 0:
            results.append(curStr)
            return
        
        if leftRem > 0:  # add "(" as long as we still have more left
            self.helper(results, leftRem-1, rightRem, curStr+"(")
        
        if rightRem > leftRem:  # add ")" only when we have more "(" than ")"
            self.helper(results, leftRem, rightRem-1, curStr+")")