class Codec:

    def encode(self, strs):
        """Encodes a list of strings to a single string.
        
        :type strs: List[str]
        :rtype: str
        """
        # encode format: <len>#str1<len>#str2...
        encoded = ""
        for s in strs:
            encoded += str(len(s)) + "#" + s
        return encoded
        

    def decode(self, s):
        """Decodes a single string to a list of strings.
        
        :type s: str
        :rtype: List[str]
        """
        i, l = 0, 0
        strs = []
        while i < len(s):
            if s[i] == '#':
                strs.append(s[i+1:i+l+1])
                i = i + l + 1
                l = 0
            else:
                l = l * 10 + int(s[i])
                i += 1
        return strs
                
# Your Codec object will be instantiated and called as such:
# codec = Codec()
# codec.decode(codec.encode(strs))