public class Solution {
    public void nextPermutation(int[] num) {
        // IMPORTANT: Please reset any member data you declared, as
        // the same Solution instance will be reused for each test case.
        int cur = num.length-1;
        while(cur > 0 && num[cur-1] >= num[cur])
            cur--;
        
        reverse(num, cur, num.length-1);
        
        int next = cur;
        cur--;
        // next: first element of reversed right most descending sequence
        // current: the first element left from that sequence
        // exp: 13,542 --> 13,245: num[next] = num[2] = 2, current = 1
        // exp: ,54321 --> ,12345: num[next] = num[0] = 1, current = -1
        // current < 0, indicates that it is the last sequence, just return the reversed one
        while(next < num.length){
            if(cur >= 0 && num[next] > num[cur]){
                swap(num, cur, next);
                break;
            }
            next++;
        }
    }
    
    private void swap(int[] num, int i, int j){
        int temp = num[i];
        num[i] = num[j];
        num[j] = temp;
    }
    
    private void reverse(int[] num, int i, int j){
        while(i < j){
            swap(num, i, j);
            i++;
            j--;
        }
    }
}