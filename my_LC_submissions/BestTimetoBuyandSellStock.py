"""
Time: O(n)
Space: O(1)
"""
class Solution(object):
    def maxProfit(self, prices):
        """
        :type prices: List[int]
        :rtype: int
        """
        if not prices:
            return 0
            
        min_price, max_profit = prices[0], 0
        for p in prices:
            max_profit = max(max_profit, p - min_price) # today's price - previous min price
            min_price = min(min_price, p)  # update min_price with today's price
        return max_profit
        