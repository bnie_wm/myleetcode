# Definition for singly-linked list with a random pointer.
# class RandomListNode(object):
#     def __init__(self, x):
#         self.label = x
#         self.next = None
#         self.random = None


# Time: O(n)
# Space: O(n)
class Solution(object):
    def copyRandomList(self, head):
        """
        :type head: RandomListNode
        :rtype: RandomListNode
        """
        
        dummy = RandomListNode(0)  # dummy head for new list
        cur = head  # move along original list
        prev = dummy  # move along new list
        copies = dict()  # old_node --> new_node
        
        
        # create new nodes and copy "next" pointers
        while cur:
            new_node = RandomListNode(cur.label)
            copies[cur] = new_node  # add old_node-->new_node mapping
            prev.next = new_node
            prev, cur = prev.next, cur.next  # move to next node in both lists
            
        # redirect to original list head
        cur = head
        # add random pointers to nodes in new list
        while cur:
            if cur.random:
                # copies[cur].random = cur.random  wrong!, cur.random (from old), copies[cur.random] (from new)
                copies[cur].random = copies[cur.random]
            cur = cur.next
        
        return dummy.next
            