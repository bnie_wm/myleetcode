# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def pathSum(self, root, sum):
        """
        :type root: TreeNode
        :type sum: int
        :rtype: List[List[int]]
        """
        results = []
        self.pathSumHelper(results, [], root, sum)
        return results
        
    
    def pathSumHelper(self, results, res, root, sum):
        if root is None:
            return results
        
        # find one root-to-leaf path
        if root.left is None and root.right is None and root.val == sum:
            results.append(res + [root.val])
            return results
        
        res.append(root.val)
        self.pathSumHelper(results, res, root.left, sum - root.val)
        self.pathSumHelper(results, res, root.right, sum - root.val)
        res.pop()