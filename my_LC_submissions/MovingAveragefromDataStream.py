class MovingAverage(object):

    def __init__(self, size):
        """
        Initialize your data structure here.
        :type size: int
        """
        self.__window = collections.deque()
        self.__size = size
        self.__sum = 0
        
    def next(self, val):
        """
        :type val: int
        :rtype: float
        """
        if len(self.__window) >= self.__size:
            self.__sum -= self.__window.popleft()
        self.__window.append(val)
        self.__sum += val
        return self.__sum * 1.0 / len(self.__window)

# Your MovingAverage object will be instantiated and called as such:
# obj = MovingAverage(size)
# param_1 = obj.next(val)