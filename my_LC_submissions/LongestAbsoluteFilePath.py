class Solution(object):
    def lengthLongestPath(self, input):
        """
        :type input: str
        :rtype: int
        """
        if not input or "." not in input:
            return 0
        
        maxLen = 0
        parts = input.split("\n")
        # dummy: depth -1 --> 0, for case where only one level and that is a file!
        # pathLen: key-->depth, val: accumulative length
        # since the input is DFS, the accu. length is always for current file,
        # so it is ok to update maxLen whenever meet a file
        pathLen = {-1:0}  
        for part in parts:
            name = part.replace("\t", "")
            depth = len(part) - len(name)
            if depth not in pathLen:
                pathLen[depth] = 0
            if "." in name:  # if file
                pathLen[depth] = pathLen[depth-1] + len(name)
                maxLen = max(pathLen[depth], maxLen)
            else:  # if dir
                pathLen[depth] = pathLen[depth-1] + len(name) + 1  # +1 --> '/'
        return maxLen