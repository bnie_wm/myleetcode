class Solution(object):
    def search(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        return self.searchHelper(nums, 0, len(nums)-1, target)
        
        
    def searchHelper(self, nums, left, right, x):
        mid = (left + right) / 2
        if x == nums[mid]: # find!
            return mid
            
        while left <= right:    
            # Either the left or right half must be normally ordered.
            # Find out which side is normally ordered, 
            # and then use the normally ordered half to figure out which side to search to find x
            if nums[left] <= nums[mid]: # left is normally ordered
                if nums[left] <= x and x <= nums[mid]:
                    return self.searchHelper(nums, left, mid-1, x)
                else:
                    return self.searchHelper(nums, mid+1, right, x)
            else: # right is normally ordered
                if nums[mid] <= x and x <= nums[right]:
                    return self.searchHelper(nums, mid+1, right, x)
                else:
                    return self.searchHelper(nums, left, mid-1, x)
        return -1
        
        