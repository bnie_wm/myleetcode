class Solution(object):
    def isOneEditDistance(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: bool
        """
        if len(s) > len(t):
            return self.isOneEditDistance(t, s)
        
        # now, len(s) <= len(t)
        if s == t:
            return False
        
        if len(t) - len(s) > 1:
            return False
        elif len(t) - len(s) == 1:
            for i in xrange(len(t)):
                 if t[:i] + t[i+1:] == s:
                     return True
            return False
        else:   # len(t) == len(s)
            first = True
            for i in xrange(len(t)):
                if s[i] != t[i]:
                    if first:
                        first = False
                    else:
                        return False
            return True
            
                 
                 
             
        
        