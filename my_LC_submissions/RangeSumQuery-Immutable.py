class NumArray(object):
    def __init__(self, nums):
        """
        initialize your data structure here.
        :type nums: List[int]
        """
        self.__dp = [0]  # dp[0] -- dummy
        acc_sum = 0
        for i in xrange(len(nums)):
            acc_sum += nums[i]
            self.__dp.append(acc_sum)
        

    def sumRange(self, i, j):
        """
        sum of elements nums[i..j], inclusive.
        :type i: int
        :type j: int
        :rtype: int
        """
        return self.__dp[j+1] - self.__dp[i]
        


# Your NumArray object will be instantiated and called as such:
# numArray = NumArray(nums)
# numArray.sumRange(0, 1)
# numArray.sumRange(1, 2)