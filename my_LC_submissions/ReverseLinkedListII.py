/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */
public class Solution {
    public ListNode reverseBetween(ListNode head, int m, int n) {
        // Note: The Solution object is instantiated only once and is reused by each test case.
        ListNode newhead = new ListNode(0);
       newhead.next = head;
       int count = n - m;
       ListNode preNode = newhead;
       while(m > 1){
    	   preNode = preNode.next;
    	   m--;
       }
       ListNode prev = preNode.next;
       ListNode cur = prev.next;
       ListNode next = null;
       while(count > 0){
    	   next = cur.next;
    	   cur.next = prev;
    	   prev = cur;
    	   cur = next;
    	   count--;
       }
       preNode.next.next = cur;
       preNode.next = prev;
       return newhead.next;
    }
}