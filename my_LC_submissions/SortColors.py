class Solution(object):
    def sortColors(self, nums):
        """
        :type nums: List[int]
        :rtype: void Do not return anything, modify nums in-place instead.
        """
        
        self.triPartition(nums, 1)
        
        
    def triPartition(self, nums, target):
        # >target: move to end
        # <target: move to begin
        i, first1, last2 = 0, 0, len(nums)-1
        while i <= last2:
            if nums[i] < target:
                nums[i], nums[first1] = nums[first1], nums[i]  # swap cur 0 and first1
                i += 1
                first1 += 1
            elif nums[i] > target:
                nums[i], nums[last2] = nums[last2], nums[i]  # swap cur 2 and last 2
                last2 -= 1
            else:
                i += 1
                
                