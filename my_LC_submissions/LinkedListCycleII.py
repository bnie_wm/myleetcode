# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def detectCycle(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        slow, fast = head, head
        while fast and fast.next:
            slow, fast = slow.next, fast.next.next
            if slow is fast:  # detect the cycle
                fast = head  # move fast to head
                while fast is not slow:
                    slow, fast = slow.next, fast.next
                return fast  # the next meeting point is the begining of cycle
        return None
                
        