class Solution(object):
    def intToRoman(self, num):
        """
        :type num: int
        :rtype: str
        """
        keys = [1, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500, 900, 1000]
        values = ['I', 'IV', 'V', 'IX', 'X', 'XL', 'L', 'XC', 'C', 'CD', 'D', 'CM', 'M']
        lookup = dict(zip(keys, values))
        roman = ""
        while num > 0:
            for key in reversed(keys):  # start from largest to smallest
                while num / key > 0:   ## use 'while', for cases like 20 (XX)
                    num -= key  ## -= key, not /= key
                    roman += lookup[key]
        return roman
                    
