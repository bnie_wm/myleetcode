class Solution(object):
    def lengthOfLongestSubstringKDistinct(self, s, k):
        """
        :type s: str
        :type k: int
        :rtype: int
        """
        if not s:
            return 0
        char_cnt = collections.defaultdict(int)
        begin = 0
        maxLen = 0
        for i in xrange(len(s)):
            char_cnt[s[i]] += 1
            while len(char_cnt) > k:
                char_cnt[s[begin]] -= 1
                if char_cnt[s[begin]] == 0:  # if the begin char's freq = 0, remove it from dictionary!
                    del char_cnt[s[begin]]
                begin += 1
            maxLen = max(maxLen, i - begin + 1)
        return maxLen