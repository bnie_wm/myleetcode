"""
DP: Time:O(n), Space: O(1)
Divide and Conquer: Time: T(n) = 2T(n/2) + O(n) = O(nlogn)
"""

"""
DP:
Time: O(n)
Space: O(1)
"""
class Solution(object):
    def maxSubArray(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """

        # init for nums[0]
        acc_sum = nums[0]
        max_sum = acc_sum

        # iterate from 2nd --> last
        for i in xrange(1, len(nums)):
            # if acc_sum<0, it means the accumulated sum will contribute negatively to the sum
            # so clear acc_sum
            if acc_sum < 0:
                acc_sum = 0
            acc_sum += nums[i]
            max_sum = max(acc_sum, max_sum)
        return max_sum

"""
Divide and Conquer: Time: T(n) = 2T(n/2) + O(n) = O(nlogn)
"""
# class Solution(object):
#     def maxSubArray(self, nums):
#         """
#         :type nums: List[int]
#         :rtype: int
#         """
#         return self.maxSubArrayHelper(nums, 0, len(nums)-1, float("-inf"))
        
#     def maxSubArrayHelper(self, nums, left, right, max_sum):
#         if left == right:  # base: only one element
#             return nums[left]
        
#         mid = (left+right)/2
#         # case 1: if targeted subarray in the left half
#         left_max = self.maxSubArrayHelper(nums, left, mid, max_sum)
        
#         # case 2: if targeted subarray in the right half
#         right_max = self.maxSubArrayHelper(nums, mid+1, right, max_sum)
        
#         # update max_sum
#         max_sum = max(max(max_sum, left_max), right_max)
        
#         # case 3: if targeted subarray across both
#         # 3-1: get max sum in the left part (mid --> left)
#         acc_left_sum, this_left_max = 0, float("-inf")
#         for i in reversed(xrange(left, mid+1)):
#             acc_left_sum += nums[i]
#             this_left_max = max(this_left_max, acc_left_sum)
#         # 3-2: get max sum in the rihgt part (mid+1 --> right)
#         acc_right_sum, this_right_max = 0, float("-inf")
#         for i in xrange(mid+1, right+1):
#             acc_right_sum += nums[i]
#             this_right_max = max(this_right_max, acc_right_sum)
        
#         # update max_sum
#         max_sum = max(max_sum, this_left_max + this_right_max)
        
#         return max_sum
        
        
        