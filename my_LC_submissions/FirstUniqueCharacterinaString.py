class Solution(object):
    def firstUniqChar(self, s):
        """
        :type s: str
        :rtype: int
        """
        freq = collections.defaultdict(int)
        for i in xrange(len(s)):
            freq[s[i]] += 1
        
        for i in xrange(len(s)):
            if freq[s[i]] == 1:
                return i
        return -1