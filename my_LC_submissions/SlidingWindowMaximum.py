class Solution(object):
    def maxSlidingWindow(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: List[int]
        """
        if not nums:
            return []
        
        res = []
        for i in xrange(len(nums)-k+1):
            res.append(max(nums[i:i+k]))
        return res