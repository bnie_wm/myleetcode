public class Solution {
    public String minWindow(String S, String T) {
        HashMap<Character, Integer> needFind = new HashMap<Character, Integer>();
        HashMap<Character, Integer> hasFind = new HashMap<Character, Integer>();
        String total = "" + S + T;
        String result = "";
        for(int i = 0; i < total.length(); i++) {
            char c = total.charAt(i);
            needFind.put(c, 0);
            hasFind.put(c, 0);
        }
        for(int i = 0; i < T.length(); i++) {
            char c = T.charAt(i);
            needFind.put(c, needFind.get(c)+1);
        }
        
        int cnt = 0; int start = 0; char startC = S.charAt(start); int minLen = Integer.MAX_VALUE;
        for(int i = 0; i < S.length(); i++) {
            char c = S.charAt(i);
            hasFind.put(c, hasFind.get(c)+1);
            if(hasFind.get(c) <= needFind.get(c)) cnt++;
            if(cnt == T.length()) {
                // while(hasFind.get(startC) == 0 ||
                    while(hasFind.get(startC) > needFind.get(startC)){
                        if(hasFind.get(startC) > needFind.get(startC)) {
                            hasFind.put(startC, hasFind.get(startC)-1);
                        }
                        start++;
                        startC = S.charAt(start);
                    }
                if(i-start+1 < minLen) {
                    minLen = i-start+1;
                    result = S.substring(start, i+1);
                }
                
            }
        }
        return result;
        
    }
}