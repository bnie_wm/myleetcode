/**
 * Definition for singly-linked list.
 * class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */
public class Solution {
    public void reorderList(ListNode head) {
        // IMPORTANT: Please reset any member data you declared, as
        // the same Solution instance will be reused for each test case.
        
        // no need to re-order for linkedlist shorter than 3
        if(head == null || head.next == null || head.next.next == null)
            return;
        
        ListNode mid = head, end = head;
        while(end != null && end.next != null){
            mid = mid.next;
            end = end.next.next;
        }
        
        reverseList(mid);
        
        ListNode prev = head;
        mid = mid.next;
        ListNode prevNext = null;
        ListNode midNext = null;
        while(mid.next != null){
            prevNext = prev.next;
            midNext = mid.next;
            prev.next = mid;
            mid.next = prevNext;
            prev = prevNext;
            mid = midNext;
        }
        prevNext = prev.next;
        prev.next = mid;
        mid.next = prevNext;
        if(end == null)
        	prevNext.next.next = null;
        else
        	prevNext.next = null;
    }
    
    private void reverseList(ListNode head){
        ListNode prev = head.next;
        if(prev.next == null) return;
        ListNode cur = prev.next;
        prev.next = null;
        ListNode next = null;
        while(cur.next != null){
            next = cur.next;
            cur.next = prev;
            prev = cur;
            cur = next;
        }
        cur.next = prev;
        head.next = cur;
    }
}