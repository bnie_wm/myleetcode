class Solution(object):
    def numIslands(self, grid):
        """
        :type grid: List[List[str]]
        :rtype: int
        """
        if not grid:
            return 0

        m, n = len(grid), len(grid[0])
        visited = [[False for j in xrange(n)] for i in xrange(m)]

        cnt = 0
        for i in xrange(m):
            for j in xrange(n):
                if grid[i][j] == '1' and not visited[i][j]:
                    visited[i][j] = True
                    self.dfs(grid, visited, m, n, i, j)
                    cnt += 1
        return cnt

    def dfs(self, grid, visited, m, n, x, y):
        directions = [(-1, 0), (1, 0), (0, 1), (0, -1)]
        for d in directions:
            i, j = x + d[0], y + d[1]
            if 0 <= i < m and 0 <=j < n and grid[i][j] == '1' and not visited[i][j]:
                visited[i][j] = True
                self.dfs(grid, visited, m, n, i, j)
        
        
                    
                    