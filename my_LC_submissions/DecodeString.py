"""
Time: O(n)
Space: O(h)
"""
class Solution(object):
    def decodeString(self, s):
        """
        :type s: str
        :rtype: str
        """
        # s = "aa2[2[bc]3[xy]]xd4[rt]end"
        if "[" not in s:  # base case
            return s

        numLeft = 0  # num of "["
        res, curStr = [], []
        for c in s:
            curStr.append(c)
            if c == '[':
                numLeft += 1
            elif c == ']':
                numLeft -= 1
                if numLeft == 0 and curStr:
                    curStr = "".join(curStr)
                    i = re.search('\d', curStr).start()  # index of first digit
                    res += curStr[:i]  # add prefix
                    m, n = curStr.find('['), curStr.rfind(']')
                    cnt, decoded = int(curStr[i:m]), self.decodeString(curStr[m+1:n])
                    res += [decoded] * cnt
                    res += curStr[n+1:]  # add suffix
                    curStr = []  # reset curStr
        return "".join(res + curStr)