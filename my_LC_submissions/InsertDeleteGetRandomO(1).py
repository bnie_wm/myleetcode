"""
insert(val), remove(val)  -- hashtable O(1)
getRandom -- array O(1)
"""
class RandomizedSet(object):

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.__arr = []
        self.__dict = {}
        

    def insert(self, val):
        """
        Inserts a value to the set. Returns true if the set did not already contain the specified element.
        :type val: int
        :rtype: bool
        """
        if val in self.__dict:
            return False
        else:
            self.__arr.append(val)
            self.__dict[val] = len(self.__arr) - 1
            return True
        

    def remove(self, val):
        """
        Removes a value from the set. Returns true if the set contained the specified element.
        :type val: int
        :rtype: bool
        """
        if val not in self.__dict:
            return False
        
        # remove val from arr and dict
        # replace current empty location with the last val in arr
        idx = self.__dict[val]
        self.__arr[idx] = self.__arr[-1]
        self.__dict[self.__arr[idx]] = idx
        self.__arr.pop()  # finish replacement first, then remove.
        del self.__dict[val]
        return True
        

    def getRandom(self):
        """
        Get a random element from the set.
        :rtype: int
        """
        return self.__arr[random.randint(0, len(self.__arr)-1)]


# Your RandomizedSet object will be instantiated and called as such:
# obj = RandomizedSet()
# param_1 = obj.insert(val)
# param_2 = obj.remove(val)
# param_3 = obj.getRandom()