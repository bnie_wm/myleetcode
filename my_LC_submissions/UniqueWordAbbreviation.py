class ValidWordAbbr(object):
    def __init__(self, dictionary):
        """
        initialize your data structure here.
        :type dictionary: List[str]
        """
        self.__dict = dict()
        for word in dictionary:
            if len(word) <= 2:
                continue
            abbr = word[0] + str(len(word)-2) + word[-1]
            if abbr not in self.__dict:
                self.__dict[abbr] = set()
            self.__dict[abbr].add(word)
        

    def isUnique(self, word):
        """
        check if a word is unique.
        :type word: str
        :rtype: bool
        """
        if len(word) <= 2:
            return True
        
        abbr = word[0] + str(len(word) - 2) + word[-1]
        wordList = self.__dict.get(abbr, [])
        if len(wordList) == 0 or (word in wordList and len(wordList) == 1):
            return True
        else:
            return False


# Your ValidWordAbbr object will be instantiated and called as such:
# vwa = ValidWordAbbr(dictionary)
# vwa.isUnique("word")
# vwa.isUnique("anotherWord")