"""
Backtracking
"""
class Solution(object):
    def subsets(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        results = []
        self.subsetsHelper(results, [], nums)
        return results
        
    def subsetsHelper(self, results, res, nums):
        if len(nums) == 0:
            results.append(res)
            return
        
        self.subsetsHelper(results, res, nums[1:])
        self.subsetsHelper(results, res + [nums[0]], nums[1:])
        
        