public class Solution {
    public int singleNumber(int[] A) {
        int single = 0;
        for(int i = 0; i < 32; i++) { // assuming dealing with int32
            int bit = 0;
            for(int j = 0; j < A.length; j++) {
                //A[j] >> i, A[j] right shift i bits,
                // & 1, get the last bit, since 0&1=0, 1&1=1
                // bit = (bit + ((A[j] >> i) & 1)) % 3;
                bit += (A[j] >> i) & 1;
            }
            bit = bit % 3;
            single += (bit << i);
        }
        
        return single;
    }
}