"""
Time: O(n)
Space: O(1)
"""
class Solution(object):
    def removeDuplicates(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if not nums:
            return 0
        
        i, j = 0, 1
        while j < len(nums):
            while j < len(nums) and nums[j] == nums[i]:
                j += 1
            if j < len(nums):   # for cases like [1, 1]
                i += 1
                nums[i] = nums[j]
                j += 1
        return i + 1
            
            