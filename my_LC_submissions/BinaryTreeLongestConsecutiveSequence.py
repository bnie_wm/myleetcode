# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def __init__(self):
        self.__maxLen = 0
        
    def longestConsecutive(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        self.longestConsecutiveHelper(root)
        return self.__maxLen

    def longestConsecutiveHelper(self, root):
        # if root.left is None and root.right is None:
        #     if self.__maxLen == 0:
        #         self.__maxLen = 1
        #     return 1
        if not root:
            return 0
        
        # thisLen = 0 --> wrong
        thisLen, leftLen, rightLen = 1, 0, 0
        if root.left:  # if has left tree
            leftLen = self.longestConsecutiveHelper(root.left)
            if root.val + 1 == root.left.val:  # if can connect to left tree
                thisLen = max(thisLen, leftLen + 1)
        if root.right:
            rightLen = self.longestConsecutiveHelper(root.right)
            if root.val + 1 == root.right.val:
                thisLen = max(thisLen, rightLen + 1)
        
        self.__maxLen = max(self.__maxLen, thisLen, leftLen, rightLen)
        return thisLen
        
    
    
        
        