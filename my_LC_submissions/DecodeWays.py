"""
any string starts with "0" will return 0, e.g. "01", "0110", ...
DP
Time: O(n)
Space: O(1)
"""
class Solution(object):
    def numDecodings(self, s):
        """
        :type s: str
        :rtype: int
        """
        if s == "" or s[0] == "0":  # "0xxxxx" will always return 0
            return 0
        
        prev, cur = 1, 1  # for i == 0
        for i in xrange(1, len(s)):
            # one: 1 ~ 9
            one = 0 if s[i] == '0' else cur
            
            # two: 1 ~ 26
            # two = prev if 1 <= int(s[i-1:i+1]) <= 26 else 0  --> wrong, i.e., '01'
            if s[i-1] == '0':
                two = 0
            elif s[i-1] == '1' or (s[i-1] == '2' and int(s[i]) <= 6):
                two = prev
            else:
                two = 0
            prev, cur = cur, one + two
        return cur
        