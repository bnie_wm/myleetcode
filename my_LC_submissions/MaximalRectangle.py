class Solution(object):
    def maximalRectangle(self, matrix):
        """
        :type matrix: List[List[str]]
        :rtype: int
        """
        if not matrix:
            return 0
        
        res = 0
        heights = [0] * len(matrix[0])
        for x in xrange(len(matrix)):
            for y in xrange(len(matrix[0])):
                heights[y] = heights[y] + 1 if matrix[x][y] == '1' else 0
            res = max(res, self.max_rect_in_histogram(heights))
        return res
            
            
    def max_rect_in_histogram(self, heights):
        increasing, area, i = [], 0, 0
        while i <= len(heights):  ## <=, not < !!!
            if not increasing or (i < len(heights) and heights[i] > heights[increasing[-1]]):
                increasing.append(i)
                i += 1
            else:
                last = increasing.pop()
                if not increasing:
                    area = max(area, heights[last] * i)
                else:
                    area = max(area, heights[last] * (i-1-increasing[-1]))
        return area
            