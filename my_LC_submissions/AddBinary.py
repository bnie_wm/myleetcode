"""
Time: O(n)
Space: O(n)
"""
class Solution(object):
    def addBinary(self, a, b):
        """
        :type a: str
        :type b: str
        :rtype: str
        """
        if len(a) < len(b):
            return self.addBinary(b, a)
        
        more = 0
        res = []
        for i in xrange(-1, -1*(len(a)+1), -1):
            d_b = int(b[i]) if -1*i <= len(b) else 0
            d = int(a[i]) + d_b + more
            if d >= 2: # d can be 0/1/2/3 !!
                more = 1
                d -= 2  
            else:
                more = 0
            res.append(d)
        res_str = "" if more == 0 else "1"
        for d in reversed(res):
            res_str += str(d)
        return res_str
        