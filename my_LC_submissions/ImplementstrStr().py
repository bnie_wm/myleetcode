"""
KMP
Time: O(n+k)
Space: O(k)  # k: len(needle)
"""
class Solution(object):
    def strStr(self, haystack, needle):
        """
        :type haystack: str
        :type needle: str
        :rtype: int
        """
        if needle == "":
            return 0

        next = self.build_next(needle)

        # ## KMP
        i, j = 0, 0  # i --> haystack, j --> needle
        while i < len(haystack) and j < len(needle):   # two contraints
            if haystack[i] == needle[j]:
                i += 1
                j += 1
            else:
                if j == 0:   # if first char is not math
                    i += 1
                else:
                    j = next[j-1] + 1   # important next[j-1] !!!

        return i - len(needle) if j == len(needle) else -1
        

    def build_next(self, needle):
        next = [-1] * len(needle)
        i, j = 0, 1  # i --> needle, j --> next
        shift = -1
        while j < len(needle):
            if needle[j] == needle[i]:
                shift += 1
                next[j] = shift
                i += 1
                j += 1
            else:  # if not math
                if i == 0:  # if back to the first char, move to next j
                    j += 1
                else:
                    i = 0  # back to first char, re-match for current j
                    shift = -1
        return next
    


# """
# Time: O(n*k)
# Space: O(k)  # k: len(needle)
# """
# class Solution(object):
#     def strStr(self, haystack, needle):
#         """
#         :type haystack: str
#         :type needle: str
#         :rtype: int
#         """
#         if needle == "":
#             return 0
        
#         for i in xrange(len(haystack)):
#             if haystack[i:i+len(needle)] == needle:
#                 return i
#         return -1