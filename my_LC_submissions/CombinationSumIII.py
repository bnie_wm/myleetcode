class Solution(object):
    def combinationSum3(self, k, n):
        """
        :type k: int
        :type n: int
        :rtype: List[List[int]]
        """
        results = []
        self.combinationSumHelper(results, [], k, n, 1)
        return results
        
    
    def combinationSumHelper(self, results, res, remain_cnt, target, cur_num):
        if remain_cnt == 0:
            if target == 0:
                results.append(res)
            return
        
        for num in xrange(cur_num, 10):
            if target - num >= 0:
                self.combinationSumHelper(results, res + [num], remain_cnt - 1, target - num, num + 1)