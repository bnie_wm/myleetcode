class Solution(object):
    def permute(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        
        # Time: O(n * n!)
        # Space: O(n)
        results = []
        used = [False] * len(nums)
        self.permuteHelper(results, [], used, nums)
        return results
        
    def permuteHelper(self, results, res, used, nums):
        if len(res) == len(nums):  # must be len(res) == len(nums), cannot be cur_idx == len(nums) (i.e., [3,2,1]
            results.append(res)
            return
    
        for i in xrange(len(nums)):
            if not used[i]:
                used[i] = True
                self.permuteHelper(results, res + [nums[i]], used, nums)
                used[i] = False
        