public class Solution {
    /**
     * careercup 11.6
     * Do binary search on every row to fine the element.
     * O(MlogN) for M*N matrix
     * */
    public boolean searchMatrix(int[][] matrix, int target) {
        // IMPORTANT: Please reset any member data you declared, as
        // the same Solution instance will be reused for each test case.
        int row = 0;
        int col = matrix[0].length - 1;
        while(row < matrix.length && col >= 0){
            if(target == matrix[row][col])
                return true;
            else if (target < matrix[row][col])
                col--;
            else
                row++;
        } 
        return false;
    }
}