class LRUCache(object):

    def __init__(self, capacity):
        """
        :type capacity: int
        """
        self._cache = collections.OrderedDict()
        self._capacity = capacity
        

    def get(self, key):
        """
        :rtype: int
        """
        if key in self._cache:
            val = self._cache[key]
            del self._cache[key]
            self._cache[key] = val
            return val
        else:
            return -1
        

    def set(self, key, value):
        """
        :type key: int
        :type value: int
        :rtype: nothing
        """
        if key in self._cache:
            del self._cache[key]
        elif len(self._cache) == self._capacity:
            self._cache.popitem(last=False)  # FIFO: pop the least used item
        self._cache[key] = value
        