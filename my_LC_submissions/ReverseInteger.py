public class Solution {
    public int reverse(int x) {
        // Note: The Solution object is instantiated only once and is reused by each test case.
		Integer xi = new Integer(x);
		String xs = xi.toString();
		boolean isNeg = false;
		if(xs.charAt(0)=='-') {
			isNeg = true;
			xs = xs.substring(1);
		}
		
		char[] str = xs.toCharArray();
		int len = str.length;
		for(int i=0;i<(len/2);i++){
			char temp = str[i];
			str[i] = str[len-1-i];
			str[len-1-i] = temp;
		}
		
		String new_xs = new String(str); //convert char array str to String, don't use str.toString()!!!

		int new_int = 0;
		try{
			new_int = Integer.parseInt(new_xs);
		}catch(java.lang.NumberFormatException ex){
			System.out.println("There is an overflow!");
			if(isNeg){
				return Integer.MIN_VALUE;
			}else{
				return Integer.MAX_VALUE;
			}
		}
		
		if(isNeg)
			new_int = -1 * new_int;

		return new_int; 
    }
}