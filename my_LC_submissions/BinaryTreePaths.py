# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

"""
DFS +  Backtracking
Time: O(n * h)
Space: O(h)  ???

"""
class Solution:
    # @param {TreeNode} root
    # @return {string[]}
    def binaryTreePaths(self, root):
        if not root:
            return []
        results = []
        self.binaryTreePathsHelper(results, "", root)
        return results
    
    
    def binaryTreePathsHelper(self, results, cur, root):
        if root.left is None and root.right is None:  # leaf node
            cur += str(root.val)
            results.append(cur)
            return
        if root.left:
            self.binaryTreePathsHelper(results, cur + str(root.val) + "->", root.left)
        if root.right:
            self.binaryTreePathsHelper(results, cur + str(root.val) + "->", root.right)
        
        