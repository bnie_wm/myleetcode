# The read4 API is already defined for you.
# @param buf, a list of characters
# @return an integer
# def read4(buf):

class Solution(object):
    def __init__(self):
        self.__buffer = [''] * 4  # create a buffer of size 4
        self.__num_used = 4 # [0, 4], how many chars in __buffer are used
        self.__num_read = 4  # [0, 4], how many chars are read in last read4
        
    def read(self, buf, n):
        """
        :type buf: Destination buffer (List[str])
        :type n: Maximum number of characters to read (int)
        :rtype: The number of characters read (int)
        """
        i = 0
        while i < n:
            # if self.__num_used == 4:  --> wrong, if last last num_read < 4
            if self.__num_used == self.__num_read:  # if used up all read chars, read new chars
                self.__num_used = 0
                self.__num_read = read4(self.__buffer)
                if self.__num_read == 0:  # no more char need to be read
                    break
            else:  # append char from chars read in self.__buffer
                # buf.append(self.__buffer[self.__num_used])  -- wrong
                buf[i] = self.__buffer[self.__num_used]
                self.__num_used += 1
                i += 1
                
        return i
