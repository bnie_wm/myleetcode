class BIT(object):
    def __init__(self, n):
        self.__bit = [0] * (n+1)

    def __lsb(self, i):
        return i & -i

    def query(self, i):
        s = 0
        while i > 0:
            s += self.__bit[i]
            i -= self.__lsb(i)
        return s

    def update(self, i, dif):
        while i < len(self.__bit):
            self.__bit[i] += dif
            i += self.__lsb(i)

"""
Time: O(nlogn)
Space: O(n)
"""
class Solution(object):
    def countSmaller(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        # order: key:num, val: num's order in sorted nums w/o duplicates
        order = {}
        for i, v in enumerate(sorted(set(nums))):
            order[v] = i
        # positions[i] = nums[i]'s order in sorted nums w/o duplicates
        positions = [order[x] for x in nums]

        bit = BIT(len(nums))
        res = []
        for pos in reversed(positions):
            # BIT index starts from 1
            # bit[i] = sum of [0, pos-1] --> # of nums < pos
            res.append(bit.query(pos))  
            bit.update(pos+1, 1)  # for nums >= pos, update count by 1
        return res[::-1]