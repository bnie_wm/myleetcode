# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def preorderTraversal(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        path, stack = [], [(root, False)]
        while stack:
            node, visited = stack.pop()
            if node is None:
                continue
            if visited:
                path.append(node.val)
            else:
                stack.append((node.right, False))
                stack.append((node.left, False))
                stack.append((node, True))
        return path
        
        
        # path, stack = [], []
        # cur = root
        # while cur or stack:
        #     while cur:
        #         stack.append(cur)
        #         path.append(cur.val)
        #         cur = cur.left
        #     cur = stack.pop()
        #     cur = cur.right
        # return path