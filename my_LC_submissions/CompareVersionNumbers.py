"""
Time: O(n)
Space: O(1)
"""
class Solution(object):
    def compareVersion(self, version1, version2):
        """
        :type version1: str
        :type version2: str
        :rtype: int
        """
        v1, v2 = version1.split("."), version2.split(".")
        if len(v1) < len(v2):
            return self.compareVersion(version2, version1) * -1
        
        for i in xrange(len(v1)):
            p1 = int(v1[i])
            p2 = int(v2[i]) if i < len(v2) else 0  # "1.0" == "1"
            if p1 > p2:
                return 1
            elif p1 < p2:
                return -1
        return 0
                