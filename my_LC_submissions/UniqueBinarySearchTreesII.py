/**
 * Definition for binary tree
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; left = null; right = null; }
 * }
 */
public class Solution {
    public ArrayList<TreeNode> generateTrees(int n) {
        return generateTrees(1, n);
    }
    
    public ArrayList<TreeNode> generateTrees(int start, int end) {
        ArrayList<TreeNode> result = new ArrayList<TreeNode>();
        if(start > end) {
            result.add(null);
            return result;
        }
        if(start == end) {
            result.add(new TreeNode(start));
            return result;
        }
        // use three-level loop to realize the multiply of left tree and right tree
        for(int i = start; i <= end; i++) {
            for(TreeNode left: generateTrees(start, i-1)) {
                for(TreeNode right: generateTrees(i+1, end)) {
                    TreeNode root = new TreeNode(i);
                    root.left = left;
                    root.right = right;
                    result.add(root);
                }
            }
        }
        return result;
    }
}