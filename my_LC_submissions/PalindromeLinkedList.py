# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

"""
Time: O(n)
Space: O(1)
"""
class Solution(object):
    def isPalindrome(self, head):
        """
        :type head: ListNode
        :rtype: bool
        """
        if not head or not head.next:
            return True
        
        prev, cur, fast = None, head, head
        # reverse first half
        while fast and fast.next:  # if fast is None: num_of_nodes=even; if fast.next is None: num_of_nodes=odd
            fast = fast.next.next
            next = cur.next
            cur.next = prev
            prev, cur = cur, next
        
        # prev is the header of left half
        # cur is the header of right half
        if fast:  # handle odd number of nodes, skip the middle node
            cur = cur.next
        
        while cur:
            if prev.val != cur.val:
                return False
            prev = prev.next
            cur = cur.next
        return True
            