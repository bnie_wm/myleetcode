class Solution(object):
    def singleNumber(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        
        # Time: O(n)
        # Space: O(1)
        
        
        # idea: XOR among the array pair-wisely, the result would be the only single number
        # reason: x XOR x = 0; 0 XOR x = x
        
        # method1:
        result = nums[0]
        for num in nums[1:]:
            result ^= num
        return result
        
        # method2:
        # return reduce(operator.xor, nums);
            
        