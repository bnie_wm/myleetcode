"""
DP
Time/Space: O(m*n)
"""
class Solution(object):
    def maxKilledEnemies(self, grid):
        """
        :type grid: List[List[str]]
        :rtype: int
        """
        if not grid:
            return 0
        
        m, n = len(grid), len(grid[0])
        
        # dp: keep two matrices: down and right
        # record number of enemies to the right and to the bottom, if place bomb in (i, j)
        # O(m*n)
        down = [[0 for _ in xrange(n)] for _ in xrange(m)]
        right = [[0 for _ in xrange(n)] for _ in xrange(m)]
        for i in xrange(m-1, -1, -1):
            for j in xrange(n-1, -1, -1):
                if grid[i][j] != "W":
                    if i + 1 < m:
                        down[i][j] += down[i + 1][j]
                    if j + 1 < n:
                        right[i][j] += right[i][j + 1]
                    if grid[i][j] == 'E':
                        down[i][j] += 1
                        right[i][j] += 1
        
        res = 0    
        up = [0 for _ in xrange(n)]
        for i in xrange(m):
            left = 0
            for j in xrange(n):
                if grid[i][j] == 'W':
                    up[j], left = 0, 0
                elif grid[i][j] == 'E':
                    up[j] += 1
                    left += 1
                else:  # place for bomb
                    res = max(res, left + up[j] + right[i][j] + down[i][j])
        return res
                    
        