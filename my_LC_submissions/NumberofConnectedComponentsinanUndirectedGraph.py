class UnionFind(object):
    def __init__(self, n):
        self.parent = range(n)  # initialize parent array with -1 (all singletons)
        self.count = n  # initial number of forests = n
    
    # find with path compression
    def find(self, x):
        if self.parent[x] != x:  # this node itself is not the root
            # recursively find the root of x
            self.parent[x] = self.find(self.parent[x])  # reset parent[x] as the tree root (instead of its current parent)
        return self.parent[x]
    
    # since we find with path compression
    # union all nodes can be done in O(nlog*n) ~= O(n)
    def union(self, x, y):
        root_x, root_y = self.find(x), self.find(y)
        if root_x != root_y:
            self.parent[min(root_x, root_y)] = max(root_x, root_y)  # set one as the other's child
            self.count -= 1


class Solution(object):
    def countComponents(self, n, edges):
        """
        :type n: int
        :type edges: List[List[int]]
        :rtype: int
        """
        
        uf = UnionFind(n)
        for x, y in edges:
            uf.union(x, y)
        return uf.count