# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None


"""
Level order traversal Binary Tree + hashtable maintain column index
Time: O(n)
Space: (n)
"""
class Solution(object):
    def verticalOrder(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        
        if not root:
            return []
        
        cur = [(root, 0)]
        cols = dict() # col idx --> node list
        while cur:
            next = []
            for (node, col) in cur:
                if col not in cols:
                    cols[col] = []
                cols[col].append(node.val)
                if node.left:
                    next.append((node.left, col-1))
                if node.right:
                    next.append((node.right, col+1))
                cur = next
        
        results = []
        for col in sorted(cols):
            results.append(cols[col])
        return results
            
                