class Solution(object):
    def romanToInt(self, s):
        """
        :type s: str
        :rtype: int
        """
        # i.e., IIVX
        lookup = {"I": 1, "V": 5, "X": 10, "L": 50, "C":100, "D": 500, "M": 1000}
        num = lookup[s[-1]]
        for i in reversed(xrange(len(s)-1)):
            if lookup[s[i]] >= lookup[s[i+1]]:   # >=, instead of >
                num += lookup[s[i]]
            else:
                num -= lookup[s[i]]
        return num
