"""
Backtracking
Time: O(n*4^n)
Space: O(n)
"""
class Solution(object):
    def letterCombinations(self, digits):
        """
        :type digits: str
        :rtype: List[str]
        """
        if digits == "":
            return []
            
        lookup = ["", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"]
        results = []
        self.letterCombinationsHelper(results, [], list(digits), lookup, 0)
        return results
        
    def letterCombinationsHelper(self, results, res, digits, lookup, cur_idx):
        if cur_idx == len(digits):
            results.append("".join(res))
            return results
        
        for letter in lookup[int(digits[cur_idx])]:
            self.letterCombinationsHelper(results, res + [letter], digits, lookup, cur_idx + 1)
        