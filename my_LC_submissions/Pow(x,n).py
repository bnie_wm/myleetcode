class Solution(object):
    def myPow(self, x, n):
        """
        :type x: float
        :type n: int
        :rtype: float
        """
        if n < 0:
            return 1. / self.myPow(x, -n)
        elif n == 0:
            return 1
        elif n == 1:
            return x
        elif n == 2:
            return x * x
        else:
            y = self.myPow(x, n/2)
            if n%2 == 0:
                return y*y
            else:
                return y*y*x
                