class Solution(object):
    def reverseWords(self, s):
        """
        :type s: str
        :rtype: str
        """
        words = s.strip().split()
        
        i, j = 0, len(words)-1
        while i < j:
            words[i], words[j] = words[j], words[i]
            i += 1
            j -= 1
        return " ".join(words)