class ZigzagIterator(object):

    def __init__(self, v1, v2):
        """
        Initialize your data structure here.
        :type v1: List[int]
        :type v2: List[int]
        """
        self.__lists = [v1, v2]
        self.__k = len(self.__lists)
        self.__idx = [0] * self.__k
        self.__size = [len(v1), len(v2)]
        self.__visit = -1  # last visited list idx
        

    def next(self):
        """
        :rtype: int
        """
        while True:
            self.__visit += 1
            if self.__visit == self.__k:
                self.__visit = 0
            if self.__idx[self.__visit] < self.__size[self.__visit]: # there are elements in this list
                val = self.__lists[self.__visit][self.__idx[self.__visit]]
                self.__idx[self.__visit] += 1
                return val
            

    def hasNext(self):
        """
        :rtype: bool
        """
        has_next = False
        for i in xrange(len(self.__idx)):
            if self.__idx[i] < self.__size[i]:
                has_next = True
                break
        return has_next
                
        

# Your ZigzagIterator object will be instantiated and called as such:
# i, v = ZigzagIterator(v1, v2), []
# while i.hasNext(): v.append(i.next())