class Solution(object):
    def longestPalindrome(self, s):
        """
        :type s: str
        :rtype: int
        """
        chars = dict()
        for c in s:
            if c not in chars:
                chars[c] = 0
            chars[c] += 1
        
        maxLen = 0
        one_odd = False
        for c in chars:
            cnt = chars[c]
            if cnt % 2 == 0:
                maxLen += cnt
            else:
                if not one_odd:
                    maxLen += cnt
                    one_odd = True
                else:
                    maxLen += cnt - 1
        return maxLen
            
            
        
            