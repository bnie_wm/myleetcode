class Solution(object):
    def findLadders(self, beginWord, endWord, wordlist):
        """
        :type beginWord: str
        :type endWord: str
        :type wordlist: Set[str]
        :rtype: List[List[int]]
        """
        ## it is likely that worList doesn't contain beginWord and endWord
        wordlist.add(endWord)
        
        letters = 'abcdefghijklmnopqrstuvwxyz'
        cur = set([beginWord])    # key1: queue cur/next save as set, instead of [] (remove duplicates!!!)
        visited = set([])
        traces = {word : [] for word in wordlist}  # key3: use traces dict to save word -> next word
        found = False  # if find shortest path
        depth = 1
        
        while cur and not found:
            for curWord in cur:
                visited.add(curWord)   # key2: add visited words level by level, otherwise, will only save one path to the destination
                
            depth += 1
            next = set()  # key1: queue cur/next save as set, instead of [] (remove duplicates!!!)
            for curWord in cur:
                for i in xrange(len(curWord)):
                    for c in letters:
                        if curWord[i] != c:
                            newWord = curWord[:i] + c + curWord[i+1:]
                            if newWord == endWord:
                                found = True
                            if newWord not in visited and newWord in wordlist:
                                next.add(newWord)
                                # visited.add(newWord)   # cannot add here, otherwise, will only save one path to the destination
                                traces[curWord].append(newWord)
                                # traces[newWord].append(curWord)
            cur = next
            
        if not found:
            return []
        
        paths = []
        self.getPaths(paths, [beginWord], endWord, traces)
        return paths
    
    
    def getPaths(self, paths, path, endWord, traces):
        if path[-1] == endWord:
            paths.append(path)
            return
        
        for nextWord in traces[path[-1]]:
            self.getPaths(paths, path + [nextWord], endWord, traces)
            
        
        
        
        
# class Solution(object):
#     def findLadders(self, beginWord, endWord, wordlist):
#         """
#         :type beginWord: str
#         :type endWord: str
#         :type wordlist: Set[str]
#         :rtype: List[List[int]]
#         """
#         ## it is likely that worList doesn't contain beginWord and endWord
#         wordlist.add(endWord)
#         results = dict()  # len --> path list
#         self.ladderLengthHelper(results, [beginWord], beginWord, endWord, wordlist)
#         if results:
#             minLen = min(results.keys())
#             return results[minLen]
#         else:
#             return []  # no path found
            
#     def ladderLengthHelper(self, results, res, curWord, endWord, wordList):
#         if curWord == endWord:
#             if not results:
#                 results[len(res)] = [res]
#             else:
#                 minLen = min(results.keys())
#                 if len(res) < minLen:
#                     results[len(res)] = [res]
#                     del results[minLen]
#                 elif len(res) == minLen:
#                     results[minLen].append(res)
#             return
        
#         letters = 'abcdefghijklmnopqrstuvwxyz'
#         for i in xrange(len(curWord)):
#             for c in letters:
#                 if c != curWord[i]:
#                     newWord = curWord[:i] + c + curWord[i+1:]
#                     if newWord in wordList and newWord not in res:
#                         self.ladderLengthHelper(results, res + [newWord], newWord, endWord, wordList)

