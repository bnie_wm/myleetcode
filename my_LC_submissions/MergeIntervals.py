# Definition for an interval.
# class Interval(object):
#     def __init__(self, s=0, e=0):
#         self.start = s
#         self.end = e

"""
Time: O(nlogn)
Space: O(n)
"""
class Solution(object):
    def merge(self, intervals):
        """
        :type intervals: List[Interval]
        :rtype: List[Interval]
        """
        if not intervals:
            return []
        
        intervals.sort(key=lambda x: x.start)  # O(nlogn)
        res = []
        for interval in intervals:
            if not res or interval.start > res[-1][1]:
                res.append([interval.start, interval.end])
            elif interval.end > res[-1][1]:  # interval.start <= res[-1][1] and interval.end > res[-1][1]
                res[-1][1] = interval.end
        return res
                