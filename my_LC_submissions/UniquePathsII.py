class Solution(object):
    def uniquePathsWithObstacles(self, obstacleGrid):
        """
        :type obstacleGrid: List[List[int]]
        :rtype: int
        """
        m, n = len(obstacleGrid), len(obstacleGrid[0])
        
        # init first row
        # in first row, after encounter the first obstacle, all num[i] should be 0!
        num = [0] * n
        obstacle = False
        for j in xrange(n):
            if obstacleGrid[0][j] == 1:
                obstacle = True
            num[j] = 1 if not obstacle else 0
        
        # DP
        for i in xrange(1, m):
            # init first column (cannot always assume 1)
            if obstacleGrid[i][0] == 1:
                num[0] = 0
            for j in xrange(1, n):
                num[j] = num[j] + num[j-1] if obstacleGrid[i][j] == 0 else 0
        return num[n-1]
        