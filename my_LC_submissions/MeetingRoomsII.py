# Definition for an interval.
# class Interval(object):
#     def __init__(self, s=0, e=0):
#         self.start = s
#         self.end = e

"""
Time: O(nlogn)
Space: O(n)
"""
class Solution(object):
    def minMeetingRooms(self, intervals):
        """
        :type intervals: List[Interval]
        :rtype: int
        """
        if not intervals:
            return 0
            
        intervals.sort(key=lambda x:x.start)  # sort by start, O(nlogn)
        meeting_rooms = []  # Priority Queue, meeting_rooms[0] --> the one with earliest end time
        for interval in intervals:
            if not meeting_rooms or interval.start < meeting_rooms[0]:  # need a new room
                heapq.heappush(meeting_rooms, interval.end)  # O(1)
            else:  # can use the room that finishes first
                # pop the earliest end room, push new end time
                heapq.heapreplace(meeting_rooms, interval.end)  # O(logn)
        return len(meeting_rooms)
                
                
        
            
            