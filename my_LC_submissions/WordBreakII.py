class Solution(object):
    def wordBreak(self, s, wordDict):
        """
        :type s: str
        :type wordDict: Set[str]
        :rtype: List[str]
        """
        # step1: word break 1 -- DP
        # check if we can break the word
        minL, maxL = float("inf"), 0
        for word in wordDict:
            minL = min(minL, len(word))
            maxL = max(maxL, len(word))
        
        canbreak = [False] * (len(s) + 1)   # --> s[:i] can break into words
        canbreak[0] = True  # dummy
        for i in xrange(1, len(s) + 1):  # iteratively check canbreak
            if i < minL:    # prune
                canbreak[i] = False
            for k in xrange(min(i, minL), min(i, maxL) + 1): # k: possible word length (minL<=k<=maxL)
                if canbreak[i-k] and s[i-k:i] in wordDict:
                    canbreak[i] = True
                    break
                
        # backtracking        
        results = []
        if canbreak[-1]:
            self.wordBreakHelper(results, [], s, wordDict, 0)
        return results
            
    
    def wordBreakHelper(self, results, res, s, wordDict, cur_idx):
        if cur_idx == len(s):
            results.append(" ".join(res))
            return
        
        for i in xrange(cur_idx, len(s)):
            if s[cur_idx:i+1] in wordDict:
                self.wordBreakHelper(results, res + [s[cur_idx:i+1]], s, wordDict, i+1)
  
        
        