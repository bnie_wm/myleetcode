"""
BFS
Time: O(m*n)
Space: O(m+n)
"""
class Solution(object):
    def solve(self, board):
        """
        :type board: List[List[str]]
        :rtype: void Do not return anything, modify board in-place instead.
        """
        if not board:
            return
        
        m, n = len(board), len(board[0])
        q = collections.deque()
        # unsurrounded region must have one 'O' in the boader!
        for i in xrange(m):
            if board[i][0] == 'O':
                q.append((i, 0))
            if board[i][n-1] == 'O':
                q.append((i, n-1))
        for j in xrange(n):
            if board[0][j] == 'O':
                q.append((0, j))
            if board[m-1][j] == 'O':
                q.append((m-1, j))
        
        # BFS
        dirs = [(-1, 0), (1, 0), (0, 1), (0, -1)]
        while q:
            i, j = q.popleft()
            board[i][j] = 'M'
            for d in dirs:
                x, y = i + d[0], j + d[1]
                if 0 <= x < m and 0 <= y < n and board[x][y] == 'O':
                    q.append((x, y))
        
        changes = {'O':'X', 'M':'O'}
        for i in xrange(m):
            for j in xrange(n):
                if board[i][j] in changes:
                    board[i][j] = changes[board[i][j]]
        
                