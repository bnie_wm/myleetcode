"""
Time: O(n^2)
Space: O(1)
"""
class Solution(object):
    def longestPalindrome(self, s):
        """
        :type s: str
        :rtype: str
        """
    	if len(s) <= 1:
    		return s
    	
    	maxLen = 0
    	mid_idx = 0
    	# if len of palindrome is odd
        for i in xrange(1, len(s)):
            for l in xrange(1, len(s)/2+1):
                if i-l >= 0 and i+l < len(s) and s[i-l] == s[i+l]:
                    if l > maxLen:
                        maxLen = l
                        mid_idx = i
                else:
                	break
        res = s[mid_idx-maxLen : mid_idx+maxLen+1]

    	# if len of palindrome is even
        updated = False
        for i in xrange(1, len(s)):
        	for l in xrange(0, len(s)/2+1):
        		if i-1-l >= 0 and i+l < len(s) and s[i-1-l] == s[i+l]:
        			if l >= maxLen:  # l >= maxLen: since equal maxLen, even is longer than odd!
        				maxLen = l
        				mid_idx = i
        				updated = True
        		else:
        			break
        if updated:
        	res = s[mid_idx-1-maxLen : mid_idx+maxLen+1]
        			
    	return res
            
            