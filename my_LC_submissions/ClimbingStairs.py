"""
DP
Time:O(n)
Space: O(1)
"""
class Solution(object):
    def climbStairs(self, n):
        """
        :type n: int
        :rtype: int
        """
        prev, cur = 0, 1
        for i in xrange(1, n+1):
            prev, cur = cur, prev + cur
        return cur
        