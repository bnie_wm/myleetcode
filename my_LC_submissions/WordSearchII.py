class TrieNode(object):   ## only have TrieNode, no Trie (so no root)
    def __init__(self):
        self.is_word = False  # indicate whether there is one word ends in this node
        self.children = {}  # dict: char --> TrieNode()
    
    def insert(self, word):
        cur = self  # not self.root
        for c in word:
            if c not in cur.children:
                cur.children[c] = TrieNode()
            cur = cur.children[c]
        cur.is_word = True
    
    
class Solution(object):
    def findWords(self, board, words):
        """
        :type board: List[List[str]]
        :type words: List[str]
        :rtype: List[str]
        """
        # build Trie O(l*h)
        # trie = Trie()
        trie = TrieNode()
        for word in words:
            trie.insert(word)

        visited = [[False for _ in xrange(len(board[0]))] for _ in xrange(len(board))]
        results = []
        for i in xrange(len(board)):
            for j in xrange(len(board[0])):
                cur_c = board[i][j]
                if cur_c in trie.children:  # only visit those chars who are the first letter of words in <words>
                    visited[i][j] = True
                    self.searchWordsinTrie(results, board, trie.children[cur_c], [cur_c], i, j, visited)
                    visited[i][j] = False
        return results

    def searchWordsinTrie(self, results, board, trie, cur_word, i, j, visited):
        if trie.is_word:  # indicates this is a word in <words>
            w = "".join(cur_word)
            if w not in results:   # remove duplicates
                results.append(w)
            # return   # key: don't reture here, in case w is a prefix of other word in words
        
        # to next char in board
        dirs = [(0, 1), (0, -1), (1, 0), (-1, 0)]
        for d in dirs:
            x, y = i + d[0], j + d[1]
            if 0 <= x < len(board) and 0 <= y < len(board[0]) \
                    and board[x][y] in trie.children and not visited[x][y]:
                visited[i][j] = True
                self.searchWordsinTrie(results, board, trie.children[board[x][y]], cur_word + [board[x][y]], x, y, visited)
                visited[i][j] = False

        