class Solution(object):
    def containsDuplicate(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        existed = set()
        for n in nums:
            if n not in existed:
                existed.add(n)
            else:
                return True
        return False