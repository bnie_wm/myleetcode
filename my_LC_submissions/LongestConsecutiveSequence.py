class Solution(object):
    def longestConsecutive(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        
        maxLen = 0
        d = {key : 0 for key in nums}  # num --> it's maximum consecutive substring len
        for x in nums:
            if d[x] == 0:  # skip duplicates
                d[x] = 1  # at least, len = 1
                left = d[x-1] if x-1 in d else 0   # get it's left's max len
                right = d[x+1] if x+1 in d else 0  # get it's right's max len
                curLen = left + right + 1
                maxLen = max(curLen, maxLen)
                d[x-left], d[x+right] = curLen, curLen  # update boundary vals' curLen
        return maxLen
                