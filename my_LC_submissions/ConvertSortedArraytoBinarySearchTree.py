/**
 * Definition for binary tree
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    /**
     * careercup 4.3
     * O(n)
     **/
    public TreeNode sortedArrayToBST(int[] num) {
        // IMPORTANT: Please reset any member data you declared, as
        // the same Solution instance will be reused for each test case.
        return createMinimalBST(num, 0, num.length-1);
        
    }
    private TreeNode createMinimalBST(int[] num, int left, int right){
        if(left > right) return null;
        int mid = left + (right-left)/2;
        TreeNode leftChild = createMinimalBST(num, left, mid-1);
        TreeNode root = new TreeNode(num[mid]);
        root.left = leftChild;
        root.right = createMinimalBST(num, mid+1, right);
        return root;
    }
}