public class Solution {
    public int[] searchRange(int[] A, int target) {
        int[] indices = {-1, -1};
        if(A == null || A.length == 0) return indices;
        int left = 0, right = A.length-1;
        // find left index
        while(left <= right) {
            int mid = left + (right-left)/2;
            if(A[mid] < target) {
                left = mid + 1;
            }else {
                right = mid - 1;
            }
        }
        if(left < 0 || left >= A.length || A[left] != target) return indices;
        indices[0] = left;
        
        // find right index
        right = A.length - 1;
        while(left <= right) {
            int mid = left + (right-left)/2;
            if(A[mid] > target) {
                right = mid - 1;
            }else {
                left = mid + 1;
            }
        }
        indices[1] = right;
        
        return indices;
        
    }
}