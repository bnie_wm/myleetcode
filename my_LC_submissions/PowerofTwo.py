class Solution(object):
    def isPowerOfTwo(self, n):
        """
        :type n: int
        :rtype: bool
        """
        if n <= 0:
            return False
        
        # power of two: it's binary only contains one 1, all other bits are 0!
        # method1: Time O(logn)=O(32), Space O(1)
        # num_ones = 0
        # for i in range(32):
        #     if num_ones > 1:
        #         return False
        #     if n&1 == 1:
        #         num_ones += 1
        #     n >>= 1
        # return True
        
        # method2: Time/Space O(1)
        return n & (n-1) == 0
        