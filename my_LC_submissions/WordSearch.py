"""
DFS
Time: O(m * n * l)
Space: (k)
"""
class Solution(object):
    def exist(self, board, word):
        """
        :type board: List[List[str]]
        :type word: str
        :rtype: bool
        """
        if word == "":
            return True
        if board is None:
            return False
        
        ### DFS + Backtracking 
        visited = [[False for _ in xrange(len(board[0]))] for _ in xrange(len(board))]
        ## since word may start from multiple positions, we need this for loops
        for i in xrange(len(board)):
            for j in xrange(len(board[0])):
                if word[0] == board[i][j]:
                    visited[i][j] = True
                    if self.searchWrod(board, word, i, j, 0, visited):
                        return True
                    visited[i][j] = False
        return False
        
    
    ## backtracking
    def searchWrod(self, board, word, i, j, cur_wid, visited):
        if cur_wid + 1 == len(word):
            return True
        
        dirs = [(0, 1), (0, -1), (1, 0), (-1, 0)]
        found = None
        for d in dirs:
            x, y = i + d[0], j + d[1]
            if 0 <= x < len(board) and 0 <= y < len(board[0]) and word[cur_wid+1] == board[x][y] and not visited[x][y]:
                visited[i][j] = True   # important! mark visited here
                found = found or self.searchWrod(board, word, x, y, cur_wid+1, visited)
                visited[i][j] = False
        
        return found

        
   