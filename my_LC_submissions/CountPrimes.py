"""
DP
Time: O(n)
Space: O(n)
Prime number cannot be divided by intergers smaller than it.
Build a boolean array, start from 2, remove numbers that can be divided by it
"""
class Solution(object):
    def countPrimes(self, n):
        """
        :type n: int
        :rtype: int
        """
        if n < 2:
            return 0
            
        primes = [1] * n
        primes[0], primes[1] = 0,  0 # 0 and 1 are not primes
        
        i = 2
        while i*i < n:
            if primes[i] == 1:
                for j in xrange(i+i, n, i):  # j starts from i+i, step is i
                    primes[j] = 0
            i += 1
        return sum(primes)