import numpy as np


class Solution(object):
    def wiggleSort(self, nums):
        """
        :type nums: List[int]
        :rtype: void Do not return anything, modify nums in-place instead.
        """
        if len(nums) <= 1:
            return
        
        # Solution 1 -- Time: O(nlogn), Space: O(logn)
        # sorted_nums = sorted(nums)
        # for i in xrange(1, len(nums), 2):  # fill in odd positions, with numbers after median
        #     nums[i] = sorted_nums.pop()
        # for i in xrange(0, len(nums), 2):  # fill in even positions, with number small or equal to median
        #     nums[i] = sorted_nums.pop()
        
        # Solution 2 -- Tri partition: Time: O(n), Space: O(1)
        med_idx = np.argsort(nums)[len(nums)//2]
        med = nums[med_idx]
        # small: idx after sorted small values
        # large: idx before sorted large values
        i, small, large = 0, 0, len(nums)-1
        while i <= large:
            if nums[self.idx(i, len(nums))] < med:
                self.swap(nums, i, large)
                large -= 1
            elif nums[self.idx(i, len(nums))] > med:
                self.swap(nums, i, small)
                small += 1
                i += 1
            else:
                i += 1

    def idx(self, i, n):
        mid = n/2
        if i < mid:
            return 2*i+1
        else:
            return (i-mid) * 2

    def swap(self, nums, i, j):
        new_i, new_j = self.idx(i, len(nums)), self.idx(j, len(nums))
        nums[new_i], nums[new_j] = nums[new_j], nums[new_i]


        
        