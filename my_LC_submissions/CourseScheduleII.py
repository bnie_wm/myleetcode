class Solution(object):
    def findOrder(self, numCourses, prerequisites):
        """
        :type numCourses: int
        :type prerequisites: List[List[int]]
        :rtype: List[int]
        """
        indegrees = [0] * numCourses
        s = collections.deque()
        res = []
        
        # build edge_list
        edge_list = dict()
        for m, n in prerequisites:
            if m not in edge_list:
                edge_list[m] = []
            edge_list[m].append(n)
            indegrees[n] += 1
        
        for nid in xrange(numCourses):
            if indegrees[nid] == 0:
                s.append(nid)
        
        while s:
            m = s.popleft()
            res.append(m)
            if m in edge_list:
                for n in edge_list[m]:
                    print m, n
                    prerequisites.remove([m, n])
                    indegrees[n] -= 1
                    if indegrees[n] == 0:
                        s.append(n)
        
        if len(prerequisites) == 0:
            return res[::-1]
        else:
            return []
            