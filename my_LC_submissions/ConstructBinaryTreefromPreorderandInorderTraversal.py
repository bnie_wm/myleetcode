# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def buildTree(self, preorder, inorder):
        """
        :type preorder: List[int]
        :type inorder: List[int]
        :rtype: TreeNode
        """
        lookup = {}
        for i in xrange(len(inorder)):
            lookup[inorder[i]] = i
        return self.build(lookup, preorder, inorder, 0, 0, len(inorder))
        
    def build(self, lookup, preorder, inorder, pre_start, in_start, in_end):
        if in_start == in_end:
            return None
        
        node = TreeNode(preorder[pre_start])
        idx = lookup[node.val]
        node.left = self.build(lookup, preorder, inorder, pre_start + 1, in_start, idx)
        node.right = self.build(lookup, preorder, inorder, pre_start + 1 + (idx - in_start), idx + 1, in_end)
        return node
        