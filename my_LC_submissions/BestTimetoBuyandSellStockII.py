"""
Greedy
Time: O(n)
Space: O(1)
"""

class Solution(object):
    def maxProfit(self, prices):
        """
        :type prices: List[int]
        :rtype: int
        """
        profit = 0
        for i in xrange(1, len(prices)):
            diff = prices[i] - prices[i-1]
            if diff > 0:
                # as long as today's price larger than yesterday's, add diff
                # so everyday, you can first sell yesterday's stock and then buy it
                profit += diff
        return profit
                