class Solution(object):
    def permuteUnique(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        results = []
        used = [False] * len(nums)
        self.permuteHelper(results, [], used, sorted(nums))
        return results
        
    def permuteHelper(self, results, res, used, nums):
        if len(res) == len(nums):  # must be len(res) == len(nums), cannot be cur_idx == len(nums) (i.e., [3,2,1]
            results.append(res)
            return
        
        prev = None
        for i in xrange(len(nums)):
            if nums[i] != prev and not used[i]:   # nums[i] != prev  --> remove duplicates!
                used[i] = True
                self.permuteHelper(results, res + [nums[i]], used, nums)
                used[i] = False
                prev = nums[i]