"""
Time: O(n)
Space: O(1)
"""
class Solution(object):
    def isPalindrome(self, s):
        """
        :type s: str
        :rtype: bool
        """
        if len(s) <= 1:
            return True
        
        s = s.lower()
        i, j = 0, len(s) - 1
        while i <= j:
            while i < j and not s[i].isalnum():  # need condition i<j!!!
                i += 1
            while i < j and not s[j].isalnum():  # need condition i<j!!!
                j -= 1
            if s[i] != s[j]:
                return False
            i += 1
            j -= 1
        return True