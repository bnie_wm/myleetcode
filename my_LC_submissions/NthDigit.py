class Solution(object):
    def findNthDigit(self, n):
        """
        :type n: int
        :rtype: int
        """
        # i -> number of digits
        i, baseIdx, lastIdx = 1, 1, 1
        while True:
            lastIdx = baseIdx + (10 ** i - 1 - 10 ** (i - 1)) * i + (i-1)
            if lastIdx >= n:
                break
            baseIdx = lastIdx + 1
            i += 1
        baseNum = 10 ** (i - 1)
        num = (n - baseIdx) / i + baseNum
        return int(str(num)[(n - baseIdx) % i])