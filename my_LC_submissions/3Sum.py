"""
Two pointers
Time: O(n^2)
Space: O(1)
"""
class Solution(object):
    def threeSum(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        nums = sorted(nums)  # O(nlogn)
        results = set()
        for i in xrange(len(nums)-2):
            if i > 0 and nums[i] == nums[i-1]:
                continue
            
            m, n = i + 1, len(nums) - 1
            while m < n:
                this_sum = nums[i] + nums[m] + nums[n]
                if this_sum == 0:
                    results.add((nums[i], nums[m], nums[n]))
                    # handle duplicates
                    while m+1 < n and nums[m+1] == nums[m]:
                        m += 1
                    while m < n-1 and nums[n-1] == nums[n]:
                        n -= 1
                    m += 1
                    n -= 1
                elif this_sum > 0:
                    n -= 1
                else:
                    m += 1
        return list(results)
                        
                    
            