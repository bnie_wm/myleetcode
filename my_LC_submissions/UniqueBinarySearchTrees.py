public class Solution {
    // Recusion
    // public int numTrees(int n) {
    //     return numTrees(1, n);
    // }
    // public int numTrees(int start, int end) {
    //     if(start >= end) return 1;
    //     int sum = 0;
    //     for(int i = start; i <= end; i++) {
    //         sum += numTrees(start, i-1) * numTrees(i+1, end);
    //     }
    //     return sum;
    // }
    
    // DP
    public int numTrees(int n) {
        int[] dp = new int[n+1];
        dp[0] = 1;
        for(int i = 1; i <= n; i++) {
            for(int j = 0; j < i; j++){
                dp[i] += dp[j] * dp[i-1-j];
            }
        }
        return dp[n];
    }

}