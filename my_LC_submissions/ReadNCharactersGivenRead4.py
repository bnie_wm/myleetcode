# The read4 API is already defined for you.
# @param buf, a list of characters
# @return an integer
# def read4(buf):

class Solution(object):
    def read(self, buf, n):
        """
        :type buf: Destination buffer (List[str])
        :type n: Maximum number of characters to read (int)
        :rtype: The number of characters read (int)
        """
        
        buffer = [''] * 4  # create a buffer of size 4
        num_read = 0  # accumulative number of chars read
        for i in xrange(n / 4 + 1):
            this_num = read4(buffer)  # read 4 chars per read4
            if this_num > 0:  # if read something, add it to buf! 
                buf[num_read : num_read + this_num] = buffer  # use num_read, instead of i, since num_read:1~4
                num_read += this_num
            else:  # if nothing to read
                break
        return min(num_read, n)
            