class Solution(object):
    def minPathSum(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        # init first row
        path_sum = grid[0]
        for j in xrange(1, len(grid[0])):
            path_sum[j] = path_sum[j-1] + grid[0][j]
        
        for i in xrange(1, len(grid)):
            # init first column
            path_sum[0] += grid[i][0]
            for j in xrange(1, len(grid[0])):
                path_sum[j] = min(path_sum[j], path_sum[j-1]) + grid[i][j]
        
        return path_sum[-1]