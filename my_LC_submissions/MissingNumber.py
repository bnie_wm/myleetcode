class Solution(object):
    def missingNumber(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """

        # compare sum(nums) and sum(list_all), where list_all contains all numbers
        all = range(max(nums)+1)
        if sum(all) == sum(nums):  # either missing max(nums)+1, or missing 0
            if len(all) == len(nums):
                return max(nums)+1
            else:
                return 0
        
        missing_num = 0
        for x in nums:
            missing_num ^= x
        for x in all:
            missing_num ^= x
        return missing_num