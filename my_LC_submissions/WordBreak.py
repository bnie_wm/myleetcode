"""
DP
Time: O(n*l^2)  # l is max word length in wordDict
Space: O(n)
"""
class Solution(object):
    def wordBreak(self, s, wordDict):
        """
        :type s: str
        :type wordDict: Set[str]
        :rtype: bool
        """
        minL, maxL = float("inf"), 0
        for word in wordDict:
            minL = min(minL, len(word))
            maxL = max(maxL, len(word))
        
        canbreak = [False] * (len(s) + 1)
        canbreak[0] = True  # dummy
        for i in xrange(1, len(s) + 1):  # iteratively check canbreak[i]
            if i < minL:    # prune
                canbreak[i] = False
            for k in xrange(min(i, minL), min(i, maxL) + 1): # k is the possible word length in dictionary
                if canbreak[i-k] and s[i-k:i] in wordDict:
                    canbreak[i] = True
                    break
        return canbreak[-1]