class Solution(object):
    def combinationSum4(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        
        ## DP[i] = sum(dp[i-v] if v in nums)
        
        nums = sorted(nums)
        dp = [0] * (target + 1)
        dp[0] = 1
        for i in xrange(target + 1):
            for v in nums:
                if v <= i:
                    dp[i] += dp[i - v]
                else:
                    break
        return dp[target]