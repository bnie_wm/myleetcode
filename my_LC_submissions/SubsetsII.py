class Solution(object):
    def subsetsWithDup(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        results = []
        self.subsetsWithDupHelper(results, [], sorted(nums))
        return results
        
    def subsetsWithDupHelper(self, results, res, nums):
        if len(nums) == 0:
            results.append(res)
            return
        
        if not res or (res[-1] != nums[0]):  # remove duplicate cases
            self.subsetsWithDupHelper(results, res, nums[1:])
        self.subsetsWithDupHelper(results, res + [nums[0]], nums[1:])
 

        