# Time:  O(nlogc), c is the count of unique characters.
# Space: O(c)
class Solution(object):
    def rearrangeString(self, str, k):
        """
        :type str: str
        :type k: int
        :rtype: str
        """
        if k == 0:
            return str

        cnts = collections.Counter(str)
        pq = [(-cnts[c], c) for c in cnts]
        heapq.heapify(pq)
        
        # res = ""  --> very inefficient when res += c and len(res)
        res = []  # use list, instead of str!
        while pq:
            used = []  # store used chars in one loop
            for _ in xrange(min(k, len(str) - len(res))):  # max num of chars need to arrange
                if not pq:
                    return ""  # if invalid
                cnt, c = heapq.heappop(pq)  # remember that cnt is neg freq
                cnt = -cnt
                res += [c]
                cnt -= 1
                if cnt > 0:
                    used.append((-cnt, c))
            for item in used:
                heapq.heappush(pq, item)  # heappush, instead of append
        return "".join(res)
