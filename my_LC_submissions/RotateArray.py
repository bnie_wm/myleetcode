class Solution(object):
    def rotate(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: void Do not return anything, modify nums in-place instead.
        """
        #1. reverse whole array
        #2. reverse first k
        #3. reverse last n-k
        
        while k >= len(nums):
            k -= len(nums)
        
        self.reverseArray(nums, 0, len(nums)-1)
        self.reverseArray(nums, 0, k-1)
        self.reverseArray(nums, k, len(nums)-1)
        
    def reverseArray(self, nums, i, j):
        while i < j:
            nums[i], nums[j] = nums[j], nums[i]
            i += 1
            j -= 1
            