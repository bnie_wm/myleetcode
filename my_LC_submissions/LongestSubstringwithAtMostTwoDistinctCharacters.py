class Solution(object):
    def lengthOfLongestSubstringTwoDistinct(self, s):
        """
        :type s: str
        :rtype: int
        """
        k = 2
        maxLen = 0
        left = 0
        char_cnt = collections.defaultdict(int)
        for i in xrange(len(s)):
            char_cnt[s[i]] += 1
            
            while len(char_cnt) > k:
                char_cnt[s[left]] -= 1
                if char_cnt[s[left]] == 0:
                    del char_cnt[s[left]]
                left += 1
            
            maxLen = max(maxLen, i-left+1)
        return maxLen
        