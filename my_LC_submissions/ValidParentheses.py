# Time: O(n)
# Spcae: O(n)
class Solution(object):
    def isValid(self, s):
        """
        :type s: str
        :rtype: bool
        """
        lookup = {'(':')', '[':']', '{':'}'}
        stack = []
        for p in s:
            if p in lookup:   # left paras
                stack.append(p)
            else:   # right paras
                # need to check if stack is empty (in case only contains right paras)
                if len(stack) == 0 or p != lookup[stack.pop()]:  
                    return False
        return len(stack) == 0  # need to check if there is any left paras left!
        