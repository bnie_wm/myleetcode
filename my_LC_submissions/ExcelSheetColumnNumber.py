"""
Time: O(n)
Space: O(1)
"""
class Solution(object):
    def titleToNumber(self, s):
        """
        :type s: str
        :rtype: int
        """
        base, i, sum = ord('A'), 0, 0
        for c in s[::-1]:
            sum += math.pow(26, i) * (ord(c) - base + 1)
            i += 1
        return int(sum)