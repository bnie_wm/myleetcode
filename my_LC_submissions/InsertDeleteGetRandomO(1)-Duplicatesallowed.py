class RandomizedCollection(object):

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.__arr = []  # numbers
        self.__dict = {} # key: numbers, val: list of number indices in arr
        

    def insert(self, val):
        """
        Inserts a value to the collection. Returns true if the collection did not already contain the specified element.
        :type val: int
        :rtype: bool
        """
        self.__arr.append(val)
        idx = len(self.__arr) - 1
        res = False
        if val not in self.__dict:
            self.__dict[val] = []
            res = True
        self.__dict[val].append(idx)
        return res
        

    def remove(self, val):
        """
        Removes a value from the collection. Returns true if the collection contained the specified element.
        :type val: int
        :rtype: bool
        """
        # remove val from arr and dict
        # replace current empty location with the last val in arr
        if val not in self.__dict:
            return False
        else:
            rm_idx = self.__dict[val][-1]
            last_val, last_idx = self.__arr[-1], len(self.__arr) - 1

            self.__arr[rm_idx] = last_val
            # del self.__dict[last_val][last_idx] -- wrong syntax
            self.__dict[last_val].remove(last_idx)
            self.__dict[last_val].append(rm_idx)
            
            self.__arr.pop()
            self.__dict[val].pop()
            if len(self.__dict[val]) == 0:
                del self.__dict[val]
            return True

    def getRandom(self):
        """
        Get a random element from the collection.
        :rtype: int
        """
        return self.__arr[random.randint(0, len(self.__arr)-1)]
        


# Your RandomizedCollection object will be instantiated and called as such:
# obj = RandomizedCollection()
# param_1 = obj.insert(val)
# param_2 = obj.remove(val)
# param_3 = obj.getRandom()