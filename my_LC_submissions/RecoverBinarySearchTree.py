# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

"""
two inorder traversal
Time: O(n)
Space: O(h)  -- stack
"""
class Solution(object):
    def recoverTree(self, root):
        """
        :type root: TreeNode
        :rtype: void Do not return anything, modify root in-place instead.
        """
        if not root:
            return
        
        stack = []
        prev, cur = None, root
        first, nodes = True, []
        while stack or cur:
            while cur:
                stack.append(cur)
                cur = cur.left
            cur = stack.pop()
            print cur.val
            # compare cur with prev
            if prev and prev.val > cur.val:
                if first:
                    nodes.append(prev)
                    first = False
                else:
                    nodes.append(cur)
            prev = cur
            cur = cur.right
            
        # if len(nodes) == 0  --> no out-of-place nodes
        if len(nodes) == 2:  # [5,7,4,2,null,6,8,1,3] -- normal case (swap not-nearby nodes)
            nodes[0].val, nodes[1].val = nodes[1].val, nodes[0].val
        elif len(nodes) == 1:   # [5,3,7,2,null,6,8,1,4]  -- edge case (swap nearby nodes in their in-order seq.)
            successor = self.inorder_succesor(root, nodes[0])
            # print nodes[0].val, successor.val
            nodes[0].val, successor.val = successor.val, nodes[0].val
            
                
    def inorder_succesor(self, root, node):
        stack, prev, cur = [], None, root
        while stack or cur:
            while cur:
                stack.append(cur)
                cur = cur.left
            cur = stack.pop()
            if prev == node:
                return cur
            prev, cur = cur, cur.right

    
        