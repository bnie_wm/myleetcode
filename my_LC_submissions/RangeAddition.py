"""
Time: O(k + n)
Space: O(1)
"""
class Solution(object):
    def getModifiedArray(self, length, updates):
        """
        :type length: int
        :type updates: List[List[int]]
        :rtype: List[int]
        """
        arr = [0] * length
    	for (start, end, inc) in updates:
    		arr[start] += inc
    		if end+1 < length:
    			arr[end+1] -= inc
    	
    	for i in xrange(1, length):
    		arr[i] += arr[i-1]
    	return arr
            