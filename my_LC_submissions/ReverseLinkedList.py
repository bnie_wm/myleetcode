# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def reverseList(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        # Time: O(n)
        # Spcae: O(1)
        
        if not head or not head.next:
            return head
        
        prev, cur = None, head
        while cur.next:
            next = cur.next
            cur.next = prev
            prev, cur = cur, next
        cur.next = prev
        return cur
        