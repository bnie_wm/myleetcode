"""
HashTable
Time: O(n)
Space: O(n)
"""
class Solution(object):
    def containsNearbyDuplicate(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: bool
        """
        if not nums:
            return False
            
        cnt = dict()
        for i in xrange(len(nums)):
            x = nums[i]
            if x not in cnt:
                cnt[x] = [i]
            else:
                if i - cnt[x][-1] <= k:
                    return True
                cnt[x].append(i)
        return False
                