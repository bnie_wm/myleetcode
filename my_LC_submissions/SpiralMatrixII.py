class Solution(object):
    def generateMatrix(self, n):
        """
        :type n: int
        :rtype: List[List[int]]
        """
        
        matrix = [[0 for _ in xrange(n)] for _ in xrange(n)]
        left, right, top, bottom = 0, n-1, 0, n-1
        num = 1
        while left <= right and top <= bottom:
            for i in xrange(left, right+1): # top row
                matrix[top][i] = num
                num += 1
            
            for i in xrange(top+1, bottom+1): # right col
                matrix[i][right] = num
                num += 1
            
            if top != bottom:
                for i in reversed(xrange(left, right)):  # bottom row
                    matrix[bottom][i] = num
                    num += 1
            
            if left != right:   
                for i in reversed(xrange(top+1, bottom)):  # left col
                    matrix[i][left] = num
                    num += 1
                
            left, right, top, bottom = left+1, right-1, top+1, bottom-1
            
        return matrix