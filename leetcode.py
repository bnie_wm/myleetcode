class Solution(object):
    def multiply(self, num1, num2):
        """
        :type num1: str
        :type num2: str
        :rtype: str
        """
        num1, num2 = num1[::-1], num2[::-1]
        print num1, num2
        res = [0] * (len(num1) + len(num2))  # save results in normal order: r[0]: highest bit
        for i in xrange(len(num1)):
            for j in xrange(len(num2)):
                p = int(num1[i]) * int(num2[j])
                res[i + j] += p % 10
                # handle carry bit in res[i+j]
                carry = 0
                if res[i + j] > 9:
                    carry = res[i + j] / 10
                    res[i + j] = res[i + j] % 10
                res[i + j + 1] += p / 10 + carry
                print res[i+j], carry, res[i+j+1]
        # no more carry bit in res[0]
        print res
        # clear redundant 0s, i.e. 0*0 = '00'
        if res[0] == 0:
            res = res[1:]

        return "".join([str(x) for x in res])


if __name__ == "__main__":
    print Solution().multiply("1", "1")
    print Solution().multiply("987", "987")