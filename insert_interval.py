# Definition for an interval.
class Interval(object):
    def __init__(self, s=0, e=0):
        self.start = s
        self.end = e

    def __str__(self):
        return "[" + str(self.start) + ", " + str(self.end) + "]"


class Solution(object):
    def insert(self, intervals, newInterval):
        """
        :type intervals: List[Interval]
        :type newInterval: Interval
        :rtype: List[Interval]
        """
        if not intervals:
            return [newInterval]

        # append newInterval to the end
        if newInterval.start > intervals[-1].end:
            return intervals + [newInterval]

        # key is how to divide if and else conditions!
        res = []
        for i in xrange(len(intervals)):
            cur = intervals[i]
            if newInterval.end < cur.start:
                # if newInterval is before cur, intert newInterval and then append rest
                res.append(newInterval)
                res += intervals[i:]
                break
            elif newInterval.start > cur.end:
                # if newInter is after cur, append cur, then continue to next interation
                res.append(cur)
            else:
                # for case where cur and newInterval somehow overlap with each other
                cur.start = min(cur.start, newInterval.start)
                cur.end = max(cur.end, newInterval.end)
                res.append(cur)
                for j in xrange(i + 1, len(intervals)):
                    if intervals[j].start > cur.end:
                        res.append(intervals[j])
                    elif intervals[j].start <= cur.end:  # if next interval overlap with merged cur,
                        if intervals[j].end > cur.end:
                            res[-1].end = intervals[j].end
                break

        return res


def show_res(res):
    for r in res:
        print r,
    print


if __name__ == "__main__":
    # intervals = [[1,2],[3,5],[6,7],[8,10],[12,16]]
    # newInterval = [4, 9]

    intervals = [[1, 5]]
    newInterval = [0, 5]
    newInterval = [1, 7]
    newInterval = [0, 6]

    interval_list = []
    for s, e in intervals:
        interval_list.append(Interval(s, e))
    res = Solution().insert(interval_list, Interval(newInterval[0], newInterval[1]))
    for r in res:
        print r,
    print