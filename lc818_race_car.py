# -*- coding: utf-8 -*-

import collections


class Solution(object):
    def racecar(self, target):
        """
        :type target: int
        :rtype: int
        """
        #### BFS/Tree Leve-order(因为只有两种actions:"A" or "R") -- TLE
        # https://www.youtube.com/watch?v=HzlEkUt2TYs
        queue = collections.deque([(0, 1)])  # (init_position, init_speed)
        # (pos, speed) -- pos相同, speed不同, 也不算re-visit!!!
        # 只记录"R"操作的位置
        visited = set([(0, 1), (0, -1)])
        num_steps = 0  # +1 every level
        while queue:
            num_steps += 1
            size = len(queue)
            for _ in xrange(size):
                pos, speed = queue.popleft()

                # choose between two actions: "A" or "R"
                for act in ["A", "R"]:
                    next_pos, next_speed = self.next_action(act, pos, speed)
                    if next_pos == target:
                        return num_steps
                    # heuristic: "A"时, 若speed/position >= 2*target, 就没有必要再走了
                    if act == "A" and (abs(speed) >= 2 * target or abs(pos) >= 2 * target):
                        continue
                    if (next_pos, next_speed) not in visited:
                        queue.append((next_pos, next_speed))
                        # 只对"R"的操作记录 -- 只保存存过的调头地点 (全存会TLE)
                        if act == "R":
                            visited.add((next_pos, next_speed))
        return -1

    def next_action(self, act, pos, speed):
        if act == "A":
            return (pos + speed, speed * 2)
        elif act == "R":
            speed = -1 if speed > 0 else 1
            return (pos, speed)


if __name__ == "__main__":
    print Solution().racecar(target=6)