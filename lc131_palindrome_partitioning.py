class Solution(object):
    def partition(self, s):
        """
        :type s: str
        :rtype: List[List[str]]
        """

        if len(s) <= 1:
            return [[s]]

        results = []
        self.dfs_helper(0, [], results, s)
        return results

    def dfs_helper(self, left, res, results, s):
        if left == len(s):
            results.append(res)

        for k in xrange(left, len(s)):
            if self.isPalindrome(s[left:k + 1]):
                self.dfs_helper(k + 1, res + [s[left:k + 1]], results, s)

    def isPalindrome(self, s):
        if len(s) <= 1:
            return True
        i, j = 0, len(s) - 1
        while i < j:
            if s[i] != s[j]:
                return False
            i += 1
            j -= 1
        return True