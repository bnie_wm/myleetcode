# Given n nodes labeled from 0 to n - 1 and a list of undirected edges (each edge is a pair of nodes), write a function to find the number of connected components in an undirected graph.
#
# Example 1:
#      0          3
#      |          |
#      1 --- 2    4
# Given n = 5 and edges = [[0, 1], [1, 2], [3, 4]], return 2.
#
# Example 2:
#      0           4
#      |           |
#      1 --- 2 --- 3
# Given n = 5 and edges = [[0, 1], [1, 2], [2, 3], [3, 4]], return 1.
#
# Note:
# You can assume that no duplicate edges will appear in edges. Since all edges are undirected, [0, 1] is the same as [1, 0] and thus will not appear together in edges.

"""
Union and Find
Time: ~O(n)
Space: O(n)
"""

import collections


class UnionFind(object):
    def __init__(self, n):
        self.parents = range(n)
        self.ranks = [0] * n
        self.count = n

    def find(self, x):
        if x != self.parents[x]:
            self.parents[x] = self.find(self.parents[x])  # find with data compression
        return self.parents[x]

    def union(self, x, y):  # union by rank
        px, py = self.find(x), self.find(y)
        if px == py:
            return False
        if self.ranks[px] > self.ranks[py]:
            self.parents[py] = px
        elif self.ranks[px] < self.ranks[py]:
            self.parents[px] = py
        else:
            self.parents[py] = px
            self.ranks[px] += 1
        self.count -= 1
        return True

    def getCount(self):
        return self.count

# Union&Find
class Solution(object):
    def countComponents(self, n, edges):
        """
        :type n: int
        :type edges: List[List[int]]
        :rtype: int
        """
        uf = UnionFind(n)
        for (u, v) in edges:
            uf.union(u, v)
        return uf.getCount()

# BFS
class Solution2(object):
    def countComponents(self, n, edges):
        """
        :type n: int
        :type edges: List[List[int]]
        :rtype: int
        """

        # O(n) build graph
        graph = collections.defaultdict(list)
        for (u, v) in edges:
            graph[u].append(v)
            graph[v].append(u)

        # bfs
        nodes = range(n)
        visited = [0] * n
        count = 0
        for node in nodes:
            if visited[node] == 1:  # visited
                continue
            count += 1
            # bfs/dfs: mark visited=1 for every node that can be reached through current node
            queue = collections.deque([node])
            while queue:
                cur_node = queue.popleft()
                visited[cur_node] = 1
                for next_node in graph[cur_node]:
                    if visited[next_node] == 1:
                        continue
                    queue.append(next_node)
        return count

### DFS
class Solution3(object):
    def countComponents(self, n, edges):
        """
        :type n: int
        :type edges: List[List[int]]
        :rtype: int
        """

        # O(n) build graph
        graph = collections.defaultdict(list)
        for (u, v) in edges:
            graph[u].append(v)
            graph[v].append(u)

        # dfs
        nodes = range(n)
        visited = [0] * n
        count = 0
        for node in nodes:
            if visited[node] == 1:  # visited
                continue
            count += 1
            # dfs: mark every node that can be reached as visited
            visited[node] = 1
            self.dfs_helper(node, visited, graph)
        return count

    def dfs_helper(self, cur_node, visited, graph):
        for next_node in graph[cur_node]:
            if visited[next_node] == 1:  # already visited
                continue
            visited[next_node] = 1
            self.dfs(next_node, visited, graph)


if __name__ == "__main__":
    n = 5
    edges = [[0, 1], [1, 2], [3, 4]]
    edges = [[0,1],[0,2],[1,2],[2,3],[2,4]]
    print Solution().countComponents(n, edges)
    print Solution2().countComponents(n, edges)
    print Solution3().countComponents(n, edges)