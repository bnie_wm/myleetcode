import math


class Solution(object):
    ## test case: "  -0012a42"  --> strip(), end when enconter non-digit
    def myAtoi(self, str):
        """
        :type str: str
        :rtype: int
        """

        INT_MAX = 2147483647    # 2^31-1
        INT_MIN = -2147483648   # -2^31

        # remove redundant space
        str = str.strip()

        if str == "" or str == '0':
            return 0

        # mark sign
        sign = 1
        if str[0] == '-':
            sign = -1
            str = str[1:]
        elif str[0] == '+':
            str = str[1:]

        # remove useless 0s in the front, i.e., 0012 = 12
        i = 0
        while i < len(str):
            if str[i] != "0":
                break
            i += 1
        str = str[i:]

        # stops at the first non-digit char, i.e., 12a42 = 12
        i = 0
        while i < len(str):
            if not str[i].isdigit():
                break
            i += 1
        str = str[:i]

        # str = reversed(str)  --> wrong
        str = str[::-1]

        num = 0
        for i in xrange(len(str)):
            num += int(str[i]) * math.pow(10, i)

        num = int(num) if sign == 1 else int(num) * sign

        if num < INT_MIN:
            return INT_MIN
        elif num > INT_MAX:
            return INT_MAX
        else:
            return num


if __name__ == "__main__":
    # str = "1"
    str = "  -0012a42"
    print Solution().myAtoi(str)