# Close to O(1) performance for as many of the following four operations.
# Maintaining sorted order while inserting an object into the container.
# Ability to peek at last value (the largest value) contained in the object.
# Allowing for pops on both sides (getting the smallest or largest values).
# Capability of getting the total size or number of objects being stored.
# Being a ready made solution like the code in Python's standard library.

import collections
import bisect

class FastTable(object):
    def __init__(self):
        self.__deque = collections.deque()

    def __len__(self):
        return len(self.__deque)

    def __str__(self):
        return str(self.__deque)

    def head(self):
        return self.__deque.popleft()

    def tail(self):
        return self.__deque.pop()

    def peek(self):
        return self.__deque[-1]

    def insert(self, obj):
        index = bisect.bisect_left(self.__deque, obj)
        self.__deque.rotate(-index)
        self.__deque.appendleft(obj)
        self.__deque.rotate(index)


if __name__ == "__main__":
    ft = FastTable()
    for x in [1,2,3]:
        ft.insert(x)
    print ft

    ft.insert(2.5)
    print ft