# Related to question Excel Sheet Column Title
#
# Given a column title as appear in an Excel sheet, return its corresponding column number.
#
# For example:
#
#     A -> 1
#     B -> 2
#     C -> 3
#     ...
#     Z -> 26
#     AA -> 27
#     AB -> 28

import math

"""
Math
Time: O(n)
Space: O(1)
"""
class Solution(object):
    def titleToNumber(self, s):
        """
        :type s: str
        :rtype: int
        """
        base, i, sum = ord('A'), 0, 0
        for c in reversed(list(s)):
            sum += math.pow(26, i) * (ord(c) - base + 1)
            i += 1
        return int(sum)


if __name__ == "__main__":
    print Solution().titleToNumber("AAAB")
    print Solution().titleToNumber("DCBD")
