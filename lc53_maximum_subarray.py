# Find the contiguous subarray within an array (containing at least one number) which has the largest sum.
#
# For example, given the array [-2,1,-3,4,-1,2,1,-5,4],
# the contiguous subarray [4,-1,2,1] has the largest sum = 6.
#
# click to show more practice.
#
# More practice:
# If you have figured out the O(n) solution, try coding another solution using the divide and conquer approach, which is more subtle.

"""
Divide and Conquer
Time: O(nlogn) --> T(n) = 2T(n/2) + O(n)
"""
class Solution2(object):
    def maxSubArray(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        return self.maxSubArrayHelper(nums, 0, len(nums) - 1, float("-inf"))

    def maxSubArrayHelper(self, nums, left, right, max_sum):
        if left == right:  # base: only one element
            return nums[left]

        mid = (left + right) / 2
        # case 1: if targeted subarray in the left half
        left_max = self.maxSubArrayHelper(nums, left, mid, max_sum)

        # case 2: if targeted subarray in the right half
        right_max = self.maxSubArrayHelper(nums, mid + 1, right, max_sum)

        # update max_sum
        max_sum = max(max(max_sum, left_max), right_max)

        # case 3: if targeted subarray across both
        # 3-1: get max sum in the left part (mid --> left)
        acc_left_sum, this_left_max = 0, float("-inf")
        for i in reversed(xrange(left, mid + 1)):
            acc_left_sum += nums[i]
            this_left_max = max(this_left_max, acc_left_sum)
        # 3-2: get max sum in the right part (mid+1 --> right)
        acc_right_sum, this_right_max = 0, float("-inf")
        for i in xrange(mid + 1, right + 1):
            acc_right_sum += nums[i]
            this_right_max = max(this_right_max, acc_right_sum)

        # update max_sum
        max_sum = max(max_sum, this_left_max + this_right_max)

        return max_sum



"""
DP:
Time: O(n)
Space: O(1)
"""
class Solution(object):
    def maxSubArray(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """

        # init for nums[0]
        acc_sum = 0
        max_sum = float("-inf")  # cannot be 0, e.g., [-1] --> return -1, not 0

        for num in nums:
            acc_sum += num
            max_sum = max(acc_sum, max_sum)

            # if acc_sum<0, it means the accumulated sum will contribute negatively to the sum
            # so clear acc_sum
            if acc_sum < 0:
                acc_sum = 0
        return max_sum


if __name__ == "__main__":
    nums = [-2, 1, -3, 4, -1, 2, 1, -5, 4]
    print Solution().maxSubArray(nums)
    print Solution2().maxSubArray(nums)

    nums = [-2, -1]
    print Solution().maxSubArray(nums)
    print Solution2().maxSubArray(nums)