class Solution(object):
    def validWordAbbreviation(self, word, abbr):
        """
        :type word: str
        :type abbr: str
        :rtype: bool
        """
        cnt, wid = 0, 0
        for c in abbr:
            if c.isdigit():
                if c == '0' and cnt == 0:
                    return False
                cnt = cnt * 10 + int(c)
            else:
                wid += cnt
                cnt = 0
                if wid >= len(word) or word[wid] != c:
                    return False
                wid += 1
        return (wid + cnt) == len(word)  # i.e., word = "a", abbr = "2"

if __name__ == "__main__":
    # word = "internationalization"
    # abbr = "i12iz4n"
    word = "a"
    abbr = "2"
    print Solution().validWordAbbreviation(word, abbr)