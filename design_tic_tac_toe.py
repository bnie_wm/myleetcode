class TicTacToe(object):
    def __init__(self, n):
        """
        Initialize your data structure here.
        :type n: int
        """
        self.n = n
        self.__rows = [0 for _ in xrange(n)]
        self.__cols = [0 for _ in xrange(n)]
        self.__diagonal = 0
        self.__anti_diagonal = 0

    def move(self, row, col, player):
        """
        Player {player} makes a move at ({row}, {col}).
        @param row The row of the board.
        @param col The column of the board.
        @param player The player, can be either 1 or 2.
        @return The current winning condition, can be either:
                0: No one wins.
                1: Player 1 wins.
                2: Player 2 wins.
        :type row: int
        :type col: int
        :type player: int
        :rtype: int
        """
        target = self.n if player == 1 else (-1 * self.n)
        adder = 1 if player == 1 else -1
        self.__rows[row] += adder
        self.__cols[col] += adder
        if row == col:
            self.__diagonal += adder
        if col == self.n - row - 1:
            self.__anti_diagonal += adder

        if target in [self.__rows[row], self.__cols[col], self.__diagonal, self.__anti_diagonal]:
            return player
        else:
            return 0


# Your TicTacToe object will be instantiated and called as such:
# obj = TicTacToe(n)
# param_1 = obj.move(row,col,player)