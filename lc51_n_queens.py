class Solution(object):
    def solveNQueens(self, n):
        """
        :type n: int
        :rtype: List[List[str]]
        """

        if n == 1: return [["Q"]]
        if n < 4: return []

        results = []
        for j in xrange(n):  # iterate through n potential positions for the first Queen
            visited = [[False for _ in xrange(n)] for _ in xrange(n)]
            visited[0][j] = True
            first_row = ["."] * n
            first_row[j] = "Q"
            res = ["".join(first_row)]
            self.dfs(0, n, visited, res, results)

        return results

    def dfs(self, row, n, visited, res, results):
        if row == n - 1:  # find one placement
            results.append(res)
            return

        # place the next Queen
        for j in xrange(n):
            if self.isValidPosition(row + 1, j, n, visited):
                visited[row + 1][j] = True
                this_row = ["."] * n
                this_row[j] = "Q"
                self.dfs(row + 1, n, visited, res + ["".join(this_row)], results)
                visited[row + 1][j] = False

    def isValidPosition(self, row, col, n, visited):
        # (1) cannot in the same row -- already satisfied

        # (2) cannot in the same coloumn
        for i in xrange(row + 1):
            if visited[i][col]:
                return False

        # (3) cannot in the same diagnal
        for i in xrange(row + 1):
            for j in xrange(n):
                if visited[i][j]:
                    if row - i == j - col:  # in the same diagnal
                        return False
                    if i - j == row - col:  # in the same back-diagnal
                        return False
        return True