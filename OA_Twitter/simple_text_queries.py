import collections

class Solution(object):
    def simple_text_queries(self, sentences, phrases):
        # get word to sentence mapping
        word_to_sentence = collections.defaultdict(list)
        for i, sentence in enumerate(sentences):
            words = sentence.split()
            for word in words:
                word_to_sentence[word].append(i)

        res = []
        for phrase in phrases:
            words = phrase.split()
            common = set(word_to_sentence[words[0]])
            for word in words[1:]:
                common = common.intersection(set(word_to_sentence[word]))
            if len(common) > 0:
                res.append(sorted(common))
            else:
                res.append([-1])
        return res


if __name__ == "__main__":
    sentences = ["bob and alice like to text each other", "bob does not like to ski", "alice likes to ski"]
    phrases = ["bob alice", "alice", "like"]
    print Solution().simple_text_queries(sentences, phrases)

    sentences = ["it go will away", "go do it", "what to will east"]
    phrases = ["it will", "go east will", "will"]
    print Solution().simple_text_queries(sentences, phrases)