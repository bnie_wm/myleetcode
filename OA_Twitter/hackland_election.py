import collections
import bisect


class Solution(object):
    def electionWinner(self, votes):
        vote_counts = collections.Counter(votes)

        max_votes = max(vote_counts.values())
        winner = ""
        for person in vote_counts:
            if vote_counts[person] == max_votes:
                winner = max(winner, person)

        return winner


if __name__ == "__main__":
    s = Solution()

    votes = ["victor", "veronica", "ryan", "dave", "maria", "farah", "farah", "ryan", "veronica"]
    print s.electionWinner(votes)

    votes = ["Alex", "Michael", "Harry", "Dave", "Michael", "Victor", "Harry", "Alex", "Mary", "Mary"]
    print s.electionWinner(votes)