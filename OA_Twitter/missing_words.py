class Solution(object):
    def missing_words(self, s, t):
        words_s = s.split()
        words_t = t.split()

        res = []
        i, j = 0, 0
        while i < len(words_s) and j < len(words_t):
            if words_s[i] != words_t[j]:
                res.append(words_s[i])
                i += 1
            else:
                i += 1
                j += 1

        # append the remaining words in s
        if i < len(words_s):
            res.append(words_s[i])

        return res


if __name__ == "__main__":
    s = "I am using HackerRank to improve programming"
    t = "am HackerRank to improve"
    print Solution().missing_words(s, t)