class Solution(object):
    def huffman_decoder(self, code_list, input):
        huffman_codes = dict()
        for item in code_list:
            char, code = item.split()
            if char == "newline":
                char = "\n"
            huffman_codes[code] = char

        res = []
        n = len(input)
        left, right = 0, 1
        while left < n:
            substr = input[left: right]
            if substr in huffman_codes:
                res.append(huffman_codes[substr])
                left, right = right, right + 1  # move to next code
            else:
                right += 1  # move right pointer forward

        return "".join(res)


if __name__ == "__main__":
    s = Solution()

    code_list = {"a 100100", "b 100101", "c 110001", "d 100000", "newline 1111111", "p 111110", "q 000001"}
    input = "1111100000011001001111111100101110001100000"
    print s.huffman_decoder(code_list, input)