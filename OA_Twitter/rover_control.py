class Solution(object):
    def rover_control(self, size, cmds):
        if size == 1:
            return 0
        directions = {
            "RIGHT": [1, 0],
            "LEFT": [-1, 0],
            "UP": [0, -1],
            "DOWN": [0, 1],
        }

        i, j = 0, 0  # start at (0, 0)
        for cmd in cmds:
            # skip invalid commands
            if (i == 0 and cmd == "LEFT") or (i == size -1 and cmd == "RIGHT") or \
                    (j == 0 and cmd == "UP") or (j == size - 1 and cmd == "DOWN"):
                continue
            else:
                i += directions[cmd][0]
                j += directions[cmd][1]

        return i + j * size


if __name__ == "__main__":
    cmds = ["RIGHT", "DOWN", "LEFT", "LEFT", "DOWN"]
    print Solution().rover_control(4, cmds)

    cmds = ["RIGHT", "UP", "DOWN", "LEFT", "LEFT", "DOWN", "DOWN"]
    print Solution().rover_control(4, cmds)

