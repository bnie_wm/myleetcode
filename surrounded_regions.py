# Given a 2D board containing 'X' and 'O' (the letter O), capture all regions surrounded by 'X'.
#
# A region is captured by flipping all 'O's into 'X's in that surrounded region.
#
# For example,
# X X X X
# X O O X
# X X O X
# X O X X
# After running your function, the board should be:
#
# X X X X
# X X X X
# X X X X
# X O X X

import collections

"""
BFS only on boarder cells
Time: O(m*n)
Space: O(m+n) -- queue ??? why not O(mn)???

"""
class Solution(object):
    def solve(self, board):
        """
        :type board: List[List[str]]
        :rtype: void Do not return anything, modify board in-place instead.
        """
        if not board:
            return

        n_rows, n_cols = len(board), len(board[0])
        q = collections.deque([])

        # unsurrounded region should has at least one 'O' in boarder
        # so only starts from boarder
        for i in xrange(n_rows):
            q.append((i, 0))
            q.append((i, n_cols-1))

        for j in xrange(1, n_cols-1):
            q.append((0, j))
            q.append((n_rows-1, j))

        # apply BFS -- queue
        # if use DFS -- exceed max recursion depth
        # mark detected unsurrounded regions as 'Y'
        directions = [(-1, 0), (1, 0), (0, -1), (0, 1)]
        while q:
            i, j = q.popleft()
            if board[i][j] == 'O':
                board[i][j] = 'Y'
                for d in directions:
                    x, y = i + d[0], j + d[1]
                    if 0 <= x < n_rows and 0 <= y < n_cols and board[x][y] == 'O':
                        q.append((x, y))

        # change 'O' --> 'X', 'Y' --> 'O'
        for i in xrange(n_rows):
            for j in xrange(n_cols):
                if board[i][j] == 'O':
                    board[i][j] = 'X'
                elif board[i][j] == 'Y':
                    board[i][j] = 'O'


if __name__ == "__main__":
    board = [['X', 'X', 'X', 'X'],
             ['X', 'O', 'O', 'X'],
             ['O', 'X', 'O', 'X'],
             ['O', 'O', 'X', 'X']]

    raw_board = ["OOOOXX","OOOOOO","OXOXOO","OXOOXO","OXOXOO","OXOOOO"]
    board = []
    for row in raw_board:
        board.append(list(row))
    Solution().solve(board)

    for row in board:
        print "".join(row),