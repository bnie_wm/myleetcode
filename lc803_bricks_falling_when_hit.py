# -*- coding: utf-8 -*-


# O(# hits) * O(mn) -- TLE
class SolutionDFS(object):
    def hitBricks(self, grid, hits):
        """
        :type grid: List[List[int]]
        :type hits: List[List[int]]
        :rtype: List[int]
        """
        if not hits:
            return hits
        if not grid:
            return [0]

        m, n = len(grid), len(grid[0])
        res = []
        for hit in hits:
            grid[hit[0]][hit[1]] = 0  # erase the brick

            # DFS: 数要掉落的brick数量
            visited = [[False for _ in xrange(n)] for _ in xrange(m)]

            # 从第一行的brick开始延伸可以到达的brick都不会掉落 -- 只需在visited上边标记为True
            for j in xrange(n):
                if grid[0][j] == 1 and not visited[0][j]:
                    self.dfs(0, j, grid, m, n, visited, False)

            # 其余的brick都则都会掉落
            tot_cnt = 0
            for i in xrange(1, m):
                for j in xrange(n):
                    if grid[i][j] == 1 and not visited[i][j]:  # 不能是被访问过的!!!
                        tot_cnt += self.dfs(i, j, grid, m, n, visited, True)
            res.append(tot_cnt)
        return res

    def dfs(self, x, y, grid, m, n, visited, drop):
        directions = [(-1, 0), (1, 0), (0, 1), (0, -1)]

        visited[x][y] = True
        if drop:
            grid[x][y] = 0

        cnt = 1
        for dx, dy in directions:
            i, j = x + dx, y + dy
            if 0 <= i < m and 0 <= j < n and not visited[i][j] and grid[i][j] == 1:
                cnt += self.dfs(i, j, grid, m, n, visited, drop)
        return cnt


class UnionFind(object):
    def __init__(self, m, n):
        # each cell's idx = 2*n+j
        # tot = m*n cells
        tot = m * n
        self.parents = range(tot)
        self.ranks = [0] * tot

        # special for this problem
        # 记录每个connected component的大小 (初始为1)
        self.sizes = [1] * tot
        # 记录每个connected component是否与roof相连(初始只有roof为1)
        # 对于每个节点x, px=self.find(x), 若px is connected to root, then x is connected to roof
        self.connected_to_roof = [1] * n + [0] * (tot - n)

    def find(self, x):
        # 如果self.parents[x]==x, 已经是root了, 不需要path compression
        if self.parents[x] != x:
            self.parents[x] = self.find(self.parents[x])  # path compression
        return self.parents[x]

    def union(self, x, y):
        px, py = self.find(x), self.find(y)
        if px == py:
            return False

        reduced_cnt = 0
        if self.ranks[px] < self.ranks[py]:
            self.parents[px] = py
            # 更新py的size和connected_to_roof
            self.sizes[py] += self.sizes[px]
            if self.connected_to_roof[px] == 1:
                self.connected_to_roof[py] = 1
        elif self.ranks[px] > self.ranks[py]:
            self.parents[py] = px
            # 更新px的size和connected_to_roof
            self.sizes[px] += self.sizes[py]
            if self.connected_to_roof[py] == 1:
                self.connected_to_roof[px] = 1
        else:
            self.parents[py] = px
            self.ranks[px] += 1
            # 更新px的size和connected_to_roof
            self.sizes[px] += self.sizes[py]
            if self.connected_to_roof[py] == 1:
                self.connected_to_roof[px] = 1
        return True

    def is_connected_to_roof(self, x):
        px = self.find(x)
        return self.connected_to_roof[px] == 1

    def get_size(self, x):
        px = self.find(x)
        return self.sizes[px]


class Solution(object):
    def hitBricks(self, grid, hits):
        """
        :type grid: List[List[int]]
        :type hits: List[List[int]]
        :rtype: List[int]
        """
        if not hits:
            return hits

        m, n = len(grid), len(grid[0])

        def index(i, j):
            return i * n + j

        # 在grid中移除hits的点
        hit_is_brick = dict()
        for hx, hy in hits:
            hit_is_brick[(hx, hy)] = True if grid[hx][hy] == 1 else False
            grid[hx][hy] = 0

        # build Union anf Find
        directions = [(0, 1), (0, -1), (-1, 0), (1, 0)]
        graph = UnionFind(m, n)
        for i in xrange(m):
            for j in xrange(n):
                if grid[i][j] == 0:  # skip non-brick!!!
                    continue
                cur_idx = index(i, j)
                for di, dj in directions:
                    ii, jj = i + di, j + dj
                    if 0 <= ii < m and 0 <= jj < n and grid[ii][jj] == 1:  # only go to valid brick cell
                        graph.union(cur_idx, index(ii, jj))

        # ***reversely***, put hitted bricks back
        results = []
        for hx, hy in hits[::-1]:
            # 如果原来是无效hit, 或者现在已经brick了, 没有变化
            if not hit_is_brick[(hx, hy)] or grid[hx][hy] == 1:
                results.append(0)
                continue

            # put brick (hx, hy) back,
            # see how many connected componets are now connected to roof!
            grid[hx][hy] = 1
            h_idx = index(hx, hy)

            # 只存邻居的parent idx -- 四个相邻的砖块有可能属于同一个connected components
            # e.g., grid=[[1,0,0,0,0], [1,0,1,1,0], [1,1,1,1,1]], hit=[[2,2]]
            # hit[0]三个方向都是brick, 但是属于两个connected components
            neighbours = {}
            for dx, dy in directions:
                x, y = hx + dx, hy + dy
                if 0 <= x < m and 0 <= y < n and grid[x][y] == 1:
                    pid = graph.find(index(x, y))
                    if pid not in neighbours:
                        neighbours[pid] = (graph.get_size(pid), graph.is_connected_to_roof(pid))

            # 看union之后是否会connect to roof, 两种情况has_connected=True
            # 1. (hx, hy) 在 roof
            # 2. (hx, hy) 有至少一个邻居的brick快和roof相连
            has_connected = True if hx == 0 else False
            tot_cnt = 0  # 只有之前不相连的才+tot_cnt
            for nei in neighbours:
                nei_size, connected = neighbours[nei]
                has_connected = has_connected or connected  # 有一个相连就全相连
                if not connected:
                    tot_cnt += nei_size

            tot_cnt = tot_cnt if has_connected else 0
            results.append(tot_cnt)

            # union neighbours
            for nei_idx in neighbours:
                graph.union(h_idx, nei_idx)

        return results[::-1]

    # [[1,0,0,0,0], [1,0,1,1,0], [1,1,1,1,1]]
    # [[2,2]]


if __name__ == "__main__":
    s = Solution()
    grid = [[1,0,0,0,0], [1,0,1,1,0], [1,1,1,1,1]]
    hits = [[2,2]]
    print s.hitBricks(grid, hits)

