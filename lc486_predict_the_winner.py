class Solution(object):
    def PredictTheWinner(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        n = len(nums)
        memo = [[-1 for _ in xrange(n)] for _ in xrange(n)]
        self.get_score(0, n - 1, 0, nums, memo)
        return self.get_score(0, n - 1, 0, nums, memo) >= 0

    def get_score(self, left, right, depth, nums, memo):
        """
        MinMax策略: 递归求解. 若depth%2==0为A的轮次; 反之B的轮次.
        计算相对score=A-B. 若score>=0, 则返回True
        迭代:
        1. 每次两种选择:
            1.1 选左: s1=self.get_score(left+1, right, depth+1, nums)
            1.2 选右: s2=self.get_score(left, right-1, depth+1, nums).
        2.  若是A的轮次: A希望score最大化, 所以取max(s1+nums[left], s2+nums[right]);
            若B的轮次: B希望score最小化 (绝对值最大的负数), 所以取min(s1-nums[left], s2-nums[right]).
        初始化: self.get_score(0, len(nums)-1, 0, nums), 若返回值>=0, 则A可以赢
        优化: with memo[left][right] -- 画搜索树是发现, 有很多重复计算. 所以用memo保存.
            而memo不需要额外存是A or B的轮次. 因为|right-left|相同的一定是同一个人的轮次. 所以不会存在两个人的结果相互overwrite的情况
        """
        if left > right:
            return 0

        if memo[left][right] != -1:
            return memo[left][right]

        choice1 = self.get_score(left + 1, right, depth + 1, nums, memo)
        choice2 = self.get_score(left, right - 1, depth + 1, nums, memo)

        #### 可以不用区分当前是谁的轮次. 每次有用当前的选择减去对方的最优选择
        #         if depth % 2 == 0: # A's turn
        #             ans = max(choice1 + nums[left], choice2 + nums[right])
        #         else:  # B'turn
        #             ans = min(choice1 - nums[left], choice2 - nums[right])

        #         memo[left][right] = ans
        #         return ans

        memo[left][right] = max(nums[left] - choice1, nums[right] - choice2)
        return memo[left][right]
