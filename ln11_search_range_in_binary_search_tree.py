"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""


class Solution:
    """
    @param root: param root: The root of the binary search tree
    @param k1: An integer
    @param k2: An integer
    @return: return: Return all keys that k1<=key<=k2 in ascending order
    """

    def searchRange(self, root, k1, k2):
        # write your code here

        if not root:
            return []

        if k1 > k2:
            k1, k2 = k2, k1

        # inorder search
        res = []
        stack = [(root, False)]
        while stack:
            cur, visited = stack.pop()
            if visited:
                res.append(cur.val)
            else:
                if cur.right and cur.val < k2:
                    stack.append((cur.right, False))
                if k1 <= cur.val <= k2:
                    stack.append((cur, True))
                if cur.left and cur.val > k1:
                    stack.append((cur.left, False))

        return res