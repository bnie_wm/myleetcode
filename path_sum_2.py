# Time:  O(n)
# Space: O(h), h is height of binary tree
#
# Given a binary tree and a sum, find all root-to-leaf paths where each path's sum equals the given sum.
#
# For example:
# Given the below binary tree and sum = 22,
#               5
#              / \
#             4   8
#            /   / \
#           11  13  4
#          /  \    / \
#         7    2  5   1
# return
# [
#    [5,4,11,2],
#    [5,8,4,5]
# ]


# Definition for a  binary tree node
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


"""
DFS
Note that, must be root-to-leaf path!!!
Time: O(n)
Space: O(h) --> h is tree height
"""
class Solution:
    def pathSum(self, root, sum):
        """
        :type root: TreeNode
        :type sum: int
        :rtype: List[List[int]]
        """
        results = []
        self.pathSumHelper(results, [], root, sum)
        return results

    def pathSumHelper(self, results, res, root, sum):
        if root is None:
            return results

        # find one root-to-leaf path
        if root.left is None and root.right is None and root.val == sum:
            results.append(res + [root.val])
            return results

        res.append(root.val)
        self.pathSumHelper(results, res, root.left, sum - root.val)
        self.pathSumHelper(results, res, root.right, sum - root.val)
        res.pop()


if __name__ == "__main__":
    root = TreeNode(5)
    root.left = TreeNode(4)
    root.right = TreeNode(8)

    root.left.left = TreeNode(11)
    root.left.left.left = TreeNode(7)
    root.left.left.right = TreeNode(2)

    root.right.left = TreeNode(13)

    root.right.right = TreeNode(4)
    root.right.right.left = TreeNode(5)
    root.right.right.right = TreeNode(1)

    results = Solution().pathSum(root, 22)

    for res in results:
        print res
