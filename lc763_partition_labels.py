import collections


class Solution(object):
    def partitionLabels(self, S):
        """
        :type S: str
        :rtype: List[int]
        """

        # Greedy: O(n)
        # https://zxi.mytechroad.com/blog/string/leetcode-763-partition-labels/
        last_pos = collections.defaultdict(int)
        for i, char in enumerate(S):
            last_pos[char] = i

        result = []
        start = end = 0
        for i, char in enumerate(S):
            end = max(end, last_pos[char])
            if i == end:
                result.append(end - start + 1)
                start = end + 1

        return result


if __name__ == "__main__":

    S = "ababcbacadefegdehijhklij"
    print Solution().partitionLabels(S)