class Solution(object):
    def kEmptySlots(self, flowers, k):
        """
        :type flowers: List[int]
        :type k: int
        :rtype: int
        """

        n = len(flowers)
        blooming = [0] * n
        for day_id, pos in enumerate(flowers):  # pos_id = pos-1, day=day_id+1
            blooming[pos - 1] = 1

            # check right
            right_valid = False
            if pos + k < n and blooming[pos + k] == 1:  # if right pos(k+1) is blooming
                right_valid = True
                # check right k are all empty slots
                i = 1
                while i <= k:
                    if (pos - 1) + i >= n or blooming[(pos - 1) + i] == 1:
                        right_valid = False
                        break
                    i += 1
            if right_valid:
                return day_id + 1

            # check left
            left_valid = False
            if pos - k - 2 >= 0 and blooming[pos - k - 2] == 1:
                left_valid = True
                i = 1
                while i <= k:
                    if pos - 1 - i < 0 or blooming[pos - 1 - i] == 1:
                        left_valid = False
                        break
                    i += 1
            if left_valid:
                return day_id + 1

        return -1


import bisect
class Solution2(object):
    def kEmptySlots(self, flowers, k):
        """
        :type flowers: List[int]
        :type k: int
        :rtype: int
        """

        # use bisect to maintain the sorted order of bloomed flower positions
        # if blooming[i] - blooming[i-1] - 1 == k or blooming[i+1] - blooming[i] - 1 == k
        # return that day
        blooming = []
        for day_id, pos in enumerate(flowers):
            i = bisect.bisect(blooming, pos)  # this insertion position
            bisect.insort(blooming, pos)
            if i - 1 >= 0 and blooming[i] - blooming[i - 1] - 1 == k:
                return day_id + 1
            if i + 1 < len(blooming) and blooming[i + 1] - blooming[i] - 1 == k:
                return day_id + 1
        return -1


#### bucket: O(n)
#### put flowers into buckets of size k+1
#### if x == min(b[p]), check if max(b[p-1]) = x - k -1
#### if x == max(b[p]), check if min(b[p+1]) = x + k + 1
class Solution3(object):
    def kEmptySlots(self, flowers, k):
        """
        :type flowers: List[int]
        :type k: int
        :rtype: int
        """
        buckets = dict()
        for day_id, pos in enumerate(flowers):
            # buck_id = pos % (k+1)   # use / instead of %!
            buck_id = pos / (k + 1)
            if buck_id not in buckets:
                buckets[buck_id] = [pos, pos]  # min, max
            else:
                buckets[buck_id] = [min(buckets[buck_id][0], pos), max(buckets[buck_id][1], pos)]

            if pos == buckets[buck_id][0]:  # pos == min
                if buck_id - 1 in buckets and buckets[buck_id - 1][1] == pos - k - 1:
                    return day_id + 1
            if pos == buckets[buck_id][1]:  # pos == max (not elif, as when init, min=max=pos)
                if buck_id + 1 in buckets and buckets[buck_id + 1][0] == pos + k + 1:
                    return day_id + 1

        return -1


if __name__ == "__main__":
    flowers = [6,5,8,9,7,1,10,2,3,4]
    print(Solution().kEmptySlots(flowers, k=2))
    print(Solution2().kEmptySlots(flowers, k=2))
    print(Solution3().kEmptySlots(flowers, k=2))