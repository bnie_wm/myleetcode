class Solution(object):
    def combinationSum(self, candidates, max_num):
        candidates.sort()
        results = []

        for num_digits in xrange(1, len(candidates) + 1):
            this_results = []
            self.combinationSumHelper(this_results, 0, candidates, max_num, num_digits)
            if len(this_results) == 0:
                break
            results += this_results
        return results

    def combinationSumHelper(self, results, res, candidates, max_num, remain_digits):
        if remain_digits == 0:
            results.append(res)
            return

        for i in xrange(len(candidates)):
            if res == 0 and candidates[i] == 0:  # 0 cannot be the first digit
                continue
            new_res = res * 10 + candidates[i]
            if new_res <= max_num:
                self.combinationSumHelper(results, new_res, candidates, max_num, remain_digits - 1)


if __name__ == "__main__":
    candidates = [3, 7, 8, 0]
    max_num = 1000
    print Solution().combinationSum(candidates, max_num)