class Solution(object):
    """
    Doesn't work for s = "(((k()(("
    # should be ['k(), '(k)]
    # but only output ['k()']
    """
    def removeInvalidParentheses(self, s):
        """
        :type s: str
        :rtype: List[str]
        """

        results = [""]

        left = []  # stack for the idx of left parenthese
        right = []  # stack for the idx of right parenthese
        n = len(s)
        cur_start_idx = 0
        for i in xrange(n):
            if s[i] == "(":
                left.append(i)
            elif s[i] == ")":
                right.append(i)  # record the idx of ")"
                if len(left) > 0:  # valid ")"
                    left.pop()  # pop the last "("
                else:  # invalid ")"
                    this_part = []
                    for j in right:
                        new_str = s[cur_start_idx:j] + s[j + 1:i + 1]
                        if new_str not in this_part:
                            this_part.append(new_str)
                    if len(results) == 0:
                        results = this_part
                    else:
                        results = self.append_parts(results, this_part)
                    cur_start_idx = i + 1
                    right = []

        last_part = ""
        for i in xrange(cur_start_idx, len(s)):
            if i not in left:
                last_part += s[i]

        for i in xrange(len(results)):
            results[i] += last_part

        return results

    def append_parts(self, prev, cur):
        res = []
        for p in prev:
            for c in cur:
                res.append(p + c)
        return res


class Solution2(object):
    def removeInvalidParentheses(self, s):
        """
        :type s: str
        :rtype: List[str]
        """


        ##### BFS
        q, ans, vis = [s], [], set([s])
        found = False  # to control levels
        while q:  # queue for BFS
            cur = q.pop(0)
            if self.isValidParentheses(cur):
                ans.append(cur)
                found = True
            elif not found:  # since remove the minimum number of parentheses, otherwise, will output all valid subsets
                for i in xrange(len(cur)):
                    if cur[i] in ['(', ')']:
                        new_cur = cur[:i] + cur[i + 1:]
                        if new_cur not in vis:
                            vis.add(new_cur)
                            q.append(new_cur)
        return ans


    def isValidParentheses(self, s):
        cnt = 0
        for c in s:
            if c == '(':
                cnt += 1
            elif c == ')':
                if cnt == 0: return False
                cnt -= 1
        return cnt == 0


if __name__ == "__main__":
    # s = "()())())((()"
    s = ""
    # s = "()(((())"
    # s = "))"
    # s = "())(((()()"
    # s = "()())()"
    # s = "(((k()(("   # should be ['k(), '(k)]
    print Solution2().removeInvalidParentheses(s)
