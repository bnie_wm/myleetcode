def solution(strs, ints):
    for i in range(len(ints)):
        j = ints[i]
        while j >= 0 and ints[j] >= 0 and i != j:
            strs[j], strs[i] = strs[i], strs[j]
            ints[i] = -1
            i = j
            j = ints[i]


if __name__ == "__main__":
    strs = ['cat', 'rabbit', 'dog', 'mouse']
    ints = [2, 0, 3, 1]
    solution(strs, ints)
    print strs

