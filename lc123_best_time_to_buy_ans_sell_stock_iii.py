class Solution(object):
    def maxProfit(self, prices):
        """
        :type prices: List[int]
        :rtype: int
        """

        n = len(prices)
        if n <= 1:
            return 0

        #         ######## [Method-1] -- TLE (问题在于，计算2nd交易时，还是O(n))
        #         # one transaction
        #         # profit_one[i]: in range [0, i], max_profit when sell at i and buy at j, where j in xrange(0, i)
        #         profit_one = [0] * n
        #         for i in xrange(1, n):
        #             for j in xrange(i):
        #                 # buy at j sell at i
        #                 if prices[i] > prices[j]:
        #                     profit_one[i] = max(profit_one[i], prices[i] - prices[j])

        #         # two transactions (O(n^3))
        #         # init: if i=0,1,2 --> only can make one transaction; else, init=0
        #         profit_two = [profit_one[i] if i < 3 else 0 for i in xrange(n)]
        #         for i in xrange(3, n):
        #             for j in xrange(1, i-1):
        #                 # two transactions:
        #                 # 1st: sell at j --> max profit = profit_one[j]
        #                 # 2nd: buy from j+1 or later, sell at i --> _max_profit
        #                 _max_profit = 0   # --> a lot duplicated computations here, should use another DP
        #                 for k in xrange(j+1, i):
        #                     if prices[i] > prices[k]:
        #                         _max_profit = max(_max_profit, prices[i]-prices[k])
        #                 profit_two[i] = max(profit_one[i], profit_two[i], profit_one[j] + _max_profit)

        #         return max(profit_two)

        #         ##### [Method-2] Optimzation: profit_one增加一维从k开始的max_profit (Method-1中永远是从i开始)
        #         # one transaction: -- 这里是O(n^3)了 -- 所以只是将计算profit_one[k][i]移到这里了，本质上没有优化，反而多浪费了一维空间
        #         # profit_one[k][i]: in range [k, i] --> max profit sell at i and buy at j, where j in [k, i])
        #         profit_one = [[0 for _ in xrange(n)] for _ in xrange(n)]
        #         for k in xrange(n):
        #             for i in xrange(1, n):
        #                 for j in xrange(k, i):
        #                     # starting from k: buy at j sell at i
        #                     if prices[i] > prices[j]:
        #                         profit_one[k][i] = max(profit_one[k][i], profit_one[k][j] + prices[i] - prices[j])

        #         # two transactions
        #         # init: if i=0,1,2 --> only can make one transaction; else, init=0
        #         profit_two = [profit_one[0][i] if i < 3 else 0 for i in xrange(n)]
        #         for i in xrange(3, n):
        #             for j in xrange(1, i-1):
        #                 # two transactions:
        #                 # 1st: buy from 0 or later, sell at j --> max profit = profit_one[0][j]
        #                 # 2nd: buy from j+1 or later, sell at i --> max profit = profit_one[j+1][i]
        #                 profit_two[i] = max(profit_one[0][i], profit_two[i], profit_one[0][j] + profit_one[j+1][i])

        #         return max(profit_two)

        #         ######## [Method-3] -- 前后遍历两次 -- TLE
        #         # 1st transaction: 从前向后扫
        #         # profit_one[i]: max profit in range [0, i]
        #         profit_one_first = [0] * n
        #         for i in xrange(1, n):
        #             for j in xrange(i):
        #                 # choose max from:
        #                 # (1) profit_one_first[i]
        #                 # (2) profit_one_first[j]: use max_profit in [0, j-1], no operation in [j, i]
        #                 # (3) prices[i] - prices[j]: no operation in [0, j-1], buy at j, set at i
        #                 profit_one_first[i] = max(profit_one_first[i], profit_one_first[j], prices[i] - prices[j])

        #         # 2nd transaction: 从后向前扫
        #         # profit_one_second[i]: max profit in range [i, n-1]
        #         profit_one_second = [0] * n
        #         for i in xrange(n-2, -1, -1):
        #             for j in xrange(i+1, n):
        #
        #                 profit_one_second[i] = max(profit_one_second[i], profit_one_second[j], prices[j] - prices[i])

        #         # add profit_one_first and profit_one_second to get max profit for two transactions -- O(n）
        #         profit_two = [0] * n
        #         for i in xrange(n):  # split range [0, n-1] into two parts: [0, i] and [i, n-1] # 同一天可以先卖再买 (不是[0,i-1], [i, n-1])
        #             profit_two[i] = max(profit_one_first[i] + profit_one_second[i], profit_two[i])
        #         return max(profit_two)

        ######## [Method-4] -- 前后遍历两次
        ### Method-3问题在于求profit_one_first和profit_one_second有O(n)的方法
        ### 这个方法的dp[i]定义和前三个方法不同，所以可以降一维j。
        # E.g., [3,3,5,0,0,3,1,4]
        # 1st transaction: 从前向后扫
        # profit_one[i]: max profit in range [0, i] (not necessarily to be buy at 0 sell at i)
        profit_one_first = [0] * n
        min_price = prices[0]
        for i in xrange(1, n):
            # add profit_one_first[i-1] -- so that profit_one_first accumulate from left to right
            profit_one_first[i] = max(profit_one_first[i], prices[i] - min_price, profit_one_first[i - 1])
            min_price = min(min_price, prices[i])
        # no dp[i-1]: profit_one_first = [0, 0, 2, 0, 0, 3, 1, 4]
        # with dp[i-1]: profit_one_first = [0, 0, 2, 2, 2, 3, 3, 4]

        # 2nd transaction: 从后向前扫
        # profit_one_second[i]: max profit in range [i, n-1] (not necessarily to be buy at i sell at n-1)
        profit_one_second = [0] * n
        max_price = prices[-1]
        for i in xrange(n - 2, -1, -1):
            # add profit_one_second[i+1] -- so that profit_one_second accumulate from right to left
            profit_one_second[i] = max(profit_one_second[i], max_price - prices[i], profit_one_second[i + 1])
            max_price = max(max_price, prices[i])
        # no dp[i+1]: profit_one_second = [2, 2, 0, 4, 4, 1, 3, 0]
        # with dp[i+1]: profit_one_second = [4, 4, 4, 4, 4, 3, 3, 0]

        # add profit_one_first and profit_one_second to get max profit for two transactions -- O(n）
        profit_two = [0] * n
        for i in xrange(
                n):  # split range [0, n-1] into two parts: [0, i] and [i, n-1] # 同一天可以先卖再买 (不是[0,i-1], [i, n-1])
            profit_two[i] = max(profit_one_first[i] + profit_one_second[i], profit_two[i])
        return max(profit_two)