class Solution(object):
    def restoreIpAddresses(self, s):
        """
        :type s: str
        :rtype: List[str]
        """
        if not s or s == "" or len(s) > 12:  # longest len of s is 12 = 3 * 4
            return []

        results = []
        self.helper(results, "", 0, s, 0)
        return results

    # "0.1.0.010"  --> "010" invalid!!
    def helper(self, results, res, cur_idx, s, num_seg):
        if cur_idx == len(s):
            if num_seg == 4:
                results.append(res[:-1])  # remove last "."
            return

        if s[cur_idx] == '0':
            self.helper(results, res + s[cur_idx] + ".", cur_idx + 1, s, num_seg + 1)
        else:
            for curLen in [1, 2, 3]:
                if cur_idx + curLen <= len(s) and 0 <= int(s[cur_idx: cur_idx + curLen]) <= 255:
                    self.helper(results, res + s[cur_idx: cur_idx + curLen] + ".", cur_idx + curLen, s, num_seg + 1)


if __name__ == "__main__":
    # s = "25525511135"
    s = "010010"
    print Solution().restoreIpAddresses(s)

    # ["255.255.11.135", "255.255.111.35"]