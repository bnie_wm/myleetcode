# -*- coding: utf-8 -*-

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def maxPathSum(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """

        self.max_path = float("-inf")

        # 返回值不是max_path
        # 而是包含root的最大路径和 -- either从左边来or从右边来
        self.dfs(root)
        return self.max_path

    def dfs(self, root):
        if not root:
            return 0

        left = self.dfs(root.left)
        right = self.dfs(root.right)

        max_path_include_root = max(left + root.val, right + root.val, 0)  # must limit by 0 -- 若和为负,则停止累加

        # update max_path
        self.max_path = max(left + root.val + right,  # 若root为拐点的和 (因为必须含至少一个点)
                            self.max_path)  # 若不含root的最大和

        return max_path_include_root


