"""
Time complexity: O(log(m+n))
Space complexity: O(log(m+n)) ??
"""

"""
Time complexity: O(log(m+n))
Space complexity: O(log(m+n)) ??
"""


class Solution(object):
    def findMedianSortedArrays(self, nums1, nums2):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :rtype: float
        """

        # convert to find the kth largest
        # if n%2 == 1: k = (n+1)/2, its index=n/2
        # if n%2 == 0: k = (n/2) and (n/2)+1, their index=(n-1)/2 and (n-1)/2+1

        n = len(nums1) + len(nums2)
        if n == 0:
            return None

        if n % 2 == 1:
            k = (n + 1) / 2
            return self.findKthLargest(k, 0, 0, nums1, nums2)
        else:
            k = n / 2
            return (self.findKthLargest(k, 0, 0, nums1, nums2) + self.findKthLargest(k + 1, 0, 0, nums1, nums2)) / 2.0

    # the index of the k-th largest element is (k - 1)!!!
    def findKthLargest(self, k, start1, start2, nums1, nums2):
        # either nums1[start1:] or nums2[start2:] is EMPTY!
        if start1 >= len(nums1):
            return nums2[start2 + k - 1]
        if start2 >= len(nums2):
            return nums1[start1 + k - 1]

        # recursion exiting case: k=1
        if k == 1:
            return min(nums1[start1], nums2[start2])

        # Divide:
        # 比较从start1/start2开始的第k/2个数 (idx=start1 + k/2-1), 丢弃较小的那个数所在的数组的前k/2个数
        # 更新k: k=k-k/2
        idx1 = start1 + k / 2 - 1
        val1 = nums1[idx1] if idx1 < len(nums1) else float("inf")
        idx2 = start2 + k / 2 - 1
        val2 = nums2[idx2] if idx2 < len(nums2) else float("inf")

        if val1 <= val2:  # 丢弃nums1的前k/2个数
            return self.findKthLargest(k - k / 2, start1 + k / 2, start2, nums1, nums2)
        else:
            return self.findKthLargest(k - k / 2, start1, start2 + k / 2, nums1, nums2)


if __name__ == "__main__":
    nums1 = [1]
    nums2 = [2, 3]
    print Solution().findMedianSortedArrays(nums1, nums2)