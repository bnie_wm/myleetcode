
### Stack
x = [1, 2, 3]
x.append(4)
print x
x.remove(2)  # remove the first occurrence of the value
print x
x.pop()  # remove the last occurrence
print x
x.pop(0)   # remove the element at idx 0
print x



# 1. sort
people = [[7,0], [4,4], [7,1], [5,0], [6,1], [5,2]]
people.sort(key=lambda x: (-x[0], x[1]))  # sort by 1st val, then by 2nd val

# 2. list insert
res = []
i, x = 3, 2
res.insert(i, x)  # insert x at position i, if i >= len(res) --> append
print res


# 3. argsort
import numpy as np
x = [1, 2, 3, 4, 5, 5, 4, 3, 3, 2, 0]
sorted_idx = np.argsort(x)
print sorted_idx
print [x[i] for i in sorted_idx]


# 4. bisect???


# ord
print ord("c")
print chr(ord("c"))


# heapq
# meeting_rooms2


### TriNode