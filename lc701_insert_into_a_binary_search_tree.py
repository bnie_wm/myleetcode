# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class SolutionIterative(object):
    def insertIntoBST(self, root, val):
        """
        :type root: TreeNode
        :type val: int
        :rtype: TreeNode
        """
        cur = root
        prev = None
        while cur:
            if val < cur.val:
                prev, cur = cur, cur.left
            else:
                prev, cur = cur, cur.right

        if val < prev.val:
            prev.left = TreeNode(val)
        else:
            prev.right = TreeNode(val)

        return root


class SolutionDivideConqur(object):
    def insertIntoBST(self, root, val):
        """
        :type root: TreeNode
        :type val: int
        :rtype: TreeNode
        """
        if root:
            self.dfs(root, val)
        return root

    def dfs(self, root, val):
        if not root.left and not root.right:
            if val < root.val:
                root.left = TreeNode(val)
            else:
                root.right = TreeNode(val)
            return

        if val < root.val:
            if root.left:
                self.dfs(root.left, val)
            else:
                root.left = TreeNode(val)
        else:
            if root.right:
                self.dfs(root.right, val)
            else:
                root.right = TreeNode(val)