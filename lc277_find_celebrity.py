# The knows API is already defined for you.
# @param a, person a
# @param b, person b
# @return a boolean, whether a knows b
# def knows(a, b):

class Solution(object):
    def findCelebrity(self, n):
        """
        :type n: int
        :rtype: int
        """

        if n <= 1:
            return -1

        # 1. find a candidate celebrity
        # call knows(a, b)=True, a cannot be celebrity; otherwise, b can't
        candidate = 0
        i = candidate + 1
        while i < n:
            if knows(candidate, i):
                # candidate += 1 --> wrong, as i-candidate can be greater than 1,
                # and people between candidate and i is already ruled out by "else" (not known by at least one person)
                candidate = i
                i = candidate + 1
            else:
                i += 1  # i cannot be candidate

        # 2. make sure every other knows candidate and candidate not know anyone
        # as in step1, people are ruled out by satisfying either "if" or "else"
        for i in xrange(n):
            if i == candidate:
                continue
            if knows(candidate, i) or not knows(i, candidate):
                return -1
        return candidate