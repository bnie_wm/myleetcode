# Given a m x n grid filled with non-negative numbers, find a path from top left to bottom right which minimizes the sum of all numbers along its path.
#
# Note: You can only move either down or right at any point in time.

"""
DP
Time: O(m*n)
Space: O(n)
"""
class Solution:
    def minPathSum(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        # init first row
        path_sum = grid[0]
        for j in xrange(1, len(grid[0])):
            path_sum[j] = path_sum[j-1] + grid[0][j]

        for i in xrange(1, len(grid)):
            # init first column
            path_sum[0] += grid[i][0]
            for j in xrange(1, len(grid[0])):
                path_sum[j] = min(path_sum[j], path_sum[j-1]) + grid[i][j]

        return path_sum[-1]


if __name__ == "__main__":
    print Solution().minPathSum([[0, 1], [1, 0]])
    print Solution().minPathSum([[1, 3, 1], [1, 5, 1], [4, 2, 1]])