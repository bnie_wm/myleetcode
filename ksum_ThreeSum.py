from collections import defaultdict


class Solution(object):
    '''
    Best solution: threeSum
    Also, solution for TwoSum (sorted numbers) --> function: twosum_sorted
    '''

    def threeSum(self, numbers=[1, 0, -1, -1, -1, -1, 0, 1, 1, 1]):
        '''
        Time: O(n^2)
        Space: O(1) --> Two pointers
        '''
        target = 0
        numbers = sorted(numbers)  ## O(nlogn) --> sort
        results = set()
        for i in range(len(numbers)):
            m, n = i+1, len(numbers)-1
            while m < n:
                this_sum = numbers[m] + numbers[n] + numbers[i]
                if this_sum == target:
                    results.add((numbers[i], numbers[m], numbers[n]))
                    # handle duplicates
                    if m+1 < n and numbers[m+1] == numbers[m]:
                        results.add((numbers[i], numbers[m+1], numbers[n]))
                    if m < n-1 and numbers[n-1] == numbers[n]:
                        results.add((numbers[i], numbers[m], numbers[n-1]))
                    m += 1
                    n -= 1
                elif this_sum > target:
                    n -= 1
                else:
                    m += 1
        return list(results)

    def threeSum_2(self, numbers):
        '''
        Time: O(n^2)
        Space: O(n) --> Two pointers to determine TwoSum, but use list to store idx pairs
        '''
        target = 0
        numbers = sorted(numbers)  ## O(nlogn) --> sort
        results = set()
        for i in range(len(numbers)):
            idx_pair_list = self.twosum_sorted(numbers, target - numbers[i])  ## find two numbers that sum with (target - this_num)
            for (idx1, idx2) in idx_pair_list:
                if idx1 is not None and i != idx1 and i != idx2:
                    results.add(tuple(sorted([numbers[idx1], numbers[idx2], numbers[i]])))
        return list(results)

    def twosum_sorted(self, sorted_numbers, target):
        """
        Two Pointers Solution for "2SUM -- sorted"
        Time: O(n)
        Space: O(n^2) --> store results
        However, for TwoSum problem, assume exactly ONE solution. So no need to store results --> Space: O(1)
        """
        i, j = 0, len(sorted_numbers) - 1
        idx_pair_list = []
        while i < j:
            this_sum = sorted_numbers[i] + sorted_numbers[j]
            if this_sum == target:
                idx_pair_list.append((i, j))
                # handle duplicates
                if i+1 < j and sorted_numbers[i+1] == sorted_numbers[i]:
                    idx_pair_list.append((i+1, j))
                if i < j-1 and sorted_numbers[j-1] == sorted_numbers[j]:
                    idx_pair_list.append((i, j-1))
                i += 1
                j -= 1
            elif this_sum > target:
                j -= 1
            else:
                i += 1
        return idx_pair_list

    def threeSum_1(self, numbers):
        '''
        Time: O(n^2) -- build HashTable
        Space: O(n^2) -- HashTable
        '''
        twosum = self.three_sum_hepler(numbers)
        results = set()
        for i in range(len(numbers)):  # O(n)
            for (idx1, idx2) in twosum[0 - numbers[i]]:  # O(1)
                if i != idx1 and i != idx2:
                    results.add(tuple(sorted([numbers[idx1], numbers[idx2], numbers[i]])))
        return list(results)

    def three_sum_hepler(self, numbers):  # O(n^2)
        twosum = defaultdict(list)

        for i in range(len(numbers)):
            for j in range(i+1, len(numbers)):
                this_sum = numbers[i] + numbers[j]
                twosum[this_sum].append((i, j))
        return twosum


if __name__ == "__main__":
    # numbers = [-1, 0, 1, 2, -1, -4]
    numbers = [-2, -3, 5, -1, -4, 5, -11, 7, 1, 2, 3, 4, -7, -1, -2, -3, -4, -5]
    # numbers = [1, 0, -1, -1, -1, -1, 0, 1, 1, 1]
    print Solution().threeSum(numbers)

