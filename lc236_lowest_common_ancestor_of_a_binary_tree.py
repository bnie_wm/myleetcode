# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def lowestCommonAncestor(self, root, p, q):
        """
        :type root: TreeNode
        :type p: TreeNode
        :type q: TreeNode
        :rtype: TreeNode
        """
        return self.dfs(root, p, q)

    def dfs(self, root, p, q):
        if not root:
            return None

        # 若root是p or q -- 则 root = LCA(p, q) = p or q
        if root.val == p.val or root.val == q.val:
            return root

        # 否则, 判断p, q在哪部分的子树
        left = self.dfs(root.left, p, q)
        right = self.dfs(root.right, p, q)

        if left and right:  # 若左右子树各一个, LCA(p, q) = root
            return root
        else:
            return left if not right else right