class Solution(object):
    def findMin(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if not nums:
            return None
        if len(nums) == 1:
            return nums[0]

        left, right = 0, len(nums) - 1
        while left <= right:
            mid = (left + right) / 2
            if nums[left] <= nums[mid]:  # left is normally ordered
                if nums[mid] <= nums[right]:  # right is also normally ordered
                    return nums[left]
                else:
                    left = mid + 1  # search right half
            else:  # right is normally ordered
                right = mid


if __name__ == "__main__":
    # nums = [4,5,6,7,0,1,2]
    nums = [2, 1]
    print Solution().findMin(nums)