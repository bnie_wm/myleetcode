# -*- coding: utf-8 -*-
# http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=438906&extra=page%3D1%26filter%3Dsortid%26sortid%3D311%26sortid%3D311
# 1. 给出一个数组，要求两这个数组分为两个部分，分别对两个部分求和，使两个和的绝对差值最小，输出这个最小差值
#     eg, [1,2,3,4,5]，可以将它分为[1, 2, 4]和[3, 5]两个部分，对应的和分别为7， 8输出1

class Solution(object):
    def subsets(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """

        if not nums:
            return 0

        return self.dfs_helper(0, [], nums, sum(nums))

    def dfs_helper(self, pos, res, nums, tot_sum):
        cur_sum = sum(res)
        cur_diff = abs(cur_sum - (tot_sum - cur_sum))

        for i in xrange(pos, len(nums)):
            if len(res) >= len(nums) / 2:
                continue
            _diff = self.dfs_helper(i + 1, res + [nums[i]], nums, tot_sum)
            cur_diff = min(cur_diff, _diff)
        return cur_diff


### Backpack DP
class Solution2(object):
    def subsets(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """

        if not nums:
            return 0

        tot_sum = sum(nums)
        min_diff = float("inf")
        prev_sums = [0]  # first i-1 elements --> all possible sum
        for i in xrange(len(nums)):
            cur_sums = []  # cur_dp: prev_sum + 1
            for prev in prev_sums:
                cur = prev + nums[i]
                if cur not in cur_sums:
                    cur_sums.append(cur)
                min_diff = min(min_diff, abs(cur - (tot_sum - cur)))
            prev_sums.extend(cur_sums)

        return min_diff


if __name__ == "__main__":
    nums = [1, 88, 79, 2]

    s = Solution()
    print s.subsets(nums=nums)

    s = Solution2()
    print s.subsets(nums=nums)
