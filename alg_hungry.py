# -*- coding: utf-8 -*-


def hungry():
    # https://blog.csdn.net/sunny_hun/article/details/80627351
    # lines[i][j]=1表示男孩i对女孩j有意向
    lines = [[1, 1, 0, 0], [0, 1, 1, 0], [1, 1, 0, 0], [0, 0, 1, 0]]
    m, n = len(lines), len(lines[0])  # m个男孩, n个女孩

    tot_match = 0
    girls = [-1] * n  # girls[j]=i, 说明男孩i与女孩j匹配
    for i in xrange(m):  # 遍历每个男孩
        visited = [False] * n   # 用来就本次DFS是否尝试过改变该女孩的匹配问题 (不能re-visit)
        if find(i, lines, visited, girls, n):  # 为男孩i找匹配
            tot_match += 1

    print girls
    print tot_match


def find(x, lines, visited, girls, n):
    for j in xrange(n):  # 遍历每个女孩
        # 如果男孩x对女孩j有意向, 且女孩j不是上层DFS尝试过要改变匹配的
        if lines[x][j] == 1 and not visited[j]:
            visited[j] = True  # 标记本层尝试了女孩j
            # 如果女孩j没有被匹配, 或者可以给女孩的匹配girls[j]找到新的匹配
            if girls[j] == -1 or find(girls[j], lines, visited, girls, n):
                girls[j] = x
                return True
    return False


if __name__ == "__main__":
    hungry()

