class Solution(object):
    def maxProfit(self, prices):
        """
        :type prices: List[int]
        :rtype: int
        """
        n = len(prices)
        if n <= 1:
            return 0

        cooldown = 1
        profit = [0] * n
        for i in xrange(n):
            for j in xrange(i):
                # compare:
                # (1) profit[i]
                # (2) profit[j] -- no operation in [j, i]
                # (3) dp[j-2]+prices[i]-prices[j] -- dp[j-2], cooldown, then buy at j sell at i
                prev_profit = profit[j - cooldown - 1] if j - cooldown - 1 >= 0 else 0
                profit[i] = max(profit[i], profit[j], prev_profit + prices[i] - prices[j])
        return max(profit)