class Solution(object):
    def rotate(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: void Do not return anything, modify nums in-place instead.
        """
        # 1. reverse whole array
        # 2. reverse first k
        # 3. reverse last n-k
        if len(nums) <= 1:
            return

        if k >= len(nums):  # i.e., [1,2], k=2: don't need to do anything
            return

        self.reverseArray(nums, 0, len(nums) - 1)
        if k == 0:  # i.e., [1, 2], k=0: just need to reverse the whole array
            return
        self.reverseArray(nums, 0, k)
        self.reverseArray(nums, k + 1, len(nums) - 1)

    def reverseArray(self, nums, i, j):
        while i < j:
            nums[i], nums[j] = nums[j], nums[i]
            i += 1
            j -= 1


if __name__ == "__main__":
    nums = [1, 2]
    k = 0
    Solution().rotate(nums, k)
    print nums