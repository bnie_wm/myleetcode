# Definition for a  binary tree node
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class BSTIterator(object):
    def __init__(self, root):
        """
        :type root: TreeNode
        """
        self.stack = [(root, False)] if root else []

    def hasNext(self):
        """
        :rtype: bool
        """
        return len(self.stack) != 0

    def next(self):
        """
        :rtype: int
        """
        while self.stack:
            cur, visited = self.stack.pop()
            if visited:
                return cur.val
            else:
                if cur.right:
                    self.stack.append((cur.right, False))
                self.stack.append((cur, True))
                if cur.left:
                    self.stack.append((cur.left, False))

# Your BSTIterator will be called like this:
# i, v = BSTIterator(root), []
# while i.hasNext(): v.append(i.next())