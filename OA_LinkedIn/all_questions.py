import collections
import heapq


class Solution():
    # int array, sort first by frequency, then by value.
    def sort_array(self, nums):
        freq = collections.Counter(nums)

        max_freq = 0
        bucket = collections.defaultdict(list)
        for k in freq:
            bucket[freq[k]].append(k)
            max_freq = max(max_freq, freq[k])

        res = []
        for freq in xrange(1, max_freq+1):
            for num in bucket[freq]:
                res += [num] * freq

        return res


if __name__ == "__main__":
    s = Solution()

    nums = [3, 3, 1, 6, 6, 1, 1, 2, 3]
    print s.sort_array(nums)