# -*- coding: utf-8 -*-

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

#### 同LC124 -- 共同点: 可不含root, 所以D&G不能直接返回题目所求
#### 而是返回必须含有root的所求
class Solution(object):
    def diameterOfBinaryTree(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        self.diameter = 0
        self.dfs(root)
        return self.diameter

    # 返回必须含有root的diameter
    def dfs(self, root):
        if not root:
            return 0

        left = self.dfs(root.left)
        right = self.dfs(root.right)

        # 更新:含有root的diameter
        diameter_include_root = max(left + 1, right + 1)

        self.diameter = max(left + right, self.diameter)

        return diameter_include_root
