class Solution(object):
    def numTrees(self, n):
        """
        :type n: int
        :rtype: int
        """

        #### DP
        if n <= 1:
            return 1

        dp = [0] * (n + 1)
        dp[0], dp[1] = 1, 1
        for i in xrange(2, n + 1):  # i为整个树的节点个数
            # j为root
            for j in xrange(1, i + 1):
                dp[i] += dp[j - 1] * dp[i - j]
        return dp[n]

        #### [DP+Memo]
#         # memo[i]: n=i时的unique BST个数
#         memo = [-1] * (n+1)
#         memo[1] = 1
#         return self.dfs(1, n, memo)

#     def dfs(self, left, right, memo):
#         if memo[right - left + 1] != -1:
#             return memo[right - left + 1]

#         count = 0
#         for i in xrange(left, right+1):
#             if i == left:
#                 count += self.dfs(i+1, right, memo)
#             elif i == right:
#                 count += self.dfs(left, i-1, memo)
#             else:
#                 count += self.dfs(left, i-1, memo) * self.dfs(i+1, right, memo)
#         memo[right - left + 1] = count
#         return count