import heapq


def MAX_Heapify(heap, HeapSize, root):
    left, right = 2 * root + 1, 2 * root + 2
    larger = root
    if left < HeapSize and heap[left] > heap[larger]:
        larger = left
    if right < HeapSize and heap[right] > heap[larger]:
        larger = right
    if larger != root:
        heap[larger], heap[root] = heap[root], heap[larger]
        MAX_Heapify(heap, HeapSize, larger)


def Build_MAX_Heap(heap):
    HeapSize = len(heap)
    for i in xrange(HeapSize/2-1, -1, -1):  # from mid to begin
        MAX_Heapify(heap, HeapSize, i)


def HeapSort(heap):
    Build_MAX_Heap(heap)
    for i in xrange(len(heap)-1, -1, -1):
        heap[0], heap[i] = heap[i], heap[0]
        MAX_Heapify(heap, i, 0)
    return heap


class PriorityQueue(object):
    def __init__(self):
        self._queue = []
        self._index = 0

    def push(self, item, priority):
        heapq.heappush(self._queue, (-priority, self._index, item))
        self._index += 1

    def pop(self):
        return heapq.heappop(self._queue)[-1]


def main():
    # intervals = [[0, 30],[5, 10],[15, 20], [10,2]]
    # print intervals
    #
    # sorted_intervals = heapq.heapify(intervals, key=lambda x:x[0])
    # print intervals
    # print sorted_intervals

    # a = [30, 50, 57, 77, 62, 78, 94, 80, 84]
    # HeapSort(a)
    # print a

    q = PriorityQueue()
    q.push("a", 1)
    q.push("c", 3)
    q.push("b", 2)
    print q._queue


if __name__ == "__main__":
    main()