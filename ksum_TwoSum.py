# Given an array of integers, return indices of the two numbers such that they add up to a specific target.
# You may assume that each input would have ***exactly*** one solution.
#
# Example:
# Given nums = [2, 7, 11, 15], target = 9,
# Because nums[0] + nums[1] = 2 + 7 = 9,
# return [0, 1].
#
# UPDATE (2016/2/13):
# The return format had been changed to zero-based indices. Please read the above updated description carefully.


class Solution(object):

    def twosum_sorted(self, nums, target):
        i, j = 0, len(nums) - 1
        while i < j:
            a, b = nums[i], nums[j]
            if a+b == target:
                return [i, j]
            elif a+b > target:
                j -= 1
            else:
                i += 1
        return None

    def twoSum(self, nums, target):
        num_dict = dict()
        for i in range(len(nums)):
            if nums[i] in num_dict:
                return [num_dict[nums[i]], i]
            else:
                num_dict[target - nums[i]] = i


if __name__ == "__main__":
    nums = [3, 2, 4]
    target = 6
    print Solution().twoSum(nums, target)

    nums = [2, 3, 4]
    print Solution().twosum_sorted(nums, target)