# -*- coding: utf-8 -*-

import collections

#### step1: BFS -- O(len(wordList) * len(word) * 26) --> build prev_words for each shortest path
#### step2: DFS -- trace back to get paths
class Solution(object):
    def findLadders(self, beginWord, endWord, wordList):
        """
        :type beginWord: str
        :type endWord: str
        :type wordList: List[str]
        :rtype: List[List[str]]
        """
        if endWord not in wordList:
            return []

        letters = [chr(ord('a') + i) for i in xrange(26)]

        #### O(len(wordList) * len(word) * 26)
        #### Find shortest path in a graph
        #### all edges: weight=1 -- BFS O(n)
        #### edges with diff. weights -- BFS/Dijkstra O(nlogn)
        prev_words = collections.defaultdict(list)
        visited = {}
        queue = collections.deque([(1, beginWord)])  # (dist, word) -- 简单图使用deque即可,不需要维护min heap
        while queue:
            cur_dist, cur_word = queue.popleft()
            if cur_word in visited:
                continue
            visited[cur_word] = cur_dist

            neighbours = self.findNeighbouringWords(cur_word, wordList, visited, letters)  # O(len(word) * 26)
            nei_dist = cur_dist + 1
            for nei_word in neighbours:
                queue.append((nei_dist, nei_word))

                # update nei_word's prev_word
                if nei_word not in prev_words:
                    prev_words[nei_word] = [cur_word]
                else:
                    # not a newly reached word (not marked visited, but added to queue)
                    # need compare with its last prev_word: append when meet equal length path
                    old_prev_word = prev_words[nei_word][0]
                    old_prev_dist = visited[old_prev_word] + 1
                    if nei_dist == old_prev_dist:
                        prev_words[nei_word].append(cur_word)

        if endWord not in visited:
            return []  # unreachable

        #### DFS trace back: endWord --> beginWord
        results = []
        self.traceback(endWord, beginWord, [endWord], results, prev_words)
        return results

    def traceback(self, cur, tgt, res, results, graph):
        if cur == tgt:
            results.append(res[::-1])
        for nei in graph[cur]:
            self.traceback(nei, tgt, res + [nei], results, graph)

    def findNeighbouringWords(self, word, wordList, visited, letters):
        neighbours = []
        for i in xrange(len(word)):  # change one char per time
            for char in letters:
                if char != word[i]:
                    this_word = word[:i] + char + word[i + 1:]
                    if this_word in wordList and this_word not in visited:
                        neighbours.append(this_word)
        return neighbours


if __name__ == "__main__":
    #### begin = "ab", end = "ef"
    #### wordList = ["ac","ad","ec","ed","ef"]

    s = Solution()
    beginWord, endWord = "ab", "ef"
    wordList = ["ac","ad","ec","ed","ef"]
    print s.findLadders(beginWord, endWord, wordList)