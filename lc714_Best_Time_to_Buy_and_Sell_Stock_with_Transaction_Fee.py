class Solution(object):
    def maxProfit(self, prices, fee):
        """
        :type prices: List[int]
        :type fee: int
        :rtype: int
        """

        n = len(prices)
        if n <= 1:
            return 0

        # #### DP-1 - O(n^2) time & O(n) space -- TLE
        # # profit[i]: max profit in range [0, i] (not neccessary to sell at i)
        # profit = [0] * n
        # for i in xrange(1, n):
        #     for j in xrange(i):
        #         # compare: profit[i], profit[j]+_profit, and profit[j] (no transaction in [j, i])
        #         # (if only allow ONE transaction: max(profit[i], _profit, profit[j])
        #         _profit = prices[i] - prices[j] - fee
        #         profit[i] = max(profit[i], profit[j] + _profit, profit[j])
        # return max(profit)

        # #### DP-2 -- O(n) Time & O(n) Space
        # buy = [-prices[0]] + [0] * (n-1)
        # sell = [0] * n
        # for i in xrange(1, n):
        #     # 今天不买 buy[i-1] vs. 今天买 sell[i-1] - prices[i] (之前有卖，所以是sell[i-1])
        #     buy[i] = max(buy[i-1], sell[i-1] - prices[i])
        #     # 今天不卖 sell[i-1] vs. 今天卖 buy[i-1] + prices[i] - fee （卖的时候付交易费）(之前必有买，所以是buy[i-1])
        #     sell[i] = max(sell[i-1], buy[i-1] + prices[i] - fee)
        # return max(sell)

        #### DP-3 -- O(n) Time & O(1) Space (优化DP-2)
        max_buy, max_sell = -prices[0], 0
        for i in xrange(1, n):
            # 今天不买 buy[i-1] vs. 今天买 sell[i-1] - prices[i] (之前有卖，所以是sell[i-1])
            this_buy = max(max_buy, max_sell - prices[i])
            # 今天不卖 sell[i-1] vs. 今天卖 buy[i-1] + prices[i] - fee （卖的时候付交易费）(之前必有买，所以是buy[i-1])
            this_sell = max(max_sell, max_buy + prices[i] - fee)
            max_buy, max_sell = this_buy, this_sell
        return max_sell