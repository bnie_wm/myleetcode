class Solution(object):
    def minCut(self, s):
        """
        :type s: str
        :rtype: int
        """
        n = len(s)
        if n <= 1:
            return 0

        # DP-1: O(n^2) time and space
        # df_palindrome[i][j] = 1 --> s[i:j+1] is palinedrome
        dp_palindrome = self.buildPalindromeCheckBoard(s)
        # print(dp_palindrome)

        # DP-2: O(n^2)
        # dp_cut[i] = min cuts needed for s[:i]
        dp_cut = [float("inf")] * (n + 1)
        dp_cut[0] = -1

        for i in xrange(1, n + 1):
            for j in xrange(i):
                # if self.isPalindrome(j, i, s):  # 直接计算 -- O(n) too slow
                if dp_palindrome[j][i - 1]:  # 查DP -- O(1）
                    dp_cut[i] = min(dp_cut[i], dp_cut[j] + 1)
        return dp_cut[n]


    def buildPalindromeCheckBoard(self, s):
        n = len(s)

        # dp[i][j]=1 --> s[i:j+1] is palindrome
        dp = [[0 for _ in xrange(n)] for _ in xrange(n)]
        for i in xrange(n):
            dp[i][i] = 1  # s[i:i+1] always is palindrome

        for i in xrange(n):
            # 递归公式：若dp[i][j]=1,且s[i-1]==s[j+1],则dp[i-1][j+1]=1
            # (1) 以s[i]为中心，
            for k in xrange(1, n / 2 + 1):  # check if s[i-k:i+k+1] is palindrome
                if i - k >= 0 and i + k < n and s[i - k] == s[i + k]:
                    if dp[i - k + 1][i + k - 1] == 1:
                        dp[i - k][i + k] = 1
            # (2) 以s[i:i+2]为中心，
            # (若不确定，可以xrange(n/2+1),第一层if会过滤掉过界的k值)
            for k in xrange(n / 2):  # check if s[i-k][i+k+1] is palindrome
                if i - k >= 0 and i + k + 1 < n and s[i - k] == s[i + k + 1]:
                    if k == 0 or dp[i - k + 1][i + k] == 1:
                        dp[i - k][i + k + 1] = 1
        return dp


    def isPalindrome(self, left, right, s):
        i, j = left, right - 1
        while i < j:
            if s[i] != s[j]:
                return 0
            i += 1
            j -= 1
        return 1



