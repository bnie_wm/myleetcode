# -*- coding: utf-8 -*-

class Solution(object):
    def nextClosestTime(self, time):
        """
        :type time: str
        :rtype: str
        """
        #### Brute Force -- O(60*24)=O(1440)
        digits = list(time.replace(":", ""))
        cur_hour, cur_min = int(time[:2]), int(time[-2:])
        while True:
            # add 1min per time
            cur_min += 1
            if cur_min == 60:
                cur_min = 0
                cur_hour += 1
            if cur_hour == 24:
                cur_hour = 0

            # check if cur_hour and cur_min is valid
            is_valid = True
            #### "{:0>3}".format(cur_hour) -- 1 --> 001 (三位,左补0)
            #### "{:0<3}".format(cur_hour) -- 1 --> 100 (三位, 右补0)
            #### 0可以替换为任意符号, 如果没有, 则补空格
            cur_time = "{:0>2}:{:0>2}".format(cur_hour, cur_min)
            for char in cur_time:
                if char != ":" and char not in digits:
                    is_valid = False
                    break
            if is_valid:
                return cur_time


if __name__ == "__main__":
    print Solution().nextClosestTime(time="23:59")
    print Solution().nextClosestTime(time="01:32")
