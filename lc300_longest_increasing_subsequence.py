"""
DP:
Time: O(n^2)
Space: O(n)
"""


class Solution(object):
    def lengthOfLIS(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if not nums:
            return 0
        # e.g., [1,3,6,7,9,4,10,5,6]
        dp = [1] * len(nums)  # at least LIS=1
        for i in xrange(1, len(nums)):
            for j in xrange(i):  # 比较每个断点，找最长的加1， 例如[1,3,6,7,9,10](i=6,j=4),而不是[1,3,4,10](i=6,j=5)
                if nums[i] > nums[j]:  # 此时，seq[0...j]的LIS增加了一个长度
                    dp[i] = max(dp[j] + 1, dp[i])
        # return dp[-1] --> wrong!
        return max(dp)  # max LIS为以10结尾,而不是dp[-1]


