# -*- coding: utf-8 -*-

class Solution(object):
    def search(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        if not nums:
            return False

        left, right = 0, len(nums) - 1
        while left + 1 < right:
            mid = left + (right - left) / 2

            # 无法判断哪边有序
            # e.g, 4 4 4 4 4 1 2 3 4: left=0, right=8, mid=4
            if nums[left] == nums[mid] == nums[right]:
                if nums[mid] == target:
                    return True
                else:
                    left += 1
                    right -= 1

            # 左边是有序的
            elif nums[left] <= nums[mid]:
                if nums[left] <= target <= nums[mid]:
                    right = mid
                else:
                    left = mid

            # 右边是有序的: nums[mid] <= nums[right]
            else:
                if nums[mid] <= target <= nums[right]:
                    left = mid
                else:
                    right = mid

        if nums[left] == target or nums[right] == target:
            return True

        return False


if __name__ == "__main__":
    nums = [4, 4, 4, 4, 4, 1, 2, 3, 4]
    print Solution().search(nums=nums, target=3)