# -*- coding: utf-8 -*-

# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
    def constructFromPrePost(self, pre, post):
        """
        :type pre: List[int]
        :type post: List[int]
        :rtype: TreeNode
        """
        if not pre:
            return None

        return self.dfs(pre, post)

    def dfs(self, pre, post):
        if not pre:
            return None
        elif len(pre) == 1:
            return TreeNode(pre[0])

        root = TreeNode(pre[0])  # root=pre[0]=post[-1]
        left, right = pre[1], post[-2]

        # 若left==right, 说明只有左子树 或 只有右子树 (但是仅通过先序和后序判断不出来, 所以默认只有左子树)
        if left == right:
            root.left = self.dfs(pre[1:], post[:-1])
        else:
            idx_left_in_pre, idx_right_in_pre = pre.index(left), pre.index(right)
            idx_left_in_post, idx_right_in_post = post.index(left), post.index(right)

            # nodes belong to root left sub-tree:
            root.left = self.dfs(pre[idx_left_in_pre:idx_right_in_pre], post[:idx_left_in_post + 1])

            if left != right:
                # nodes belong to root right sub-tree:
                root.right = self.dfs(pre[idx_right_in_pre:], post[idx_left_in_post + 1:idx_right_in_post + 1])

        return root


if __name__ == "__main__":
    pre = [3, 9, 20, 15, 1, 2, 7]
    post = [9, 1, 2, 15, 7, 20, 3]
    root = Solution().constructFromPrePost(pre, post)


    # root = pre[0] = post[-1]
    # left_tree_root = pre[1]
    # right_tree_root = post[-2]

    # left_tree_preorder: 9
    # left_tree_postorder: 9

    # right_tree_preorder: 20 15 1 2 7
    # right_tree_postorder: 1 2 15 7 20


