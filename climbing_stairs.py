# You are climbing a stair case. It takes n steps to reach to the top.
#
# Each time you can either climb 1 or 2 steps. In how many distinct ways can you climb to the top?

"""
DP
Time: O(n)
Space: O(1)
"""
class Solution2(object):
    def climbStairs(self, n):
        """
        :type n: int
        :rtype: int
        # """
        prev, cur = 0, 1  # init: for n=0: prev=0, cur=1
        for i in xrange(1, n+1):  # iterate 1 --> n
            prev, cur = cur, prev + cur

        return cur


"""
DP
f(n) = f(n-1) + f(n-2)
Time: O(n)
Space: O(n)
"""
class Solution(object):
    def climbStairs(self, n):
        """
        :type n: int
        :rtype: int
        """

        # num[0] = 0
        # num[1] = 1
        # num[2] = 2
        num = [0, 1, 2]
        for i in xrange(3, n+1):
            num.append(num[i-2] + num[i-1])
        return num[n]


if __name__ == "__main__":
    n = 30
    print Solution().climbStairs(n)
    print Solution2().climbStairs(n)
