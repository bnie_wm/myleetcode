# -*- coding: utf-8 -*-
# http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=438906&extra=page%3D1%26filter%3Dsortid%26sortid%3D311%26sortid%3D311
# 1. 给出一个数组，要求两这个数组分为两个部分，分别对两个部分求和，使两个和的绝对差值最小，输出这个最小差值
#     eg, [1,2,3,4,5]，可以将它分为[1, 2, 4]和[3, 5]两个部分，对应的和分别为7， 8输出1

class Solution(object):
    def subsets(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """

        if not nums:
            return 0

        min_diff = [float("inf")]
        self.dfs_helper(0, [], nums, sum(nums), min_diff)
        return min_diff[0]

    def dfs_helper(self, pos, res, nums, tot_sum, min_diff):
        cur_sum = sum(res)
        cur_diff = abs(cur_sum - (tot_sum - cur_sum))
        min_diff[0] = min(min_diff[0], cur_diff)
        print res, cur_diff

        for i in xrange(pos, len(nums)):
            if len(res) >= len(nums)/2:
                continue
            self.dfs_helper(i + 1, res + [nums[i]], nums, tot_sum, min_diff)



if __name__ == "__main__":
    nums = [1, 2, 3, 4, 5]

    s = Solution()
    print s.subsets(nums=nums)

    s = Solution2()
    print s.subsets(nums=nums)
