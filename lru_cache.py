# Design and implement a data structure for Least Recently Used (LRU) cache.
# It should support the following operations: get and set.
#
# get(key) - Get the value (will always be positive) of the key if the key exists in the cache, otherwise return -1.
# set(key, value) - Set or insert the value if the key is not already present.
#   When the cache reached its capacity, it should invalidate the least recently used item before inserting a new item.

class ListNode(object):
    def __init__(self, key, value):
        self.key = key
        self.value = value
        self.next = None
        self.prev = None


# double linked list --> work as a LIFO queue
# head --> least used
# tail --> most recently used
class LinkedList(object):
    def __init__(self):
        self.head = None
        self.tail = None

    def insert(self, node):
        if self.head is None:
            self.head = node
        else:
            self.tail.next = node
            node.prev = self.tail
        self.tail = node

    def delete(self, node):
        if node.prev:
            node.prev.next = node.next
        else:
            self.head = node.next
        if node.next:
            node.next.prev = node.prev
        else:
            self.tail = node.prev

    def pop(self):
        self.delete(self.head)


class LRUCache(object):
    def __init__(self, capacity):
        """
        :type capacity: int
        """
        self.list = LinkedList()
        self.cache = dict()
        self.capacity = capacity

    def _insert(self, key, value):
        node = ListNode(key, value)
        self.list.insert(node)  # insert to list first (assing node.next and node.prev)
        self.cache[key] = node  # then, insert to cache

    def get(self, key):
        """
        :rtype: int
        """
        if key in self.cache:
            val = self.cache[key].value
            # re-push to tail
            self.list.delete(self.cache[key])
            # note that, need to re-insert for both list and cache (since node.next and node.prev changed!)
            self._insert(key, val)
            return val
        else:
            return -1

    def set(self, key, value):
        """
        :type key: int
        :type value: int
        :rtype: nothing
        """
        if key in self.cache:
            self.list.delete(self.cache[key])
        elif len(self.cache) == self.capacity:
            del self.cache[self.list.head.key]  # remove from dict!!!
            self.list.pop()  # pop top (LRU element)
        self._insert(key, value)



import collections
class LRUCache2(object):
    def __init__(self, capacity):
        """
        :type capacity: int
        """
        self.cache = collections.OrderedDict()
        self.capacity = capacity

    def get(self, key):
        """
        :rtype: int
        """
        if key in self.cache:
            val = self.cache[key]
            del self.cache[key]
            self.cache[key] = val
            return val
        else:
            return -1

    def set(self, key, value):
        """
        :type key: int
        :type value: int
        :rtype: nothing
        """
        if key in self.cache:
            del self.cache[key]
        elif len(self.cache) == self.capacity:
            self.cache.popitem(last=False)   # if last=True: LIFO; if last=False: FIFO
        self.cache[key] = value



if __name__ == "__main__":
    lru = LRUCache2(3)
    lru = LRUCache(3)
    lru.set(1, 2)
    print lru.cache
    print lru.get(1)
    lru.set(1, 3)
    print lru.cache
    lru.set(2, 10)
    print lru.cache
    lru.set(3, -2)
    print lru.cache
    lru.set(2, 12)
    print lru.cache
    print lru.get(1)
    print lru.cache
