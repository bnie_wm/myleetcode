# -*- coding: utf-8 -*-

class Solution(object):
    def longestMountain(self, A):
        """
        :type A: List[int]
        :rtype: int
        """

        n = len(A)
        if n < 3:
            return 0

        largest_mountain = 0
        i = 0
        while i < n:
            # 找到左边第一个最低点, 是第一个mountain的左边界
            while i < n - 1 and A[i] >= A[i + 1]:
                i += 1

            if i >= n - 2:  # mountain至少有三个点, 所以不可能再有mountain了
                break

            left = i

            # 找最高点
            while i < n - 1 and A[i] < A[i + 1]:
                i += 1
            peak = i

            # 找右边界
            while i < n - 1 and A[i] > A[i + 1]:
                i += 1
            right = i

            if A[peak] > A[right]:
                largest_mountain = max(largest_mountain, right - left + 1)

            i = right  # reset the left of the next mountain

        return largest_mountain


if __name__ == "__main__":
    A = [2,1,4,7,3,2,5]
    print Solution().longestMountain(A)

    A = [1, 1, 2, 3, 3, 1, 2, 1, 2]
    print Solution().longestMountain(A)