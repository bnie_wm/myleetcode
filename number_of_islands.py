## Given a boolean 2D matrix, find the number of islands.
## 0 is represented as the sea, 1 is represented as the island.
## If two 1 is adjacent, we consider them in the same island. We only consider up/down/left/right adjacent.

"""
DFS
Time: O(m*n)
Space: O(m*n)
"""


class UnionFind(object):
    def __init__(self, k):  # k = m*n, total num of nodes in the 2D grid
        self.parent = [-1] * k  # -1 means it is water not land
        self.num_islands = 0

    def find(self, x):
        if self.parent[x] != x:
            self.parent[x] = self.find(self.parent[x])
        return self.parent[x]

    def union(self, x, y):
        rx, ry = self.find(x), self.find(y)
        if rx != ry:
            self.parent[min(rx, ry)] = max(rx, ry)
            self.num_islands -= 1

    def addLand(self, x):
        if self.parent[x] == -1:
            self.parent[x] = x  # change water to land
            self.num_islands += 1

    def isLand(self, x):
        return self.parent[x] != -1


class Solution2(object):
    def numIslands(self, grid):
        """
        :type grid: List[List[str]]
        :rtype: int
        """
        if not grid:
            return 0

        m, n = len(grid), len(grid[0])
        uf = UnionFind(m * n)
        directions = [(-1, 0), (0, -1)]  # only need to look at two directions (left and up)
        for i in xrange(m):
            for j in xrange(n):
                if grid[i][j] == '1':
                    uf.addLand(i * n + j)
                    for d in directions:
                        x, y = i + d[0], j + d[1]
                        if 0 <= x < m and 0 <= y < n and grid[x][y] == '1':
                            uf.union(i * n + j, x * n + y)
        return uf.num_islands


class Solution(object):

    # @param {boolean[][]} grid a boolean 2D matrix
    # @return {int} an integer
    def numIslands(self, grid):
        if not grid:
            return 0

        n_row = len(grid)
        n_col = len(grid[0])
        visited = [[False for j in xrange(n_col)] for i in xrange(n_row)]

        cnt = 0
        for i in xrange(n_row):
            for j in xrange(n_col):
                if grid[i][j] == 1 and not visited[i][j]:
                    self.dfs(grid, visited, n_row, n_col, i, j)
                    cnt += 1
        return cnt

    def dfs(self, grid, visited, n_row, n_col, x, y):
        if grid[x][y] == 0 or visited[x][y]:
            return
        visited[x][y] = True

        if x != 0:
            self.dfs(grid, visited, n_row, n_col, x-1, y)
        if x != n_row-1:
            self.dfs(grid, visited, n_row, n_col, x+1, y)
        if y != 0:
            self.dfs(grid, visited, n_row, n_col, x, y-1)
        if y != n_col-1:
            self.dfs(grid, visited, n_row, n_col, x, y+1)


if __name__ == "__main__":
    grid = [[1,1,0,0,0],[0,1,0,0,1],[0,0,0,1,1],[0,0,0,0,0],[0,0,0,0,1]]
    print Solution().numIslands(grid)