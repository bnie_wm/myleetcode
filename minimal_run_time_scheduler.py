# Facebook
# Given a task sequence tasks such as ABBABBC, and an integer k, which is the cool down time between two same tasks.
# Assume the execution for each individual task is 1 unit.
#
# For example, if k = 3, then tasks BB takes a total of 5 unit time to finish,
# because B takes 1 unit of time to execute, then wait for 3 unit,
# and execute the second B again for another unit of time. So 1 + 3 + 1 = 5.
#
# Given a task sequence and the cool down time, return the total execution time.
#
# Follow up: Given a task sequence and the cool down time,
# rearrange the task sequence such that the execution time is minimal.
#  --> LeetCode 358: Rearrange String K Distance Apart


class Solution(object):
    def scheduler(self, jobs, k):
        last = dict()  # save job's last finish time
        unit = 1  # job execution time
        finish_t = 0
        for i in xrange(len(jobs)):
            if jobs[i] not in last:
                finish_t += unit
                last[jobs[i]] = finish_t
            else:
                # if previously executed this job
                # check if we can arrange the job
                # (1) in the following slots: finish_t +  1
                # (2) or need to wait: last[jobs[i] + k] --> the earliest start time after its last execution
                finish_t = max(finish_t + 1, last[jobs[i]] + k) + unit
        return finish_t


if __name__ == "__main__":
    jobs = list("ABBABC")
    # jobs = list("BB")
    k = 3
    print Solution().scheduler(jobs, k)