# -*- coding: utf-8 -*-

class Solution(object):
    def majorityElement(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        # Boyer-Moor Vote: Time: O(n), Space: O(1)
        # 经过举了很多例子分析得出，任意一个数组出现次数大于n/3的众数最多有两个
        m1, m2 = None, None
        cnt1, cnt2 = 0, 0
        for num in nums:
            if num == m1:
                cnt1 += 1
            elif num == m2:
                cnt2 += 1
            elif cnt1 == 0:
                m1 = num
                cnt1 += 1
            elif cnt2 == 0:
                m2 = num
                cnt2 += 1
            else:
                cnt1 -= 1
                cnt2 -= 1

        cnt1, cnt2 = 0, 0
        for num in nums:
            if num == m1:
                cnt1 += 1
            elif num == m2:
                cnt2 += 1

        res = []
        n = len(nums)
        if cnt1 > n / 3:
            res.append(m1)
        if cnt2 > n / 3:
            res.append(m2)
        return res