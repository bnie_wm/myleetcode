class Solution(object):
    def findNumberOfLIS(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if not nums:
            return 0

        n = len(nums)

        #### DP-1: 求longest LIS
        # lis[i] = length of longest LIS for sequence nums[0:i+1]
        lis = [1] * n
        for i in xrange(1, n):
            for j in xrange(i):
                if nums[i] > nums[j]:
                    lis[i] = max(lis[j] + 1, lis[i])

        #### DP-2：求number of longest LIS (使用到了DP-1的结果lis)
        nlis = [0] * n  # num of longest LIS in sequence nums[:i+1]
        nlis[0] = 1
        # 错！反例:[1,3,5,4,7] -> nlis[3]不应该初始化为1
        # 这里的目标是找到seq nums[:i+1]中单个元素构成LIS的，将它的nlis附为1
        # --> 需要双重循环遍历，所以嵌套在DP-2中，即nlis[i]==0的条件
        # for i in xrange(1, len(nums)):
        #     if nums[i-1] >= nums[i]:
        #         nlis[i] = 1

        for i in xrange(1, n):
            for j in xrange(i):
                if nums[i] > nums[j] and lis[i] == lis[j] + 1:
                    nlis[i] += nlis[j]  # accumulate num of LIS in sequence nums[:j+1]
            # e.g., [1,1,1,2,2,3,4]
            # 内层循环结束后，nlis[1]仍为0 (说明不满足if条件，序列[:i+1]的LIS由nums[i]单个元素组成)
            # 需要初始化nlis[i]=1 (和初始化nlis[0]=1同理)
            # 其他例子：[2, 1]的nlis[1], [1,2,3,1,2,3,1,2,3]的nlis[3]和nlis[6]
            if nlis[i] == 0:
                nlis[i] = 1

        #### check all endings, 若nums[:i+1]的LIS==longest_lis, 累加nlis[i]
        longest_lis = max(lis)
        tot_cnt = 0
        for i in xrange(n):
            if lis[i] == longest_lis:
                tot_cnt += nlis[i]
        return tot_cnt


