# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def isValidBST(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        return self.dfs(root, [float("-inf"), float("inf")])

    def dfs(self, root, boundaries):
        if not root:
            return True

        if boundaries[0] < root.val < boundaries[1]:
            left = self.dfs(root.left, [boundaries[0], root.val])
            right = self.dfs(root.right, [root.val, boundaries[1]])
            return left and right
        else:
            return False



