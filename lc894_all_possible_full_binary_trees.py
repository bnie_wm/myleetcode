# -*- coding: utf-8 -*-
# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
    def allPossibleFBT(self, N):
        """
        :type N: int
        :rtype: List[TreeNode]
        """

        # full binary tree must contain 1, 3, 5, 7 ... nodes
        if N % 2 == 0:
            return []

        memo = [None] * (N + 1)
        memo[1] = [TreeNode(0)]
        return self.dfs(N, memo)

    def dfs(self, num_remaining, memo):
        if memo[num_remaining] is not None:
            return memo[num_remaining]

        res = []
        left = 1
        right = num_remaining - 1 - left
        while left <= right:
            left_trees = self.dfs(left, memo)
            right_trees = self.dfs(right, memo)

            for lt in left_trees:
                for rt in right_trees:
                    root = TreeNode(0)
                    root.left = lt
                    root.right = rt
                    res.append(root)

                    if left != right:  # 大小不等的左右子树, 反过来放就是另一种组合
                        root = TreeNode(0)
                        root.left = rt
                        root.right = lt
                        res.append(root)

            left += 2
            right = num_remaining - 1 - left

        memo[num_remaining] = res
        return res

