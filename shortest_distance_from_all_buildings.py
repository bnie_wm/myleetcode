"""
Time: O(k * m * n)
Space: O(m * n)
"""
class Solution(object):
    def shortestDistance(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        if not grid:
            return -1

        m, n = len(grid), len(grid[0])
        # visited[i][j] --> number of visited, if less than number of buildings, invalid position
        # dp[i][j] --> shortest sum distance
        dp = [[0 for _ in xrange(n)] for _ in xrange(m)]
        visited = [[0 for _ in xrange(n)] for _ in xrange(m)]

        num_buildings = 0
        for i in xrange(m):
            for j in xrange(n):
                if grid[i][j] == 1:
                    num_buildings += 1
                    self.bfs(grid, m, n, dp, visited, i, j, num_buildings)

        shortest = float("inf")
        found = False
        for i in xrange(m):
            for j in xrange(n):
                if visited[i][j] == num_buildings and dp[i][j] < shortest:
                    shortest = dp[i][j]
                    found = True
        return shortest if found else -1

    def bfs(self, grid, m, n, dp, visited, i, j, bIdx):
        dirs = [(-1, 0), (1, 0), (0, 1), (0, -1)]
        curLevel = [(i, j, 0)]  # pos_x, pos_y, distance
        while curLevel:
            nextLevel = set()  # use set to remove duplicate locations!!!
            for i, j, d in curLevel:
                if bIdx-1 <= visited[i][j] < bIdx \
                        and grid[i][j] == 0:  # if haven't visited + valid location
                    # visited[x][y] == bIdx -1 --> visited by previous building, but haven't been visited by current building
                    visited[i][j] = bIdx
                    dp[i][j] += d

                for dir in dirs:
                    x, y = i + dir[0], j + dir[1]
                    if 0 <= x < m and 0 <= y < n and grid[x][y] == 0 \
                            and bIdx-1 <= visited[x][y] < bIdx:
                        nextLevel.add((x, y, d+1))
            curLevel = list(nextLevel)


if __name__ == "__main__":
    # grid = [[1, 0, 2, 0, 1], [0, 0, 0, 0, 0], [0, 0, 1, 0, 0]]
    # grid = [[0, 2, 1], [1, 0, 2], [0, 1, 0]]
    grid = [[0,2,0,0,2,2,2,2,2,2,2,0,0,0,0,2,2,1,0,0,2,0,2,0,2,0,0,2,2,2,0,0,2,0,2,0,2,2,2,0,2],[0,0,0,0,0,2,2,0,2,0,0,0,0,0,2,0,0,2,2,0,2,2,2,2,0,0,2,2,0,0,2,2,1,0,0,2,2,0,2,0,0],[0,0,0,0,2,2,0,0,0,0,0,0,2,2,0,2,2,0,0,0,2,2,2,2,0,0,0,0,2,2,0,0,0,0,0,2,0,0,2,2,0],[2,2,0,2,0,0,2,0,0,0,0,0,2,2,2,0,2,2,2,0,0,0,0,0,0,1,2,2,0,0,0,0,2,0,0,2,0,0,0,0,2],[0,0,2,2,0,0,2,1,2,0,0,0,0,2,1,0,2,2,0,2,0,0,0,2,1,0,2,2,0,0,0,2,0,0,0,2,2,0,2,0,0],[0,0,2,2,0,0,0,0,0,2,2,2,2,2,0,0,0,0,0,0,0,2,2,2,2,2,0,2,2,1,2,0,2,0,0,0,2,0,2,2,0],[2,0,0,0,2,2,0,0,0,2,1,0,2,0,0,0,0,2,0,2,0,2,2,2,0,2,2,2,0,0,0,0,0,0,2,0,0,0,0,0,2],[1,0,2,2,0,0,2,0,0,0,2,0,0,0,2,2,2,2,0,0,0,0,2,0,0,2,0,2,1,2,2,0,0,2,0,0,0,0,0,0,0],[0,2,0,0,0,1,0,0,2,2,0,0,0,0,2,0,0,2,0,2,2,2,0,0,2,2,0,2,2,2,2,0,0,0,0,2,0,2,0,0,2],[0,0,2,2,0,2,2,0,2,0,1,0,0,0,0,0,2,0,0,2,0,2,0,0,0,0,0,0,0,2,2,0,0,0,0,0,2,2,0,0,1],[1,2,0,0,0,0,0,0,0,2,2,0,0,0,0,0,0,0,2,2,2,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,2,0,0,2,0],[0,1,2,2,2,2,0,2,0,0,2,2,0,0,0,0,0,2,0,2,0,0,0,0,2,0,0,0,0,0,0,2,2,2,0,0,2,2,0,0,0],[2,0,2,0,2,0,0,2,0,0,0,2,0,0,2,2,0,0,0,2,0,0,2,2,2,0,2,2,0,2,2,1,2,0,0,2,0,0,0,2,0],[0,0,2,0,0,0,2,2,2,0,2,2,2,0,0,2,0,0,0,2,1,2,2,0,2,0,2,0,0,2,2,0,0,0,2,2,0,0,0,0,0],[0,0,0,0,0,2,2,0,2,0,0,2,0,2,0,0,0,2,0,0,0,0,0,0,0,1,2,0,0,0,0,1,0,2,0,0,2,0,1,0,0],[1,2,0,2,0,0,0,2,0,2,0,0,0,0,0,2,1,2,0,0,0,0,0,2,0,0,0,0,0,2,2,0,0,0,2,0,0,1,0,2,2],[1,0,0,2,0,2,0,2,0,2,0,0,0,0,1,0,0,0,0,0,2,2,0,2,0,2,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0],[0,0,0,0,2,0,0,2,0,0,0,0,0,0,0,2,0,0,0,0,1,2,2,0,0,2,2,0,0,0,0,0,0,2,0,2,0,2,2,0,1],[2,0,0,1,1,0,1,0,2,0,0,2,1,0,2,0,0,2,2,0,2,0,2,2,0,1,2,0,2,0,0,0,0,0,0,2,0,0,2,0,0],[2,2,0,0,0,2,2,0,2,2,0,0,0,2,0,0,0,2,0,0,2,0,0,2,0,0,0,2,0,0,0,2,2,2,0,2,0,0,2,0,2]]
    print Solution().shortestDistance(grid)