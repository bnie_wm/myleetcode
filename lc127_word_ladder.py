# -*- coding: utf-8 -*-

import collections

#### BFS -- O(len(wordList) * len(word) * 26) -- AC
#### 多维护一个visited, 每访问一次添加 -- 会TLE
#### 直接在wordList上删除
class SolutionBFS(object):
    def ladderLength(self, beginWord, endWord, wordList):
        """
        :type beginWord: str
        :type endWord: str
        :type wordList: List[str]
        :rtype: int
        """

        if endWord not in wordList:
            return 0

        letters = [chr(ord('a') + i) for i in xrange(26)]

        #### O(len(wordList) * len(word) * 26)
        #### Find shortest path in a graph
        #### all edges: weight=1 -- BFS O(n)
        #### edges with diff. weights -- BFS/Dijkstra O(nlogn)
        words = set(wordList + [beginWord])  # 不使用visited, 而是每访问一个node, 则从words中删除
        queue = collections.deque([(1, beginWord)])  # (dist, word) -- 简单图使用deque即可,不需要维护min heap
        while queue:
            cur_dist, cur_word = queue.popleft()
            if cur_word not in words:
                continue
            words.remove(cur_word)

            neighbours = self.findNeighbouringWords(cur_word, words, letters)  # O(len(word) * 26)
            nei_dist = cur_dist + 1
            if endWord in neighbours:
                return nei_dist
            for nei_word in neighbours:
                queue.append((nei_dist, nei_word))

        return 0  # unreachable from beginWord

    def findNeighbouringWords(self, word, words, letters):
        neighbours = []
        for i in xrange(len(word)):  # change one char per time
            for char in letters:
                if char != word[i]:
                    this_word = word[:i] + char + word[i + 1:]
                    if this_word in words:
                        neighbours.append(this_word)
        return neighbours


#### Bidirectional BFS -- AC
class SolutionBiBFS(object):
    def ladderLength(self, beginWord, endWord, wordList):
        """
        :type beginWord: str
        :type endWord: str
        :type wordList: List[str]
        :rtype: int
        """

        if endWord not in wordList:
            return 0

        letters = [chr(ord('a') + i) for i in xrange(26)]

        qs = [beginWord]
        qe = [endWord]
        wordDict = set(wordList)
        wordDict.remove(endWord)

        depth = 1
        while qs and qe:
            depth += 1
            # 正反交替扩展
            if len(qs) > len(qe):
                qs, qe = qe, qs
            queue = []
            for cur_word in qs:
                neighbours = self.findNeighbouringWords(cur_word, wordDict, letters)
                for nei_word in neighbours:
                    if nei_word in qe:
                        return depth
                    if nei_word not in wordDict:
                        continue
                    wordDict.remove(nei_word)
                    queue.append(nei_word)
            qs = queue
        return 0

    def findNeighbouringWords(self, word, wordDict, letters):
        neighbours = []
        for i in xrange(len(word)):  # change one char per time
            for char in letters:
                if char != word[i]:
                    this_word = word[:i] + char + word[i + 1:]
                    # if this_word in wordDict:
                    neighbours.append(this_word)
        return neighbours

if __name__ == "__main__":
    #### begin = "ab", end = "ef"
    #### wordList = ["ac","ad","ec","ed","ef"]

    beginWord, endWord = "ab", "ef"
    wordList = ["ac","ad","ec","ed","ef"]
    print SolutionBFS().ladderLength(beginWord, endWord, wordList)
    print SolutionBiBFS().ladderLength(beginWord, endWord, wordList)
