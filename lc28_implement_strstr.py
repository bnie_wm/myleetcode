# -*- coding: utf-8 -*-


"""
Naive:
Time: O(n*k)
Space: O(1)
"""
class Solution(object):
    def strStr(self, haystack, needle):
        """
        :type haystack: str
        :type needle: str
        :rtype: int
        """

        n, k = len(haystack), len(needle)
        if k == 0:
            return 0

        for i in xrange(n - k + 1):
            if haystack[i: i + k] == needle:
                return i
        return -1


"""
Rabin-Karp Substring Search
"""


class SolutionRK(object):
    def strStr(self, haystack, needle):
        """
        :type haystack: str
        :type needle: str
        :rtype: int
        """
        #### Rabin-Karp Substring Search
        #### Time: O(n+k), worst case: O(nk)
        #### Space O(1)

        n, k = len(haystack), len(needle)
        if k == 0:
            return 0
        if k > n:
            return -1

        self.d = 256  # this is the size of ASCII table
        self.q = 101  # a prime number, suggested by CLRS book
        self.h = pow(self.d, k - 1, self.q)  # (d ** (k-1)) % q, 用于递推计算新的hash (leading position乘以的那个数)

        # use a sliding window of size k -- when they share the same hash_code, check if they are the same substr
        tgt_hash = self.init_hash(needle, k)
        cur_hash = self.init_hash(haystack[:k], k)

        for i in xrange(n - k + 1):
            # 判断当前window是否是needle
            if tgt_hash == cur_hash and haystack[i: i + k] == needle:
                return i

            # update cur_hash for next window
            if i < n - k:
                cur_hash = self.update_hash(cur_hash, haystack[i], haystack[i + k])

        return -1

    #### Rolling hash function:
    #### E.g., haystack="abcd", k=3
    #### hash("abc") = ord("a") * d ** 2 + ord("b") * d + ord("c")
    #### hash("bcd") = (hash("abc") - ord("a") * d ** 2) * d + ord("d")

    def update_hash(self, cur_hash, left, right):
        # cur_window: haystack[i: i+k-1], next_window: haystack[i+1: i+k]
        # remove left: haystack[i]
        cur_hash = cur_hash - ord(left) * self.h

        # append next: haystack[i+k]
        cur_hash = cur_hash * self.d + ord(right)
        cur_hash %= self.q

        # we might get negative value of cur_hash
        if cur_hash < 0:
            cur_hash += self.q

        return cur_hash

    def init_hash(self, window, k):
        cur_hash = 0
        for i in xrange(k):
            cur_hash = cur_hash * self.d + ord(window[i])
            cur_hash %= self.q
        return cur_hash

"""
KMP
Time: O(n+k)
Space: O(k)  # k: len(needle)
"""
class SolutionKMP(object):
    def strStr(self, haystack, needle):
        """
        :type haystack: str
        :type needle: str
        :rtype: int
        """
        if needle == "":
            return 0

        next = self.build_next(needle)

        # ## KMP
        i, j = 0, 0  # i --> haystack, j --> needle
        while i < len(haystack) and j < len(needle):  # two contraints
            if haystack[i] == needle[j]:
                i += 1
                j += 1
            else:
                if j == 0:  # if first char is not math
                    i += 1
                else:
                    j = next[j - 1] + 1  # important next[j-1] !!!

        return i - len(needle) if j == len(needle) else -1

    def build_next(self, needle):
        next = [-1] * len(needle)
        i, j = 0, 1  # i --> needle, j --> next
        shift = -1
        while j < len(needle):
            if needle[j] == needle[i]:
                shift += 1
                next[j] = shift
                i += 1
                j += 1
            else:  # if not math
                if i == 0:  # if back to the first char, move to next j
                    j += 1
                else:
                    i = 0  # back to first char, re-match for current j
                    shift = -1
        return next



if __name__ == "__main__":
    haystack = "geeks for geeks"
    needle = "geek"

    print Solution().strStr(haystack, needle)
    print SolutionRK().strStr(haystack, needle)
    print SolutionKMP().strStr(haystack, needle)

