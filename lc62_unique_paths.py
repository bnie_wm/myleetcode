# A robot is located at the top-left corner of a m x n grid (marked 'Start' in the diagram below).
#
# The robot can only move either down or right at any point in time.
# The robot is trying to reach the bottom-right corner of the grid (marked 'Finish' in the diagram below).
#
# How many possible unique paths are there?
#
# Note: m and n will be at most 100.


"""
DP
f(n) = f(n-1) + f(n)
Time: O(m*n)
Space: O(n)
"""


class Solution2(object):
    def uniquePaths(self, m, n):
        """
        :type m: int
        :type n: int
        :rtype: int
        """

        dp = [1] * n
        for i in xrange(1, m):
            for j in xrange(1, n):
                dp[j] = dp[j - 1] + dp[j]
        return dp[n - 1]


"""
DP
f(m, n) = f(m, n-1) + f(m-1, n)
Time: O(m*n)
Space: O(m*n)
"""
class Solution(object):
    def uniquePaths(self, m, n):
        """
        :type m: int
        :type n: int
        :rtype: int
        """

        num = [[1] * n] * m  # init: m*n 2D matrix

        for i in xrange(1, m):
            for j in xrange(1, n):
                num[i][j] = num[i-1][j] + num[i][j-1]
        return num[m-1][n-1]


if __name__ == "__main__":
    m, n = 3, 7
    print Solution().uniquePaths(m, n)
    print Solution2().uniquePaths(m, n)