# -*- coding: utf-8 -*-

class BinaryIndexTree(object):
    def __init__(self, n):
        self.sums = [0] * (n + 1)  # 数组下标从1开始

    def lowbit(self, i):
        return i & -i

    # O(logn)
    def update(self, i, delta):  # delta = new_val - old_val
        while i < len(self.sums):
            self.sums[i] += delta
            i += self.lowbit(i)

    # O(logn)
    def query(self, i):  # query返回到i(含i)为止的前缀和
        _sum = 0
        while i > 0:
            _sum += self.sums[i]
            i -= self.lowbit(i)
        return _sum


class NumArray(object):
    def __init__(self, nums):
        self.nums = nums  # 下标从0开始
        self.bit = BinaryIndexTree(len(nums))  # 下标从1开始
        # init: O(nlogn)
        for i in xrange(len(nums)):
            self.bit.update(i + 1, nums[i])

    def update(self, i, val):
        self.bit.update(i + 1, val - self.nums[i])
        self.nums[i] = val  # don't forget to update nums!

    def sumRange(self, i, j):
        return self.bit.query(j + 1) - self.bit.query(i)


if __name__ == "__main__":
    obj = NumArray(nums=[1, 2, 3, 4, 5, 6, 7, 8])
    print obj.bit.sums

    print obj.sumRange(0, 7)
    print obj.sumRange(1, 4)

    obj.update(4, 100)
    print obj.bit.sums
    print obj.sumRange(4, 4)

