# -*- coding: utf-8 -*-


class Solution(object):
    def majorityElement(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        ##### 基于Counting的方法
        # # [Method-1] dictionary -- Time:O(n), Space:O(n) 52ms
        # n = len(nums)
        # counter = collections.Counter(nums)
        # print counter
        # for val in counter:
        #     if counter[val] > n/2:
        #         return val
        # return -1

        # # [Method-2] randomization -- Time: O(n), Space: O(1) 40ms
        # # 因为majority number occur > floor(n/2),
        # # 所以从概率上来讲平均去两次就可以找到了
        # n = len(nums)
        # while True:
        #     idx = random.randint(0, n-1)  # random pick a position
        #     # check its count
        #     cnt = 0
        #     for num in nums:
        #         if num == nums[idx]:
        #             cnt += 1
        #             if cnt > n/2: return num
        # return -1

        # ##### 基于Voting的方法
        # # [Method-3] Bit vote -- Time: O(32*n), Space: O(1)
        # # 将中位数按位来建立，从0到31位，每次统计下数组中该位上0和1的个数，
        # # 如果1多，那么我们将结果res中该位变为1，最后累加出来的res就是中位数了
        # # get bit i: mask = 1 << i, if num & (1 << i) != 0, ith-bit is 1, otherwise, is 0
        # n = len(nums)
        # res = 0
        # for i in xrange(32):  # 32-bit int
        #     mask = 1 << i  # only ith-bit is 1, others are 0
        #     cnt = 0  # cnt of 1
        #     for num in nums:
        #         if num & mask:  # if ith bit is 1
        #             cnt += 1
        #     if cnt > n/2:
        #         res = res | mask  # set res ith bit as 1
        # return res

        # # [Method-4] Boyer-Moore vote -- Time: O(n), Space: O(1)
        # 一开始初始化majority=None, cnt=0. 当碰到众数时, cnt+=1, 否则cnt-=1.
        # 当cnt==0时, 重置众数.
        majority = None
        cnt = 0
        for num in nums:
            if num == majority:
                cnt += 1
            elif cnt == 0:
                majority = num
                cnt += 1
            else:
                cnt -= 1
        return majority

        #### 基于Sorting的方法 -- 排序之后中位数一定是majority
        # # [Method-5] Full sorting -- Time: O(nlogn), Space: O(1)
        # nums.sort()
        # n = len(nums)
        # return nums[n/2]





