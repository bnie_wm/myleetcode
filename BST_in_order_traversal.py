# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


"""
Iterative with Stack
"""


class Action(object):
    def __init__(self, visited, node):
        self.visited = visited
        self.node = node


class Solution(object):
    def inorderTraversal(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        result = []
        path = [Action(False, root)]  # add root node
        while path:
            current = path.pop()
            if current.node is None:  # defensive
                continue

            if current.visited:
                result.append(current.node.val)
            else:
                path.append(Action(False, current.node.right))
                path.append(Action(True, current.node))
                path.append(Action(False, current.node.left))

        return result


        """
        Post-order: root visited twice, append to path on the 2nd visit
        This method mimic the recursion stack
        """
        # path, stack = [], [(root, False)]
        # while stack:
        #     root, visited = stack.pop()
        #     if root is None:
        #         continue
        #     if visited:
        #         path.append(root.val)
        #     else:
        #         stack.append((root, True))
        #         stack.append((root.right, False))
        #         stack.append((root.left, False))
        # return path



"""
Recursive
Time: O(n)
Space: O(h)
"""
class Solution2(object):
    def inorderTraversal(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """

        path = []
        self.inorderTraversalHelper(path, root)
        return path

    def inorderTraversalHelper(self, path, root):
        if root is None:
            return

        # path.append(root.val) # --> if pre-order
        self.inorderTraversalHelper(path, root.left)
        path.append(root.val)
        self.inorderTraversalHelper(path, root.right)
        # path.append(root.val)  # r--> if post-order


if __name__ == "__main__":
    root = TreeNode(6)
    root.left = TreeNode(2)
    root.left.left = TreeNode(0)
    root.left.right = TreeNode(4)
    root.left.right.left = TreeNode(3)
    root.left.right.right = TreeNode(5)
    root.right = TreeNode(8)
    root.right.left = TreeNode(7)
    root.right.right = TreeNode(9)

    # print Solution2().inorderTraversal(root)
    print Solution().inorderTraversal(root)