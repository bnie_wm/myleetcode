# -*- coding: utf-8 -*-
import math

# O(nlogn)
def heapsort(x):
    # 建立max-heap -- O(n)
    heapify(x)

    # heapsort:
    # (n-1)次siftdown调用 = (n-1) * O(logn) = O(nlogn)
    n = len(x)
    last_idx = n - 1
    while last_idx > 0:
        x[0], x[last_idx] = x[last_idx], x[0] # 将x[0]与x[n-1]交换 -- 最大值归位!
        siftdown(x, 0, last_idx)  # 调整x[0]的位置(siftdown), 从而x[0:n]为最大堆
        last_idx -= 1

# O(n): while一半的元素, 而每次siftdown都少一个元素
def heapify(x):
    n = len(x)
    # 从最后一个元素的parent开始, 从低向上确认每个元素的位置
    i = int(math.floor((n-1)/2))  # n的parent=floor((n-1)/2)
    while i >= 0:
        siftdown(x, i, n)
        i -= 1

# O(logn): 将栈顶元素向下交换,
def siftdown(x, i, max):
    while i < max:
        index = i

        left = 2 * i + 1
        right = left + 1

        if left < max and x[left] > x[index]:
            index = left

        if right < max and x[right] > x[index]:
            index = right

        if index == i:
            return

        x[i], x[index] = x[index], x[i]

        i = index


if __name__ == "__main__":
    x = [3, 19, 1, 14, 8, 7]
    heapsort(x)
    print x

