"""
BFS
"""
import collections
class Solution(object):
    def ladderLength(self, beginWord, endWord, wordList):
        """
        :type beginWord: str
        :type endWord: str
        :type wordList: Set[str]
        :rtype: int
        """
        letters = 'abcdefghijklmnopqrstuvwxyz'

        """
        Level order tree traversal
        """
        cur = [beginWord]
        visited = set([beginWord])
        depth = 1
        while cur:
            depth += 1
            next = []
            for curWord in cur:
                for i in xrange(len(curWord)):
                    for c in letters:
                        if c != curWord[i]:
                            newWord = curWord[:i] + c + curWord[i + 1:]
                            if newWord == endWord:
                                return depth
                            if newWord in wordList and newWord not in visited:
                                next.append(newWord)
                                visited.add(newWord)
            cur = next
        return 0


class Solution2(object):
    def ladderLength(self, beginWord, endWord, wordList):
        """
        :type beginWord: str
        :type endWord: str
        :type wordList: Set[str]
        :rtype: int
        """

        ## it is likely that worList doesn't contain beginWord and endWord
        wordList.add(endWord)

        visited = set([beginWord])
        minLen = self.ladderLengthHelper(beginWord, endWord, wordList, 1, visited)
        return minLen if minLen != float("inf") else 0

    def ladderLengthHelper(self, curWord, endWord, wordList, curLen, visited):
        if curWord == endWord:
            return curLen

        minLen = float("inf")
        letters = 'abcdefghijklmnopqrstuvwxyz'
        for i in xrange(len(curWord)):
            for c in letters:
                if c != curWord[i]:
                    newWord = curWord[:i] + c + curWord[i+1:]
                    if newWord in wordList and newWord not in visited:
                        visited.add(newWord)
                        minLen = min(minLen, self.ladderLengthHelper(newWord, endWord, wordList, curLen + 1, visited))
                        visited.remove(newWord)
        return minLen


if __name__ == "__main__":
    beginWord = "hit"
    endWord = "cog"
    wordList = ["hot","dot","dog","lot","log"]
    # beginWord = "hot"
    # endWord = "dog"
    # wordList = ["hot", "dot", "dog"]
    print Solution().ladderLength(beginWord, endWord, set(wordList))
    print Solution2().ladderLength(beginWord, endWord, set(wordList))