import collections

# ##### TLE
# class Solution(object):
#     def lengthOfLongestSubstringKDistinct(self, s, k):
#         """
#         :type s: str
#         :type k: int
#         :rtype: int
#         """
#         if len(s) <= k:
#             return len(s)
#
#         max_len = 0
#         for i in xrange(len(s)):
#             bucket = collections.defaultdict(int)
#             for j in xrange(i, len(s)):
#                 if s[j] not in bucket.keys():
#                     if len(bucket) >= k:
#                         break
#                     else:
#                         bucket[s[j]] = 1
#                 else:
#                     bucket[s[j]] += 1
#             max_len = max(max_len, sum(bucket.values()))
#
#         return max_len


class Solution(object):
    def lengthOfLongestSubstringKDistinct(self, s, k):
        """
        :type s: str
        :type k: int
        :rtype: int
        """
        n = len(s)
        if n <= k:
            return n

        bucket = collections.defaultdict(int)
        begin = 0
        maxLen = 0
        for i in xrange(n):
            bucket[s[i]] += 1
            # if exceed bucket size, move begin to right
            # util completely delete one char
            while len(bucket) > k:
                bucket[s[begin]] -= 1
                if bucket[s[begin]] == 0:
                    del bucket[s[begin]]
                begin += 1
            maxLen = max(maxLen, i - begin + 1)
        return maxLen


if __name__ == "__main__":
    s = "eceba"
    k = 2
    print Solution().lengthOfLongestSubstringKDistinct(s, k)