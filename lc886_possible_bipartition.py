# -*- coding: utf-8 -*-
import collections


class Solution(object):
    def possibleBipartition(self, N, dislikes):
        """
        :type N: int
        :type dislikes: List[List[int]]
        :rtype: bool
        """
        #### 和 lc785 Is Graph Bipartite? 完全一样
        #### peope -- node
        #### a dislikes b -- edge (a, b) 的两个点不能在同一个set

        graph = collections.defaultdict(list)
        for (a, b) in dislikes:
            graph[a].append(b)
            graph[b].append(a)

        people = range(1, N + 1)
        visited = dict()
        while people:
            queue = collections.deque([(people[0], 1)])  # (node, color) -- color could be either -1 or 1
            # 一次LEVEL ORDER Traversal
            while queue:
                size = len(queue)  # size表示了当前level的点的个数, 每次之访问这么多
                for i in xrange(size):
                    cur, color = queue.popleft()
                    if cur in visited:
                        continue
                    visited[cur] = color
                    people.remove(cur)

                    for nei in graph[cur]:
                        # 若发现"cur"和他的一个dislike "nei"颜色相同-- return False
                        if nei in visited and visited[nei] == color:
                            return False
                        if nei not in visited:
                            queue.append((nei, -color))
        return True


if __name__ == "__main__":
    N = 4
    dislikes = [[1,2],[1,3],[2,4]]
    print Solution().possibleBipartition(N, dislikes)